`timescale 1ns/1ps
module nv_dual_port_rom_tb ();
  
  localparam RAM_WIDTH = 1;
  localparam RAM_DEPTH = 2048;
  localparam INIT_FILE = "/home/s3471830/gitlab/IPLookup/python/memMap_mgm/bf10.mem";

  reg [(2*clogb2(RAM_DEPTH-1))-1:0] sram_addra;
  reg [(2*clogb2(RAM_DEPTH-1))-1:0] sram_addrb;
  reg                               ki;
  reg                               rst_n;
  wire [(2*RAM_WIDTH)-1:0]          sram_douta;
  wire [(2*RAM_WIDTH)-1:0]          sram_doutb;
  wire                              ko;

  initial
  begin
    rst_n = 1;
#10 rst_n = 0;
#10 rst_n = 1;
#1000 $finish;
  end

  initial 
  begin
    ki            = 0;
    all_null();

#40 ki            = 1;
#50 sram_addra    = 20'b01011001011001011001; sram_addrb    = 20'b01101001011001011001;

#3  ki            = 0;    
#5  ki            = 1;  
#5  all_null();

#50 sram_addra    = 20'b01101001011001011001; sram_addrb    = 20'b01101001011001011010;

    ki            = 0;
#5  ki            = 1;
#5  all_null();
#5  ki            = 0;

#50 sram_addra    = 20'b01011001011001011010; sram_addrb    = 20'b01011001011010011001;

#5  ki            = 1;
#5  all_null();
#5  ki            = 0;

#50 sram_addra    = 20'b01011001011010011001; sram_addrb    =20'b01011001011001011010;

#5  ki            = 1;  
#5  all_null();

// Trying to read an address without without reading out result of last
// operation
#50 sram_addra    = 20'b01011001010101011001; sram_addrb    = 20'b01010101011001011001;
#5  all_null();

// Only the result of the following data wavefront should come out. The
// previous data wavefront would be lost, because there wasn't a corresponding
// ki to pull the value out. 
#50 sram_addra    = 20'b01101001011001011001; sram_addrb    = 20'b01011001011010011001;

#3  ki            = 0;
#5  ki            = 1;  
#5  all_null();
#5  ki            = 0;


  end

  initial
  begin
    $dumpfile("nv_dual_port_rom_dump.fst");
    $dumpvars;
  end

  nv_dual_port_rom #(
    .RAM_WIDTH(RAM_WIDTH),
    .RAM_DEPTH(RAM_DEPTH),
    .INIT_FILE(INIT_FILE)
  )u_dut (
    .addra (sram_addra  ),
    .addrb (sram_addrb  ),
    .ki    (ki          ),
    .rst_n (rst_n       ),
    .douta (sram_douta  ),
    .doutb (sram_doutb  ),
    .ko    (ko          )
  );

task all_null;
  sram_addra = 20'b0;
  sram_addrb = 20'b0;
endtask

function integer clogb2;
  input integer depth;
    if (depth > 1)
      for (clogb2=0; depth>0; clogb2=clogb2+1)
        depth = depth >> 1;
    else
      clogb2=1;
endfunction

endmodule
