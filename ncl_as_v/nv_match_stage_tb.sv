`timescale 1ns/1ps
`define MSTAGE_SIM 1
module nv_match_stage_tb();

  localparam TDEPTH        = 8;
  localparam PTRIE         = 2;
  localparam IP_ADDRW      = 32;
  localparam NHI_WIDTH     = 8;
  localparam USE_BLOOM_RAM = 1;
  localparam USE_BLOOM_CRC = 1;
  
  localparam MEM_SIZE      = 241;
  localparam MEM_AWIDTH    = clogb2(241)+1;
  localparam CP_WIDTH      = clogb2(457)+1;
  localparam PFX_INIT_FILE = "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/pfxMap8_testing.mem";
  localparam LRC_INIT_FILE = "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/lrcMap8_testing.mem";
  localparam TOT_DEPTH     = 30;
  localparam BF_SIZE       = 1024;
  localparam BF_AWIDTH     = 10;//clogb2(1024) + 1;
  localparam BF_INIT_FILE  = "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/bf8_testing.mem";
  
  reg                         rst_n;
  reg                         ki;
  wire                        ko;
  reg  [1:0]                  msbk_i;
  reg  [5:0]                  mk_i;
   // Inputs from the previous match stage's CRC computation block
  reg  [63:0]                 crc_sr_i;
  reg  [1:0]                  tlevel_inc_i;

  // Inputs from the previous match stage's child computation block
  reg  [(2*MEM_AWIDTH)-1:0]   address_i;
  reg  [(2*IP_ADDRW)-1:0]     apk_i;
  reg  [1:0]                  daddr_vld_i;

  // Inputs from the previous match stage's prefix match block
  reg  [(2*NHI_WIDTH)-1:0]    nhi_i;
  reg  [11:0]                 matchlength_i;

  // Outputs from the CRC computation block appropriately delayed
  wire [63:0]                 crc_sr_d6_o;

  // Outputs from the next child computation block appropriately delayed
  wire [1:0]                  tlevel_inc_d6_o;
  wire [(2*CP_WIDTH)-1:0]     address_d6_o;
  wire [(2*IP_ADDRW)-1:0]     apk_d6_o;

  // Outputs from the prefix match block appropriately delayed
  wire [(2*NHI_WIDTH)-1:0]    nhi_d6_o;
  wire [11:0]                 matchlength_d6_o;
  wire [1:0]                  match_found_d6_o;
  

//============================================================================== 
  initial
  begin
      rst_n = 1;
#10   rst_n = 0;
#10   rst_n = 1;
#1000 $finish;
  end

  initial 
  begin
    ki            = 0;
    all_null();
#40 ki            = 1;

// Non null next address
// BF does not match
#50 msbk_i        = 2'b01;
    mk_i          = 6'b011010;
    crc_sr_i      = 64'b01010101_01100110_10100110_01010101_01101001_01011010_10011010_10100101;
    tlevel_inc_i  = 2'b10;
    address_i     = 20'h59656;  //91
    apk_i         = 64'b01010110_01100110_10100110_01010101_01101001_01011010_10011010_10100101;
    daddr_vld_i   = 2'b10;
    nhi_i         = 16'h5A96; 
    matchlength_i = 12'h555;  


#3  ki            = 0;    
#10 if ((nhi_d6_o == 16'h5A96) & (match_found_d6_o == 2'b01) & (address_d6_o == 18'h15A65) & (apk_d6_o == 64'h599A_9955_A56A_6A95))
      $display("Pass Test 'Non Null Next Address, No BF Match' Data Wavefront");
    else
      $display ("FAIL Test 'Non Null Next Address, No BF Match' Data Wavefront");

#5  ki            = 1;  
#10 all_null();
#10 if ((nhi_d6_o == 'd0) & (match_found_d6_o == 2'b00) & (address_d6_o == 'h0) & (apk_d6_o == 64'h0))
      $display("Pass Test 'Null Wavefront'");
    else
      $display ("FAIL Test  'Null Wavefront'");

/////// Interesting //////////
// This address is null, so no comparison required. 
// Bf matches
// prefix matches
#50 msbk_i        = 2'b0;
    mk_i          = 6'b0;
    crc_sr_i      = 64'b0;
    tlevel_inc_i  = 2'b00;
    address_i     = 20'b0; // 0
    apk_i         = 64'b0;
    daddr_vld_i   = 2'b01;
    nhi_i         = 16'b0101100110010110;
    matchlength_i = 12'b0;

#3  ki            = 0;    
#10 if ((nhi_d6_o == 16'h5996) & (match_found_d6_o == 2'b00) & (address_d6_o == 'h0) & (apk_d6_o == 64'h0))
      $display("Pass Test 'Input address is Null' Data Wavefront");
    else
      $display("FAIL Test 'Input address is Null' Data Wavefront");

#5  ki            = 1;  
#10 all_null();
#10 if ((nhi_d6_o == 'd0) & (match_found_d6_o == 2'b00) & (address_d6_o == 'h0) & (apk_d6_o == 64'h0))
      $display("Pass Test 'Null Wavefront'");
    else
      $display ("FAIL Test  'Null Wavefront'");

// Non Null Next address
// Bf matches
// prefix matches
#50 msbk_i        = 2'b01;
    mk_i          = 6'b101010;
    crc_sr_i      = 64'b01010110_01100110_10100110_01010101_01101001_01011010_10011010_10100101;
    tlevel_inc_i  = 2'b10;
    address_i     = 20'h5965A; // 93
    apk_i         = 64'b10101010_01100110_10100110_01010101_01101001_01011010_10011010_10100101;
    daddr_vld_i   = 2'b10;
    nhi_i         = 16'hA996;
    matchlength_i = 12'h555;

#3  ki            = 0;    
#10 if ((nhi_d6_o == 16'h56A9) & (match_found_d6_o == 2'b10) & (address_d6_o == 'h256A5) & (apk_d6_o == 64'hA99A_9955_A56A_6A95))
      $display("Pass Test 'Non Null Next Address, BF Matches, Prefix matches' Data Wavefront");
    else       
      $display("FAIL Test 'Non Null Next Address, BF Matches, Prefix matches' Data Wavefront");

#5  ki            = 1;  
#10 all_null();
#10 if ((nhi_d6_o == 'd0) & (match_found_d6_o == 2'b00) & (address_d6_o == 'h0) & (apk_d6_o == 64'h0))
      $display("Pass Test 'Null Wavefront'");
    else
      $display ("FAIL Test  'Null Wavefront'");

// Non Null Next address
// Bf matches
// prefix matches
// Match length is not 0, so lp2 has to be greater than the value of matchlength
#50 msbk_i        = 2'b01;
    mk_i          = 6'b101010;
    crc_sr_i      = 64'b01010110_01100110_10100110_01010101_01101001_01011010_10011010_10100101;
    tlevel_inc_i  = 2'b10;
    address_i     = 20'h5965A; // 93
    apk_i         = 64'b10101010_01100110_10100110_01010101_01101001_01011010_10011010_10100101;
    daddr_vld_i   = 2'b10;
    nhi_i         = 16'hA996;
    matchlength_i = 12'h655;

#3  ki            = 0;    
#10 if ((nhi_d6_o == 16'hA996) & (match_found_d6_o == 2'b01) & (address_d6_o == 'h256A5) & (apk_d6_o == 64'hA99A_9955_A56A_6A95))
      $display("Pass Test 'Non Null Next Address, BF Matches, Prefix matches previous ml greater' Data Wavefront");
    else       
      $display("FAIL Test 'Non Null Next Address, BF Matches, Prefix matches previous ml greater' Data Wavefront");

#5  ki            = 1;  
#10 all_null();
#10 if ((nhi_d6_o == 'd0) & (match_found_d6_o == 2'b00) & (address_d6_o == 'h0) & (apk_d6_o == 64'h0))
      $display("Pass Test 'Null Wavefront'");
    else
      $display ("FAIL Test  'Null Wavefront'");

////  /////// Interesting //////////
////  // This address is null, so no comparison required. 
////  // Bf matches
////  // prefix matches
////  #50 msbk_i        = 2'b0;
////      mk_i          = 6'b0;
////      crc_sr_i      = 64'b0;
////      tlevel_inc_i  = 2'b00;
////      address_i     = 20'b0; // 0
////      apk_i         = 64'b0;
////      daddr_vld_i   = 2'b01;
////      nhi_i         = 16'h9596;
////      matchlength_i = 12'b0;
////  
////  #3  ki            = 0;    
////  #10 if ((nhi_d6_o == 16'h9596) & (match_found_d6_o == 2'b00) & (address_d6_o == 'h0) & (apk_d6_o == 64'h0))
////        $display("Pass Test 'Input address is Null' Data Wavefront");
////      else
////        $display("FAIL Test 'Input address is Null' Data Wavefront");
////  
////  #5  ki            = 1;  
////  #10 all_null();
////  #10 if ((nhi_d6_o == 'd0) & (match_found_d6_o == 2'b00) & (address_d6_o == 'h0) & (apk_d6_o == 64'h0))
////        $display("Pass Test 'Null Wavefront'");
////      else
////        $display ("FAIL Test  'Null Wavefront'");

// Null next address
// Bf matches
// prefix does not match
#50 msbk_i        = 2'b01;
    mk_i          = 6'b011010;
    crc_sr_i      = 64'b01010110_01100110_10100110_01010101_01101001_01011010_10011010_10100101;
    tlevel_inc_i  = 2'b10;
    address_i     = 20'h59659; //92
    apk_i         = 64'b01010110_01100110_10100110_01010101_01101001_01011010_10011010_10100101;
    daddr_vld_i   = 2'b10;
    nhi_i         = 16'h9995;
    matchlength_i = 12'h555;

#3  ki            = 0;    
#10 if ((nhi_d6_o == 16'h9995) & (match_found_d6_o == 2'b01) & (address_d6_o == 'h0) & (apk_d6_o == 64'h0))
      $display("Pass Test 'Null Next Address, BF Matches, Prefix does not match' Data Wavefront");
    else      
      $display("FAIL Test 'Null Next Address, BF Matches, Prefix does not match' Data Wavefront");

#5  ki            = 1;  
#10 all_null();
#10 if ((nhi_d6_o == 'd0) & (match_found_d6_o == 2'b00) & (address_d6_o == 'h0) & (apk_d6_o == 64'h0))
      $display("Pass Test 'Null Wavefront'");
    else
      $display ("FAIL Test  'Null Wavefront'");

// One address is Null next address
// Bf matches
// prefix does not match
#50 msbk_i        = 2'b01;
    mk_i          = 6'b011010;
    crc_sr_i      = 64'b01010110_01100110_10100110_10101010_01101001_01011010_10011010_10100101;
    tlevel_inc_i  = 2'b10;
    address_i     = 20'h59665; //94
    apk_i         = 64'b10010110_01100110_10100110_01010101_01101001_01011010_10011010_10100101;
    daddr_vld_i   = 2'b10;
    nhi_i         = 16'h9995;
    matchlength_i = 12'h555;

#3  ki            = 0;    
#10 if ((nhi_d6_o == 16'h9995) & (match_found_d6_o == 2'b01) & (address_d6_o == 'h0) & (apk_d6_o == 64'h0))
      $display("Pass Test 'One address is Null, BF Matches, Prefix does not match' Data Wavefront");
    else      
      $display("FAIL Test 'One address is Null, BF Matches, Prefix does not match' Data Wavefront");

#5  ki            = 1;  
#10 all_null();
#10 if ((nhi_d6_o == 'd0) & (match_found_d6_o == 2'b00) & (address_d6_o == 'h0) & (apk_d6_o == 64'h0))
      $display("Pass Test 'Null Wavefront'");
    else
      $display ("FAIL Test  'Null Wavefront'");

// Not Null next address but epsilon link
// Bf matches
// prefix does not match
#50 msbk_i        = 2'b01;
    mk_i          = 6'b011010;
    crc_sr_i      = 64'b01010110_01100110_10100110_10101010_01101001_01011010_10011010_10100101;
    tlevel_inc_i  = 2'b10;
    address_i     = 20'h59666; //95
    apk_i         = 64'b01010110_01100110_10100110_01010101_01101001_01011010_10011010_10100101;
    daddr_vld_i   = 2'b10;
    nhi_i         = 16'h9995;
    matchlength_i = 12'h555;

#3  ki            = 0;    
#10 if ((nhi_d6_o == 16'h9995) & (match_found_d6_o == 2'b01) & (address_d6_o == 'h15AA9) & (apk_d6_o == 64'b0101100110011010100110010101010110100101011010100110101010010101))
      $display("Pass Test 'Epsilon link out next address not Null, BF Matches, Prefix does not match' Data Wavefront");
    else      
      $display("FAIL Test 'Epsilon link out next address not Null, BF Matches, Prefix does not match' Data Wavefront");

#5  ki            = 1;  
#10 all_null();
#10 if ((nhi_d6_o == 'd0) & (match_found_d6_o == 2'b00) & (address_d6_o == 'h0) & (apk_d6_o == 64'h0))
      $display("Pass Test 'Null Wavefront'");
    else
      $display ("FAIL Test  'Null Wavefront'");

/////// Interesting //////////
// This address is null, so no comparison required. 
// Bf matches
// prefix matches
#50 msbk_i        = 2'b0;
    mk_i          = 6'b0;
    crc_sr_i      = 64'b0;
    tlevel_inc_i  = 2'b00;
    address_i     = 20'b0; // 0
    apk_i         = 64'b0;
    daddr_vld_i   = 2'b01;
    nhi_i         = 16'h999A;
    matchlength_i = 12'b0;

#3  ki            = 0;    
#10 if ((nhi_d6_o == 16'h999A) & (match_found_d6_o == 2'b00) & (address_d6_o == 'h0) & (apk_d6_o == 64'h0))
      $display("Pass Test 'Input address is Null' Data Wavefront");
    else
      $display("FAIL Test 'Input address is Null' Data Wavefront");

#5  ki            = 1;  
#10 all_null();
#10 if ((nhi_d6_o == 'd0) & (match_found_d6_o == 2'b00) & (address_d6_o == 'h0) & (apk_d6_o == 64'h0))
      $display("Pass Test 'Null Wavefront'");
    else
      $display ("FAIL Test  'Null Wavefront'");

// Non Null Next address
// Bf matches
// prefix does not matches
#50 msbk_i        = 2'b01;
    mk_i          = 6'b011010;
    crc_sr_i      = 64'b01010110_01100110_10100110_01010101_01101001_01011010_10011010_10100101;
    tlevel_inc_i  = 2'b10;
    address_i     = 20'h5965A; // 93
    apk_i         = 64'b10101010_01100110_10100110_01010101_01101001_01011010_10011010_10100101;
    daddr_vld_i   = 2'b10;
    nhi_i         = 16'h99A6;
    matchlength_i = 12'h555;

#3  ki            = 0;    
#10 if ((nhi_d6_o == 16'h99A6) & (match_found_d6_o == 2'b01) & (address_d6_o == 'h256A5) & (apk_d6_o == 64'hA99A_9955_A56A_6A95))
      $display("Pass Test 'Non Null Next Address, BF Matches, Prefix does not match' Data Wavefront");
    else       
      $display("FAIL Test 'Non Null Next Address, BF Matches, Prefix does not match' Data Wavefront");

#5  ki            = 1;  
#10 all_null();
#10 if ((nhi_d6_o == 'd0) & (match_found_d6_o == 2'b00) & (address_d6_o == 'h0) & (apk_d6_o == 64'h0))
      $display("Pass Test 'Null Wavefront'");
    else
      $display ("FAIL Test  'Null Wavefront'");

/////// Interesting //////////
// This address is null, so no comparison required. 
// Bf matches
// prefix matches
#50 msbk_i        = 2'b0;
    mk_i          = 6'b0;
    crc_sr_i      = 64'b0;
    tlevel_inc_i  = 2'b00;
    address_i     = 20'b0; // 0
    apk_i         = 64'b0;
    daddr_vld_i   = 2'b01;
    nhi_i         = 16'h9A96;
    matchlength_i = 12'b0;

#3  ki            = 0;    
#10 if ((nhi_d6_o == 16'h9A96) & (match_found_d6_o == 2'b00) & (address_d6_o == 'h0) & (apk_d6_o == 64'h0))
      $display("Pass Test 'Input address is Null' Data Wavefront");
    else
      $display("FAIL Test 'Input address is Null' Data Wavefront");

#5  ki            = 1;  
#10 all_null();
#10 if ((nhi_d6_o == 'd0) & (match_found_d6_o == 2'b00) & (address_d6_o == 'h0) & (apk_d6_o == 64'h0))
      $display("Pass Test 'Null Wavefront'");
    else
      $display ("FAIL Test  'Null Wavefront'");

  end

  initial
  begin
    $dumpfile("nv_match_stage_dump.fst");
    $dumpvars;
  end

nv_match_stage #(
  .TDEPTH           (TDEPTH             ),
  .PTRIE            (PTRIE              ),
  .IP_ADDRW         (IP_ADDRW           ),
  .NHI_WIDTH        (NHI_WIDTH          ),
  .USE_BLOOM_RAM    (USE_BLOOM_RAM      ),
  .USE_BLOOM_CRC    (USE_BLOOM_CRC      ),
  .MEM_SIZE         (MEM_SIZE           ),
  .MEM_AWIDTH       (MEM_AWIDTH         ),
  .CP_WIDTH         (CP_WIDTH           ),
  .PFX_INIT_FILE    (PFX_INIT_FILE      ),
  .LRC_INIT_FILE    (LRC_INIT_FILE      ),
  .BF_SIZE          (BF_SIZE            ),
  .BF_AWIDTH        (BF_AWIDTH          ),
  .BF_INIT_FILE     (BF_INIT_FILE       )
)u_dut(
  .rst_n            (rst_n              ),
  .ki               (ki                 ),
  .ko               (ko                 ),
  // Key Information that is passed from one match stage to the next without
  // any change
  .msbk_i           (msbk_i             ),
  .mk_i             (mk_i               ),
  // Inputs from the previous match stage's CRC computation block
  .crc_sr_i         (crc_sr_i           ),
  .tlevel_inc_i     (tlevel_inc_i       ),
  // Inputs from the previous match stage's child computation block
  .address_i        (address_i          ),
  .apk_i            (apk_i              ),
  .daddr_vld_i      (daddr_vld_i        ),
  // Inputs from the previous match stage's prefix match block
  .nhi_i            (nhi_i              ),
  .matchlength_i    (matchlength_i      ),
  // Outputs from the CRC computation block appropriately delayed
  .crc_sr_d6_o      (crc_sr_d6_o        ),
  // Outputs from the next child computation block appropriately delayed
  .tlevel_inc_d6_o  (tlevel_inc_d6_o    ),
  .address_d6_o     (address_d6_o       ),
  .apk_d6_o         (apk_d6_o           ),
  // Outputs from the prefix match block appropriately delayed
  .nhi_d6_o         (nhi_d6_o           ),
  .matchlength_d6_o (matchlength_d6_o   ),
  .match_found_d6_o (match_found_d6_o   )
);



task all_null;
  msbk_i        = 2'b0;
  mk_i          = 6'b0;
  crc_sr_i      = 64'b0;
  tlevel_inc_i  = 2'b0;
  address_i     = 20'b0;
  apk_i         = 64'b0;
  daddr_vld_i   = 2'b0;
  nhi_i         = 16'b0;
  matchlength_i = 12'b0;
endtask

function integer clogb2;
  input [31:0] value;
  begin
    if(value > 1)
    begin
      value = value - 1;
      for (clogb2 = 0; value > 0; clogb2 = clogb2 + 1)
      begin
        value = value >> 1;
      end
    end
    else
      clogb2 = 1;
  end
endfunction

endmodule
