`timescale 1ps/1ps
module nv_dual_port_rom #(
  parameter RAM_WIDTH      = 16, 
  parameter RAM_DEPTH      = 1024, 
  parameter INIT_FILE      = ""
)(
  input [(2*clogb2(RAM_DEPTH-1))-1:0] addra, // Write address bus, width determined from RAM_DEPTH
  input [(2*clogb2(RAM_DEPTH-1))-1:0] addrb, // Write address bus, width determined from RAM_DEPTH
  input                               ki,    // ki from the following stage used as read enable
  input                               rst_n, // Output reset (does not affect memory contents)
  output [(2*RAM_WIDTH)-1:0]          douta, // RAM output data
  output [(2*RAM_WIDTH)-1:0]          doutb, // RAM output data
  output                              ko     // Indicator that data has been read by the RAM
);

//============================================================================== 
wire [clogb2(RAM_DEPTH-1)-1:0]  s_addra;
wire [clogb2(RAM_DEPTH-1)-1:0]  s_addrb;
wire [RAM_WIDTH-1:0]            s_douta;
wire [RAM_WIDTH-1:0]            s_doutb;
wire                            ren;
wire                            ko_addra;
wire                            ko_addrb;
wire                            ko_douta;
wire                            ko_doutb;

wire                            ip_cmplt;
wire                            op_cmplt;

assign ren = ~ki;
//------------------------------------------------------------------------------ 
// op_delval, ko_delval and seed value are defined in the included file. 
// The MEAN and SD of the delay value are also specified in the included file
//------------------------------------------------------------------------------ 

// These are the declarations for the delay synthesis logic
reg  [9:0]  op_delval;
reg  [9:0]  ko_delval;
reg  [31:0] seed;

initial
begin
  seed = 1;
end

reg [31:0] delay_params[0:7];
initial
begin
  $readmemh("dual_port_rom_delay.mem",delay_params);
end  
                                                                
//------------------------------------------------------------------------------ 
// Input side Dual rail to Single rail
//------------------------------------------------------------------------------ 
d2sreg #(
  .DW(clogb2(RAM_DEPTH-1))
)u_addra_d2s(
  .rst_n          (rst_n    ),
  .dual_in        (addra    ),
  .single_out     (s_addra  ),
  .ko             (ko_addra )
);

d2sreg #(
  .DW(clogb2(RAM_DEPTH-1))
)u_addrb_d2s(
  .rst_n          (rst_n    ),
  .dual_in        (addrb    ),
  .single_out     (s_addrb  ),
  .ko             (ko_addrb )
);


nv_thxx_zerodelay #(
  .NUM_IP(3)
)u_ip_cmplt(
  .rst_n(rst_n),
  .din({ko_addrb,ko_addra,ren}),
  .dout(ip_cmplt)
);
//------------------------------------------------------------------------------ 
// Because we are working with a behavioural model here and artificially
// snthesizing the required delays, we can use the output completeness signal
// that indicates when the outputs are ready, and then delay it by a further 
// ko_delval which is the delay of the actual completion detection circuit to 
// generate the final ko signal
//------------------------------------------------------------------------------ 
psDelay u_ko_delay (
  .rst_n          (rst_n    ),
  .trig           (op_cmplt ),
  .del_value      (ko_delval),
  .dlyd_trig      (ko       )
);

//------------------------------------------------------------------------------ 
// creating a dummy delayed ip_cmplt_d1 signal used to sample the result of the 
// boolean operation and select the appropriate mean and sdev of the delay if 
// it is data dependent 
//------------------------------------------------------------------------------ 
wire ip_cmplt_d1;
assign #1 ip_cmplt_d1 = ip_cmplt;

wire op_cmplt_d1;
assign #1 op_cmplt_d1 = op_cmplt;

always @(ip_cmplt_d1,rst_n)
begin
  if (~rst_n)
    op_delval = 'h3ff;
  else if (ip_cmplt_d1)
    op_delval = $dist_normal(seed,delay_params[0],delay_params[1]);
  else
    op_delval = $dist_normal(seed,delay_params[2],delay_params[3]);
end

always @(op_cmplt_d1,rst_n)
begin
  if (~rst_n)
    op_delval = 'h3ff;
  else if (op_cmplt_d1)
    ko_delval = $dist_normal(seed,delay_params[4],delay_params[5]); 
  else
    ko_delval = $dist_normal(seed,delay_params[6],delay_params[7]); 
end

//------------------------------------------------------------------------------ 
// Boolean Single rail module
//------------------------------------------------------------------------------ 

xilinx_true_dual_port_no_change_1_clock_ram #(
  .RAM_WIDTH            (RAM_WIDTH          ),
  .RAM_DEPTH            (RAM_DEPTH          ),
  .RAM_PERFORMANCE      ("LOW_LATENCY"      ),     //
  .INIT_FILE            (INIT_FILE          )      //
)u_RAM (
  .addra                (s_addra            ),     //
  .addrb                (s_addrb            ),     //
  .dina                 ({RAM_WIDTH{1'b0}}  ),     //
  .dinb                 ({RAM_WIDTH{1'b0}}  ),     //
  .clka                 (ip_cmplt_d1        ),     //
  .ena                  (ren                ),     //
  .enb                  (ren                ),     //
  .wea                  (1'b0               ),
  .web                  (1'b0               ),
  .rsta                 (~rst_n             ),     //
  .rstb                 (~rst_n             ),     //
  .regcea               (1'b0               ),     //
  .regceb               (1'b0               ),     //
  .douta                (s_douta            ),     //
  .doutb                (s_doutb            )      //
);


//------------------------------------------------------------------------------ 
// Output side Single rail to Dual rail conversion
//------------------------------------------------------------------------------

s2dreg #(
  .DW(RAM_WIDTH)
)u_douta_s2d(
  .rst_n          (rst_n      ),
  .del_value      (op_delval  ),
  .single_in      (s_douta    ),
  .ki             (ki         ),
  .ip_cmplt       (ip_cmplt   ),
  .ko             (ko_douta   ),
  .dual_out       (douta      )
);

s2dreg #(
  .DW(RAM_WIDTH)
)u_doutb_s2d(
  .rst_n          (rst_n      ),
  .del_value      (op_delval  ),
  .single_in      (s_doutb    ),
  .ki             (ki         ),
  .ip_cmplt       (ip_cmplt   ),
  .ko             (ko_doutb   ),
  .dual_out       (doutb      )
);

nv_thxx_zerodelay #(
  .NUM_IP(2)
)u_op_cmplt(
  .rst_n(rst_n),
  .din({ko_doutb,ko_douta}),
  .dout(op_cmplt)
);


//  The following function calculates the address width based on specified RAM depth
function integer clogb2;
  input integer depth;
    if (depth > 1)
      for (clogb2=0; depth>0; clogb2=clogb2+1)
        depth = depth >> 1;
    else
      clogb2=1;
endfunction

endmodule

