`timescale 1ns/1ps
module nv_key_stripper_tb ();

  reg rst_n;
  reg [63:0] key_i;
  reg ki;
  wire ko;
  wire [1:0] msbk_o;
  wire [5:0] mk_o;
  wire [63:0] apk_o;

  initial
  begin
    rst_n = 1;
#10 rst_n = 0;
#10 rst_n = 1;
#1000 $finish;
  end

  initial 
  begin
    ki = 0;
    key_i = 64'b0;

#5  ki    = 1;
#50 key_i = 64'b1001100110101010010110101001011010010101101001010110100101010110;
#5  ki    = 0;
#5  ki    = 1;
#5  key_i = 64'b0;

#50 key_i = 64'b1010101001101001011001010110011010100110011010101010101001011010;
#5  ki    = 0;
#5  ki    = 1;
#5  key_i = 64'b0;
#5  ki    = 0;

#50 key_i = 64'b1010101010101010101010101010101010101001010101100110101010011010;
#5  ki    = 1;
#5  key_i = 64'b0;
#5  ki    = 0;

#50 key_i = 64'b1010101010101010101010101010101010101010100101101001101001100110;
#5  ki    = 1;
#5  key_i = 64'b0;

// Trying to force a Data Null Data value without reading out result of last
// operation
#50 key_i = 64'b1010101010101010101010101010101010101010101010101010101010010110;
#5  ki    = 1;
#5  key_i = 64'b0;
#5  ki    = 0;

#50 key_i = 64'b1010101010101010101010101010101010101001010101100110101010011010;
#5  ki    = 0;
#5  key_i = 64'b0;
#5  ki    = 0;

  end

  initial
  begin
    $dumpfile("nv_key_stripper_dump.fst");
    $dumpvars;
  end

  nv_key_stripper u_dut (
    .rst_n(rst_n),
    .ki(ki),
    .ko(ko),
    .key_i(key_i),
    .msbk_o(msbk_o),
    .mk_o(mk_o),
    .apk_o(apk_o)
  );
endmodule
