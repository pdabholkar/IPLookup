`timescale 1ps/1ps
module nv_bloom_filter #(
  parameter BF_AWIDTH = 32
)(
  input                         rst_n,
  input                         ki,
  output                        ko,
  input  [63:0]                 crc_sr_i,
  input  [1:0]                  apk_i,
  input  [1:0]                  tlevel_inc_i,
  output [63:0]                 crc_sr_d1_o,
  output [((2*BF_AWIDTH)-1):0]  bf_idx0_d1_o,
  output [((2*BF_AWIDTH)-1):0]  bf_idx1_d1_o
);
//============================================================================== 
wire [31:0]           s_crc_sr_in;
wire                  s_apk;
wire                  s_tlevel_inc;
wire [31:0]           s_crc_sr_out;
wire [BF_AWIDTH-1:0]  s_bf_idx0;
wire [BF_AWIDTH-1:0]  s_bf_idx1;

wire                  ko_crc_sr_in;
wire                  ko_apk;
wire                  ko_tlevel_inc;
wire                  ip_cmplt;

wire                  ko_crc_sr_o;
wire                  ko_bfidx0_o;
wire                  ko_bfidx1_o;
wire                  op_cmplt;

//------------------------------------------------------------------------------ 
// op_delval, ko_delval and seed value are defined in the included file. 
// The MEAN and SD of the delay value are also specified in the included file
//------------------------------------------------------------------------------ 
// These are the declarations for the delay synthesis logic
reg  [9:0]  op_delval;
reg  [9:0]  ko_delval;
reg  [31:0] seed;

initial
begin
  seed = 1;
end

reg [31:0] delay_params[0:7];
initial
begin
  $readmemh("bloom_filter_delay.mem",delay_params);
end        
//------------------------------------------------------------------------------ 
// Input side Dual rail to Single rail
//------------------------------------------------------------------------------ 
d2sreg #(
  .DW(1)
)u_apk_d2s(
  .rst_n          (rst_n        ),
  .dual_in        (apk_i        ),
  .single_out     (s_apk        ),
  .ko             (ko_apk       )
);

d2sreg #(
  .DW(1)
)u_tlevel_inc_d2s(
  .rst_n          (rst_n        ),
  .dual_in        (tlevel_inc_i ),
  .single_out     (s_tlevel_inc ),
  .ko             (ko_tlevel_inc)
);

d2sreg #(
  .DW(32)
)u_crc_d2s(
  .rst_n          (rst_n        ),
  .dual_in        (crc_sr_i     ),
  .single_out     (s_crc_sr_in  ),
  .ko             (ko_crc_sr_in )
);

nv_thxx #(.NUM_IP(3))u_ip_cmplt(.rst_n(rst_n),.din({ko_crc_sr_in,ko_apk,ko_tlevel_inc}),.dout(ip_cmplt));         

//------------------------------------------------------------------------------ 
// Because we are working with a behavioural model here and artificially
// snthesizing the required delays, we can use the output completeness signal
// that indicates when the outputs are ready, and then delay it by a further 
// ko_delval which is the delay of the actual completion detection circuit to 
// generate the final ko signal
//------------------------------------------------------------------------------ 
psDelay u_ko_delay (
  .rst_n          (rst_n    ),
  .trig           (op_cmplt ),
  .del_value      (ko_delval),
  .dlyd_trig      (ko       )
);

//------------------------------------------------------------------------------ 
// creating a dummy ip_cmplt_d1 signal used to clock the boolean bloom_filter
// module. This is necessary because the boolean module in this case needs
// a clock signal.
// also creating a dummy delayed ip_cmplt_d2 signal used to sample the result of 
// the boolean operation and select the appropriate mean and sdev of the delay 
// if it is data dependent.
//------------------------------------------------------------------------------ 
wire ip_cmplt_d1;
wire ip_cmplt_d2;

assign #1 ip_cmplt_d1 = ip_cmplt;
assign #1 ip_cmplt_d2 = ip_cmplt_d1;

always @(rst_n,ip_cmplt_d2)
begin
  if (~rst_n)
  begin
    op_delval = 10'h3ff;
    ko_delval = 10'h3ff;
  end
  else if (ip_cmplt_d2)
  begin
    op_delval = $dist_normal(seed,delay_params[0],delay_params[1]);
    ko_delval = $dist_normal(seed,delay_params[4],delay_params[5]); 
  end
  else
  begin
    op_delval = $dist_normal(seed,delay_params[2],delay_params[3]);
    ko_delval = $dist_normal(seed,delay_params[6],delay_params[7]); 
  end
end

//------------------------------------------------------------------------------ 
// Boolean Single rail module
// BF_DEL2 parameter is set to '0' to allow the boolean module to be used in 
// here and ensure that the functionality is in line with the NCL schematic of 
// the bloom_filter module
//------------------------------------------------------------------------------ 
bloom_filter#(
  .BF_AWIDTH            (BF_AWIDTH          ),
  .BF_DEL2              (0                  )
)u_bloom_filter(
  .clk_i                (ip_cmplt_d2        ),
  .rst_n_i              (rst_n              ),
  .tlevel_inc_i         (s_tlevel_inc       ),
  .apk_i                (s_apk              ),
  .daddr_vld_i          (ip_cmplt           ),
  .crc_sr_i             (s_crc_sr_in        ),
  .crc_sr_d1_o          (s_crc_sr_out       ),
  .bf_idx0_d2_o         (s_bf_idx0          ), // These are d1 and not d2 because 
  .bf_idx1_d2_o         (s_bf_idx1          )  // BF_DEL2 parameter is set to '0'
);

//------------------------------------------------------------------------------ 
// Output side Single rail to Dual rail conversion
//------------------------------------------------------------------------------ 
s2dreg #(
  .DW(32)
)u_crc_sr_s2d(
  .rst_n          (rst_n        ),
  .del_value      (op_delval    ),
  .single_in      (s_crc_sr_out ),
  .ki             (ki           ),
  .ip_cmplt       (ip_cmplt     ),
  .ko             (ko_crc_sr_o  ),
  .dual_out       (crc_sr_d1_o  )
);

s2dreg #(
  .DW(BF_AWIDTH)
)u_bfidx0_s2d(
  .rst_n          (rst_n        ),
  .del_value      (op_delval    ),
  .single_in      (s_bf_idx0    ),
  .ki             (ki           ),
  .ip_cmplt       (ip_cmplt     ),
  .ko             (ko_bfidx0_o  ),
  .dual_out       (bf_idx0_d1_o )
);

s2dreg #(
  .DW(BF_AWIDTH)
)u_bfidx1_s2d(
  .rst_n          (rst_n        ),
  .del_value      (op_delval    ),
  .single_in      (s_bf_idx1    ),
  .ki             (ki           ),
  .ip_cmplt       (ip_cmplt     ),
  .ko             (ko_bfidx1_o  ),
  .dual_out       (bf_idx1_d1_o )
);

nv_thxx #(.NUM_IP(3))u_op_cmplt(.rst_n(rst_n),.din({ko_crc_sr_o,ko_bfidx1_o,ko_bfidx0_o}),.dout(op_cmplt));        
endmodule
