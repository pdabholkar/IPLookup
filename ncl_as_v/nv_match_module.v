`timescale 1ps/1ps
module nv_match_module #(
  parameter TDEPTH    = 1,
  parameter PTRIE     = 1,
  parameter NHI_WIDTH = 8
)(
  input                       rst_n,
  input                       ki,
  output                      ko,
  // Key information
  input    [1:0]              msbk_i,
  input    [5:0]              mk_i,
  input    [63:0]             apk_i,
  input    [1:0]              daddr_vld_i,
  // Info generated from previous pipeline stage's match stage
  input    [2*NHI_WIDTH-1:0]  nhi_i,
  input    [11:0]             matchlength_i,
  // Children information
  input    [1:0]              epsilon_i,
  // Prefix Information read. 
  input                       pfx_ram_read_i,
  input    [1:0]              bf_match_i,
  // First prefix in the node
  input    [1:0]              msb1_i,
  input    [1:0]              lsb1_i,
  input    [5:0]              mp1_i,
  input    [9:0]              lp1_i,
  input    [15:0]             nhi1_i,
  // Second prefix in the node
  input    [1:0]              msb2_i,
  input    [1:0]              lsb2_i,
  input    [5:0]              mp2_i,
  input    [9:0]              lp2_i,
  input    [15:0]             nhi2_i,
  // Info generated for next pipeline stage's match stage
  output   [15:0]             nhi_d2_o,
  output   [11:0]             matchlength_d2_o,
  output   [1:0]              match_found_d2_o
);
//============================================================================== 
wire [2*18-1:0]               pfx1_i;
wire [2*18-1:0]               pfx2_i;

wire                          s_msbk;
wire [2:0]                    s_mk;
wire [31:0]                   s_apk;
wire                          s_daddr_vld;
wire [7:0]                    s_nhi;
wire [5:0]                    s_matchlength;
wire                          s_epsilon;
wire [17:0]                   s_pfx1;
wire [17:0]                   s_pfx2;
wire                          ko_msbk;
wire                          ko_mk;
wire                          ko_apk;
wire                          ko_daddr_vld;
wire                          ko_nhi;
wire                          ko_ml;
wire                          ko_eps;
wire                          ko_pfx1;
wire                          ko_pfx2;
wire                          ko_predly;
wire                          ko_bf_match;

wire                          s_msb1;
wire                          s_lsb1;
wire [2:0]                    s_mp1;
wire [4:0]                    s_lp1;
wire [NHI_WIDTH-1:0]          s_nhi1;

wire                          s_msb2;
wire                          s_lsb2;
wire [2:0]                    s_mp2;
wire [4:0]                    s_lp2;
wire [NHI_WIDTH-1:0]          s_nhi2;

wire                          s_bf_match;

wire                          s_pfx_notnull;
wire [NHI_WIDTH-1:0]          s_nhi_intrnl;
wire [5:0]                    s_matchlength_intrnl;
wire                          s_match_found_intrnl;

wire [(1+NHI_WIDTH+6+1)-1:0]  s_intrnl;
wire [(1+NHI_WIDTH+6+1)-1:0]  s_intrnl_d1;
wire [(1+NHI_WIDTH+6+1)-1:0]  s_intrnl_d2;
wire [2*(1+NHI_WIDTH+6+1)-1:0]d_intrnl_d1;
wire [2*(1+NHI_WIDTH+6+1)-1:0]d_intrnl_d2;
wire [2*(1+NHI_WIDTH+6+1)-1:0]d_intrnl_d3;
wire                          ko_d1;
wire                          ko_d2;
wire                          ko_d3;
wire                          ip_cmplt_dvld0;
wire                          ip_cmplt_dvld1;
wire                          ip_cmplt;
wire                          ko_d2s_d1;
wire                          ko_d2s_d2;

wire                          pfx_notnull_d2;

wire                          ip_cmplt_d1;
wire                          ip_cmplt_d2;
wire                          ip_cmplt_d3;
wire                          ip_cmplt_d4;
wire                          ip_cmplt_d5;
wire                          ip_cmplt_d6;
wire                          bool_module_clk;

wire                          pfx_notnull;
//------------------------------------------------------------------------------ 
// op_delval, ko_delval and seed value are defined in the included file. 
// The MEAN and SD of the delay value are also specified in the included file
//------------------------------------------------------------------------------ 
// These are the declarations for the delay synthesis logic
reg  [9:0]  op_delval;
reg  [9:0]  ko_delval;
reg  [31:0] seed;

initial
begin
  seed = 1;
end

reg [31:0] delay_params[0:11];
initial
begin
  $readmemh("match_module_delay.mem",delay_params);
end           
//------------------------------------------------------------------------------ 
// Input side Dual rail to Single rail
//------------------------------------------------------------------------------ 

assign pfx1_i = {msb1_i,lsb1_i,mp1_i,lp1_i,nhi1_i};
assign pfx2_i = {msb2_i,lsb2_i,mp2_i,lp2_i,nhi2_i};

d2sreg #(
  .DW(1)
)u_daddr_vld_d2s(
  .rst_n          (rst_n    ),
  .dual_in        (daddr_vld_i),
  .single_out     (s_daddr_vld),
  .ko             (ko_daddr_vld)
);

d2sreg #(
  .DW(32)
)u_apk_d2s(
  .rst_n          (rst_n    ),
  .dual_in        (apk_i    ),
  .single_out     (s_apk    ),
  .ko             (ko_apk   )
);

d2sreg #(
  .DW(1)
)u_msbk_d2s(
  .rst_n          (rst_n    ),
  .dual_in        (msbk_i   ),
  .single_out     (s_msbk   ),
  .ko             (ko_msbk  )
);

d2sreg #(
  .DW(3)
)u_mk_d2s(
  .rst_n          (rst_n    ),
  .dual_in        (mk_i     ),
  .single_out     (s_mk     ),
  .ko             (ko_mk    )
);

d2sreg #(
  .DW(NHI_WIDTH)
)u_nhi_d2s(
  .rst_n          (rst_n    ),
  .dual_in        (nhi_i    ),
  .single_out     (s_nhi    ),
  .ko             (ko_nhi   )
);

d2sreg #(
  .DW(6)
)u_ml_d2s(
  .rst_n          (rst_n    ),
  .dual_in        (matchlength_i),
  .single_out     (s_matchlength),
  .ko             (ko_ml    )
);

d2sreg #(
  .DW(1)
)u_eps_d2s(
  .rst_n          (rst_n    ),
  .dual_in        (epsilon_i),
  .single_out     (s_epsilon),
  .ko             (ko_eps   )
);

d2sreg #(
  .DW(18)
)u_pfx1_d2s(
  .rst_n          (rst_n    ),
  .dual_in        (pfx1_i   ),
  .single_out     (s_pfx1   ),
  .ko             (ko_pfx1  )
);

//always @(ko_pfx1)
//  if (s_pfx1 == 'd0) $display("Prefix 1 info is all zeros");

d2sreg #(
  .DW(18)
)u_pfx2_d2s(
  .rst_n          (rst_n    ),
  .dual_in        (pfx2_i   ),
  .single_out     (s_pfx2   ),
  .ko             (ko_pfx2  )
);

//always @(ko_pfx2)
//  if (s_pfx2 == 'd0) $display("Prefix 2 info is all zeros");

d2sreg #(
  .DW(1)
)u_bf_match_d2s(
  .rst_n          (rst_n    ),
  .dual_in        (bf_match_i),
  .single_out     (s_bf_match),
  .ko             (ko_bf_match)
);

// This is to handle the case of null outputs from the Prefix RAM. The pfx_ram_read_i signal which is the output
// of the Prefix RAM indicates whether the prefixes were read. If the prefixes are data, then ko_pfx1 and 
// ko_pfx2 will be data. If they are not, then pfx_ram_read_i will ensure that the ip_cmplt signal is asserted
// and the wavefront can move forward. 
// THIS IS GOING TO BE A COMPLEX FUNCTIONALITY TO CODE !!! CAREFUL !!!
//
// HOWEVER How do we ensure that ko_pfx1 and ko_pfx2 both are generated if
// one of the prefix is DATA and the other is NULL

// Using bf_match values to control whether ko_pfx1 and ko_pfx2 are used to indicate input completeness

nv_thxx_zerodelay #(
  .NUM_IP(9)
)u_ip_cmplt_dvld1(
  .rst_n(rst_n),
  //          8           7         6     5       4      3     2    1                                  0
  .din({ko_daddr_vld,ko_bf_match,ko_apk,ko_msbk,ko_mk,ko_nhi,ko_ml,ko_eps,(((pfx_ram_read_i | ko_pfx1 | ko_pfx2) & bf_match_i[1]) | bf_match_i[0])}),
  .dout(ip_cmplt_dvld1)
);

nv_thxx_zerodelay #(
  .NUM_IP(1)
)u_ip_cmplt_dvld0(
  .rst_n(rst_n),
  .din({ko_daddr_vld}),
  .dout(ip_cmplt_dvld0)
);

//nv_thxx #(.NUM_IP(2))u_ip_cmplt_t(.rst_n(rst_n),.din({daddr_vld_d1[1],daddr_t_ko}),.dout(daddr_vld_t_ko));
//nv_thxx #(.NUM_IP(2))u_ip_cmplt_f(.rst_n(rst_n),.din({daddr_vld_d1[0],daddr_f_ko}),.dout(daddr_vld_f_ko));

assign ip_cmplt = s_daddr_vld ? ip_cmplt_dvld1 : ip_cmplt_dvld0;
//------------------------------------------------------------------------------ 
// generate a pseudo clock that toggles twice for every complete input to
// ensure that the boolean module is clocked correctly and it generates the
// appropriate output for the given input.
//------------------------------------------------------------------------------ 

assign #2 ip_cmplt_d1 = ip_cmplt;
assign #2 ip_cmplt_d2 = ip_cmplt_d1;
assign #2 ip_cmplt_d3 = ip_cmplt_d2;
assign #2 ip_cmplt_d4 = ip_cmplt_d3;
assign #2 ip_cmplt_d5 = ip_cmplt_d4;
assign #2 ip_cmplt_d6 = ip_cmplt_d5;

assign bool_module_clk = (ip_cmplt_d1 & ~ip_cmplt_d2) | (ip_cmplt_d3 & ~ip_cmplt_d4) | (ip_cmplt_d5 & ~ip_cmplt_d6);

always @(ip_cmplt_d2,rst_n)
begin
if(~rst_n)
  ko_delval = 10'h3ff;
else if (ip_cmplt_d1)
  ko_delval = $dist_normal(seed,delay_params[4],delay_params[5]); 
else
  ko_delval = $dist_normal(seed,delay_params[6],delay_params[7]); 
end

always @(ko_d2s_d2,rst_n)
begin
  if (~rst_n)
    op_delval = 10'h3ff;
  else if (ko_d2s_d2)
    if (pfx_notnull_d2)
      op_delval = $dist_normal(seed,delay_params[0],delay_params[1]);
    else
      op_delval = $dist_normal(seed,delay_params[8],delay_params[9]);
  else
    if (pfx_notnull_d2)
      op_delval = $dist_normal(seed,delay_params[2],delay_params[3]);
    else
      op_delval = $dist_normal(seed,delay_params[10],delay_params[11]);
end

//------------------------------------------------------------------------------ 
// ko from first stage delayed appropriately to generate module ko
//------------------------------------------------------------------------------ 

psDelay u_ko_delay (
  .rst_n          (rst_n    ),
  .trig           (ko_d1    ),
  .del_value      (ko_delval),
  .dlyd_trig      (ko       )
);

//------------------------------------------------------------------------------ 
// If the prefix is not null, then the single rail prefix is decomposed into the 
// individual components. If the prefix is null, then force the decomposed 
// individual components to take the value of zero, which will ensure that the 
// nhi and ml values that have flowed in from the previous stage are passed on 
// as outputs of this stage. This update has to happen only when pfx_ram_read_i is high,
// indicating a data wavefront in the dual rail input. When pfx_ram_read_i is zero

//assign {s_msb1,s_lsb1,s_mp1,s_lp1,s_nhi1} = pfx_ram_read_i ? (ko_pfx1 ? s_pfx1 : 18'd0 ) : {s_msb1,s_lsb1,s_mp1,s_lp1,s_nhi1} ; 
//assign {s_msb2,s_lsb2,s_mp2,s_lp2,s_nhi2} = pfx_ram_read_i ? (ko_pfx2 ? s_pfx2 : 18'd0 ) : {s_msb2,s_lsb2,s_mp2,s_lp2,s_nhi2} ;

// if s_bf_match is 1, then decompose the prefix read, else all fields are 0

assign {s_msb1,s_lsb1,s_mp1,s_lp1,s_nhi1} = s_bf_match ? s_pfx1 : 18'd0;
assign {s_msb2,s_lsb2,s_mp2,s_lp2,s_nhi2} = s_bf_match ? s_pfx2 : 18'd0;

//------------------------------------------------------------------------------ 
// Boolean Single rail module
//------------------------------------------------------------------------------ 
match_module #(
  .TDEPTH               (TDEPTH             ),
  .PTRIE                (PTRIE              ),
  .NHI_WIDTH            (NHI_WIDTH          )
)u_match_module(
  .clk_i                (bool_module_clk    ),
  .rst_n_i              (rst_n              ),
  // Key information
  .msbk_i               (s_msbk             ),
  .mk_i                 (s_mk               ),
  .apk_a1_i             (s_apk              ),
  .pfx_notnull_a1_i     (pfx_notnull        ),
  .daddr_vld_a1_i       (ip_cmplt & ~ip_cmplt_d2 & s_daddr_vld),
  // Info generated from previous pipeline stage's match stage
  .nhi_i                (s_nhi              ),
  .matchlength_i        (s_matchlength      ),
  // Children information
  .epsilon_i            (s_epsilon          ),
  // First prefix in the node
  .msb1_i               (s_msb1             ),
  .lsb1_i               (s_lsb1             ),
  .mp1_i                (s_mp1              ),
  .lp1_i                (s_lp1              ),
  .nhi1_i               (s_nhi1             ),
  // Second prefix in the node
  .msb2_i               (s_msb2             ),
  .lsb2_i               (s_lsb2             ),
  .mp2_i                (s_mp2              ),
  .lp2_i                (s_lp2              ),
  .nhi2_i               (s_nhi2             ),
  // Third prefix in the node
  .msb3_i               (1'd0               ),
  .lsb3_i               (1'd0               ),
  .mp3_i                (3'd0               ),
  .lp3_i                (5'd0               ),
  .nhi3_i               (8'd0               ),
  // Info generated for next pipeline stage's match stage
  .pfx_notnull_d2_o     (s_pfx_notnull      ),
  .nhi_d2_o             (s_nhi_intrnl       ),
  .matchlength_d2_o     (s_matchlength_intrnl),
  .match_found_d2_o     (s_match_found_intrnl)
);
assign pfx_notnull = ko_pfx1 | ko_pfx2;

assign s_intrnl = {s_pfx_notnull,s_nhi_intrnl,s_matchlength_intrnl,s_match_found_intrnl};

s2dreg #(
  .DW((1+NHI_WIDTH+6+1))
)u_stg1_s2d(
  .rst_n          (rst_n        ),
  .del_value      (10'd2        ),
  .single_in      (s_intrnl     ),
  .ki             (ko_d2        ),
  .ip_cmplt       (ip_cmplt_d6  ),
  .ko             (ko_d1        ),
  .dual_out       (d_intrnl_d1  )
);

//------------------------------------------------------------------------------ 
// second NCL registration stage
//------------------------------------------------------------------------------ 
d2sreg #(
  .DW(1+NHI_WIDTH+6+1)
)u_stg2_d2s(
  .rst_n          (rst_n      ),
  .dual_in        (d_intrnl_d1),
  .single_out     (s_intrnl_d1),
  .ko             (ko_d2s_d1  )
);

s2dreg #(
  .DW(1+NHI_WIDTH+6+1)
)u_stg2_s2d(
  .rst_n          (rst_n        ),
  .del_value      (10'd2        ),
  .single_in      (s_intrnl_d1  ),
  .ki             (ko_d3        ),
  .ip_cmplt       (ko_d2s_d1    ),
  .ko             (ko_d2        ),
  .dual_out       (d_intrnl_d2  )
);

//------------------------------------------------------------------------------ 
// third NCL registration stage
//------------------------------------------------------------------------------ 
d2sreg #(
  .DW(1+NHI_WIDTH+6+1)
)u_stg3_d2s(
  .rst_n          (rst_n      ),
  .dual_in        (d_intrnl_d2),
  .single_out     (s_intrnl_d2),
  .ko             (ko_d2s_d2  )
);

assign pfx_notnull_d2=s_intrnl_d2[(1+NHI_WIDTH+6+1)-1];

s2dreg #(
  .DW(1+NHI_WIDTH+6+1)
)u_stg3_s2d(
  .rst_n          (rst_n        ),
  .del_value      (op_delval    ),
  .single_in      (s_intrnl_d2  ),
  .ki             (ki           ),
  .ip_cmplt       (ko_d2s_d2    ),
  .ko             (ko_d3        ),
  .dual_out       (d_intrnl_d3  )
);

assign {nhi_d2_o,matchlength_d2_o,match_found_d2_o} = d_intrnl_d3;

endmodule

