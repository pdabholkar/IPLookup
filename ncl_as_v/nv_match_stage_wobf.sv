// All parameters that are passed to instantiated blocks are specified as Boolean widths. 
// The 2x for Null and Data is only used when defining buses or nets in the design

`timescale 1ns/1ps
module nv_match_stage_wobf #(

`ifdef MSTAGE_SIM
  parameter MEM_SIZE      = 1,
  parameter MEM_AWIDTH    = 1,
  parameter CP_WIDTH      = 1,
  parameter PFX_INIT_FILE = "",
  parameter LRC_INIT_FILE = "",
  parameter BF_SIZE       = 1,
  parameter BF_AWIDTH     = 1,
  parameter BF_INIT_FILE  = "",
`endif  
  parameter TDEPTH        = 1,
  parameter PTRIE         = 2,
  parameter IP_ADDRW      = 32,
  parameter NHI_WIDTH     = 8,
  parameter USE_BLOOM_RAM = 1,
  parameter USE_BLOOM_CRC = 1

)(
  input                           rst_n,
  input                           ki,
  output                          ko,

  // Key Information that is passed from one match stage to the next without
  // any change
  input [1:0]                     msbk_i,
  input [5:0]                     mk_i,

  // Inputs from the previous match stage's child computation block
  input [1:0]                     tlevel_inc_i,
  input [(2*IP_ADDRW)-1:0]        address_i,
  input [(2*IP_ADDRW)-1:0]        apk_i,
  input [1:0]                     daddr_vld_i,

  // Inputs from the previous match stage's prefix match block
  input [(2*NHI_WIDTH)-1:0]       nhi_i,
  input [11:0]                    matchlength_i,

  // Outputs from the CRC computation block appropriately delayed

  // Outputs from the next child computation block appropriately delayed
  output     [1:0]                tlevel_inc_d6_o,
  output     [(2*IP_ADDRW)-1:0]   address_d6_o,
  output     [(2*IP_ADDRW)-1:0]   apk_d6_o,
  output     [1:0]                daddr_vld_d6_o,

  // Outputs from the prefix match block appropriately delayed
  output     [(2*NHI_WIDTH)-1:0]  nhi_d6_o,
  output reg [11:0]               matchlength_d6_o,
  output     [1:0]                match_found_d6_o
);
//============================================================================== 

`ifndef MSTAGE_SIM
  `include "../python/memMap_mgm/mdepth_vs_tdepth_memMap_mgm.h"
  `include "../python/memMap_mgm/bfdepth_vs_tdepth_memMap_mgm.h"
`endif

//`include "mem_and_param_config.h"

localparam PFX_RAM_WIDTH = (PTRIE*(1+1+3+5+NHI_WIDTH)); // There is no extra Null bit
// PTRIE x (MSB, LSB, |x|, |z|, NHI BITS)
localparam LRC_RAM_WIDTH = (2*CP_WIDTH) + 1 + 1 ;
// 2 x [CHILD_POINTER WIDTH)] + eps_flag + nodeinuse_flag
// The 2x here is Left and Right child

wire                              lrc_ram_ki;
wire                              lrc_ram_ko;
wire [(2*LRC_RAM_WIDTH)-1:0]      lrc_ram_info;  // 2x for Null and Data rails
wire [(2*PFX_RAM_WIDTH)-1:0]      pfx_ram_info;  // 2x for Null and Data rails

wire [(2*NHI_WIDTH)-1:0]          nhi_d3;
wire [(2*NHI_WIDTH)-1:0]          nhi_d6;
wire [(2*NHI_WIDTH)-1:0]          mm_nhi_d6;
wire [11:0]                       matchlength_d3;

wire [1:0]                        msb1_d3;
wire [1:0]                        lsb1_d3;
wire [5:0]                        mp1_d3;
wire [9:0]                        lp1_d3;
wire [(2*NHI_WIDTH)-1:0]          nhi1_d3;

wire [1:0]                        msb2_d3;
wire [1:0]                        lsb2_d3;
wire [5:0]                        mp2_d3;
wire [9:0]                        lp2_d3;
wire [(2*NHI_WIDTH)-1:0]          nhi2_d3;

wire [(2*CP_WIDTH)-1:0]           lc_d1;                // This is still CP_WIDTH because it has to be extracted from the lrc_ram_info output
wire [(2*CP_WIDTH)-1:0]           rc_d1;                // This is still CP_WIDTH because it has to be extracted from the lrc_ram_info output
wire [1:0]                        node_in_use_d1;
reg  [1:0]                        epsilon_d1;
wire                              next_child_ki;
wire                              next_child_ko;

wire  [(2*(MEM_AWIDTH-1))-1:0]    pfx_ram_addr;         // Change to address
wire  [1:0]                       msbk_d3;
wire  [5:0]                       mk_d3;
wire                              key_info_reg_ki;
wire                              key_info_reg_ko;
wire                              nhi_reg1_ki;
wire                              nhi_reg1_ko;
wire                              nhi_reg2_ki;
wire                              nhi_reg2_ko;
wire                              ml_reg_ki;
wire                              ml_reg_ko;
wire                              epsilon_reg1_ki;
wire                              epsilon_reg2_ki;
wire                              epsilon_reg2_ko;
wire  [(2*IP_ADDRW)-1:0]          apk_d1;
wire                              apk_reg1_ki;
wire                              apk_reg1_ko;

wire                              daddr_reg1_ki;
wire                              daddr_reg1_ko;
wire                              daddr_reg2_ki;
wire                              daddr_reg2_ko;
wire                              daddr_reg3_ki;
wire                              daddr_reg3_ko;
wire                              daddr_reg4_ki;
wire                              daddr_reg4_ko;

wire                              daddr_g1_ko;
wire                              daddr_g2_ko;

wire [(2*IP_ADDRW)-1:0]           apk_d2_o;
wire [(2*(CP_WIDTH-1))-1:0]       address_d2_o;         // This is 1 less that CP_WIDTH because the MSB indicating NULL is discarded.
wire [1:0]                        tlevel_inc_d2_o;
wire [1:0]                        daddr_vld_d2_o;

wire [(2*(CP_WIDTH-1))-1:0]       address_d6;           // This is 1 less that CP_WIDTH because the MSB indicating NULL is discarded.
wire [1:0]                        tlevel_inc_d6;
wire [(2*IP_ADDRW)-1:0]           apk_d6;


wire                              nc_reg_stage_ko;
wire                              dvld_reg_stage_ko;

wire                              pfx_ram_ki;
wire                              pfx_ram_ko;
wire                              pfx_ram_read;

wire                              match_module_ko;
wire                              match_module_ki;

wire                              daddr_t_ko;
wire                              daddr_f_ko;
wire [1:0]                        daddr_vld_d6;
wire [1:0]                        daddr_vld_d4;
wire [1:0]                        daddr_vld_d3;
wire [1:0]                        daddr_vld_d2;
wire [1:0]                        daddr_vld_d1;

wire                              lrc_ram_read_d1;

wire [(2*NHI_WIDTH)-1:0]          nhi_i_dvld_1;
wire [(2*NHI_WIDTH)-1:0]          nhi_i_dvld_0;

wire [2*NHI_WIDTH-1:0]            nhi_dvld0_d6;
wire [2*NHI_WIDTH-1:0]            nhi_dvld1_d6;

wire  [1:0]                       epsilon_rawread;

wire  [1:0]                       nc_daddr_vld_d6;
wire                              daddr_vld_d6_o_pre;                     

wire  [11:0]                      matchlength_d6;
wire  [1:0]                       match_found_d6;
wire                              daddr_vld_d6_o_cmplt;
wire                              matchlength_d6_cmplt;      

wire                              daddr_vld_t_ko;
wire                              daddr_vld_f_ko;

wire [1:0]                        addr_d2_dvldout;
wire [1:0]                        nhi_d3_dvldout;
wire [1:0]                        ml_d3_dvldout;
wire [1:0]                        epsilon_d3_dvldout;
wire [1:0]                        key_info_d3_dvldout;
wire [1:0]                        apk_d1_dvldout;
wire [1:0]                        apk_d2_dvldout;
wire [1:0]                        bf_match_d3_dvldout;

wire                              daddr_eps_reg1_ki;
wire                              daddr_eps_reg2_ki;

wire 				                      pfx_ram_ko_bf_match;

//============================================================================== 
// ko generation
//============================================================================== 
assign ko = daddr_g1_ko;

//============================================================================== 
// Next child RAM
//==============================================================================

assign lrc_ram_ki = next_child_ko;

nv_single_port_rom_wNC #(
  .RAM_WIDTH        (LRC_RAM_WIDTH      ), 
  .RAM_DEPTH        (MEM_SIZE           ), 
  .INIT_FILE        (LRC_INIT_FILE      )
)u_lrc_ram(
  .A                (address_i[2*(MEM_AWIDTH-1)-1:0]), // Write address bus, width determined from RAM_DEPTH
  .ki               (lrc_ram_ki         ), // ki from the following stage used as read enable
  .rst_n            (rst_n              ), // Output reset (does not affect memory contents)
  .DOUT             (lrc_ram_info       ), // RAM output data
  .ko               (lrc_ram_ko         )  // Indicator that data has been read by the RAM
);

assign lrc_ram_read_d1 = lrc_ram_ko;

assign {lc_d1,rc_d1,epsilon_rawread,node_in_use_d1} = lrc_ram_info;

// This epsilon_d1 is used in the match_module and if the data read from the RAM is Null, then
// the match module still needs to be indicated that the epsilon is "zero" and cannot just be
// kept at NULL.
always @(lrc_ram_read_d1)
if (lrc_ram_read_d1 & (epsilon_rawread == 2'b00))
  epsilon_d1 = 2'b01;
else
  epsilon_d1 = epsilon_rawread;

//============================================================================== 
// Registration stage for all inputs because a few of them are to be used after 
// the next child information has been read from the RAM
//============================================================================== 

assign nhi_reg1_ki      = match_module_ko;
assign key_info_reg_ki  = match_module_ko;
assign ml_reg_ki        = match_module_ko;
assign epsilon_reg2_ki  = match_module_ko;
assign epsilon_reg1_ki  = match_module_ko;
assign addr_reg_ki      = pfx_ram_read;

assign daddr_reg1_ki    = daddr_g2_ko;
assign daddr_reg2_ki    = daddr_reg3_ko;
assign daddr_reg3_ki    = daddr_reg4_ko;
assign daddr_reg4_ki    = ki;

nv_thxx #(
  .NUM_IP(7)
)u_daddr_g1_ko(
  .rst_n(rst_n),
  //        6              5           4           3          2         1          0
  .din({apk_reg1_ko,key_info_reg_ko,ml_reg_ko,nhi_reg1_ko,pfx_ram_ko,lrc_ram_ko,daddr_reg1_ko}),
  .dout(daddr_g1_ko)
);

nv_thxx #(
  .NUM_IP(2)
)u_daddr_g2_ko(
  .rst_n(rst_n),
  .din({match_module_ko,daddr_reg2_ko}),
  .dout(daddr_g2_ko)
);
 
nv_reg #(
  .DWIDTH           (1                  )
)u_dut_daddr1(
  .rst_n            (rst_n              ),
  .din              (daddr_vld_i        ),
  .ki               (daddr_reg1_ki      ),
  .ko               (daddr_reg1_ko      ),
  .dout             (daddr_vld_d1       )
);

nv_reg #(
  .DWIDTH           (1                  )
)u_dut_daddr2(
  .rst_n            (rst_n              ),
  .din              (daddr_vld_d1       ),
  .ki               (daddr_reg2_ki      ),
  .ko               (daddr_reg2_ko      ),
  .dout             (daddr_vld_d2       )
);

nv_reg #(
  .DWIDTH           (1                  )
)u_dut_daddr3(
  .rst_n            (rst_n              ),
  .din              (daddr_vld_d2       ),
  .ki               (daddr_reg3_ki      ),
  .ko               (daddr_reg3_ko      ),
  .dout             (daddr_vld_d3       )
);

nv_reg #(
  .DWIDTH           (1                  )
)u_dut_daddr4(
  .rst_n            (rst_n              ),
  .din              (daddr_vld_d3       ),
  .ki               (daddr_reg4_ki      ),
  .ko               (daddr_reg4_ko      ),
  .dout             (daddr_vld_d4       )
);

nv_reg_stages_gated #(
  .STAGES           (1                  ),
  .DWIDTH           (NHI_WIDTH          )
)u_nhi_reg1(
  .rst_n            (rst_n              ),
  .dvldin           (daddr_vld_i        ),
  .din              (nhi_i              ),
  .ki               (nhi_reg1_ki        ),
  .ko               (nhi_reg1_ko        ),
  .dvldout          (nhi_d1_dvldout     ),
  .dout             (nhi_d1             )
);

nv_reg_stages_gated #(
  .STAGES           (1                  ),
  .DWIDTH           (6                  )
)u_ml_reg(
  .rst_n            (rst_n              ),
  .dvldin           (daddr_vld_i        ),
  .din              (matchlength_i      ),
  .ki               (ml_reg_ki          ),
  .ko               (ml_reg_ko          ),
  .dvldout          (ml_d1_dvldout      ),
  .dout             (matchlength_d1     )
);

nv_reg_stages_gated #(
  .STAGES           (1                  ),
  .DWIDTH           (3+1                )
)u_key_info_reg(
  .rst_n            (rst_n              ),
  .dvldin           (daddr_vld_i        ),
  .din              ({mk_i,msbk_i}      ),
  .ki               (key_info_reg_ki    ),
  .ko               (key_info_reg_ko    ),
  .dvldout          (key_info_d1_dvldout),
  .dout             ({mk_d1,msbk_d1}    )
);

assign nhi_reg2_ki  = ki;
// nhi is not gated because it has to be sent down the whole pipeline
// and extracted out of the last stage.
// Can this be sent along with the inverted daddr_vld_i signal so that 
// it remains in sync with the daddr_vld signal and at the output, all that
// would be needed is 'OR'ing the nhi_d6 outputs out of the match_module and 
// the delay register and all would be done.
nv_reg_stages #(
  .STAGES           (4                  ),
  .DWIDTH           (NHI_WIDTH          )
)u_nhi_reg2(
  .rst_n            (rst_n              ),
  .din              (nhi_i              ),
  .ki               (nhi_reg2_ki        ),
  .ko               (nhi_reg2_ko        ),
  .dout             (nhi_d4             )
);

assign apk_reg1_ki = next_child_ko;

nv_reg_stages_gated #(
  .STAGES           (1                  ),
  .DWIDTH           (IP_ADDRW           )
)u_apk_reg1(
  .rst_n            (rst_n              ),
  .dvldin           (daddr_vld_i        ),
  .din              (apk_i              ),
  .ki               (apk_reg1_ki        ),
  .ko               (apk_reg1_ko        ),
  .dvldout          (apk_d1_dvldout     ),
  .dout             (apk_d1             )
);

//============================================================================== 
// Next child computation and registration stages to match those found in the 
// registration stage of match module and prefix RAMs
//==============================================================================

assign next_child_ki = nc_reg_stage_ko; // To be completed
 
nv_next_child #(
  .CP_WIDTH         (CP_WIDTH           ),    // This has one extra bit
  .IP_ADDRW         (IP_ADDRW           )
)u_next_child(
  .rst_n            (rst_n              ),
  .ki               (next_child_ki      ),
  .ko               (next_child_ko      ),

  .lc_i             (lc_d1[2*CP_WIDTH-1:0]), // This has one extra bit than required by the next_child_module
  .rc_i             (rc_d1[2*CP_WIDTH-1:0]), // This has one extra bit than required by the next_child_module
  .lrc_ram_read_i   (lrc_ram_read_d1    ),
  .apk_i            (apk_d1             ),    
  .epsilon_i        (epsilon_rawread    ),
 
  .apk_d1_o         (apk_d2_o           ),
  .address_d1_o     (address_d2_o       ),    // The port has one extra bit, but the wire has the exact number of bits ignore warning
  .tlevel_inc_d1_o  (tlevel_inc_d2_o    ),  
  .daddr_vld_d1_o   (daddr_vld_d2_o     )
);

nv_reg_stages_gated #(
  .STAGES           (2                  ),
  .DWIDTH           (1+1+(CP_WIDTH-1)+IP_ADDRW)     // Change to address
)u_nc_reg_stage(
  .rst_n            (rst_n              ),
  .din              ({daddr_vld_d2_o,tlevel_inc_d2_o,address_d2_o,apk_d2_o}),
  .dvldin           (daddr_vld_d2       ),
  .ki               (ki                 ),
  .ko               (nc_reg_stage_ko    ),
  .dout             ({nc_daddr_vld_d4,tlevel_inc_d4,address_d4,apk_d6}),
  .dvldout          ()
);

// Padding the address output.
assign address_d4_o    =  (nc_daddr_vld_d4[1:0] == 2'b10) ? {{(IP_ADDRW-CP_WIDTH+1){1'b0,nc_reg_stage_ko}},address_d4} : 'd0;
assign apk_d4_o        =  (nc_daddr_vld_d4[1:0] == 2'b10) ? apk_d4 : 'd0;
assign tlevel_inc_d4_o =  (nc_daddr_vld_d4[1:0] == 2'b10) ? tlevel_inc_d4 : 'd0;

// Generating the final daddr_vld signal
nv_thxx #(
  .NUM_IP(2)
)u_daddr_vld_d4_o_1(
  .rst_n(rst_n),
  .din({daddr_vld_d4[1],nc_daddr_vld_d4[1]}),
  .dout(daddr_vld_d4_o[1])
);

nv_thxx #(
  .NUM_IP(2)
)u_daddr_vld_d4_o_0(
  .rst_n(rst_n),
  .din({daddr_vld_d4[1],nc_daddr_vld_d4[0]}),
  .dout(daddr_vld_d4_o_pre)
);

assign daddr_vld_d4_o[0] = daddr_vld_d4_o_pre | daddr_vld_d4[0];

// Gating the outputs of the match_stage on the basis of the daddr_vld_d6_o signal
assign daddr_vld_d4_o_cmplt = |daddr_vld_d4_o;

//============================================================================== 
// Prefix RAM
//============================================================================== 
// ki from the match module used as a read enable
assign pfx_ram_ki = match_module_ko;   
// No BF...... So pfx_ram_addr is same as lrc_ram_addr
assign pfx_ram_addr = address_i;      // Change to address

nv_single_port_rom_wNC #(
  .RAM_WIDTH        (PFX_RAM_WIDTH      ), 
  .RAM_DEPTH        (MEM_SIZE           ), 
  .INIT_FILE        (PFX_INIT_FILE      )
)u_pfx_ram(
  .A                (pfx_ram_addr       ),    // This is already MEM_AWIDTH-1 in size and the port is also the exact size.
  .ki               (pfx_ram_ki         ),    
  .rst_n            (rst_n              ),    // Output reset (does not affect memory contents)
  .DOUT             (pfx_ram_info       ),    // RAM output data. If bf_match is null, then pfx_ram_info will also be null
  .ko               (pfx_ram_ko         )     // Indicator that data has been read by the RAM
);

assign pfx_ram_read = pfx_ram_ko;

assign {msb1_d1[1:0],lsb1_d1[1:0],mp1_d1[5:0],lp1_d1[9:0],nhi1_d1[(2*NHI_WIDTH)-1:0],msb2_d1[1:0],lsb2_d1[1:0],mp2_d1[5:0],lp2_d1[9:0],nhi2_d1[(2*NHI_WIDTH)-1:0]} = pfx_ram_info;

//============================================================================== 
// Match Module
//============================================================================== 

assign match_module_ki = ki;

nv_match_module #(
  .TDEPTH           (TDEPTH             ),
  .PTRIE            (PTRIE              ),
  .NHI_WIDTH        (NHI_WIDTH          )
)u_match_module(
  .rst_n            (rst_n              ),
  .ki               (match_module_ki    ),
  .ko               (match_module_ko    ),
  // Key information
  .msbk_i           (msbk_d1            ),
  .mk_i             (mk_d1              ),
  .apk_i            (apk_d1             ),
  .daddr_vld_i      (daddr_vld_d1       ),
  // Info generated from previous pipeline stage's match stage
  .nhi_i            (nhi_d1             ),
  .matchlength_i    (matchlength_d1     ),
  // Children information
  .epsilon_i        (epsilon_d1         ),
  // Prefix information read. Functionality not implemented as of now
  .pfx_ram_read_i   (pfx_ram_read       ),
  .bf_match_i       (bf_match_d1        ),
  // First prefix in the node
  .msb1_i           (msb1_d1            ),
  .lsb1_i           (lsb1_d1            ),
  .mp1_i            (mp1_d1             ),
  .lp1_i            (lp1_d1             ),
  .nhi1_i           (nhi1_d1            ),
  // Second prefix in the node
  .msb2_i           (msb2_d1            ),
  .lsb2_i           (lsb2_d1            ),
  .mp2_i            (mp2_d1             ),
  .lp2_i            (lp2_d1             ),
  .nhi2_i           (nhi2_d1            ),
  // Info generated for next pipeline stage's match stage
  .nhi_d2_o         (mm_nhi_d4          ),
  .matchlength_d2_o (matchlength_d4     ),
  .match_found_d2_o (match_found_d4     )
);

nv_thxx_wide #(
  .WIDTH(NHI_WIDTH)
)u_nhi_dvld1_d6(
  .rst_n(rst_n),
  .din({daddr_vld_d4[1],mm_nhi_d4}),
  .dout(nhi_dvld1_d4)
);

nv_thxx_wide #(
  .WIDTH(NHI_WIDTH)
)u_nhi_dvld0_d6(
  .rst_n(rst_n),
  .din({daddr_vld_d4[0],nhi_d4}),
  .dout(nhi_dvld0_d4)
);

assign nhi_d4_o = /*nhi_dvld0_d6 |*/ nhi_dvld1_d4;

assign matchlength_d4_cmplt = |matchlength_d4[1:0];

always @(daddr_vld_d4_o_cmplt,matchlength_d4_cmplt,rst_n)
begin
  if(~rst_n)
    matchlength_d4_o = 'd0;
  else if (daddr_vld_d4_o == 2'b10)
    matchlength_d4_o = matchlength_d4;
  else
    matchlength_d4_o = 'd0;
end

// The match_found signal is not gated as it indicates the prefix was found in 
// this stage. match_found should show a data value irrespective of whether the 
// next stage address is valid or invalid.
assign match_found_d4_o = match_found_d4;
//============================================================================== 
// Bloom filtering
//============================================================================== 

// DEBUG and LOGGING
reg [15:0] bf_match_counter;
reg [15:0] bf_not_match_counter;

always @(posedge (match_found_d4[0] | match_found_d4[1])  or negedge rst_n)
begin
  if(~rst_n)
    bf_match_counter <= 'd0;
  else
    bf_match_counter <= bf_match_counter + 1'd1;
end

always @(posedge (match_found_d4[0] | match_found_d4[1]) or negedge rst_n)
begin
  if(~rst_n)
    bf_not_match_counter <= 'd0;
  else
    bf_not_match_counter <= bf_not_match_counter + 1'd1;
end


endmodule
