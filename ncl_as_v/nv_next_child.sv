//****************************************************************************** 
// IN THE FINAL IMPLEMENTATION WE MAY WANT THE DOUT LINES OF THE RAM TO REMAIN *
// AS NULL AND NOT TRANSITION TO DATA AT ALL. THIS WILL THEN CAUSE A CHANGE IN *
// THE DOWNSTREAM IMPLEMENTATION                                               *
//****************************************************************************** 
`timescale 1ps/1ps
module nv_next_child #(
  parameter CP_WIDTH = 16,            // This has one extra bit
  parameter IP_ADDRW = 16
)(
  input                       rst_n,
  input                       ki,
  output                      ko,

  input [(2*CP_WIDTH)-1:0]    lc_i,   // This has one extra bit
  input [(2*CP_WIDTH)-1:0]    rc_i,   // This has one extra bit
  input [1:0]                 epsilon_i,
  input [(2*IP_ADDRW)-1:0]    apk_i,    
  input                       lrc_ram_read_i,
 
  output [1:0]                tlevel_inc_d1_o,
  output [(2*CP_WIDTH)-1:0]   address_d1_o,  // This has one extra bit
  output [(2*IP_ADDRW)-1:0]   apk_d1_o,
  output [1:0]                daddr_vld_d1_o
);

//============================================================================== 
wire [CP_WIDTH-1:0]   s_lc;
wire [CP_WIDTH-1:0]   s_rc;
wire                  s_epsilon;
wire [IP_ADDRW-1:0]   s_apk;
reg  [CP_WIDTH-1:0]   s_lc_gated;
reg  [CP_WIDTH-1:0]   s_rc_gated;
reg                   s_epsilon_gated;
wire                  s_tlvl_inc_o;
wire [CP_WIDTH-1:0]   s_address_o;
wire [IP_ADDRW-1:0]   s_apk_o;
wire                  s_daddr_vld_o;

wire                  ko_lc;
wire                  ko_rc;
wire                  ko_apk;
wire                  ko_epsilon;
wire                  ip_cmplt;
wire                  ko_addr_o;
wire                  ko_daddr_vld_o;

wire                  ko_tlvl_inc_o;
wire                  ko_apk_o;
wire                  op_cmplt;

wire                  op_cmplt_data;
wire                  ip_cmplt_data;
wire                  ko_b4delay;
wire [1:0]                tlevel_inc_d1_preo;
wire [(2*CP_WIDTH)-1:0]   address_d1_preo;  // This has one extra bit
wire [(2*IP_ADDRW)-1:0]   apk_d1_preo;
//------------------------------------------------------------------------------ 
// op_delval, ko_delval and seed value are defined in the included file. 
// The MEAN and SD of the delay value are also specified in the included file
//------------------------------------------------------------------------------ 
// These are the declarations for the delay synthesis logic
reg  [9:0]  op_delval;
reg  [9:0]  ko_delval;
reg  [31:0] seed;

initial
begin
  seed = 1;
end

reg [31:0] delay_params[0:11];
initial
begin
  $readmemh("next_child_delay.mem",delay_params);
end    
//------------------------------------------------------------------------------ 
// Input side Dual rail to Single rail
//------------------------------------------------------------------------------ 
d2sreg #(
  .DW(CP_WIDTH)
)u_lc_d2s(
  .rst_n          (rst_n        ),
  .dual_in        (lc_i         ),
  .single_out     (s_lc         ),
  .ko             (ko_lc        )
);

d2sreg #(
  .DW(CP_WIDTH)
)u_rc_d2s(
  .rst_n          (rst_n        ),
  .dual_in        (rc_i         ),
  .single_out     (s_rc         ),
  .ko             (ko_rc        )
);

d2sreg #(
  .DW(IP_ADDRW)
)u_apk_d2s(
  .rst_n          (rst_n        ),
  .dual_in        (apk_i        ),
  .single_out     (s_apk        ),
  .ko             (ko_apk       )
);

d2sreg #(
  .DW(1)
)u_eps_d2s(
  .rst_n          (rst_n        ),
  .dual_in        (epsilon_i    ),
  .single_out     (s_epsilon    ),
  .ko             (ko_epsilon   )
);

//------------------------------------------------------------------------------ 
// If s_daddr_vld is 1, then check for completeness of ko_lc, ko_rc, ko_apk
// and ko_epsilon. If the addresses are not valid, then assert the input
// complete when the ko_daddr_vld is generated. This is done to improve the
// latency if the prefious stage has detected a null child in the search
// path.
// The input completeness signal remains asserted as long as any of the inputs 
// is complete. The deassertion does not depend on whether the addresses are 
// valid or invalid.
//------------------------------------------------------------------------------ 

nv_thxx_zerodelay #(
  .NUM_IP(2)
)u_ip_cmplt_data(
  .rst_n(rst_n),
  .din({lrc_ram_read_i,ko_apk}),
  .dout(ip_cmplt)
);

//------------------------------------------------------------------------------ 
// This is done because the Boolean module assumes that a Null child is 
// represented by an all ones value, whereas in the NCL domain a null value
// would be represented by a 'NULL' state of the lines. 
// Converting from a 'NULL' indication in the NCL domain to the Boolean domain. 
// Additionally to force the outgoing daddr_vld to have a value 'zero', the
// incoming daddr_vld signal is also checked, and if the incoming daddr_vld is
// 'zero' then force an all-ones value in the s_lc_gated and s_rc_gated
// signals.
// the s_epsilon signal also needs to be gated. Otherwise the all ones value of
// the s_lc gets passed to the next stage as a valid address when the s_epsilon
// was raised to 1 for an earlier address and it stays high.
//------------------------------------------------------------------------------ 
always @(posedge lrc_ram_read_i or negedge rst_n)
  if (~rst_n)
  begin
    s_lc_gated = {CP_WIDTH{1'b0}};
    s_epsilon_gated = 1'b0;
  end
  else if (ko_lc)
  begin
    s_lc_gated = s_lc;
    s_epsilon_gated = s_epsilon;
  end
  else
  begin
    s_lc_gated = {CP_WIDTH{1'b1}};
    s_epsilon_gated = 1'b0;
  end

always @(posedge lrc_ram_read_i or negedge rst_n)
  if (~rst_n)
    s_rc_gated = {CP_WIDTH{1'b0}};
  else if (ko_rc)
    s_rc_gated = s_rc;
  else
    s_rc_gated = {CP_WIDTH{1'b1}};

//------------------------------------------------------------------------------ 
// creating a dummy ip_cmplt_d1 signal used to clock the boolean bloom_filter
// module. This is necessary because the boolean module in this case needs
// a clock signal.
// also creating a dummy delayed ip_cmplt_d2 signal used to sample the result of 
// the boolean operation and select the appropriate mean and sdev of the delay 
// if it is data dependent.
//------------------------------------------------------------------------------ 
wire ip_cmplt_d1;
wire ip_cmplt_d2;

assign #1 ip_cmplt_d1 = ip_cmplt;
assign #1 ip_cmplt_d2 = ip_cmplt_d1;

always @(ip_cmplt_d2,rst_n)
begin
  if (~rst_n)
    op_delval = 10'h3ff;
  else if (ip_cmplt_d2)
    // a different op_delval distribution to be generated when the incoming 
    // daddr_vld signal is 0. In all other cases, the op_delval follows
    // the regular different distribution. This is done because if the
    // incoming daddr_vld is 0, then no comparison is necesssary, and the
    // wavefront can be completed quicker.
    if (s_daddr_vld_o)
      op_delval = $dist_normal(seed,delay_params[0],delay_params[1]);
    else
      op_delval = $dist_normal(seed,delay_params[8],delay_params[9]);
  else
    if (s_daddr_vld_o)
      op_delval = $dist_normal(seed,delay_params[2],delay_params[3]);
    else
      op_delval = $dist_normal(seed,delay_params[10],delay_params[11]);
end

wire ko_b4delay_d1;
assign #1 ko_b4delay_d1 = ko_b4delay;

always @(ko_b4delay_d1,rst_n)
begin
  if(~rst_n)
    ko_delval = 10'h3ff;
  else if (ko_b4delay_d1)
    ko_delval = $dist_normal(seed,delay_params[4],delay_params[5]); 
  else
    ko_delval = $dist_normal(seed,delay_params[6],delay_params[7]); 
end
    

//------------------------------------------------------------------------------ 
// Boolean Single rail module
//------------------------------------------------------------------------------ 
next_child #(
  .IP_ADDRW(IP_ADDRW),
  .CP_WIDTH(CP_WIDTH)
)u_next_child(
  .clk_i          (ip_cmplt_d1        ),
  .rst_n_i        (rst_n              ), 
  .lc_i           (s_lc_gated         ),
  .rc_i           (s_rc_gated         ),
  .epsilon_i      (s_epsilon_gated    ),

  .apk_i          (s_apk              ),
  .daddr_vld_i    (ip_cmplt           ),
    
  .tlevel_inc_d1_o(s_tlvl_inc_o       ),
  .address_d1_o   (s_address_o        ),
  .apk_d1_o       (s_apk_o            ),
  .daddr_vld_d1_o (s_daddr_vld_o      )
);


//------------------------------------------------------------------------------ 
// Output side Single rail to Dual rail conversion
// For the address, apk and tlevel signals, force the output to a null by
// anding the ip_cmplt signal with the s_daddr_vld_o signal, which ensures
// that the ip_cmplt of the s2dreg is not asserted, which causes the outputs
// of these registers to stay at null.
// However there is a problem between the time the s_daddr_vld_o signal is asserted 0
// and the ip_cmplt signal is received.
// The ip_complt signal will come in a few ps before the s_daddr_vld_o signal. This 
// will cause the s2dreg conversion to trigger because the ki is low and ip_cmplt signal
// has been received. However now if the s_daddr_vld_o signal hoes high, the ki signal of 
// the s2dreg goes high, and this will screw up the whole timing. 
// So now what I am doing here is connecting the ip_cmplt_d2 signal so that the ip_cmplt trigger is 
// a delayed trigger
// However this is causing a problem when the s_daddr_vld_o signal goes from 0 to 1.
//------------------------------------------------------------------------------ 
s2dreg #(
  .DW(CP_WIDTH)
)u_addr_s2d(
  .rst_n          (rst_n          ),
  .del_value      (op_delval      ),
  .single_in      (s_address_o    ),
  .ki             (ki             ),
  .ip_cmplt       (ip_cmplt       ),
  .ko             (ko_addr_o      ),
  .dual_out       (address_d1_preo   )
);

s2dreg #(
  .DW(IP_ADDRW)
)u_apk_s2d(
  .rst_n          (rst_n          ),
  .del_value      (op_delval      ),
  .single_in      (s_apk_o        ),
  .ki             (ki             ),
  .ip_cmplt       (ip_cmplt       ),
  .ko             (ko_apk_o       ),
  .dual_out       (apk_d1_preo       )
);

s2dreg #(
  .DW(1)
)u_tlvl_inc_s2d(
  .rst_n          (rst_n          ),
  .del_value      (op_delval      ),
  .single_in      (s_tlvl_inc_o   ),
  .ki             (ki             ),
  .ip_cmplt       (ip_cmplt       ),
  .ko             (ko_tlvl_inc_o  ),
  .dual_out       (tlevel_inc_d1_preo)
);

s2dreg #(
  .DW(1)
)u_daddr_vld_s2d(
  .rst_n          (rst_n          ),
  .del_value      (op_delval      ),
  .single_in      (s_daddr_vld_o  ),
  .ki             (ki             ),
  .ip_cmplt       (ip_cmplt       ),
  .ko             (ko_daddr_vld_o ),
  .dual_out       (daddr_vld_d1_o )
);

assign address_d1_o    = /*(daddr_vld_d1_o == 2'b01) ? 'd0 : */address_d1_preo;
assign apk_d1_o        = /*(daddr_vld_d1_o == 2'b01) ? 'd0 : */apk_d1_preo;
assign tlevel_inc_d1_o = /*(daddr_vld_d1_o == 2'b01) ? 'd0 : */tlevel_inc_d1_preo;

//------------------------------------------------------------------------------ 
// The op_cmplt logic also follows the same idea as the ip_cmplt logic
//------------------------------------------------------------------------------ 

nv_thxx_zerodelay #(
  .NUM_IP(3)
)u_op_cmplt_data(
  .rst_n(rst_n),
  .din({ko_addr_o,ko_apk_o,ko_tlvl_inc_o}),
  .dout(op_cmplt)
);

//------------------------------------------------------------------------------ 
// Because we are working with a behavioural model here and artificially
// snthesizing the required delays, we can use the output completeness signal
// that indicates when the outputs are ready, and then delay it by a further 
// ko_delval which is the delay of the actual completion detection circuit to 
// generate the final ko signal
//------------------------------------------------------------------------------ 

assign ko_b4delay = op_cmplt ;

psDelay u_ko_delay (
  .rst_n          (rst_n          ),
  .trig           (ko_b4delay     ),
  .del_value      (ko_delval      ),
  .dlyd_trig      (ko             )
);

endmodule
