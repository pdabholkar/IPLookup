`timescale 1ns/1ps
module nv_reg_tb ();
  parameter DWIDTH = 4;
  parameter STAGES = 5;

  reg                   rst_n;
  reg   [2*DWIDTH-1:0]  din;
  reg   [1:0]           dvldin;
  reg                   ki;
  wire  [2*DWIDTH-1:0]  dout;
  wire  [1:0]           dvldout;
  wire                  ko;

  initial
  begin
    din   = 8'h00;
    dvldin= 2'b00;
    ki    = 0;
    rst_n = 1'b1;
#10 rst_n = 1'b0;
#10 rst_n = 1'b1;
    ki    = 1'b1;

#10 din   = 8'h6A;  dvldin = 2'b10;
#10 ki    = 1'b0;
#1  if (dout == 8'h6A) $display("PASS: Data received correctly"); else $display("ERROR");
#1  if (ko == 1'b1) $display("PASS: data acked"); else $display("ERROR");
      
#2  ki    = 1'b1;
#10 din   = 8'h00; dvldin = 2'b00;
#1  if (ko == 1'b0) $display("PASS: null acked"); else $display("ERROR");
#1  if (dout == 8'h00) $display("PASS: Null received correctly"); else $display("ERROR");
      
#10 din   = 8'hA6; dvldin = 2'b10;
#10 ki    = 1'b0;
#1  if (ko == 1'b1) $display("PASS: data acked"); else $display("ERROR");  
#1  if (dout == 8'hA6) $display("PASS: Data received correctly"); else $display("ERROR");
      
#10 din   = 8'h00; dvldin = 2'b00;
#2  ki    = 1'b1;
#1  if (ko == 1'b0) $display("PASS: null acked"); else $display("ERROR");
#1  if (dout == 8'h00) $display("PASS: Null received correctly"); else $display("ERROR");

#10 din   = 8'h96; dvldin = 2'b01;
#20  ki    = 1'b0;
#1   if (ko == 1'b1) $display("PASS: data acked"); else $display("ERROR");
#1   if (dout == 8'h00) $display("PASS: Data suppressed correctly"); else $display("ERROR");

#10 din   = 8'h00; dvldin = 2'b00;
//#10  ki    = 1'b1; No ki change  because of previous dvldin = 2'b01
#1   if (ko == 1'b0) $display("PASS: null acked"); else $display("ERROR");
#1   if (dout == 8'h00) $display("PASS: Null received correctly"); else $display("ERROR");

#10 din   = 8'h56; dvldin = 2'b10;
//#10 ki    = 1'b0; No ki change because of previos dvldin = 2'b01
      
#10  ki    = 1'b1;
#1  if (ko == 1'b1) $display("PASS: data acked"); else $display("ERROR");  
#1  if (dout == 8'h56) $display("PASS: Data received correctly"); else $display("ERROR");

#10 din   = 8'h00; dvldin = 2'b00;
#1   if (ko == 1'b0) $display("PASS: null acked"); else $display("ERROR");
#1   if (dout == 8'h00) $display("PASS: Null received correctly"); else $display("ERROR");

#10 din   = 8'h00; dvldin = 2'b01;
#20  ki    = 1'b0;
#1   if (ko == 1'b1) $display("PASS: data acked"); else $display("ERROR");
#1   if (dout == 8'h00) $display("PASS: Data suppressed correctly"); else $display("ERROR");

#10 din   = 8'h00; dvldin = 2'b00;
//#10  ki    = 1'b1; No ki change because of previos dvldin = 2'b01
#1   if (ko == 1'b0) $display("PASS: null acked"); else $display("ERROR");
#1   if (dout == 8'h00) $display("PASS: Null received correctly"); else $display("ERROR");

#10 din   = 8'h99; dvldin = 2'b10;
//#10 ki    = 1'b0; No ki change because of previos dvldin = 2'b01
      
#10  ki    = 1'b1;
#1  if (ko == 1'b1) $display("PASS: data acked"); else $display("ERROR");  
#1  if (dout == 8'h99) $display("PASS: Data received correctly"); else $display("ERROR");

#10 din   = 8'h00; dvldin = 2'b00;
#1   if (ko == 1'b0) $display("PASS: null acked"); else $display("ERROR");
#1   if (dout == 8'h00) $display("PASS: Null received correctly"); else $display("ERROR");
#10  ki     = 1'b0;

// The following test will work only if the number of stages is greater than 1. Even then 
// some of the tests may fail because the ki may not come in quickly enough and the 
// number of wavefronts inside the pipeline may be overwritten by the incoming wavefronts. 
// There is no auto-generate or an auto-consume block in the test bench.

if(STAGES >=5)
begin : STG_5
$display("*** for 5 Stage register ***");
#100 din   = 8'h96; dvldin = 2'b01;
#1  if (ko == 1'b1) $display("PASS: data acked"); else $display("ERROR");

#10 if (dout == 8'h00) $display("PASS: Data suppressed correctly"); else $display("ERROR");

#10 din   = 8'h00; dvldin = 2'b00;
#1  if (ko == 1'b0) $display("PASS: null acked"); else $display("ERROR");
#1  if (dout == 8'h00) $display("PASS: Null received correctly"); else $display("ERROR");

#10 din   = 8'hAA; dvldin = 2'b10;
#1  if (ko == 1'b1) $display("PASS: data acked"); else $display("ERROR");  

#10 din   = 8'h00; dvldin = 2'b00;
#1  if (ko == 1'b0) $display("PASS: null acked"); else $display("ERROR");

#10 din   = 8'hA9; dvldin = 2'b10;
#1  if (ko == 1'b1) $display("PASS: data acked"); else $display("ERROR");  

#1  if (dout == 8'hAA) $display("PASS: Data received correctly"); else $display("ERROR");

#10 ki    = 1'b1;
#1  if (dout == 8'h00) $display("PASS: Null received correctly"); else $display("ERROR");

#10 ki    = 1'b0;
#1  if (dout == 8'hA9) $display("PASS: Data received correctly"); else $display("ERROR");

#40 din   = 8'h00; dvldin = 2'b00;
#1  if (ko == 1'b0) $display("PASS: null acked"); else $display("ERROR");

#2  din   = 8'h9A; dvldin = 2'b01;
#1  if (ko == 1'b1) $display("PASS: data acked"); else $display("ERROR");  

#2  din   = 8'h00; dvldin = 2'b00;
#1  if (ko == 1'b0) $display("PASS: null acked"); else $display("ERROR");

#2  din   = 8'h6A; dvldin = 2'b01;
#1  if (ko == 1'b1) $display("PASS: data acked"); else $display("ERROR");  

#5  ki = 1'b1;
#1  if (dout == 8'h00) $display("PASS: Null received correctly"); else $display("ERROR");
    
#5  ki = 1'b0;

#20 din   = 8'h00; dvldin = 2'b00;
#1  if (ko == 1'b0) $display("PASS: null acked"); else $display("ERROR");

#1  if (dout == 8'h00) $display("PASS: Data suppressed correctly"); else $display("ERROR");

#1  if (dout == 8'h00) $display("PASS: Null received correctly"); else $display("ERROR");

#1  if (dout == 8'h00) $display("PASS: Data suppressed correctly"); else $display("ERROR");

#1  if (dout == 8'h00) $display("PASS: Null received correctly"); else $display("ERROR");

#2  din   = 8'h5A; dvldin = 2'b10;
#1  if (ko == 1'b1) $display("PASS: data acked"); else $display("ERROR");  

#20 din   = 8'h00; dvldin = 2'b00;
#1  if (ko == 1'b0) $display("PASS: null acked"); else $display("ERROR");

#1  if (dout == 8'h5A) $display("PASS: Data received correctly"); else $display("ERROR");
     
#5  ki    = 1'b1;
#1  if (dout == 8'h00) $display("PASS: Null received correctly"); else $display("ERROR");
end

#10 $finish;
  end

  initial
  begin
    $dumpfile("nv_reg_stages_gated_dump.fst");
    $dumpvars;
  end

  nv_reg_stages_gated #(
    .DWIDTH(DWIDTH),
    .STAGES(STAGES)
  )u_dut(
    .rst_n(rst_n),
    .din(din),
    .dvldin(dvldin),
    .ki(ki),
    .ko(ko),
    .dout(dout),
    .dvldout(dvldout)
  );

endmodule
