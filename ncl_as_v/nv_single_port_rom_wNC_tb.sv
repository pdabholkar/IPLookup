`timescale 1ns/1ps
module nv_single_port_rom_wNC_tb ();
  
  localparam RAM_WIDTH = 22;
  localparam RAM_DEPTH = 1024;
  localparam DEPTH = 10;
  localparam INIT_FILE = (DEPTH == 10) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/lrcMap8_testing.mem" : "thisfile.mem";

  reg [(2*clogb2(RAM_DEPTH-1))-1:0] sram_addr;
  reg                               ki;
  reg                               rst_n;
  wire [(2*(RAM_WIDTH+1))-1:0]      sram_dout;
  wire                              ko;

  initial
  begin
    rst_n = 1;
#10 rst_n = 0;
#10 rst_n = 1;
#1000 $finish;
  end

  initial 
  begin
    ki            = 0;
    all_null();

#40 ki            = 1;
#50 sram_addr     = 20'b01011001011001011001;

#3  ki            = 0;    
#5  ki            = 1;  
#5  all_null();

#50 sram_addr     = 20'b01011001011001100101;

    ki            = 0;
#5  ki            = 1;  
#5  all_null();
#5  ki            = 0;

#50 sram_addr     = 20'b01011001011001011010; 

#5  ki            = 1;  
#5  all_null();
#5  ki            = 0;

#50 sram_addr     = 20'b01011001011010011001;

#5  ki            = 1;  
#5  all_null();

#50 sram_addr     = 20'b01010101011001011001;

#3  ki            = 0;
#5  ki            = 1;  
#5  all_null();

#50 sram_addr     = 20'b01010110011001011001;
#5  ki            = 0;

#10  ki            = 1;  
#5  all_null();


#50 sram_addr     = 20'b01010101010110101010;

#5  ki            = 0;

#20  all_null();
#5  ki            = 1;
// Read a NULL column at address location 0x92
#20 sram_addr     = 20'b01011001011001011001;

#20  ki            = 0;
#20  ki            = 1;  
#5  all_null();

#50 sram_addr     = 20'b01011001011001100101;

#20  ki            = 0;
#20  ki            = 1;  
#5  all_null();

#50 sram_addr     = 20'b01011001011001011010; 

#20  ki            = 0;
#20  ki            = 1;  
#5  all_null();
#5  ki            = 0;


  end

  initial
  begin
    $dumpfile("nv_single_port_rom_wNC_dump.fst");
    $dumpvars;
  end

  nv_single_port_rom_wNC #(
    .RAM_WIDTH(RAM_WIDTH),
    .RAM_DEPTH(RAM_DEPTH),
    .INIT_FILE(INIT_FILE)
  )u_dut (
    .A     (sram_addr        ),
    .ki    (ki               ),
    .rst_n (rst_n            ),
    .DOUT  (sram_dout        ),
    .ko    (ko               )
  );

task all_null;
  sram_addr = 20'b0;
endtask

function integer clogb2;
  input integer depth;
    if (depth > 1)
      for (clogb2=0; depth>0; clogb2=clogb2+1)
        depth = depth >> 1;
    else
      clogb2=1;
endfunction

endmodule
