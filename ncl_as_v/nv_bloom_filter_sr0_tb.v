`timescale 1ns/1ps
module nv_bloom_filter_sr0_tb ();

  reg rst_n;
  reg ki;
  wire ko;
  reg  [1:0] msbk_i;
  reg  [5:0] mk_i;
  wire [63:0] crc_sr_o;

  initial
  begin
    rst_n = 1;
#10 rst_n = 0;
#10 rst_n = 1;
#1000 $finish;
  end

  initial 
  begin
    ki    = 0;
    msbk_i= 0;
    mk_i  = 0;
#40 ki    = 1;
#50 msbk_i= 2'b10;
    mk_i  = 6'b100101;
#3  ki    = 0;        // Late ki
#5  ki    = 1;
#5  msbk_i= 6'b0;
    mk_i  = 0;

#50 msbk_i= 2'b01;
    mk_i  = 6'b010110;
#3  ki    = 0;        // Late ki
#5  ki    = 1;
#5  msbk_i= 6'b0;
    mk_i  = 0;

#5  ki    = 0;        // Regular ki
#50 msbk_i= 2'b10;
#2  mk_i  = 6'b011010;  
#5  ki    = 1;
#5  msbk_i= 6'b0;
    mk_i  = 0;

#5  ki    = 0;
#50 msbk_i= 2'b01;
    mk_i  = 6'b011001;
#5  ki    = 1;
#5  msbk_i= 6'b0;
#5  mk_i  = 0;

// Trying to force a Data Null Data value without reading out result of last
// operation
#50 msbk_i= 2'b10;
    mk_i  = 6'b100101;
#5  msbk_i= 6'b0;
    mk_i  = 0;

// Only the result of the following data wavefront should come out. The
// previous data wavefront would be lost, because there wasn't a corresponding
// ki to pull the value out. 

#50 msbk_i= 2'b01;
    mk_i  = 6'b010110;
#3  ki    = 0;        
#5  ki    = 1;
#5  msbk_i= 6'b0;
    mk_i  = 0;

#5  ki    = 0;        // Regular ki
#50 msbk_i= 2'b10;
#2  mk_i  = 6'b011010;  
#5  ki    = 1;
#5  msbk_i= 6'b0;
    mk_i  = 0;

#5  ki    = 0;
  end

  initial
  begin
    $dumpfile("nv_bloom_filter_sr0_dump.fst");
    $dumpvars;
  end

  nv_bloom_filter_sr0 u_dut (
    .rst_n        (rst_n    ),
    .ki           (ki       ),
    .ko           (ko       ),
    .msbk_i       (msbk_i   ),
    .mk_i         (mk_i     ),
    .crc_sr_o     (crc_sr_o )
  );
endmodule

