`ifndef __S2DREG__
`define __S2DREG__

`timescale 1ps/1ps
module s2dreg #(
  parameter DW       = 32
)(
  input                       rst_n,
  input       [9:0]           del_value,
  input       [DW-1:0]        single_in,
  output  reg [((2*DW)-1):0]  dual_out,
  output  reg                 ko,
  input                       ki,         // ki from the next stage that receives this data output
  input                       ip_cmplt    // ip_cmplt of the previous stage that feeds data in
);

wire op_cmplt;
wire dlyd_op_cmplt;
wire dlyd_ko_trig;
reg [(2*DW)-1:0] dual_out_val;

// The change here is that the dual_out_val is now generated first and then delayed and not the other way around, 
// Earlier I was delaying the ki and ip_cmplt signal and then sampling the single_in value. This was leading to an 
// incorrect sampling.
generate
genvar i;
for (i=0;i<DW;i=i+1)
begin : S2D

  always @(*)
  begin
    if(~rst_n)
    begin
      dual_out_val[2*i+1] = 1'b0;
      dual_out_val[2*i]   = 1'b0;
    end
    else 
      if(~ki & ip_cmplt)
      begin
        dual_out_val[2*i+1] = single_in[i];
        dual_out_val[2*i]   = ~single_in[i];
      end
      else if (ki & ~ip_cmplt) 
      begin
        dual_out_val[2*i+1] = 1'b0;
        dual_out_val[2*i]   = 1'b0;
      end
      else
      begin
        dual_out_val[2*i+1] = dual_out_val[2*i+1];
        dual_out_val[2*i]   = dual_out_val[2*i];
      end
  end

end
endgenerate


assign op_cmplt = (dual_out_val[0] | dual_out_val [1]); // using just the first bit, since all bits are going to be aligned anyway because of the behavioural model

// 
psDelay u_op_delay
(
  .rst_n(rst_n),
  .trig(op_cmplt),
  .del_value(del_value),
  .dlyd_trig(dlyd_op_cmplt)
);

psDelay u_ko_delay
(
  .rst_n(rst_n),
  .trig(dlyd_op_cmplt),
  .del_value(10'd10),
  .dlyd_trig(dlyd_ko_trig)
);

always @(dlyd_op_cmplt or negedge rst_n)
  if (~rst_n)
    dual_out = 'd0;
  else
    dual_out = dual_out_val;  

always @(dlyd_ko_trig or negedge rst_n)
  if (~rst_n)
    ko = 1'b0;
  else
    ko = op_cmplt;  

endmodule

`endif
