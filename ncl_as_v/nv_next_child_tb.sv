`timescale 1ns/1ps
module nv_next_child_tb ();
  
  localparam IP_ADDRW = 32;
  localparam CP_WIDTH = 32;
  reg rst_n;
  reg [63:0]  lc_i;
  reg [63:0]  rc_i;
  reg [63:0]  apk_i;
  reg [1:0]   epsilon_i;
  reg         lrc_ram_read_i;
  reg [1:0]   daddr_vld_i;
  reg         ki;
  wire        ko;
  wire [63:0] address_d1_o;
  wire [63:0] apk_d1_o;
  wire [1:0]  tlevel_inc_d1_o;
  wire [1:0]  daddr_vld_d1_o;
  wire [63:0] address_d5_o;
  wire [63:0] apk_d5_o;
  wire [1:0]  tlevel_inc_d5_o;
  wire [1:0]  daddr_vld_d5_o;

  wire        nc_reg_stage_ko;


  initial
  begin
    rst_n = 1;
#10 rst_n = 0;
#10 rst_n = 1;
#1500 $finish;
  end

  initial 
  begin
    ki            = 0;
    all_null();
// 1
#40 ki            = 1;
#50 lc_i          = 64'b1001100101010110010110100110100101010101101001101010011001010101;
    rc_i          = 64'b1010011001010110011010010101010110010110101010010110100110101010;
    apk_i         = 64'b0101101010101010101010011010011001100110101010011010100110100110;
    lrc_ram_read_i= 1'b1;
    daddr_vld_i   = 2'b10;
    epsilon_i     = 2'b01;
#3  ki            = 0;    
#5  ki            = 1;  

#5  all_null();

// 2
#50 lc_i          = 64'b1001011010101010011010101001100101100101101010101001011001100101;
    rc_i          = 64'b1010011001101010100110101001101010010110101010101010011001010101;
    apk_i         = 64'b0101101001101010011010011001100101100110011001010110010110101001;
    lrc_ram_read_i= 1'b1;
    daddr_vld_i   = 2'b10;
    epsilon_i     = 2'b01;

    ki            = 0;
#5  ki            = 1;  

#5  all_null();
#5  ki            = 0;


// 3
#50 lc_i          = 64'b0110011010011001011001011010010101100101101010010110010101011001;
    rc_i          = 64'b1001011001010110010101101001101001100110100101010110101010010101;
    apk_i         = 64'b1010100110010101100110100110011010101001010101101010100101010101;
    lrc_ram_read_i= 1'b1;
    daddr_vld_i   = 2'b10;
    epsilon_i     = 2'b01;
#5  ki            = 1;  

#5  all_null();
#5  ki            = 0;

// 4
#50 lc_i          = 64'b1010011001010101010110101010010110100110011010101010100110011001;
    rc_i          = 64'b0110010110100110011010010110010101100101100101010101011001100101;
    apk_i         = 64'b1001011001010110011010011001010101100110101010100110101010011010;
    lrc_ram_read_i= 1'b1;
    daddr_vld_i   = 2'b10;
    epsilon_i     = 2'b01;
#5  ki            = 1;  
#5  all_null();

// 5
// Trying to force a Data Null Data value without reading out result of last
// operation
#50 lc_i          = 64'b1001101010011010011010011001011001101001011001100110010110010110;
    rc_i          = 64'b1010011010100110010110011001101010010101101010101001010101011001;
    apk_i         = 64'b1010010110010101011010011010010101010101101010101010101001010110;
    lrc_ram_read_i= 1'b1;
    daddr_vld_i   = 2'b10;
    epsilon_i     = 2'b01;
#5  all_null();

// 6
// Only the result of the following data wavefront should come out. The
// previous data wavefront would be lost, because there wasn't a corresponding
// ki to pull the value out. 
#50 lc_i          = 64'b0110101010010110101001010101101010010110010110011001011010101010;
    rc_i          = 64'b0101100110010101101010101001011010101010101001101001101001100110;
    apk_i         = 64'b1010010110101010010110101010010110011010011001100101100110010101;
    lrc_ram_read_i= 1'b1;
    daddr_vld_i   = 2'b10;
    epsilon_i     = 2'b01;

#3  ki            = 0;
#5  ki            = 1;  
#5  all_null();
#5  ki            = 0;

// 7
#50 lc_i          = 64'b0101010101010101010101101001101001011010010101010101100110100110;
    rc_i          = 64'b0101101010101010100101010101100110101010100110100110011010010110;
    apk_i         = 64'b0101100110011010100101011010011010010110010110010101100101100110;
    lrc_ram_read_i= 1'b1;
    daddr_vld_i   = 2'b10;
    epsilon_i     = 2'b01;

#5  ki            = 1;  
#5  all_null();
#5  ki            = 0;

// 8
// Send a NULL column
#50 lc_i          = 64'b0;
    rc_i          = 64'b0101101010101010100101010101100110101010100110100110011010010110;
    apk_i         = 64'b0101100110011010100101011010011010010110010110010101100101100110;
    lrc_ram_read_i= 1'b1;
    daddr_vld_i   = 2'b10;
    epsilon_i     = 2'b01;

#5  ki            = 1;  
#5  all_null();
#5  ki            = 0;

// 9
#50 lc_i          = 64'b0101101010101010100101010101100110101010100110100110011010010110;
    rc_i          = 64'b0;
    apk_i         = 64'b0101100110011010100101011010011010010110010110010101100101100110;
    lrc_ram_read_i= 1'b1;
    daddr_vld_i   = 2'b10;
    epsilon_i     = 2'b01;

#5  ki            = 1;  
#5  all_null();
#5  ki            = 0;

// 10
#50 lc_i          = 64'b0;
    rc_i          = 64'b0;
    apk_i         = 64'b0101100110011010100101011010011010010110010110010101100101100110;
    lrc_ram_read_i= 1'b1;
    daddr_vld_i   = 2'b10;
    epsilon_i     = 2'b01;

#5  all_null();

// 11
// Regular data
#50 lc_i          = 64'b1001101010011010011010011001011001101001011001100110010110010110;
    rc_i          = 64'b1010011010100110010110011001101010010101101010101001010101011001;
    apk_i         = 64'b1010010110010101011010011010010101010101101010101010101001010110;
    lrc_ram_read_i= 1'b1;
    daddr_vld_i   = 2'b10;
    epsilon_i     = 2'b01;

#5  ki            = 1;
#5  ki            = 0;
#5  ki            = 1;
#5  ki            = 0;

#5  all_null();

// 12
#50 lc_i          = 64'b0110101010010110101001010101101010010110010110011001011010101010;
    rc_i          = 64'b0101100110010101101010101001011010101010101001101001101001100110;
    apk_i         = 64'b1010010110101010010110101010010110011010011001100101100110010101;
    lrc_ram_read_i= 1'b1;
    daddr_vld_i   = 2'b10;
    epsilon_i     = 2'b01;


#5  all_null();

// 13 Ram contains Null values, but lrc_ram_read is asserted
#50 lc_i          = 64'b0;
    rc_i          = 64'b0;
    apk_i         = 64'b0101100110011010100101011010011010010110010110010101100101100110;
    lrc_ram_read_i= 1'b1;
    daddr_vld_i   = 2'b10;
    epsilon_i     = 2'b00;

#5  ki            = 1;
#5  ki            = 0;

#5  ki            = 1;
#5  ki            = 0;

#5  all_null();

#5  ki            = 1;
#5  ki            = 0;

// 14
#50 lc_i          = 64'b0110011010011001011001011010010101100101101010010110010101011001;
    rc_i          = 64'b1001011001010110010101101001101001100110100101010110101010010101;
    apk_i         = 64'b1010100110010101100110100110011010101001010101101010100101010101;
    lrc_ram_read_i= 1'b1;
    daddr_vld_i   = 2'b10;
    epsilon_i     = 2'b01;
#5  ki            = 1;  

#5  all_null();
#5  ki            = 0;

// 15
#50 lc_i          = 64'b1010011001010101010110101010010110100110011010101010100110011001;
    rc_i          = 64'b0110010110100110011010010110010101100101100101010101011001100101;
    apk_i         = 64'b1001011001010110011010011001010101100110101010100110101010011010;
    lrc_ram_read_i= 1'b1;
    daddr_vld_i   = 2'b10;
    epsilon_i     = 2'b01;
#5  ki            = 1;  
#5  all_null();
#5  ki            = 0;

// 16 Incoming address is null, so no ram data and lrc_ram_read is also null
#50 lc_i          = 64'b0;
    rc_i          = 64'b0;
    apk_i         = 64'b0;
    lrc_ram_read_i= 1'b0;
    daddr_vld_i   = 2'b01;
    epsilon_i     = 2'b0;
#5  ki            = 1;  
#5  all_null();
#5  ki            = 0;

// 17
#50 lc_i          = 64'b0110011010011001011001011010010101100101101010010110010101011001;
    rc_i          = 64'b1001011001010110010101101001101001100110100101010110101010010101;
    apk_i         = 64'b1010100110010101100110100110011010101001010101101010100101010101;
    lrc_ram_read_i= 1'b1;
    daddr_vld_i   = 2'b10;
    epsilon_i     = 2'b01;
#5  ki            = 1;  

#5  all_null();
#5  ki            = 0;

  end

  initial
  begin
    $dumpfile("nv_next_child_dump.fst");
    $dumpvars;
  end

  nv_next_child #(
    .IP_ADDRW(IP_ADDRW),
    .CP_WIDTH(CP_WIDTH)
  )u_dut (
    .rst_n            (rst_n            ),
    .ki               (nc_reg_stage_ko  ),
    .ko               (ko               ),
    .lc_i             (lc_i             ),
    .rc_i             (rc_i             ),
    .lrc_ram_read_i   (lrc_ram_read_i   ),
    .apk_i            (apk_i            ),
    .epsilon_i        (epsilon_i        ),
    .apk_d1_o         (apk_d1_o         ),
    .address_d1_o     (address_d1_o     ),
    .tlevel_inc_d1_o  (tlevel_inc_d1_o  ),
    .daddr_vld_d1_o   (daddr_vld_d1_o   )
  );

  nv_reg_stages_gated #(
    .STAGES           (4                  ),
    .CONSUME_EN       (0                  ),
    .DWIDTH           (1+CP_WIDTH+IP_ADDRW)
  )u_nc_reg_stage(
    .rst_n            (rst_n              ),
    .consume          (2'b01              ),
    .din              ({tlevel_inc_d1_o,address_d1_o,apk_d1_o}),
    .dvld             (daddr_vld_d1_o     ),
    .ki               (ki                 ),
    .ko               (nc_reg_stage_ko    ),
    .dout             ({tlevel_inc_d5_o,address_d5_o,apk_d5_o})
  );


task all_null;

  lc_i      = 66'b0;
  rc_i      = 66'b0;
  apk_i     = 64'b0;
  epsilon_i = 2'b0;
  lrc_ram_read_i= 1'b0;
  daddr_vld_i   = 2'b00;

endtask

endmodule
