`ifndef __D2SREG__
`define __D2SREG__

`timescale 1ps/1ps
module d2sreg #(
  parameter DW = 32
)(
  input                       rst_n,
  input       [((2*DW)-1):0]  dual_in,
  output  reg [DW-1:0]        single_out,
  output                      ko
);

reg [DW-1:0] kobit;
generate
genvar i;
for (i=0;i<DW;i=i+1)
begin : D2S

  always @(*)
  begin
    if (~rst_n)
    begin
      single_out[i] = 1'b0; // if rst_n, reset the output. Not the best way, but suitable for the time being
      kobit[i] = 1'b0;      // ko is null   
    end
    else if (dual_in[2*i])
    begin
      single_out[i] = 1'b0; // if dual_in[0] is high, then output is '0'
      kobit[i] = 1'b1;      // ko is data                
    end
    else if (dual_in[2*i+1])
    begin
      single_out[i] = 1'b1; // if dual_in[1] is high, then output is '1'
      kobit[i] = 1'b1;      // ko is data  
    end  
    else
    begin
      single_out[i] = single_out[i];// else hold on to the old value during the null cycle
      kobit[i] = 1'b0;      // ko is null when none of the inputs are high
    end
  end

end
endgenerate

assign ko = &kobit[DW-1:0];

endmodule

`endif
