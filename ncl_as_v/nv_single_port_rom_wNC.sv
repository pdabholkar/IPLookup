`timescale 1ps/1ps

// THIS ASSUMPTION IS BEING REVISITED

// MEM_SIZE is the same as RAM_DEPTH in the discussion below.
// An important thing to note here is that the size of the RAM required to
// store MEM_SIZE entries is clogb2(MEM_SIZE-1). However because we need to
// distinguish between a valid all-ones address in this address range and
// an all-ones pattern that is used to indicate a NULL location, the CP_WIDTH
// parameter is set to clogb2(MEM_SIZE-1)+1 in the .h file. 
// However the address that is passed to the RAM is the exact width

//****************************************************************************** 
// IN THE FINAL IMPLEMENTATION WE MAY WANT THE DOUT LINES OF THE RAM TO REMAIN *
// AS NULL AND NOT TRANSITION TO DATA AT ALL. THIS WILL THEN CAUSE A CHANGE IN *
// THE DOWNSTREAM IMPLEMENTATION                                               *
//****************************************************************************** 

module nv_single_port_rom_wNC #(
  parameter RAM_WIDTH      = 16, 
  parameter RAM_DEPTH      = 1024, 
  parameter INIT_FILE      =""
)(
  input [(2*clogb2(RAM_DEPTH-1))-1:0] A,    // Write address bus, width determined from RAM_DEPTH
  input                               ki,   // ki from the following stage used as read enable
  input                               rst_n,// Output reset (does not affect memory contents)
  output [(2*RAM_WIDTH)-1:0]          DOUT, // RAM output data
  output                              ko    // Indicator that data has been read by the RAM
);

//============================================================================== 
wire [clogb2(RAM_DEPTH-1)-1:0]  s_addr;
wire [RAM_WIDTH-1:0]            s_dout;
wire                            ren;
wire                            ko_addr;
wire                            ko_dout;
wire [(2*RAM_WIDTH)-1:0]        dout_temp; // RAM output data Nullified
wire                            ip_cmplt;
wire                            op_cmplt;
wire                            op_cmplt_null;
wire                            null_read;
wire                            non_null_ko;
wire                            null_ko;
//------------------------------------------------------------------------------ 

// DEBUG and LOGGING
assign non_null_ko = ~null_read & ko;
assign null_ko     =  null_read & ko;

reg [15:0] null_ko_counter;
reg [15:0] non_null_ko_counter;

always @(posedge null_ko or negedge rst_n)
begin
  if(~rst_n)
    null_ko_counter <= 'd0;
  else
    null_ko_counter <= null_ko_counter + 1'd1;
end

always @(posedge non_null_ko or negedge rst_n)
begin
  if(~rst_n)
    non_null_ko_counter <= 'd0;
  else
    non_null_ko_counter <= non_null_ko_counter + 1'd1;
end

assign ren = ~ki;
assign null_read = (s_dout[RAM_WIDTH-1] == 1'b1) | (s_dout[RAM_WIDTH-1:0] == {RAM_WIDTH{1'd0}});
//------------------------------------------------------------------------------ 
// op_delval, ko_delval and seed value are defined in the included file. 
// The MEAN and SD of the delay value are also specified in the included file
//------------------------------------------------------------------------------ 
// These are the declarations for the delay synthesis logic
reg  [9:0]  op_delval;
reg  [9:0]  ko_delval;
reg  [31:0] seed;

initial
begin
  seed = 1;
end

reg [31:0] delay_params[0:11];
initial
begin
  $readmemh("single_port_rom_wNC_delay.mem",delay_params);
end  

//------------------------------------------------------------------------------ 
// Input side Dual rail to Single rail
//------------------------------------------------------------------------------ 
d2sreg #(
  .DW(clogb2(RAM_DEPTH-1))
)u_addr_d2s(
  .rst_n          (rst_n    ),
  .dual_in        (A        ),
  .single_out     (s_addr   ),
  .ko             (ko_addr  )
);

assign ip_cmplt = ko_addr;

//------------------------------------------------------------------------------ 
// Because we are working with a behavioural model here and artificially
// snthesizing the required delays, we can use the output completeness signal
// that indicates when the outputs are ready, and then delay it by a further 
// ko_delval which is the delay of the actual completion detection circuit to 
// generate the final ko signal
//------------------------------------------------------------------------------ 
psDelay u_ko_delay (
  .rst_n          (rst_n    ),
  .trig           (op_cmplt ),
  .del_value      (ko_delval),
  .dlyd_trig      (ko       )
);

//------------------------------------------------------------------------------ 
// creating a dummy delayed ip_cmplt_d1 signal used to sample the result of the 
// boolean operation and select the appropriate mean and sdev of the delay if 
// it is data dependent 
//------------------------------------------------------------------------------ 
wire ip_cmplt_d1;
wire ip_cmplt_d2;
assign #1 ip_cmplt_d1 = ip_cmplt;
assign #1 ip_cmplt_d2 = ip_cmplt_d1;

wire op_cmplt_d1;
assign #1 op_cmplt_d1 = op_cmplt;

always @(ip_cmplt_d2,rst_n)
begin
  if (~rst_n)
    op_delval = 'h3ff;
  else if (ip_cmplt_d2)
    if(~null_read)
      op_delval = $dist_normal(seed,delay_params[0],delay_params[1]);
    else
      op_delval = $dist_normal(seed,delay_params[8],delay_params[9]);
  else
    if(~null_read)
      op_delval = $dist_normal(seed,delay_params[2],delay_params[3]);
    else
      op_delval = $dist_normal(seed,delay_params[10],delay_params[11]);
end

always @(op_cmplt_d1,rst_n)
begin
  if (~rst_n)
    ko_delval = 'h3ff;
  else if (op_cmplt_d1)
    ko_delval = $dist_normal(seed,delay_params[4],delay_params[5]); 
  else
    ko_delval = $dist_normal(seed,delay_params[6],delay_params[7]); 
end

//------------------------------------------------------------------------------ 
// Boolean Single rail module
//------------------------------------------------------------------------------ 
xilinx_simple_dual_port_2_clock_ram #(
  .RAM_WIDTH            (RAM_WIDTH          ),     // prefix data width
  .RAM_DEPTH            (RAM_DEPTH          ),     // RAM depth (number of entries) defined by the mdepth_vs_tdepth.h file
  .RAM_PERFORMANCE      ("LOW_LATENCY"      ),     // because we wish to finish the operation with a single dummy clock pulse
  .INIT_FILE            (INIT_FILE          )      //  name/location of RAM initialization file
) u_RAM (
  .clka                 (1'b0               ),     // Write clock, unused 
  .addra                ({clogb2(RAM_DEPTH-1){1'b0}}), // Write address bus, width determined from RAM_DEPTH
  .dina                 ({RAM_WIDTH{1'b0}}  ),     // RAM input data, width determined from RAM_WIDTH
  .wea                  (1'd0               ),     // Write enable

  .clkb                 (ip_cmplt_d1        ),     // Read clock
  .addrb                (s_addr             ),     // Read address
//.enb                  (ren                ),     // Read Enable
//.enb                  (1'b1               ),     // Read Enable is always 1 because with the old ~ki read enable, the ip_cmplt_d1 and ki signals were gettin out of phase
  .enb                  (ip_cmplt           ),     // Read Enable is ip_cmplt to sync the clk and the en signal
  .rstb                 (~rst_n             ),     // Output reset (does not affect memory contents)
  .regceb               (1'b0               ),     // Output register enable
  .doutb                (s_dout             )      // RAM output data
);

//------------------------------------------------------------------------------ 
// Output side Single rail to Dual rail conversion
//------------------------------------------------------------------------------
// It is not needed to convert the all ones output of the Boolean RAM to
// a special code with Null Column as has to be sent out of this NCL RAM
// because the width of the data is already one greater than that required to
// correctly address the locations in the next stage.
s2dreg #(
  .DW(RAM_WIDTH)
)u_dout_s2d(
  .rst_n          (rst_n      ),
  .del_value      (op_delval  ),
  .single_in      (s_dout     ),
  .ki             (ki         ),
  .ip_cmplt       ((ip_cmplt )),//& ~s_dout[RAM_WIDTH-1])),
  .ko             (ko_dout    ),
  .dual_out       (dout_temp  )
);

// (SIMPLE) s_dout[RAM_WIDTH-1] == 1 is the same as (COMPLEX) s_dout[RAM_WIDTH-1:0] == {RAM_WIDTH{1'b1}}. If the size is reduced subsequently, then the COMPLEX form will have to be 
// used. However this is not recommended for the Boolean case as the all ones value can also occur in the actual address.

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// This single bit to indicate Null for the whole entry may not always work because for both the prefix RAM and the LRC RAM one of the fields (LC/RC address or PFX1/PFX2) may be NULL, while 
// the other field may contain valid data.
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// do not use null_read here, because for the sake of simulation we need DOUT to toggle
assign DOUT = /*null_read*/ s_dout[RAM_WIDTH-1] ? 'd0 : dout_temp; 

// This has been changed from a logical AND of ~ki and ko_addr to ensure that
// the op_cmplt does not toggle on its own without the ki indicating that the
// previous op_cmplt has been processed by the downstream node.
//nv_thxx #(.NUM_IP(2)) u_op_cmplt_null (.rst_n(rst_n), .din({ip_cmplt,~ki}), .dout(op_cmplt_null));

// The op_cmplt_null to be used when the data out is a NULL
assign op_cmplt = ko_dout;// | (op_cmplt_null & s_dout[RAM_WIDTH-1]);


//  The following function calculates the address width based on specified RAM depth
function integer clogb2;
  input integer depth;
    if (depth > 1)
      for (clogb2=0; depth>0; clogb2=clogb2+1)
        depth = depth >> 1;
    else
      clogb2=1;
endfunction

endmodule

