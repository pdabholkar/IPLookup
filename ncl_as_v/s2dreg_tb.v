`timescale 1ns/1ns
module s2dreg_tb();
localparam DW = 32;

  reg rst_n;
  reg ki;
  reg ko_a1;
  reg [DW-1:0] single_in;
  wire [2*DW-1:0] dual_out;

  initial 
  begin
    rst_n = 1;
    ki = 0;
    ko_a1 = 0;
#20 rst_n = 0;
#10 rst_n = 1;

#10 single_in = 32'd234;
#10 ko_a1 = 1;
#20 ki = 1;
#10 ko_a1 = 0;
#20 ki = 0;

#10 single_in = 32'd324;
#10 ko_a1 = 1;
#20 ki = 1;
#10 ko_a1 = 0;
#20 ki = 0;

#10 single_in = 32'd124;
#10 ko_a1 = 1;
#20 ki = 1;
#10 ko_a1 = 0;
#20 ki = 0;

#50 $finish;
  end

  s2dreg #(
    .DW(32)
  )u_dut(
    .rst_n(rst_n),
    .single_in(single_in),
    .ki(ki),
    .ko_a1(ko_a1),
    .dual_out(dual_out)
  );

  initial
  begin
    $dumpfile("dump.vcd");
    $dumpvars;
 end
endmodule






