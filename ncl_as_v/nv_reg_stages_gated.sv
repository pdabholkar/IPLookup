`ifndef __NV_REG_STAGES_GATED__
`define __NV_REG_STAGES_GATED__

`timescale 1ns/1ps
module nv_reg_stages_gated #(
  parameter STAGES = 2,
//  parameter CONSUME_EN = 0,
  parameter DWIDTH = 8
)(
  input                 rst_n,
//  input [1:0]           consume,
  input [2*DWIDTH-1:0]  din,
  input [1:0]           dvldin,
  input                 ki,
  output                ko,
  output [2*DWIDTH-1:0] dout,
  output [1:0]          dvldout
);

wire [2*(DWIDTH+1)-1:0] reg_out[STAGES-1:0];
wire                    ko_interm[STAGES-1:0];
wire                    go_interm[STAGES-1:0];

wire [2*(DWIDTH+1)-1:0] reg_in[STAGES-1:0];
wire                    ki_interm[STAGES-1:0];
wire                    gi_interm[STAGES-1:0];

//wire [1:0]              consume_interm[STAGES-1:0];
wire                    ip_cmplt;
wire [2*(DWIDTH+1)-1:0] din_int;
wire [2*(DWIDTH+1)-1:0] din_int0;
wire [2*(DWIDTH+1)-1:0] din_int1;
wire [DWIDTH:0]         ip_bit;
wire                    ko_del;

assign din_int = {dvldin,din};

generate
genvar i;
for(i=0;i<=DWIDTH;i=i+1)
begin:IP
  assign ip_bit[i] = din_int[2*i+1] | din_int[2*i];
end
endgenerate

assign ip_cmplt = &ip_bit;

nv_thxx_wide #(
  .WIDTH(DWIDTH+1)
)u_din1(
  .rst_n(rst_n),
  .din({(ip_cmplt&dvldin[1]),din_int}),
  .dout(din_int1)
);

nv_thxx_wide #(
  .WIDTH(DWIDTH+1)
)u_din0(
  .rst_n(rst_n),
  .din({dvldin[0],{(DWIDTH+1){dvldin}}}),
  .dout(din_int0)
);

assign reg_in[0]                = din_int0 | din_int1;
assign ki_interm[STAGES-1]      = (dvldout == 2'b01) ? ko_del : ki; // This is used to toggle ki, when the downstream ki does not toggle
assign ko                       = ko_interm[0];

assign #0.005 ko_del = ko_interm[STAGES-1]; // This is equivalent to the auto-consume

generate
genvar j;

for (j=0;j<=STAGES-1;j=j+1)
begin : NSTG

  nv_reg #(
    .DWIDTH(DWIDTH+1)
  )u_reg_stage(
    .rst_n(rst_n),
    .din(reg_in[j]),
    .ki(ki_interm[j]),
    .ko(ko_interm[j]),
    .dout(reg_out[j])
  );

  if (j < STAGES-1)  
  begin
    assign ki_interm[j] = ko_interm[j+1];
  end

  if (j>0)
  begin
    assign reg_in[j] = reg_out[j-1];
  end

end
endgenerate

assign dvldout = reg_out[STAGES-1][2*(DWIDTH+1)-1:2*(DWIDTH+1)-2];


nv_thxx_wide #(
  .WIDTH(DWIDTH)
)u_op_gate(
  .rst_n(rst_n),
  .din({dvldout[1],reg_out[STAGES-1][2*DWIDTH-1:0]}),
  .dout(dout)
);


endmodule

`endif
