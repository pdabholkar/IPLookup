`timescale 1ps/1ps

module nv_bloom_filter_sr0 (
  input rst_n,
  input ki,
  output ko,
  input [1:0] msbk_i,
  input [5:0] mk_i,
  output [63:0] crc_sr_o
);
//============================================================================== 
wire        s_msbk;
wire [2:0]  s_mk;
wire [31:0] s_crc_sr;
wire        ko_msbk;
wire        ko_mk;
wire        ip_cmplt;
wire        op_cmplt;
wire        ko_crc_sr_o;


//------------------------------------------------------------------------------ 
// op_delval, ko_delval and seed value are defined in the included file. 
// The MEAN and SD of the delay value are also specified in the included file
//------------------------------------------------------------------------------ 
// These are the declarations for the delay synthesis logic
reg  [9:0]  op_delval;
reg  [9:0]  ko_delval;
reg  [31:0] seed;

initial
begin
  seed = 1;
end

reg [31:0] delay_params[0:7];
initial
begin
  $readmemh("bloom_filter_sr0_delay.mem",delay_params);
end        
//------------------------------------------------------------------------------ 
// Input side Dual rail to Single rail
//------------------------------------------------------------------------------ 
d2sreg #(
  .DW(1)
)u_msbk_d2s(
  .rst_n          (rst_n    ),
  .dual_in        (msbk_i   ),
  .single_out     (s_msbk   ),
  .ko             (ko_msbk  )
);

d2sreg #(
  .DW(3)
)u_mk_d2s(
  .rst_n          (rst_n    ),
  .dual_in        (mk_i     ),
  .single_out     (s_mk     ),
  .ko             (ko_mk    )
);

nv_thxx #(.NUM_IP(2))u_ip_cmplt(.rst_n(rst_n),.din({ko_mk,ko_msbk}),.dout(ip_cmplt));

//------------------------------------------------------------------------------ 
// Because we are working with a behavioural model here and artificially
// snthesizing the required delays, we can use the output completeness signal
// that indicates when the outputs are ready, and then delay it by a further 
// ko_delval which is the delay of the actual completion detection circuit to 
// generate the final ko signal
//------------------------------------------------------------------------------ 
psDelay u_ko_delay (
  .rst_n          (rst_n    ),
  .trig           (op_cmplt ),
  .del_value      (ko_delval),
  .dlyd_trig      (ko       )
);

//------------------------------------------------------------------------------ 
// creating a dummy delayed ip_cmplt_d1 signal used to sample the result of the 
// boolean operation and select the appropriate mean and sdev of the delay if 
// it is data dependent 
//------------------------------------------------------------------------------ 
wire ip_cmplt_d1;
assign #1 ip_cmplt_d1 = ip_cmplt;

always @(ip_cmplt_d1,rst_n)
begin
  if (~rst_n)
  begin
    op_delval = 10'h3ff;
    ko_delval = 10'h3ff;
  end
  else if (ip_cmplt_d1)
  begin
    op_delval = $dist_normal(seed,delay_params[0],delay_params[1]);
    ko_delval = $dist_normal(seed,delay_params[4],delay_params[5]); 
  end
  else
  begin
    op_delval = $dist_normal(seed,delay_params[2],delay_params[3]);
    ko_delval = $dist_normal(seed,delay_params[6],delay_params[7]); 
  end
end

//------------------------------------------------------------------------------ 
// Boolean Single rail module
//------------------------------------------------------------------------------ 
bloom_filter_sr0 u_bloom_filter_sr0 (
  .msbk_i         (s_msbk   ),
  .mk_i           (s_mk     ),
  .daddr_vld_i    (1'b1     ),  // This is because the s_msbk and s_mk will be generated only when the data is valid, unlike the Boolean case
  .crc_sr_o       (s_crc_sr )
);

//------------------------------------------------------------------------------ 
// Output side Single rail to Dual rail conversion
//------------------------------------------------------------------------------ 
s2dreg #(
  .DW(32)
)u_crc_sr_s2d(
  .rst_n          (rst_n      ),
  .del_value      (op_delval  ),
  .single_in      (s_crc_sr   ),
  .ki             (ki         ),
  .ip_cmplt       (ip_cmplt   ),
  .ko             (ko_crc_sr_o),
  .dual_out       (crc_sr_o   )
);

assign op_cmplt = ko_crc_sr_o;


endmodule
