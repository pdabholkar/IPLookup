`ifndef __NV_THXX_WIDE__
`define __NV_THXX_WIDE__

`timescale 1ns/1ps
module nv_thxx_wide #(
  parameter WIDTH = 1
)(
  input rst_n,
  input [2*WIDTH  :0] din,  // The most significant rail to gate the output
  output[2*WIDTH-1:0] dout
);

generate
genvar i;
for (i=0;i<=2*WIDTH-1;i=i+1)
  begin: BITS

    nv_thxx #(
      .NUM_IP(2)
    )u_bit(
      .rst_n(rst_n),
      .din({din[i],din[2*WIDTH]}),
      .dout(dout[i])
    );
  end
endgenerate


endmodule

`endif
