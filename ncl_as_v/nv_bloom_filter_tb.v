`timescale 1ns/1ps
module nv_bloom_filter_tb ();

  reg rst_n;
  reg [63:0] crc_sr_i;
  reg [1:0]  apk_i;
  reg [1:0]  tlevel_inc_i;
  reg ki;
  wire ko;
  wire [31:0] bf_idx1_d1_o;
  wire [31:0] bf_idx0_d1_o;
  wire [63:0] crc_sr_d1_o;

  initial
  begin
    rst_n = 1;
#10 rst_n = 0;
#10 rst_n = 1;
#1000 $finish;
  end

  initial 
  begin
    ki            = 0;
    crc_sr_i      = 64'b0;
    apk_i         = 2'b0;
    tlevel_inc_i  = 2'b0;
#40 ki            = 1;
#50 crc_sr_i      = 64'b1010010101010110100110011010011010011001101001011010010101100101;
    apk_i         = 2'b01;
    tlevel_inc_i  = 2'b01;
#3  ki            = 0;    
#5  ki            = 1;  
#5  crc_sr_i      = 64'b0;
    apk_i         = 2'b0;
    tlevel_inc_i  = 2'b0;

#50 crc_sr_i      = 64'b1001101010011010011001100110011001101010011010011010010101100101;
    apk_i         = 2'b10;
    tlevel_inc_i  = 2'b01;

#3  ki            = 0;
#5  ki            = 1;  
#5  crc_sr_i      = 64'b0;
    apk_i         = 2'b0;
    tlevel_inc_i  = 2'b0;
#5  ki            = 0;

#50 crc_sr_i      = 64'b1001010110011001010101011001100110100110100110101001011001100101;
    apk_i         = 2'b01;
    tlevel_inc_i  = 2'b10;
#5  ki            = 1;  
#5  crc_sr_i      = 64'b0;
    apk_i         = 2'b0;
    tlevel_inc_i  = 2'b0;
#5  ki            = 0;

#50 crc_sr_i      = 64'b1001010110010110101001010101101010010110101010010110010110101010;
    apk_i         = 2'b10;
    tlevel_inc_i  = 2'b10;
#5  ki            = 1;  
#5  crc_sr_i      = 64'b0;
    apk_i         = 2'b0;
    tlevel_inc_i  = 2'b0;

// Trying to force a Data Null Data value without reading out result of last
// operation
#50 crc_sr_i      = 64'b1010010101010110100110011010011010011001101001011010010101100101;
    apk_i         = 2'b01;
    tlevel_inc_i  = 2'b01;
#5  crc_sr_i      = 64'b0;
    apk_i         = 2'b0;
    tlevel_inc_i  = 2'b0;

// Only the result of the following data wavefront should come out. The
// previous data wavefront would be lost, because there wasn't a corresponding
// ki to pull the value out. 
#5  crc_sr_i      = 64'b1001101010011010011001100110011001101010011010011010010101100101;
    apk_i         = 2'b10;
    tlevel_inc_i  = 2'b01;

#3  ki            = 0;
#5  ki            = 1;  
#5  crc_sr_i      = 64'b0;
    apk_i         = 2'b0;
    tlevel_inc_i  = 2'b0;
#5  ki            = 0;

#50 crc_sr_i      = 64'b1001010110011001010101011001100110100110100110101001011001100101;
    apk_i         = 2'b01;
    tlevel_inc_i  = 2'b10;
#5  ki            = 1;  
#5  crc_sr_i      = 64'b0;
    apk_i         = 2'b0;
    tlevel_inc_i  = 2'b0;
#5  ki            = 0;

  end

  initial
  begin
    $dumpfile("nv_bloom_filter_dump.fst");
    $dumpvars;
  end

  nv_bloom_filter #(
    .BF_AWIDTH(16)
  )u_dut (
    .rst_n        (rst_n),
    .ki           (ki),
    .ko           (ko),
    .crc_sr_i     (crc_sr_i),
    .apk_i        (apk_i),
    .tlevel_inc_i (tlevel_inc_i),
    .crc_sr_d1_o  (crc_sr_d1_o),
    .bf_idx0_d1_o (bf_idx0_d1_o),
    .bf_idx1_d1_o (bf_idx1_d1_o)
  );
endmodule
