module nv_thxx_tb ();

reg [1:0] din;
wire dout;

initial
begin
    din = 2'b00;
#10 din = 2'b10;
#10 din = 2'b11;
#10 din = 2'b11;
#10 din = 2'b01;
#10 din = 2'b00;
#10 din = 2'b00;
#10 din = 2'b00;
#10 $finish;
end

initial
begin
  $dumpfile("nv_thxx_dump.vcd");
  $dumpvars;
end

nv_thxx #(
  .NUM_IP(2)
)u_dut(
  .din(din),
  .dout(dout)
);


endmodule
