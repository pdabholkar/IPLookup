`ifndef __NV_THXX__
`define __NV_THXX__

`timescale 1ps/1ps
module nv_thxx #(
  parameter NUM_IP = 5
)(
  input               rst_n,
  input  [NUM_IP-1:0] din,
  output              dout
);


reg dout_temp;

always @(din or rst_n)
if(~rst_n)
  dout_temp = 0;
else if(din == {NUM_IP{1'b1}})
  dout_temp = 1;
else if(din == {NUM_IP{1'b0}})
  dout_temp = 0;


assign #10 dout = dout_temp;
endmodule

`endif
