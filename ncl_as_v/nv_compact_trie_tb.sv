`timescale 1ps/1ps
module nv_compact_trie_tb ();

localparam IP_ADDRW    = 32;
localparam PTRIE       = 2;
localparam NHI_WIDTH   = 8;

`include "nv_compact_trie_tb_include.vh"

reg                     rst_n;
reg  [2*IP_ADDRW-1:0]   dst_addr_i;
wire                    outport_ko;
wire                    dut_ki;
wire  [2*NHI_WIDTH-1:0] outport_o;
wire  [79:0]            match_found_o;
wire  [39:0]            done_o;
wire                    dut_ko;

reg   [IP_ADDRW-1:0]    dst_addr[0:800000];
wire  [IP_ADDRW-1:0]    dst_addr_dut;
wire  [IP_ADDRW-1:0]    dst_addr_dut_d1;
wire                    ip_changed;
reg   [23:0]            tvec_counter;

wire  [NHI_WIDTH-1:0]   outport_dut;

reg                     enable;

reg [11:0]              old_time;
reg [11:0]              time_diff;
integer                 fp,fp1,fp3;
integer                 lvl;

//============================================================================== 

initial
begin
  //$recordfile("filename.trn");
  //$recordvars();
  $dumpfile("nv_compact_trie_wbf_wonc_dump.vcd");
  $dumpvars(4,nv_compact_trie_tb);
  $display("setup dump file");
end

  initial
  begin
        rst_n = 1;
        fp3 = $fopen("ko_dump.txt","w");
        $fwrite(fp3,"Dumping the cycle times\n");
        $fclose(fp3);
#10000  rst_n = 0;
#10000  rst_n = 1;
      
        $readmemb("../python/memMap_mgm/tvec_dump.log",dst_addr);
        $display("Done loading memory");

        @(tvec_counter > 'd480120) // Wait till the tvec_counter goes greater than 480210 - the number of vectors in tvec_dump.log

#100    fp1 = $fopen("counter_dump.log","w");

        $fwrite(fp1,"0  %d %d %d %d %d %d\n",nv_compact_trie_tb.u_dut.TRIE_STAGES[0].u_match_stage.bf_match_counter, nv_compact_trie_tb.u_dut.TRIE_STAGES[0].u_match_stage.bf_not_match_counter ,nv_compact_trie_tb.u_dut.TRIE_STAGES[0].u_match_stage.u_pfx_ram.null_ko_counter ,nv_compact_trie_tb.u_dut.TRIE_STAGES[0].u_match_stage.u_pfx_ram.non_null_ko_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[0].u_match_stage.u_lrc_ram.null_ko_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[0].u_match_stage.u_lrc_ram.non_null_ko_counter);
        $fwrite(fp1,"1  %d %d %d %d %d %d\n",nv_compact_trie_tb.u_dut.TRIE_STAGES[1].u_match_stage.bf_match_counter, nv_compact_trie_tb.u_dut.TRIE_STAGES[1].u_match_stage.bf_not_match_counter ,nv_compact_trie_tb.u_dut.TRIE_STAGES[1].u_match_stage.u_pfx_ram.null_ko_counter ,nv_compact_trie_tb.u_dut.TRIE_STAGES[1].u_match_stage.u_pfx_ram.non_null_ko_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[1].u_match_stage.u_lrc_ram.null_ko_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[1].u_match_stage.u_lrc_ram.non_null_ko_counter);
        $fwrite(fp1,"2  %d %d %d %d %d %d\n",nv_compact_trie_tb.u_dut.TRIE_STAGES[2].u_match_stage.bf_match_counter, nv_compact_trie_tb.u_dut.TRIE_STAGES[2].u_match_stage.bf_not_match_counter ,nv_compact_trie_tb.u_dut.TRIE_STAGES[2].u_match_stage.u_pfx_ram.null_ko_counter ,nv_compact_trie_tb.u_dut.TRIE_STAGES[2].u_match_stage.u_pfx_ram.non_null_ko_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[2].u_match_stage.u_lrc_ram.null_ko_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[2].u_match_stage.u_lrc_ram.non_null_ko_counter);
        $fwrite(fp1,"3  %d %d %d %d %d %d\n",nv_compact_trie_tb.u_dut.TRIE_STAGES[3].u_match_stage.bf_match_counter, nv_compact_trie_tb.u_dut.TRIE_STAGES[3].u_match_stage.bf_not_match_counter ,nv_compact_trie_tb.u_dut.TRIE_STAGES[3].u_match_stage.u_pfx_ram.null_ko_counter ,nv_compact_trie_tb.u_dut.TRIE_STAGES[3].u_match_stage.u_pfx_ram.non_null_ko_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[3].u_match_stage.u_lrc_ram.null_ko_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[3].u_match_stage.u_lrc_ram.non_null_ko_counter);
        $fwrite(fp1,"4  %d %d %d %d %d %d\n",nv_compact_trie_tb.u_dut.TRIE_STAGES[4].u_match_stage.bf_match_counter, nv_compact_trie_tb.u_dut.TRIE_STAGES[4].u_match_stage.bf_not_match_counter ,nv_compact_trie_tb.u_dut.TRIE_STAGES[4].u_match_stage.u_pfx_ram.null_ko_counter ,nv_compact_trie_tb.u_dut.TRIE_STAGES[4].u_match_stage.u_pfx_ram.non_null_ko_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[4].u_match_stage.u_lrc_ram.null_ko_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[4].u_match_stage.u_lrc_ram.non_null_ko_counter);
        $fwrite(fp1,"5  %d %d %d %d %d %d\n",nv_compact_trie_tb.u_dut.TRIE_STAGES[5].u_match_stage.bf_match_counter, nv_compact_trie_tb.u_dut.TRIE_STAGES[5].u_match_stage.bf_not_match_counter ,nv_compact_trie_tb.u_dut.TRIE_STAGES[5].u_match_stage.u_pfx_ram.null_ko_counter ,nv_compact_trie_tb.u_dut.TRIE_STAGES[5].u_match_stage.u_pfx_ram.non_null_ko_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[5].u_match_stage.u_lrc_ram.null_ko_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[5].u_match_stage.u_lrc_ram.non_null_ko_counter);
        $fwrite(fp1,"6  %d %d %d %d %d %d\n",nv_compact_trie_tb.u_dut.TRIE_STAGES[6].u_match_stage.bf_match_counter, nv_compact_trie_tb.u_dut.TRIE_STAGES[6].u_match_stage.bf_not_match_counter ,nv_compact_trie_tb.u_dut.TRIE_STAGES[6].u_match_stage.u_pfx_ram.null_ko_counter ,nv_compact_trie_tb.u_dut.TRIE_STAGES[6].u_match_stage.u_pfx_ram.non_null_ko_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[6].u_match_stage.u_lrc_ram.null_ko_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[6].u_match_stage.u_lrc_ram.non_null_ko_counter);
        $fwrite(fp1,"7  %d %d %d %d %d %d\n",nv_compact_trie_tb.u_dut.TRIE_STAGES[7].u_match_stage.bf_match_counter, nv_compact_trie_tb.u_dut.TRIE_STAGES[7].u_match_stage.bf_not_match_counter ,nv_compact_trie_tb.u_dut.TRIE_STAGES[7].u_match_stage.u_pfx_ram.null_ko_counter ,nv_compact_trie_tb.u_dut.TRIE_STAGES[7].u_match_stage.u_pfx_ram.non_null_ko_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[7].u_match_stage.u_lrc_ram.null_ko_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[7].u_match_stage.u_lrc_ram.non_null_ko_counter);
        $fwrite(fp1,"8  %d %d %d %d %d %d\n",nv_compact_trie_tb.u_dut.TRIE_STAGES[8].u_match_stage.bf_match_counter, nv_compact_trie_tb.u_dut.TRIE_STAGES[8].u_match_stage.bf_not_match_counter ,nv_compact_trie_tb.u_dut.TRIE_STAGES[8].u_match_stage.u_pfx_ram.null_ko_counter ,nv_compact_trie_tb.u_dut.TRIE_STAGES[8].u_match_stage.u_pfx_ram.non_null_ko_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[8].u_match_stage.u_lrc_ram.null_ko_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[8].u_match_stage.u_lrc_ram.non_null_ko_counter);
        $fwrite(fp1,"9  %d %d %d %d %d %d\n",nv_compact_trie_tb.u_dut.TRIE_STAGES[9].u_match_stage.bf_match_counter, nv_compact_trie_tb.u_dut.TRIE_STAGES[9].u_match_stage.bf_not_match_counter ,nv_compact_trie_tb.u_dut.TRIE_STAGES[9].u_match_stage.u_pfx_ram.null_ko_counter ,nv_compact_trie_tb.u_dut.TRIE_STAGES[9].u_match_stage.u_pfx_ram.non_null_ko_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[9].u_match_stage.u_lrc_ram.null_ko_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[9].u_match_stage.u_lrc_ram.non_null_ko_counter);
        $fwrite(fp1,"10 %d %d %d %d %d %d\n",nv_compact_trie_tb.u_dut.TRIE_STAGES[10].u_match_stage.bf_match_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[10].u_match_stage.bf_not_match_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[10].u_match_stage.u_pfx_ram.null_ko_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[10].u_match_stage.u_pfx_ram.non_null_ko_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[10].u_match_stage.u_lrc_ram.null_ko_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[10].u_match_stage.u_lrc_ram.non_null_ko_counter);
        $fwrite(fp1,"11 %d %d %d %d %d %d\n",nv_compact_trie_tb.u_dut.TRIE_STAGES[11].u_match_stage.bf_match_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[11].u_match_stage.bf_not_match_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[11].u_match_stage.u_pfx_ram.null_ko_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[11].u_match_stage.u_pfx_ram.non_null_ko_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[11].u_match_stage.u_lrc_ram.null_ko_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[11].u_match_stage.u_lrc_ram.non_null_ko_counter);
        $fwrite(fp1,"12 %d %d %d %d %d %d\n",nv_compact_trie_tb.u_dut.TRIE_STAGES[12].u_match_stage.bf_match_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[12].u_match_stage.bf_not_match_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[12].u_match_stage.u_pfx_ram.null_ko_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[12].u_match_stage.u_pfx_ram.non_null_ko_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[12].u_match_stage.u_lrc_ram.null_ko_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[12].u_match_stage.u_lrc_ram.non_null_ko_counter);
        $fwrite(fp1,"13 %d %d %d %d %d %d\n",nv_compact_trie_tb.u_dut.TRIE_STAGES[13].u_match_stage.bf_match_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[13].u_match_stage.bf_not_match_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[13].u_match_stage.u_pfx_ram.null_ko_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[13].u_match_stage.u_pfx_ram.non_null_ko_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[13].u_match_stage.u_lrc_ram.null_ko_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[13].u_match_stage.u_lrc_ram.non_null_ko_counter);
        $fwrite(fp1,"14 %d %d %d %d %d %d\n",nv_compact_trie_tb.u_dut.TRIE_STAGES[14].u_match_stage.bf_match_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[14].u_match_stage.bf_not_match_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[14].u_match_stage.u_pfx_ram.null_ko_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[14].u_match_stage.u_pfx_ram.non_null_ko_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[14].u_match_stage.u_lrc_ram.null_ko_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[14].u_match_stage.u_lrc_ram.non_null_ko_counter);
        $fwrite(fp1,"15 %d %d %d %d %d %d\n",nv_compact_trie_tb.u_dut.TRIE_STAGES[15].u_match_stage.bf_match_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[15].u_match_stage.bf_not_match_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[15].u_match_stage.u_pfx_ram.null_ko_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[15].u_match_stage.u_pfx_ram.non_null_ko_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[15].u_match_stage.u_lrc_ram.null_ko_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[15].u_match_stage.u_lrc_ram.non_null_ko_counter);
        $fwrite(fp1,"16 %d %d %d %d %d %d\n",nv_compact_trie_tb.u_dut.TRIE_STAGES[16].u_match_stage.bf_match_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[16].u_match_stage.bf_not_match_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[16].u_match_stage.u_pfx_ram.null_ko_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[16].u_match_stage.u_pfx_ram.non_null_ko_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[16].u_match_stage.u_lrc_ram.null_ko_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[16].u_match_stage.u_lrc_ram.non_null_ko_counter);
        $fwrite(fp1,"17 %d %d %d %d %d %d\n",nv_compact_trie_tb.u_dut.TRIE_STAGES[17].u_match_stage.bf_match_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[17].u_match_stage.bf_not_match_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[17].u_match_stage.u_pfx_ram.null_ko_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[17].u_match_stage.u_pfx_ram.non_null_ko_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[17].u_match_stage.u_lrc_ram.null_ko_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[17].u_match_stage.u_lrc_ram.non_null_ko_counter);
        $fwrite(fp1,"18 %d %d %d %d %d %d\n",nv_compact_trie_tb.u_dut.TRIE_STAGES[18].u_match_stage.bf_match_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[18].u_match_stage.bf_not_match_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[18].u_match_stage.u_pfx_ram.null_ko_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[18].u_match_stage.u_pfx_ram.non_null_ko_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[18].u_match_stage.u_lrc_ram.null_ko_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[18].u_match_stage.u_lrc_ram.non_null_ko_counter);
        $fwrite(fp1,"19 %d %d %d %d %d %d\n",nv_compact_trie_tb.u_dut.TRIE_STAGES[19].u_match_stage.bf_match_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[19].u_match_stage.bf_not_match_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[19].u_match_stage.u_pfx_ram.null_ko_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[19].u_match_stage.u_pfx_ram.non_null_ko_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[19].u_match_stage.u_lrc_ram.null_ko_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[19].u_match_stage.u_lrc_ram.non_null_ko_counter);
        $fwrite(fp1,"20 %d %d %d %d %d %d\n",nv_compact_trie_tb.u_dut.TRIE_STAGES[20].u_match_stage.bf_match_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[20].u_match_stage.bf_not_match_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[20].u_match_stage.u_pfx_ram.null_ko_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[20].u_match_stage.u_pfx_ram.non_null_ko_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[20].u_match_stage.u_lrc_ram.null_ko_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[20].u_match_stage.u_lrc_ram.non_null_ko_counter);
        $fwrite(fp1,"21 %d %d %d %d %d %d\n",nv_compact_trie_tb.u_dut.TRIE_STAGES[21].u_match_stage.bf_match_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[21].u_match_stage.bf_not_match_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[21].u_match_stage.u_pfx_ram.null_ko_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[21].u_match_stage.u_pfx_ram.non_null_ko_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[21].u_match_stage.u_lrc_ram.null_ko_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[21].u_match_stage.u_lrc_ram.non_null_ko_counter);
        $fwrite(fp1,"22 %d %d %d %d %d %d\n",nv_compact_trie_tb.u_dut.TRIE_STAGES[22].u_match_stage.bf_match_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[22].u_match_stage.bf_not_match_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[22].u_match_stage.u_pfx_ram.null_ko_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[22].u_match_stage.u_pfx_ram.non_null_ko_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[22].u_match_stage.u_lrc_ram.null_ko_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[22].u_match_stage.u_lrc_ram.non_null_ko_counter);
        $fwrite(fp1,"23 %d %d %d %d %d %d\n",nv_compact_trie_tb.u_dut.TRIE_STAGES[23].u_match_stage.bf_match_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[23].u_match_stage.bf_not_match_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[23].u_match_stage.u_pfx_ram.null_ko_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[23].u_match_stage.u_pfx_ram.non_null_ko_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[23].u_match_stage.u_lrc_ram.null_ko_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[23].u_match_stage.u_lrc_ram.non_null_ko_counter);
        $fwrite(fp1,"24 %d %d %d %d %d %d\n",nv_compact_trie_tb.u_dut.TRIE_STAGES[24].u_match_stage.bf_match_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[24].u_match_stage.bf_not_match_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[24].u_match_stage.u_pfx_ram.null_ko_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[24].u_match_stage.u_pfx_ram.non_null_ko_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[24].u_match_stage.u_lrc_ram.null_ko_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[24].u_match_stage.u_lrc_ram.non_null_ko_counter);
        $fwrite(fp1,"25 %d %d %d %d %d %d\n",nv_compact_trie_tb.u_dut.TRIE_STAGES[25].u_match_stage.bf_match_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[25].u_match_stage.bf_not_match_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[25].u_match_stage.u_pfx_ram.null_ko_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[25].u_match_stage.u_pfx_ram.non_null_ko_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[25].u_match_stage.u_lrc_ram.null_ko_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[25].u_match_stage.u_lrc_ram.non_null_ko_counter);
        $fwrite(fp1,"26 %d %d %d %d %d %d\n",nv_compact_trie_tb.u_dut.TRIE_STAGES[26].u_match_stage.bf_match_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[26].u_match_stage.bf_not_match_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[26].u_match_stage.u_pfx_ram.null_ko_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[26].u_match_stage.u_pfx_ram.non_null_ko_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[26].u_match_stage.u_lrc_ram.null_ko_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[26].u_match_stage.u_lrc_ram.non_null_ko_counter);
        $fwrite(fp1,"27 %d %d %d %d %d %d\n",nv_compact_trie_tb.u_dut.TRIE_STAGES[27].u_match_stage.bf_match_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[27].u_match_stage.bf_not_match_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[27].u_match_stage.u_pfx_ram.null_ko_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[27].u_match_stage.u_pfx_ram.non_null_ko_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[27].u_match_stage.u_lrc_ram.null_ko_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[27].u_match_stage.u_lrc_ram.non_null_ko_counter);
        $fwrite(fp1,"28 %d %d %d %d %d %d\n",nv_compact_trie_tb.u_dut.TRIE_STAGES[28].u_match_stage.bf_match_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[28].u_match_stage.bf_not_match_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[28].u_match_stage.u_pfx_ram.null_ko_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[28].u_match_stage.u_pfx_ram.non_null_ko_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[28].u_match_stage.u_lrc_ram.null_ko_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[28].u_match_stage.u_lrc_ram.non_null_ko_counter);
        $fwrite(fp1,"29 %d %d %d %d %d %d\n",nv_compact_trie_tb.u_dut.TRIE_STAGES[29].u_match_stage.bf_match_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[29].u_match_stage.bf_not_match_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[29].u_match_stage.u_pfx_ram.null_ko_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[29].u_match_stage.u_pfx_ram.non_null_ko_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[29].u_match_stage.u_lrc_ram.null_ko_counter,nv_compact_trie_tb.u_dut.TRIE_STAGES[29].u_match_stage.u_lrc_ram.non_null_ko_counter);


        $fclose(fp1);
#10000  $finish;
  end


always @(enable,dut_ko,rst_n)
begin
  if(~rst_n)
  begin
    tvec_counter = 'd0;
    $display("%t reset",$time);
    old_time = $time;
  end
  else if (enable & ~dut_ko)
  begin
    tvec_counter = tvec_counter + VEC_INC;
    $display("%t data",$time);
    time_diff = $time - old_time;
    // Opening the file in the initial block results in a log that is not
    // synced and so does not contain the complete data. So opening and
    // closing the file with each write
    fp = $fopen("ko_dump.txt","a");
    $fwrite(fp,"%d\n",time_diff);
    $fclose(fp);
    old_time = $time;
  end
  else if (enable & dut_ko)
  begin
    $display("%t null",$time);
    time_diff = $time - old_time;
    // Opening the file in the initial block results in a log that is not
    // synced and so does not contain the complete data. So opening and
    // closing the file with each write
    fp = $fopen("ko_dump.txt","a");
    $fwrite(fp,"%d\n",time_diff);
    $fclose(fp);
    old_time = $time;
  end
end

assign dst_addr_dut = dst_addr[tvec_counter[23:0]];

generate
genvar i;
for (i=0; i<=IP_ADDRW-1; i=i+1)
  begin : IP_ADDR_GEN

    always @(enable,dut_ko,rst_n)
    if (~rst_n)
      dst_addr_i[2*i+1:2*i] = 'd0;
    else if (enable & ~dut_ko)
      dst_addr_i[2*i+1:2*i] = {dst_addr_dut[i],~dst_addr_dut[i]};
    else if (enable & dut_ko)
      dst_addr_i[2*i+1:2*i] = 2'b00;
    else
      dst_addr_i[2*i+1:2*i] = dst_addr_i[2*i+1:2*i];
  
  end
endgenerate


//assign #6 dst_addr_dut_d1 = dst_addr_dut;

//assign ip_changed = |(dst_addr_dut_d1 ^ dst_addr_dut);
/*
s2dreg #(
  .DW(32)
)u_addr_s2dreg(
  .rst_n(rst_n),
  .del_value(10'd5),
  .single_in(dst_addr_dut),
  .dual_out(dst_addr_i),
  .ko(),
  .ki(dut_ko),              // ki from the next stage that receives this data output
  .ip_cmplt(ip_changed)        // ip_cmplt of the previous stage that feeds data in
);
*/

  initial 
  begin
    enable            = 0;
#50000
    enable            = 1;
#100000000
    enable            = 0;
  end

nv_compact_trie #(
  .IP_ADDRW             (IP_ADDRW           ),
  .PTRIE                (PTRIE              ),
  .NHI_WIDTH            (NHI_WIDTH          ),
  .USE_BLOOM            (USE_BLOOM          )
)u_dut(
  .rst_n_i              (rst_n              ),
  .dst_addr_i           (dst_addr_i         ),
  .ki                   (dut_ki             ),
  .ko                   (dut_ko             ),
  .outport_o            (outport_o          ),
  .match_found_o        (match_found_o      ),
  .done_o               (done_o             )
);


d2sreg #(
  .DW(NHI_WIDTH)
)u_outport(
  .rst_n(rst_n),
  .dual_in(outport_o),
  .single_out(outport_dut),
  .ko(outport_ko)
);


assign #100 dut_ki = outport_ko;

endmodule

