`ifndef __NV_REG_STAGES__
`define __NV_REG_STAGES__

`timescale 1ns/1ps
module nv_reg_stages #(
  parameter STAGES = 1,
//  parameter CONSUME_EN = 0,
  parameter DWIDTH = 8
)(
  input                 rst_n,
  input [2*DWIDTH-1:0]  din,
  input                 ki,
  output                ko,
  output [2*DWIDTH-1:0] dout
);

wire [2*DWIDTH-1:0] reg_out[STAGES-1:0];
wire                ko_interm[STAGES-1:0];

wire [2*DWIDTH-1:0] reg_in[STAGES-1:0];
wire                ki_interm[STAGES-1:0];

assign reg_in[0] = din;
assign ki_interm[STAGES-1] = ki;
assign ko = ko_interm[0];

generate
genvar j;

for (j=0;j<=STAGES-1;j=j+1)
begin : NSTG

  nv_reg #(
    .DWIDTH(DWIDTH)
  )u_reg_stage(
    .rst_n(rst_n),
    .din(reg_in[j]),
    .ki(ki_interm[j]),
    .ko(ko_interm[j]),
    .dout(reg_out[j])
  );

  if (j < STAGES-1)  
  begin
    assign ki_interm[j] = ko_interm[j+1];
  end

  if (j>0)
  begin
    assign reg_in[j] = reg_out[j-1];
  end

end
endgenerate

assign dout = reg_out[STAGES-1];

endmodule

`endif
