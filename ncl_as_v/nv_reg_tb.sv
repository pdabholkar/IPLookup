`timescale 1ns/1ps
module nv_reg_tb ();
  parameter DWIDTH = 4;

  reg                   rst_n;
  reg   [2*DWIDTH-1:0]  din;
  reg                   ki;
  wire  [2*DWIDTH-1:0]  dout;
  wire                  ko;

  initial
  begin
    all_null;
    ki    = 0;
    rst_n = 1'b1;
#10 rst_n = 1'b0;
#10 rst_n = 1'b1;
    ki    = 1'b1;
#10 din   = 8'h6A; // Corresponds to 8'h7F
#10 ki    = 1'b0;
//#10 assert (dout == 8'h6A) $display("PASS: Data %d received correctly",din); else $display("ERROR");
      
#2  ki    = 1'b1;
#10 all_null;
//#10 assert (dout == 8'h00) $display("PASS: Null received correctly"); else $display("ERROR");
      
#10 din   = 8'hA6;
//#10 assert (dout == 8'h00) $display("PASS: Output at null because ki is still at data"); else $display("ERROR");
     
#2  ki    = 1'b0;
//#10 assert (dout == 8'hA6) $display("PASS: second data wavefront now received correctly"); else $display("ERROR");
     
#10 $finish;
  end

  initial
  begin
    $dumpfile("nv_reg_dump.vcd");
    $dumpvars;
  end

  nv_reg #(
    .DWIDTH(DWIDTH)
  )u_dut(
    .rst_n(rst_n),
    .din(din),
    .ki(ki),
    .ko(ko),
    .dout(dout)
  );

task all_null;
  din = 'd0;
  
endtask

endmodule
