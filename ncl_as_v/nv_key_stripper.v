`timescale 1ps/1ps

module nv_key_stripper (
  input rst_n,
  input ki,
  output ko,
  input [63:0] key_i,
  output [1:0] msbk_o,
  output [5:0] mk_o,
  output [63:0] apk_o
);
//============================================================================== 
wire [31:0] s_key;
wire        s_msbk;
wire [2:0]  s_mk;
wire [31:0] s_apk;
wire        ko_key;
wire        ip_cmplt;
wire        op_cmplt;
wire        ko_mk_o;
wire        ko_msbk_o;
wire        ko_apk_o;

//------------------------------------------------------------------------------ 
// del_value and seed value are defined in the included file. The MEAN and SD
// of the delay value are also specified in the included file
//------------------------------------------------------------------------------ 
// These are the declarations for the delay synthesis logic
reg  [9:0]  op_delval;
reg  [9:0]  ko_delval;
reg  [31:0] seed;

initial
begin
  seed = 1;
end

reg [31:0] delay_params[0:7];
initial
begin
  $readmemh("key_stripper_delay.mem",delay_params);
end   
//------------------------------------------------------------------------------ 
// Input side Dual rail to Single rail
//------------------------------------------------------------------------------ 
d2sreg #(
  .DW(32)
)u_key_d2s(
  .rst_n          (rst_n    ),
  .dual_in        (key_i    ),
  .single_out     (s_key    ),
  .ko             (ko_key   )
);

assign ip_cmplt = ko_key;

//------------------------------------------------------------------------------ 
// Because we are working with a behavioural model here and artificially
// snthesizing the required delays, we can use the output completeness signal
// that indicates when the outputs are ready, and then delay it by a further 
// ko_delval which is the delay of the actual completion detection circuit to 
// generate the final ko signal
//------------------------------------------------------------------------------ 
psDelay u_ko_delay (
  .rst_n          (rst_n    ),
  .trig           (op_cmplt ),
  .del_value      (ko_delval),
  .dlyd_trig      (ko       )
);

//------------------------------------------------------------------------------ 
// Boolean Single rail module
//------------------------------------------------------------------------------ 
key_stripper u_key_stripper (
  .key_i          (s_key    ),
  .msbk_o         (s_msbk   ),
  .apk_o          (s_apk    ),
  .mk_o           (s_mk     )
);

//------------------------------------------------------------------------------ 
// creating a dummy delayed ip_cmplt_d1 signal that will be used to sample the mk   
// value of the present key. The sampled s_mk value is used to select the mean 
// of the rise and fall time distributions. This is because the key_stripper   
// module has a different distribution based on the number of bits that have   
// the same value as the msbk (represented by mk)                              
//------------------------------------------------------------------------------ 
wire ip_cmplt_d1;
assign #1 ip_cmplt_d1 = ip_cmplt;

always @(ip_cmplt_d1)
begin
  if (ip_cmplt_d1)
  begin
    op_delval = $dist_normal(seed,delay_params[0],delay_params[1]);
    ko_delval = $dist_normal(seed,delay_params[4],delay_params[5]); 
  end
  else
  begin
    op_delval = $dist_normal(seed,delay_params[2],delay_params[3]);
    ko_delval = $dist_normal(seed,delay_params[6],delay_params[7]);
  end
end

//------------------------------------------------------------------------------ 
// Output side Single rail to Dual rail conversion                              
//------------------------------------------------------------------------------ 
s2dreg #(
  .DW(1)
)u_msbk_s2d(
  .rst_n          (rst_n    ),
  .del_value      (op_delval),
  .single_in      (s_msbk   ),
  .ki             (ki       ),
  .ip_cmplt       (ip_cmplt ),
  .ko             (ko_msbk_o),
  .dual_out       (msbk_o   )
);

s2dreg #(
  .DW(3)
)u_mk_s2d(
  .rst_n          (rst_n    ),
  .del_value      (op_delval),
  .single_in      (s_mk     ),
  .ki             (ki       ),
  .ip_cmplt       (ip_cmplt ),
  .ko             (ko_mk_o  ),
  .dual_out       (mk_o     )
);

s2dreg #(
  .DW(32)
)u_apk_s2d(
  .rst_n          (rst_n    ),
  .del_value      (op_delval),
  .single_in      (s_apk    ),
  .ki             (ki       ),
  .ip_cmplt       (ip_cmplt ),
  .ko             (ko_apk_o ),
  .dual_out       (apk_o    )
);

nv_thxx #(.NUM_IP(3))u_op_cmplt(.rst_n(rst_n),.din({ko_apk_o,ko_mk_o,ko_msbk_o}),.dout(op_cmplt));        

endmodule

