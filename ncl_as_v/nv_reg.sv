`ifndef __NV_REG__
`define __NV_REG__

`timescale 1ns/1ps
module nv_reg #(
  parameter DWIDTH = 8
)(
  input                 rst_n,
  input [2*DWIDTH-1:0]  din,
  input                 ki,
  output                ko,
  output [2*DWIDTH-1:0] dout
);

wire [2*DWIDTH-1:0] th22_out;
wire [DWIDTH-1:0]   op_cmplt;
wire [DWIDTH-1:0]   ip_cmplt;
wire                ko_ip;
wire                ko_op;

generate
genvar i;

  for (i=0;i<=DWIDTH-1;i=i+1)
  begin:DW

    // zero rail
    nv_thxx #(
      .NUM_IP(2)
    )u_th22_0rail(
      .rst_n(rst_n),
      .din({~ki,din[2*i]}),
      .dout(th22_out[2*i])
    );

    // one rail
    nv_thxx #(
      .NUM_IP(2)
    )u_th22_1rail(
      .rst_n(rst_n),
      .din({~ki,din[2*i+1]}),
      .dout(th22_out[2*i+1])
    );

    assign #0.008 op_cmplt[i] = th22_out[2*i] | th22_out[2*i+1];

  end
endgenerate

nv_thxx #(
  .NUM_IP(DWIDTH)
)u_thxx_opcmplt_det(
  .rst_n(rst_n),
  .din(op_cmplt),
  .dout(ko)
);

assign dout = th22_out;

endmodule

`endif
