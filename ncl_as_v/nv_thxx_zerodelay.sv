`ifndef __NV_THXX_ZERODELAY__
`define __NV_THXX_ZERODELAY__

`timescale 1ps/1ps
module nv_thxx_zerodelay #(
  parameter NUM_IP = 5
)(
  input               rst_n,
  input  [NUM_IP-1:0] din,
  output reg          dout
);

always @(din or rst_n)
if(~rst_n)
  dout = 0;
else if(&din)
  dout = 1;
else if (~(|din))
  dout = 0;

endmodule

`endif
