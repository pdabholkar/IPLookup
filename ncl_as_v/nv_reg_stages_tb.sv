`timescale 1ns/1ps
module nv_reg_tb ();
  parameter DWIDTH = 4;
  parameter STAGES = 5;

  reg                   rst_n;
  reg   [2*DWIDTH-1:0]  din;
  reg                   ki;
  wire  [2*DWIDTH-1:0]  dout;
  wire                  ko;

  initial
  begin
    din   = 8'h00;
    ki    = 0;
    rst_n = 1'b1;
#10 rst_n = 1'b0;
#10 rst_n = 1'b1;
    ki    = 1'b1;
#10 din   = 8'h6A;
#1  assert (ko == 1'b1) $display("PASS: First data enters"); else $display("ERROR");

#10 ki    = 1'b0;
#1  assert (dout == 8'h6A) $display("PASS: Data WF1 received correctly"); else $display("ERROR");
      
#2  ki    = 1'b1;
#10 din   = 8'h00;
#1  assert (ko == 1'b0) $display("PASS: First null enters"); else $display("ERROR");

#1  assert (dout == 8'h00) $display("PASS: Null WF1 received correctly"); else $display("ERROR");
      
#10 din   = 8'hA6;
#1  assert (ko == 1'b1) $display("PASS: Second data enters"); else $display("ERROR");  

#10 din   = 8'h00;
#1  assert (ko == 1'b0) $display("PASS: Second null enters"); else $display("ERROR");

#10 din   = 8'h96;
#1  assert (ko == 1'b1) $display("PASS: Third  data enters"); else $display("ERROR");

#10 din   = 8'h00;
#1  assert (ko == 1'b0) $display("PASS: Third  null enters"); else $display("ERROR");

#1 assert (dout == 8'h00) $display("PASS: Output at null because ki == data"); else $display("ERROR");
     
#10  ki    = 1'b0;
#1   assert (dout == 8'hA6) $display("PASS: Data WF2 received correctly"); else $display("ERROR");

#10  ki    = 1'b1;
#1   assert (dout == 8'h00) $display("PASS: Null WF2 received correctly"); else $display("ERROR");

#10  ki    = 1'b0;
#1   assert (dout == 8'h96) $display("PASS: Data WF3 received correctly"); else $display("ERROR");

#10  ki    = 1'b1;
#1   assert (dout == 8'h00) $display("PASS: Null WF3 received correctly"); else $display("ERROR");

     
#10 $finish;
  end

  initial
  begin
    $dumpfile("nv_reg_stages_dump.fst");
    $dumpvars;
  end

  nv_reg_stages #(
    .DWIDTH(DWIDTH),
    .STAGES(STAGES)
  )u_dut(
    .rst_n(rst_n),
    .din(din),
    .ki(ki),
    .ko(ko),
    .dout(dout)
  );

endmodule
