// All parameters that are passed to instantiated blocks are specified as Boolean widths. 
// The 2x for Null and Data is only used when defining buses or nets in the design

`timescale 1ns/1ps
module nv_compact_trie #(
  parameter IP_ADDRW    = 32,
  parameter PTRIE       = 2,
  parameter NHI_WIDTH   = 8,
  parameter USE_BLOOM   = 1
)(
  input                   rst_n_i,
  input [2*IP_ADDRW-1:0]  dst_addr_i,
  input                   ki,
  output                  ko,
  output[2*NHI_WIDTH-1:0] outport_o,
  output[79:0]            match_found_o,
  output[39:0]            done_o
);
//============================================================================== 

`include "../python/memMap_mgm/memMap_mgm_const_param.h"
//`include "../python/memMap_mgm/mdepth_vs_tdepth_memMap_mgm.h"
//`include "../python/memMap_mgm/bfdepth_vs_tdepth_memMap_mgm.h"

wire                key_stripper_ki;
wire                key_stripper_ko;
wire [1:0]          msbk_ks;
wire [5:0]          mk_ks;
wire [63:0]         apk_ks;

wire                bf0_ki;
wire                bf0_ko;
wire [63:0]         crc_sr_d1;

wire                key_info_reg_ki;
wire                key_info_reg_ko;

wire [1:0]          msbk_ms_i         [TOT_DEPTH-1:0];
wire [1:0]          msbk_ms_o         [TOT_DEPTH-1:0];

wire [5:0]          mk_ms_i           [TOT_DEPTH-1:0];
wire [5:0]          mk_ms_o           [TOT_DEPTH-1:0];

wire [63:0]         crc_sr_ms_i       [TOT_DEPTH-1:0];
wire [63:0]         crc_sr_ms_o       [TOT_DEPTH-1:0];
 
wire [1:0]          tlevel_inc_ms_i   [TOT_DEPTH-1:0];
wire [1:0]          tlevel_inc_ms_o   [TOT_DEPTH-1:0];
 
wire [63:0]         address_ms_i      [TOT_DEPTH-1:0];
wire [63:0]         address_ms_o      [TOT_DEPTH-1:0];
 
wire [63:0]         apk_ms_i          [TOT_DEPTH-1:0];
wire [63:0]         apk_ms_o          [TOT_DEPTH-1:0];
 
wire [1:0]          daddr_vld_ms_i    [TOT_DEPTH-1:0];
wire [1:0]          daddr_vld_ms_o    [TOT_DEPTH-1:0];
 
wire [2*NHI_WIDTH-1:0]nhi_ms_i        [TOT_DEPTH-1:0];
wire [2*NHI_WIDTH-1:0]nhi_ms_o        [TOT_DEPTH-1:0];
 
wire [11:0]         matchlength_ms_i  [TOT_DEPTH-1:0];
wire [11:0]         matchlength_ms_o  [TOT_DEPTH-1:0];

wire [1:0]          match_found_ms_o  [TOT_DEPTH-1:0];

wire                ms_op_cmplt       [TOT_DEPTH-1:0];
 
wire                ms_ki             [TOT_DEPTH-1:0];
wire                ms_ko             [TOT_DEPTH-1:0];

wire                key_info_reg_stages_ki[TOT_DEPTH-1:0];
wire                key_info_reg_stages_ko[TOT_DEPTH-1:0];

wire                stage_ko          [TOT_DEPTH-1:0];
//============================================================================== 
/*
nv_thxx #(
  .DWIDTH (2)
)u_ko(
  .rst_n(rst_n_i),
  .din({key_stripper_ko,addr_reg_ko}),
  .dout(ko)
);
*/

assign ko = key_stripper_ko;
assign ms_ki[TOT_DEPTH-1] = ki;
assign key_info_reg_stages_ki[TOT_DEPTH-1] = ki;

nv_thxx #(
  .NUM_IP               (2                  )
)u_ko(
  .rst_n                (rst_n_i            ),
  .din                  ({bf0_ko,key_info_reg_ko}),
  .dout                 (key_stripper_ki    )
);

nv_key_stripper u_nv_key_stripper (
  .rst_n                (rst_n_i            ),
  .ki                   (key_stripper_ki    ),
  .ko                   (key_stripper_ko    ),
  .key_i                (dst_addr_i         ),
  .msbk_o               (msbk_ks            ),
  .mk_o                 (mk_ks              ),
  .apk_o                (apk_ks             )
);

assign key_info_reg_ki = stage_ko[0];

nv_reg #(
  .DWIDTH               (1+3+32             )
)u_key_info_reg(
  .rst_n                (rst_n_i            ),
  .din                  ({msbk_ks,mk_ks,apk_ks}),
  .ki                   (key_info_reg_ki    ),
  .ko                   (key_info_reg_ko    ),
  .dout                 ({msbk_ms_i[0],mk_ms_i[0],apk_ms_i[0]})
);

assign nhi_ms_i[0]          = {NHI_WIDTH{1'b0,key_info_reg_ko}};
assign matchlength_ms_i[0]  = {6{1'b0,key_info_reg_ko}};
assign address_ms_i[0]      = {1'b0,key_info_reg_ko};
assign tlevel_inc_ms_i[0]   = {key_info_reg_ko,1'b0};
assign daddr_vld_ms_i[0]    = {key_info_reg_ko,1'b0};

generate
if (USE_BLOOM == 1)
begin : BLOOM

  assign bf0_ki = stage_ko[0];

  nv_bloom_filter_sr0 u_nv_bloom_filter_sr0 (
    .rst_n              (rst_n_i            ),
    .ki                 (bf0_ki             ),
    .ko                 (bf0_ko             ),
    .msbk_i             (msbk_ks            ),
    .mk_i               (mk_ks              ),
    .crc_sr_o           (crc_sr_ms_i[0]     )
  );

end
else
begin : NOBLOOM

  assign bf0_ki = stage_ko[0];
  nv_thxx #(
    .NUM_IP             (2                  )
  )u_bloom_zero(
    .rst_n              (rst_n_i            ),
    .din                ({~bf0_ki,key_stripper_ko}),
    .dout               (bf0_ko             )
  );

  assign crc_sr_ms_i[0][63:0] = {32{1'b0,bf0_ko}};

end
endgenerate

generate
  genvar i,j,k;
  for (i=0; i<= (TOT_DEPTH-1); i=i+1)
  begin: TRIE_STAGES
  
    localparam USE_BLOOM_MSTAGE = ((i>=BF_LB)&&(i<=BF_UB))? USE_BLOOM : 0; // This is defined in the table specific define file

    if (i < TOT_DEPTH-1)
    begin: NOT_LAST_STAGE

      nv_thxx_wide #(.WIDTH(32))u_crc( .rst_n( rst_n_i ), .din( {daddr_vld_ms_o[i][1],crc_sr_ms_o[i]     } ), .dout( crc_sr_ms_i[i+1]      ) );
      nv_thxx_wide #(.WIDTH(1 ))u_tlv( .rst_n( rst_n_i ), .din( {daddr_vld_ms_o[i][1],tlevel_inc_ms_o[i] } ), .dout( tlevel_inc_ms_i[i+1]  ) );
      nv_thxx_wide #(.WIDTH(32))u_add( .rst_n( rst_n_i ), .din( {daddr_vld_ms_o[i][1],address_ms_o[i]    } ), .dout( address_ms_i[i+1]     ) );
      nv_thxx_wide #(.WIDTH(32))u_apk( .rst_n( rst_n_i ), .din( {daddr_vld_ms_o[i][1],apk_ms_o[i]        } ), .dout( apk_ms_i[i+1]         ) );
      nv_thxx_wide #(.WIDTH(1 ))u_dvl( .rst_n( rst_n_i ), .din( {daddr_vld_ms_o[i][1],daddr_vld_ms_o[i]  } ), .dout( daddr_vld_ms_i[i+1]   ) );
      nv_thxx_wide #(.WIDTH(8 ))u_nhi( .rst_n( rst_n_i ), .din( {daddr_vld_ms_o[i][1],nhi_ms_o[i]        } ), .dout( nhi_ms_i[i+1]         ) );
      nv_thxx_wide #(.WIDTH(6 ))u_ml ( .rst_n( rst_n_i ), .din( {daddr_vld_ms_o[i][1],matchlength_ms_o[i]} ), .dout( matchlength_ms_i[i+1] ) );
      nv_thxx_wide #(.WIDTH(1 ))u_msb( .rst_n( rst_n_i ), .din( {daddr_vld_ms_o[i][1],msbk_ms_o[i]       } ), .dout( msbk_ms_i[i+1]        ) );
      nv_thxx_wide #(.WIDTH(3 ))u_mk ( .rst_n( rst_n_i ), .din( {daddr_vld_ms_o[i][1],mk_ms_o[i]         } ), .dout( mk_ms_i[i+1]          ) );

      assign ms_ki[i]                   = stage_ko[i+1] | ms_op_cmplt[i];
      assign key_info_reg_stages_ki[i]  = stage_ko[i+1] | ms_op_cmplt[i];

    end

    nv_thxx #(
      .NUM_IP           (2                  )
    )u_ms_ko(
      .rst_n            (rst_n_i            ),
      .din              ({key_info_reg_stages_ko[i],ms_ko[i]}),
      .dout             (stage_ko[i]        )
    );

    nv_reg_stages #(
      .STAGES           (6                  ),
      .DWIDTH           (3+1                )
    )u_key_info_reg_stages(
      .rst_n            (rst_n_i            ),
      .din              ({msbk_ms_i[i],mk_ms_i[i]}),
      .ki               (key_info_reg_stages_ki[i]),
      .ko               (key_info_reg_stages_ko[i]),
      .dout             ({msbk_ms_o[i],mk_ms_o[i]})
    );

    nv_match_stage #(
      .TDEPTH           (i                   ),
      .PTRIE            (PTRIE               ),
      .IP_ADDRW         (IP_ADDRW            ),
      .NHI_WIDTH        (NHI_WIDTH           ),
      .USE_BLOOM_RAM    (USE_BLOOM_MSTAGE    ),
      .USE_BLOOM_CRC    (USE_BLOOM           )
    ) u_match_stage(
      .rst_n            (rst_n_i             ),
      .ki               (ms_ki[i]            ),
      .ko               (ms_ko[i]            ),

      .msbk_i           (msbk_ms_i[i]        ),
      .mk_i             (mk_ms_i[i]          ),

      .crc_sr_i         (crc_sr_ms_i[i]      ),

      .tlevel_inc_i     (tlevel_inc_ms_i[i]  ),
      .address_i        (address_ms_i[i]     ),
      .apk_i            (apk_ms_i[i]         ),
      .daddr_vld_i      (daddr_vld_ms_i[i]   ),

      .nhi_i            (nhi_ms_i[i]         ),
      .matchlength_i    (matchlength_ms_i[i] ),

      .crc_sr_d6_o      (crc_sr_ms_o[i]      ),

      .tlevel_inc_d6_o  (tlevel_inc_ms_o[i]  ),
      .address_d6_o     (address_ms_o[i]     ),
      .apk_d6_o         (apk_ms_o[i]         ),
      .daddr_vld_d6_o   (daddr_vld_ms_o[i]   ),

      .nhi_d6_o         (nhi_ms_o[i]         ),
      .matchlength_d6_o (matchlength_ms_o[i] ),
      .match_found_d6_o (match_found_ms_o[i] )
    );

    wire [NHI_WIDTH-1:0] nhi_ms_o_bits;
    wire [5:0]           ml_ms_o_bits;
    wire                 nhi_ms_cmplt;
    wire                 ml_ms_cmplt;
    wire                 mf_ms_cmplt;
    
    for (j=0;j<=NHI_WIDTH-1;j=j+1)
      assign nhi_ms_o_bits[j] = nhi_ms_o[i][2*j] | nhi_ms_o[i][2*j+1];

    assign nhi_ms_cmplt = &nhi_ms_o_bits;

    assign mf_ms_cmplt = match_found_ms_o[i][1] | match_found_ms_o[i][0];

    nv_thxx #(.NUM_IP(3))u_ms_op_cmplt(.rst_n(rst_n_i),.din({daddr_vld_ms_o[i][0],nhi_ms_cmplt,mf_ms_cmplt}),.dout(ms_op_cmplt[i]));

    assign match_found_o[2*(i+1)-1:2*i] = match_found_ms_o[i];
    assign done_o[i]                    = ms_op_cmplt[i];

  end
endgenerate 

assign outport_o = nhi_ms_o[TOT_DEPTH-1];

endmodule
