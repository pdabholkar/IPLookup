`timescale 1ns/1ns
module d2sreg_tb ();
  localparam DW = 32;

  reg rst_n;

  reg [2*DW-1:0] din;
  wire [DW-1:0] dout;
  wire ko;


  initial
  begin
    rst_n = 1;
#20 rst_n = 0;
#10 rst_n = 1;

#22 din = 64'b1001011001100110101010010110101010100101011001101001101010100110;
#11 din = 64'd0;
#11 din = 64'b1001010110101010011001100101010101101001011001100101101010010101;
#11 din = 64'd0;
#100 $finish;
  end

  d2sreg #(
    .DW(DW)
  )u_dut(
    .rst_n(rst_n),
    .dual_in(din),
    .single_out(dout),
    .ko(ko)
  );

  initial
  begin
    $dumpfile("dump.vcd");
    $dumpvars;
  end
endmodule

