`ifndef __PSDELAY__
`define __PSDELAY__

`timescale 1ps/1ps
//============================================================================== 
// Synthesizing Transport and Inertial propagation delays
// This task uses a local delay clock has been generated using the
// delay_clock.vh and the file has been included before the delay_task.v is
// included
//============================================================================== 
module psDelay (
  input       trig,
  input       rst_n,
  input [9:0] del_value,
  output reg  dlyd_trig
);

reg [9:0] del_cntr;
reg       trig_d1;
reg       trig_d2;
wire      trig_edge;
wire      dly_hit; 

// Define a del_clk to generate the delay value 
reg del_clk;

initial
begin
  del_clk = 1;
end

always #1 del_clk = ~del_clk;

//////////////////////////////////////
always @(posedge del_clk)
begin
  trig_d1 <= #1 trig;
  trig_d2 <= #1 trig_d1;
end

assign trig_edge = trig_d1 ^ trig_d2;

always @(posedge del_clk)
  if (trig_edge)
    del_cntr <= 'd0;
  else if (del_cntr[9:0] <= (del_value[9:0] + 2)) 
    del_cntr <= del_cntr + 'd2;
  else
    del_cntr <= 'h3FF;

//------------------------------------------------------------------------------   
// The dly_hit is what will be used by the subsequent modules/logic that
// will sample their outputs when the delay value is reached after the trig
// occured  
//------------------------------------------------------------------------------   
assign dly_hit = (del_cntr[9:1] == del_value[9:1]);

always @(*)
  if (~rst_n)
    dlyd_trig = 1'd0;
  else if (dly_hit)
    dlyd_trig = trig;
  else
    dlyd_trig = dlyd_trig;

endmodule

`endif
