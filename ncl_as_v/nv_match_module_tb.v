`timescale 1ns/1ps
module nv_match_module_tb ();
  localparam NHI_WIDTH = 8;
  reg         rst_n;
  reg         ki;
  wire        ko;
  reg [1:0]              msbk_i;
  reg [5:0]              mk_i;
  reg [63:0]             apk_i;
  // generated from previous pipeline stag
  reg [2*NHI_WIDTH-1:0]  nhi_i;
  reg [11:0]             matchlength_i;
  // Children information
  reg [1:0]              epsilon_i;
  reg                    pfx_ram_read_i;
  reg  [1:0]             bf_match_i;
  // First prefix in the node
  reg  [2*18-1:0]        pfx1_i;
  wire [1:0]             msb1_i;
  wire [1:0]             lsb1_i;
  wire [5:0]             mp1_i;
  wire [9:0]             lp1_i;
  wire [15:0]            nhi1_i;
  // Second prefix in the node
  reg  [2*18-1:0]        pfx2_i;
  wire [1:0]             msb2_i;
  wire [1:0]             lsb2_i;
  wire [5:0]             mp2_i;
  wire [9:0]             lp2_i;
  wire [15:0]            nhi2_i;
  // generated for next pipeline stage's m  begin
  wire [15:0]            nhi_d2_o;
  wire [11:0]            matchlength_d2_o;
  wire [1:0]             match_found_d2_o;

  reg                    tb_clk;
   
//============================================================================== 
/*
integer fp;
integer fp2;
reg [31:0] dummy;

initial 
begin
  fp = $fopen("../python_for_testing/test_file.txt", "r"); //Opening text file
    if (fp == 0) begin                        // If file is not found raise error and finish
      $display("data_file handle was NULL");
      $finish;
    end
    else
      #1000 $finish;
end

initial
  tb_clk = 1;
  
always #2.5 tb_clk = ~tb_clk;


always @(posedge tb_clk)
begin
  fp2 = $fscanf(fp,"%d %b %b %b %b %b",dummy,apk_i,epsilon_i,msbk_i,mk_i,nhi_i,matchlength_i);
  $display(dummy,apk_i,mk_i);
end

*/  

assign {msb1_i,mp1_i,lsb1_i,lp1_i,nhi1_i} = pfx1_i;
assign {msb2_i,mp2_i,lsb2_i,lp2_i,nhi2_i} = pfx2_i;

  initial
  begin
      rst_n = 1;
#10   rst_n = 0;
#10   rst_n = 1;
#1200 $finish;
  end

  initial 
  begin
    ki            = 0;
    all_null();
#40 ki            = 1;

#50 apk_i         = 64'b0101011001100110101001100101010101101001010110101001101010100101;
    epsilon_i     = 2'b01;
    msbk_i        = 2'b01;  
    mk_i          = 6'b011010;
    nhi_i         = 16'h5555;
    matchlength_i = 12'b010101010101;
    pfx1_i        = 36'b01_011010_10_0101011001_0101010101101010;
    pfx2_i        = 36'b01_011010_01_0101011010_1001010110010110;
    pfx_ram_read_i= 1'b1;
    bf_match_i    = 2'b10;
    
#3  ki            = 0;    
#5  ki            = 1;  
#10 all_null();

#50 apk_i         = 64'b0101011001100110101001100101010101101001010110101001101010100101;
    epsilon_i     = 2'b01;
    msbk_i        = 2'b10;  
    mk_i          = 6'b011010;
    nhi_i         = 16'h5556;
    matchlength_i = 12'b010101010101;
    pfx1_i        = 36'b01_011010_10_0101011001_0101010101101010;
    pfx2_i        = 36'b01_011010_01_0101011010_1001010110010110;
    pfx_ram_read_i= 1'b1;
    bf_match_i    = 2'b10;

#3  ki            = 0;
#5  ki            = 1;  
#5  all_null();

#50 apk_i         = 64'b0101011001100110101001100101010101101001010110101001101010100101;
    epsilon_i     = 2'b01;
    msbk_i        = 2'b10;  
    mk_i          = 6'b011010;
    nhi_i         = 16'h5559;
    matchlength_i = 12'b010101010101;
    pfx1_i        = 36'b01_011010_01_0101011010_1001010110011010;
    pfx2_i        = 36'b01_011010_10_0101011001_0101010101100110;
    pfx_ram_read_i= 1'b1;
    bf_match_i    = 2'b10;

#3  ki            = 0;
#5  ki            = 1;  
#5  all_null();


#50 apk_i         = 64'b0101011001100110101001100101010101101001010110101001101010100101;
    epsilon_i     = 2'b01;
    msbk_i        = 2'b01;  
    mk_i          = 6'b011010;
    nhi_i         = 16'h555A;
    matchlength_i = 12'b010101010101;
    pfx1_i        = 36'b01_011010_01_0101011010_1010010110010110;
    pfx2_i        = 36'b01_011010_10_0101011001_0110010101101010;
    pfx_ram_read_i= 1'b1;
    bf_match_i    = 2'b10;
    
#3  ki            = 0;    
#5  ki            = 1;  
#10 all_null();



#5  ki            = 0;

#50 apk_i         = 64'b0101011001100110101001100101010101101001010110101001101010100101;
    epsilon_i     = 2'b01;
    msbk_i        = 2'b01;  
    mk_i          = 6'b011010;
    nhi_i         = 16'h5565;
    matchlength_i = 12'b010101010101;
    pfx1_i        = 36'b01_011010_10_0101011001_0101010101101010;
    pfx2_i        = 36'b01_011010_01_0101011010_1001010110010110;
    pfx_ram_read_i= 1'b1;
    bf_match_i    = 2'b10;

#5  ki            = 1;  
#5  all_null(); 

#5  ki            = 0;

// Test case where the bf_match is '0'. The match_module should ignore the
// prefix and instead forward incoming nhi_i and matchlength_i forward
#50 apk_i         = 64'b0101011001100110101001100101010101101001010110101001101010100101;
    epsilon_i     = 2'b01;
    msbk_i        = 2'b01;  
    mk_i          = 6'b011010;
    nhi_i         = 16'h5566;
    matchlength_i = 12'b010101010101;
    pfx1_i        = 36'b01_011010_10_0101011001_0101010101101010;
    pfx2_i        = 36'b01_011010_01_0101011010_1001010110010110;
    pfx_ram_read_i= 1'b1;
    bf_match_i    = 2'b01;

#5  ki            = 1;  
#5  all_null(); 

// Trying to force a Data Null Data value without reading out result of last
// operation
#50 apk_i         = 64'b0101011001100110101001100101010101101001010110101001101010100101;
    epsilon_i     = 2'b01;
    msbk_i        = 2'b01;  
    mk_i          = 6'b011010;
    nhi_i         = 16'h5569;
    matchlength_i = 12'b010101010101;
    pfx1_i        = 36'b01_011010_10_0101011001_0101010101101010;
    pfx2_i        = 36'b01_011010_01_0101011010_1001010110010110;
    pfx_ram_read_i= 1'b1;
    bf_match_i    = 2'b10;

#5  ki            = 1;  
#5  all_null(); 
#35 ki            = 0;

#50 apk_i         = 64'b0101011001100110101001100101010101101001010110101001101010100101;
    epsilon_i     = 2'b01;
    msbk_i        = 2'b10;  
    mk_i          = 6'b011010;
    nhi_i         = 16'h556A;
    matchlength_i = 12'b010101010101;
    pfx1_i        = 36'b01_011010_10_0101011001_0101010101101010;
    pfx2_i        = 36'b01_011010_01_0101011010_1001010110010110;
    pfx_ram_read_i= 1'b1;
    bf_match_i    = 2'b10;

#5  all_null();

// Because the match_module has two stages internally, this data wavefront
// would be stuck inside and be drawn from the output when ki goes low next,
// which is in the following test vector 
#5  ki            = 0;

#50 apk_i         = 64'b0101011001100110101001100101010101101001010110101001101010100101;
    epsilon_i     = 2'b01;
    msbk_i        = 2'b01;  
    mk_i          = 6'b011010;
    nhi_i         = 16'h5595;
    matchlength_i = 12'b010101010101;
    pfx1_i        = 36'b01_011010_10_0101011001_0101010101101010;
    pfx2_i        = 36'b01_011010_01_0101011010_1001010110010110;
    pfx_ram_read_i= 1'b1;
    bf_match_i    = 2'b10;

#5  ki            = 1;  
#5  all_null(); 
#35 ki            = 0;


#50 apk_i         = 64'b0101011001100110101001100101010101101001010110101001101010100101;
    epsilon_i     = 2'b01;
    msbk_i        = 2'b10;  
    mk_i          = 6'b011010;
    nhi_i         = 16'h5596;
    matchlength_i = 12'b010101010101;
    pfx1_i        = 36'b01_011010_10_0101011001_0101010101101010;
    pfx2_i        = 36'b01_011010_01_0101011010_1001010110010110;
    pfx_ram_read_i= 1'b1;
    bf_match_i    = 2'b10;

#3  ki            = 0;
#5  ki            = 1;  
#5  all_null();

// pfx1 and pfx2 are null though BF matches
#50 apk_i         = 64'b0101011001100110101001100101010101101001010110101001101010100101;
    epsilon_i     = 2'b01;
    msbk_i        = 2'b10;  
    mk_i          = 6'b011010;
    nhi_i         = 16'h5599;
    matchlength_i = 12'b010101010101;
    pfx1_i        = 36'b0;
    pfx2_i        = 36'b0;
    pfx_ram_read_i= 1'b1;
    bf_match_i    = 2'b10;

#3  ki            = 0;
#5  ki            = 1;  
#5  all_null();


#50 apk_i         = 64'b0101011001100110101001100101010101101001010110101001101010100101;
    epsilon_i     = 2'b01;
    msbk_i        = 2'b10;  
    mk_i          = 6'b011010;
    nhi_i         = 16'h559A;
    matchlength_i = 12'b010101010101;
    pfx1_i        = 36'b01_011010_10_0101011001_0101010101101010;
    pfx2_i        = 36'b01_011010_01_0101011010_1001010110010110;
    pfx_ram_read_i= 1'b1;
    bf_match_i    = 2'b10;

#3  ki            = 0;
#5  ki            = 1;  
#5  all_null();

// pfx1 is null though BF matches
#50 apk_i         = 64'b0101011001100110101001100101010101101001010110101001101010100101;
    epsilon_i     = 2'b01;
    msbk_i        = 2'b10;  
    mk_i          = 6'b011010;
    nhi_i         = 16'h55A5;
    matchlength_i = 12'b010101010101;
    pfx1_i        = 36'b0;
    pfx2_i        = 36'b01_011010_01_0101011010_1001010110010110;;
    pfx_ram_read_i= 1'b1;
    bf_match_i    = 2'b10;

#3  ki            = 0;
#5  ki            = 1;  
#5  all_null();

// pfx2 is null though BF matches
#50 apk_i         = 64'b0101011001100110101001100101010101101001010110101001101010100101;
    epsilon_i     = 2'b01;
    msbk_i        = 2'b10;  
    mk_i          = 6'b011010;
    nhi_i         = 16'h55A6;
    matchlength_i = 12'b010101010101;
    pfx1_i        = 36'b01_011010_10_0101011001_0101010101101010;
    pfx2_i        = 36'b0;
    pfx_ram_read_i= 1'b1;
    bf_match_i    = 2'b10;

#3  ki            = 0;
#5  ki            = 1;  
#5  all_null();


#50 apk_i         = 64'b0101011001100110101001100101010101101001010110101001101010100101;
    epsilon_i     = 2'b01;
    msbk_i        = 2'b10;  
    mk_i          = 6'b011010;
    nhi_i         = 16'h55A9;
    matchlength_i = 12'b010101010101;
    pfx1_i        = 36'b01_011010_10_0101011001_0101010101101010;
    pfx2_i        = 36'b01_011010_01_0101011010_1001010110010110;
    pfx_ram_read_i= 1'b1;
    bf_match_i    = 2'b10;

#3  ki            = 0;
#5  ki            = 1;  
#5  all_null();

// pfx1 is null though BF matches
#50 apk_i         = 64'b0101011001100110101001100101010101101001010110101001101010100101;
    epsilon_i     = 2'b01;
    msbk_i        = 2'b10;  
    mk_i          = 6'b011010;
    nhi_i         = 16'h55AA;
    matchlength_i = 12'b010101010101;
    pfx1_i        = 36'b0;
    pfx2_i        = 36'b01_011010_01_0101011010_1001010110010110;;
    pfx_ram_read_i= 1'b1;
    bf_match_i    = 2'b10;

#3  ki            = 0;
#5  ki            = 1;  
#5  all_null();

#10  ki            = 0;
#15  ki            = 1;  
#10  ki            = 0;
  end

  initial
  begin
    $dumpfile("nv_match_module_dump.fst");
    $dumpvars;
  end

nv_match_module #(
  .TDEPTH           (1                 ),
  .PTRIE            (2                 ),
  .NHI_WIDTH        (8                 )
)u_dut(
  .rst_n            (rst_n             ),
  .ki               (ki                ),
  .ko               (ko                ),
  // Key information
  .msbk_i           (msbk_i            ),
  .mk_i             (mk_i              ),
  .apk_i            (apk_i             ),
  // Info generated from previous pipeline stage's match stage
  .nhi_i            (nhi_i             ),
  .matchlength_i    (matchlength_i     ),
  // Prefix information read
  .pfx_ram_read_i   (pfx_ram_read_i          ),
  .bf_match_i       (bf_match_i        ),
  // Children information
  .epsilon_i        (epsilon_i         ),
  // First prefix in the node
  .msb1_i           (msb1_i            ),
  .lsb1_i           (lsb1_i            ),
  .mp1_i            (mp1_i             ),
  .lp1_i            (lp1_i             ),
  .nhi1_i           (nhi1_i            ),
  // Second prefix in the node
  .msb2_i           (msb2_i            ),
  .lsb2_i           (lsb2_i            ),
  .mp2_i            (mp2_i             ),
  .lp2_i            (lp2_i             ),
  .nhi2_i           (nhi2_i            ),
  // Info generated for next pipeline stage's match stage
  .nhi_d2_o         (nhi_d2_o          ),
  .matchlength_d2_o (matchlength_d2_o  ),
  .match_found_d2_o (match_found_d2_o  )
);

wire [NHI_WIDTH-1:0]  nhi_d2_o_s;
wire [5:0]            matchlength_d2_o_s;
wire                  match_found_d2_o_s;

d2sreg #(
  .DW(NHI_WIDTH+6+1)
)u_dout_tb(
  .rst_n(rst_n),
  .dual_in({nhi_d2_o,matchlength_d2_o,match_found_d2_o}),
  .single_out({nhi_d2_o_s,matchlength_d2_o_s,match_found_d2_o_s})
);  

task all_null;

  apk_i         = 64'b0;
  epsilon_i     = 2'b0;
  msbk_i        = 2'b0;
  mk_i          = 3'd0;
  nhi_i         = 8'd0;
  matchlength_i = 6'd0;
  pfx1_i        = 36'd0;
  pfx2_i        = 36'd0;
  pfx_ram_read_i= 1'b0;
  bf_match_i    = 2'b00;

endtask

endmodule
