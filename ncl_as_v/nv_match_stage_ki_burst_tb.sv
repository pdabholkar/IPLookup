`timescale 1ns/1ps
`define MSTAGE_SIM 1
module nv_match_stage_tb();

  localparam TDEPTH        = 8;
  localparam PTRIE         = 2;
  localparam IP_ADDRW      = 32;
  localparam NHI_WIDTH     = 8;
  localparam USE_BLOOM_RAM = 1;
  localparam USE_BLOOM_CRC = 1;
  
  localparam MEM_SIZE      = 241;
  localparam MEM_AWIDTH    = clogb2(241)+1;
  localparam CP_WIDTH      = clogb2(457)+1;
  localparam PFX_INIT_FILE = "E:/gitlab/IPLookup/python/memMap_mgm/pfxMap8_testing.mem";
  localparam LRC_INIT_FILE = "E:/gitlab/IPLookup/python/memMap_mgm/lrcMap8_testing.mem";
  localparam TOT_DEPTH     = 30;
  localparam BF_SIZE       = 1024;
  localparam BF_AWIDTH     = 10;//clogb2(1024) + 1;
  localparam BF_INIT_FILE  = "E:/gitlab/IPLookup/python/memMap_mgm/bf8_testing.mem";
  
  reg                         rst_n;
  reg                         ki;
  wire                        ko;
  reg  [1:0]                  msbk_i;
  reg  [5:0]                  mk_i;
   // Inputs from the previous match stage's CRC computation block
  reg  [63:0]                 crc_sr_i;
  reg  [1:0]                  tlevel_inc_i;

  // Inputs from the previous match stage's child computation block
  reg  [(2*MEM_AWIDTH)-1:0]   address_i;
  reg  [(2*IP_ADDRW)-1:0]     apk_i;
  reg  [1:0]                  daddr_vld_i;

  // Inputs from the previous match stage's prefix match block
  reg  [(2*NHI_WIDTH)-1:0]    nhi_i;
  reg  [11:0]                 matchlength_i;

  // Outputs from the CRC computation block appropriately delayed
  wire [63:0]                 crc_sr_d6_o;

  // Outputs from the next child computation block appropriately delayed
  wire [1:0]                  tlevel_inc_d6_o;
  wire [(2*CP_WIDTH)-1:0]     address_d6_o;
  wire [(2*IP_ADDRW)-1:0]     apk_d6_o;
  wire [1:0]                  daddr_vld_d6_o;

  // Outputs from the prefix match block appropriately delayed
  wire [(2*NHI_WIDTH)-1:0]    nhi_d6_o;
  wire [11:0]                 matchlength_d6_o;
  wire [1:0]                  match_found_d6_o;
  reg  [4:0]                  vec_cntr;

//============================================================================== 
  initial
  begin
      rst_n = 1;
#10   rst_n = 0;
#10   rst_n = 1;
//@(vec_cntr > 5'd10);
#1000 $finish;
  end

always @(posedge ko or negedge rst_n)
begin
  if (~rst_n)
     vec_cntr <= 'd0;
  else
     vec_cntr <= vec_cntr + 1'd1;
end

  initial 
  begin
    ki            = 0;
    all_null();
//#40 ki            = 1;
//#3  ki            = 0;    

// Non null next address
// BF does not match
#20 msbk_i        = 2'b01;
    mk_i          = 6'b011010;
    crc_sr_i      = 64'b01010101_01100110_10100110_01010101_01101001_01011010_10011010_10100101;
    tlevel_inc_i  = 2'b10;
    address_i     = 16'h9656;  //91
    apk_i         = 64'b01010110_01100110_10100110_01010101_01101001_01011010_10011010_10100101;
    daddr_vld_i   = 2'b10;
    nhi_i         = 16'h5556; 
    matchlength_i = 12'h555;  


#10 if ((nhi_d6_o == 16'h5556) & (match_found_d6_o == 2'b01) & (address_d6_o[17:0] == 18'h25699) & (apk_d6_o == 64'h599A_9955_A56A_6A95) & ((USE_BLOOM_CRC & (crc_sr_d6_o == 64'h5559A9955A56A6A9)) | (~USE_BLOOM_CRC & (crc_sr_d6_o == 64'h5566_A655_695A_9AA5))) & (daddr_vld_d6_o == 2'b10))
      $display("%d Pass Test 'Non Null Next Address, No BF Match' Data Wavefront",$time);
    else
    begin
      $display ("%d FAIL Test 'Non Null Next Address, No BF Match' Data Wavefront",$time);
      $display("%h %h %h %h %h",nhi_d6_o,match_found_d6_o,address_d6_o,apk_d6_o,crc_sr_d6_o);
    end

#5  ki            = 1;  
#10 all_null();
#10 if ((nhi_d6_o == 'd0) & (match_found_d6_o == 2'b00) & (address_d6_o[17:0] == 'h0) & (apk_d6_o == 64'h0) & (crc_sr_d6_o == 64'h0) & (daddr_vld_d6_o == 2'b00))
      $display("%d Pass Test 'Null Wavefront'",$time);
    else
    begin
      $display ("%d FAIL Test  'Null Wavefront'",$time);
      $display("%h %h %h %h %h",nhi_d6_o,match_found_d6_o,address_d6_o,apk_d6_o,crc_sr_d6_o);
    end
#3  ki            = 0;    

/////// Interesting //////////
// This address is null, so no comparison required. 
// Bf matches
// prefix matches
#20 msbk_i        = 2'b0;
    mk_i          = 6'b0;
    crc_sr_i      = 64'b0;
    tlevel_inc_i  = 2'b00;
    address_i     = 16'b0; // 0
    apk_i         = 64'b0;
    daddr_vld_i   = 2'b01;
    nhi_i         = 16'h5559;
    matchlength_i = 12'b0;

#10 if ((nhi_d6_o == 16'h5559) & (match_found_d6_o == 2'b01) & (address_d6_o[17:0] == 'h0) & (apk_d6_o == 64'h0) & (crc_sr_d6_o == 64'h0) & (daddr_vld_d6_o == 2'b01))
      $display("%d Pass Test 'Input address is Null' Data Wavefront",$time);
    else
    begin
      $display("%d FAIL Test 'Input address is Null' Data Wavefront",$time);
      $display("%h %h %h %h %h",nhi_d6_o,match_found_d6_o,address_d6_o,apk_d6_o,crc_sr_d6_o);
    end

#5  ki            = 1;  
#10 all_null();
#10 if ((nhi_d6_o == 'd0) & (match_found_d6_o == 2'b00) & (address_d6_o[17:0] == 'h0) & (apk_d6_o == 64'h0) & (crc_sr_d6_o == 64'h0) & (daddr_vld_d6_o == 2'b00)) 
      $display("%d Pass Test 'Null Wavefront'",$time);
    else
    begin
      $display("%d FAIL Test  'Null Wavefront'",$time);
      $display("%h %h %h %h %h",nhi_d6_o,match_found_d6_o,address_d6_o,apk_d6_o,crc_sr_d6_o);
    end

// Non Null Next address
// Bf matches
// prefix matches
#20 msbk_i        = 2'b01;
    mk_i          = 6'b101010;
    crc_sr_i      = 64'b01010110_01100110_10100110_01010101_01101001_01011010_10011010_10100101;
    tlevel_inc_i  = 2'b10;
    address_i     = 16'h965A; // 93
    apk_i         = 64'b10101010_01100110_10100110_01010101_01101001_01011010_10011010_10100101;
    daddr_vld_i   = 2'b10;
    nhi_i         = 16'h555A;
    matchlength_i = 12'h555;

#10 all_null();
//////
#3  ki            = 0;    
#10 if ((nhi_d6_o == 16'hAA55) & (match_found_d6_o == 2'b10) & (address_d6_o[17:0] == 18'h256A5) & (apk_d6_o == 64'hA99A_9955_A56A_6A95) & ((USE_BLOOM_CRC & (crc_sr_d6_o == 64'hA96A66559A59AAA9)) | (~USE_BLOOM_CRC & (crc_sr_d6_o == 64'h5666_A655_695A_9AA5))) & (daddr_vld_d6_o == 2'b10))
      $display("%d Pass Test 'Non Null Next Address, BF Matches, Prefix matches' Data Wavefront",$time);
    else
    begin
      $display("%d FAIL Test 'Non Null Next Address, BF Matches, Prefix matches' Data Wavefront",$time);
      $display("%h %h %h %h %h",nhi_d6_o,match_found_d6_o,address_d6_o,apk_d6_o,crc_sr_d6_o);
    end

#5  ki            = 1;  
#10 if ((nhi_d6_o == 'd0) & (match_found_d6_o == 2'b00) & (address_d6_o[17:0] == 18'h0) & (apk_d6_o == 64'h0) & (crc_sr_d6_o == 64'h0) & (daddr_vld_d6_o == 2'b00))
      $display("%d Pass Test 'Null Wavefront'",$time);
    else
    begin
      $display("%d FAIL Test  'Null Wavefront'",$time);
      $display("%h %h %h %h %h",nhi_d6_o,match_found_d6_o,address_d6_o,apk_d6_o,crc_sr_d6_o);
    end


// Non Null Next address
// Bf matches
// prefix matches
// Match length is not 0, so lp2 has to be greater than the value of matchlength
#20 msbk_i        = 2'b01;
    mk_i          = 6'b101010;
    crc_sr_i      = 64'b01010110_01100110_10100110_01010101_01101001_01011010_10011010_10100101;
    tlevel_inc_i  = 2'b10;
    address_i     = 16'h965A; // 93
    apk_i         = 64'b10101010_01100110_10100110_01010101_01101001_01011010_10011010_10100101;
    daddr_vld_i   = 2'b10;
    nhi_i         = 16'h5565;
    matchlength_i = 12'h655;

#10 all_null();
//////
#3  ki            = 0;    
#10 if ((nhi_d6_o == 16'h5565) & (match_found_d6_o == 2'b01) & (address_d6_o[17:0] == 18'h256A5) & (apk_d6_o == 64'hA99A_9955_A56A_6A95) & ((USE_BLOOM_CRC & (crc_sr_d6_o == 64'hA96A66559A59AAA9)) | (~USE_BLOOM_CRC & (crc_sr_d6_o == 64'h5666_A655_695A_9AA5))) & (daddr_vld_d6_o == 2'b10))
      $display("%d Pass Test 'Non Null Next Address, BF Matches, Prefix matches previous ml greater' Data Wavefront",$time);
    else
    begin
      $display("%d FAIL Test 'Non Null Next Address, BF Matches, Prefix matches previous ml greater' Data Wavefront",$time);
      $display("%h %h %h %h %h",nhi_d6_o,match_found_d6_o,address_d6_o,apk_d6_o,crc_sr_d6_o);
    end

#5  ki            = 1;  
#10 if ((nhi_d6_o == 'd0) & (match_found_d6_o == 2'b00) & (address_d6_o[17:0] == 'h0) & (apk_d6_o == 64'h0) & (crc_sr_d6_o == 64'h0) & (daddr_vld_d6_o == 2'b00))
      $display("%d Pass Test 'Null Wavefront'",$time);
    else
    begin
      $display("%d FAIL Test  'Null Wavefront'",$time);
      $display("%h %h %h %h %h",nhi_d6_o,match_found_d6_o,address_d6_o,apk_d6_o,crc_sr_d6_o);
    end

// Both pointers Null address
// Bf matches
// prefix does not match
#20 msbk_i        = 2'b01;
    mk_i          = 6'b011010;
    crc_sr_i      = 64'b01010110_01100110_10100110_01010101_01101001_01011010_10011010_10100101;
    tlevel_inc_i  = 2'b10;
    address_i     = 16'h9659; //92
    apk_i         = 64'b01010110_01100110_10100110_01010101_01101001_01011010_10011010_10100101;
    daddr_vld_i   = 2'b10;
    nhi_i         = 16'h5566;
    matchlength_i = 12'h555;

#10 all_null();
//////
#3  ki            = 0;    
#10 if ((nhi_d6_o == 16'h5566) & (match_found_d6_o == 2'b01) & (address_d6_o[17:0] == 'h0) & (apk_d6_o == 64'h0) & (crc_sr_d6_o == 64'h0) & (daddr_vld_d6_o == 2'b01))
      $display("%d Pass Test 'Both next address Null, BF Matches, Prefix does not match' Data Wavefront",$time);
    else
    begin
      $display("%d FAIL Test 'Both next address Null, BF Matches, Prefix does not match' Data Wavefront",$time);
      $display("%h %h %h %h %h",nhi_d6_o,match_found_d6_o,address_d6_o,apk_d6_o,crc_sr_d6_o);
    end

#5  ki            = 1;  
#10 if ((nhi_d6_o == 'd0) & (match_found_d6_o == 2'b00) & (address_d6_o[17:0] == 'h0) & (apk_d6_o == 64'h0) & (crc_sr_d6_o == 64'h0) & (daddr_vld_d6_o == 2'b00))
      $display("%d Pass Test 'Null Wavefront'",$time);
    else
    begin
      $display("%d FAIL Test  'Null Wavefront'",$time);
      $display("%h %h %h %h %h",nhi_d6_o,match_found_d6_o,address_d6_o,apk_d6_o,crc_sr_d6_o);
    end


// One address is Null next address
// Bf matches
// prefix does not match
#20 msbk_i        = 2'b01;
    mk_i          = 6'b011010;
    crc_sr_i      = 64'b01010110_01100110_10100110_10101010_01101001_01011010_10011010_10100101;
    tlevel_inc_i  = 2'b10;
    address_i     = 16'h9665; //94
    apk_i         = 64'b10010110_01100110_10100110_01010101_01101001_01011010_10011010_10100101;
    daddr_vld_i   = 2'b10;
    nhi_i         = 16'h5569;
    matchlength_i = 12'h555;

#10 all_null();

// Not Null next address but epsilon link
// Bf matches
// prefix does not match
#20 msbk_i        = 2'b01;
    mk_i          = 6'b011010;
    crc_sr_i      = 64'b01010110_01100110_10100110_10101010_01101001_01011010_10011010_10100101;
    tlevel_inc_i  = 2'b10;
    address_i     = 16'h9666; //95
    apk_i         = 64'b0101011001100110101001100101010101101001010110101001101010100101;
    daddr_vld_i   = 2'b10;
    nhi_i         = 16'h556A;
    matchlength_i = 12'h555;

#10 all_null();
#3  ki            = 0;    

/////// Interesting //////////
// This address is null, so no comparison required. 
// Bf matches
// prefix matches
#20 msbk_i        = 2'b0;
    mk_i          = 6'b0;
    crc_sr_i      = 64'b0;
    tlevel_inc_i  = 2'b00;
    address_i     = 16'b0; // 0
    apk_i         = 64'b0;
    daddr_vld_i   = 2'b01;
    nhi_i         = 16'h5595;
    matchlength_i = 12'b0;

//////
#10 if ((nhi_d6_o == 16'h5569) & (match_found_d6_o == 2'b01) & (address_d6_o[17:0] == 'h0) & (apk_d6_o == 64'h0) & (crc_sr_d6_o == 64'h0) & (daddr_vld_d6_o == 2'b01))
      $display("%d Pass Test 'One address is Null, BF Matches, Prefix does not match' Data Wavefront",$time);
    else
    begin
      $display("%d FAIL Test 'One address is Null, BF Matches, Prefix does not match' Data Wavefront",$time);
      $display("%h %h %h %h %h",nhi_d6_o,match_found_d6_o,address_d6_o,apk_d6_o,crc_sr_d6_o);
    end

#5  ki            = 1;  
#10 if ((nhi_d6_o == 'd0) & (match_found_d6_o == 2'b00) & (address_d6_o[17:0] == 'h0) & (apk_d6_o == 64'h0) & (crc_sr_d6_o == 64'h0) & (daddr_vld_d6_o == 2'b00))
      $display("%d Pass Test 'Null Wavefront'",$time);
    else
    begin
      $display("%d FAIL Test  'Null Wavefront'",$time);
      $display("%h %h %h %h %h",nhi_d6_o,match_found_d6_o,address_d6_o,apk_d6_o,crc_sr_d6_o);
    end

//////
#3  ki            = 0;    
#10 if ((nhi_d6_o == 16'h556A) & (match_found_d6_o == 2'b01) & (address_d6_o[17:0] == 18'h256AA) & (apk_d6_o == 64'h5666_a655_695a_9aa5) & ((USE_BLOOM_CRC & (crc_sr_d6_o == 64'h5599A9AA9A56A6A9)) | (~USE_BLOOM_CRC & (crc_sr_d6_o == 64'h5666_A6AA_695A_9AA5))) & (daddr_vld_d6_o == 2'b10)) // because apk is not shifted in epsilon link
      $display("%d Pass Test 'Epsilon link out next address not Null, BF Matches, Prefix does not match' Data Wavefront",$time);
    else
    begin
      $display("%d FAIL Test 'Epsilon link out next address not Null, BF Matches, Prefix does not match' Data Wavefront",$time);
      $display("%h %h %h %h %h",nhi_d6_o,match_found_d6_o,address_d6_o,apk_d6_o,crc_sr_d6_o);
    end

#5  ki            = 1;  
#10 if ((nhi_d6_o == 'd0) & (match_found_d6_o == 2'b00) & (address_d6_o[17:0] == 'h0) & (apk_d6_o == 64'h0) & (crc_sr_d6_o == 64'h0) & (daddr_vld_d6_o == 2'b00))
      $display("%d Pass Test 'Null Wavefront'",$time);
    else
    begin
      $display("%d FAIL Test  'Null Wavefront'",$time);
      $display("%h %h %h %h %h",nhi_d6_o,match_found_d6_o,address_d6_o,apk_d6_o,crc_sr_d6_o);
    end

//////
#3  ki            = 0;    
#10 if ((nhi_d6_o == 16'h5595) & (match_found_d6_o == 2'b01) & (address_d6_o[17:0] == 'h0) & (apk_d6_o == 64'h0) & (crc_sr_d6_o == 64'h0) & (daddr_vld_d6_o == 2'b01))
      $display("%d Pass Test 'Input address is Null' Data Wavefront",$time);
    else
    begin
      $display("%d FAIL Test 'Input address is Null' Data Wavefront",$time);
      $display("%h %h %h %h %h",nhi_d6_o,match_found_d6_o,address_d6_o,apk_d6_o,crc_sr_d6_o);
    end

#5  ki            = 1;  
#10 all_null();
#10 if ((nhi_d6_o == 'd0) & (match_found_d6_o == 2'b00) & (address_d6_o[17:0] == 'h0) & (apk_d6_o == 64'h0) & (crc_sr_d6_o == 64'h0) & (daddr_vld_d6_o == 2'b00))
      $display("%d Pass Test 'Null Wavefront'",$time);
    else
    begin
      $display("%d FAIL Test  'Null Wavefront'",$time);
      $display("%h %h %h %h %h",nhi_d6_o,match_found_d6_o,address_d6_o,apk_d6_o,crc_sr_d6_o);
    end
#3  ki            = 0;    

// Non Null Next address
// Bf matches
// prefix does not matches
#30 msbk_i        = 2'b01;
    mk_i          = 6'b011010;
    crc_sr_i      = 64'b01010110_01100110_10100110_01010101_01101001_01011010_10011010_10100101;
    tlevel_inc_i  = 2'b10;
    address_i     = 16'h965A; // 93
    apk_i         = 64'b10101010_01100110_10100110_01010101_01101001_01011010_10011010_10100101;
    daddr_vld_i   = 2'b10;
    nhi_i         = 16'h5596;
    matchlength_i = 12'h555;

#10 if ((nhi_d6_o == 16'h5596) & (match_found_d6_o == 2'b01) & (address_d6_o[17:0] == 18'h256A5) & (apk_d6_o == 64'hA99A_9955_A56A_6A95) & ((USE_BLOOM_CRC & (crc_sr_d6_o == 64'hA96A66559A59AAA9)) | (~USE_BLOOM_CRC & (crc_sr_d6_o == 64'h5666_A655_695A_9AA5))) & (daddr_vld_d6_o == 2'b10))
      $display("%d Pass Test 'Non Null Next Address, BF Matches, Prefix does not match' Data Wavefront",$time);
    else
    begin
      $display("%d FAIL Test 'Non Null Next Address, BF Matches, Prefix does not match' Data Wavefront",$time);
      $display("%h %h %h %h %h",nhi_d6_o,match_found_d6_o,address_d6_o,apk_d6_o,crc_sr_d6_o);
    end

#5  ki            = 1;  
#10 all_null();
#10 if ((nhi_d6_o == 'd0) & (match_found_d6_o == 2'b00) & (address_d6_o[17:0] == 'h0) & (apk_d6_o == 64'h0) & (crc_sr_d6_o == 64'h0) & (daddr_vld_d6_o == 2'b00))
      $display("%d Pass Test 'Null Wavefront'",$time);
    else
    begin
      $display("%d FAIL Test  'Null Wavefront'",$time);
      $display("%h %h %h %h %h",nhi_d6_o,match_found_d6_o,address_d6_o,apk_d6_o,crc_sr_d6_o);
    end
#3  ki            = 0;    

/////// Interesting //////////
// This address is null, so no comparison required. 
// Bf matches
// prefix matches
#20 msbk_i        = 2'b0;
    mk_i          = 6'b0;
    crc_sr_i      = 64'b0;
    tlevel_inc_i  = 2'b00;
    address_i     = 16'b0; // 0
    apk_i         = 64'b0;
    daddr_vld_i   = 2'b01;
    nhi_i         = 16'h5599;
    matchlength_i = 12'b0;

#10 if ((nhi_d6_o == 16'h5599) & (match_found_d6_o == 2'b01) & (address_d6_o[17:0] == 'h0) & (apk_d6_o == 64'h0) & (crc_sr_d6_o == 64'h0) & (daddr_vld_d6_o == 2'b01))
      $display("%d Pass Test 'Input address is Null' Data Wavefront",$time);
    else
    begin
      $display("%d FAIL Test 'Input address is Null' Data Wavefront",$time);
      $display("%h %h %h %h %h",nhi_d6_o,match_found_d6_o,address_d6_o,apk_d6_o,crc_sr_d6_o);
    end

#5  ki            = 1;  
#10 all_null();
#10 if ((nhi_d6_o == 'd0) & (match_found_d6_o == 2'b00) & (address_d6_o[17:0] == 'h0) & (apk_d6_o == 64'h0) & (crc_sr_d6_o == 64'h0) & (daddr_vld_d6_o == 2'b00))
      $display("%d Pass Test 'Null Wavefront'",$time);
    else
    begin
      $display("%d FAIL Test  'Null Wavefront'",$time);
      $display("%h %h %h %h %h",nhi_d6_o,match_found_d6_o,address_d6_o,apk_d6_o,crc_sr_d6_o);
    end
#3  ki            = 0;    


/////// Interesting //////////
// Next address is null, but BF matches and prefix matches. 
// Bf matches
// prefix matches
#20 msbk_i        = 2'b01;
    mk_i          = 6'b101010;
    crc_sr_i      = 64'b01010110_01100110_10100110_01010101_01101001_01011010_10011010_10100101;
    tlevel_inc_i  = 2'b10;
    address_i     = 16'h9669; // 96
    apk_i         = 64'b10101010_01100110_10100110_01010101_01101001_01011010_10011010_10100101;
    daddr_vld_i   = 2'b10;
    nhi_i         = 16'h9999;
    matchlength_i = 12'h555;

#10 if ((nhi_d6_o == 16'hAA55) & (match_found_d6_o == 2'b10) & (address_d6_o[17:0] == 18'h0) & (apk_d6_o == 64'h0) & (crc_sr_d6_o == 64'h0) & (daddr_vld_d6_o == 2'b01))
      $display("%d Pass Test 'Next address is Null, BF matches, prefix matches' Data Wavefront",$time);
    else
    begin
      $display("%d FAIL Test 'Next address is Null, BF matches, prefix matches' Data Wavefront",$time);
      $display("%h %h %h %h %h",nhi_d6_o,match_found_d6_o,address_d6_o,apk_d6_o,crc_sr_d6_o);
    end

#5  ki            = 1;  
#10 all_null();
#10 if ((nhi_d6_o == 'd0) & (match_found_d6_o == 2'b00) & (address_d6_o[17:0] == 18'h0) & (apk_d6_o == 64'h0) & (crc_sr_d6_o == 64'h0) & (daddr_vld_d6_o == 2'b00))
      $display("%d Pass Test 'Null Wavefront'",$time);
    else
    begin
      $display("%d FAIL Test  'Null Wavefront'",$time);
      $display("%h %h %h %h %h",nhi_d6_o,match_found_d6_o,address_d6_o,apk_d6_o,crc_sr_d6_o);
    end
  end


  initial
  begin
    $dumpfile("nv_match_stage_dump.fst");
    $dumpvars;
  end

nv_match_stage #(
  .TDEPTH           (TDEPTH             ),
  .PTRIE            (PTRIE              ),
  .IP_ADDRW         (IP_ADDRW           ),
  .NHI_WIDTH        (NHI_WIDTH          ),
  .USE_BLOOM_RAM    (USE_BLOOM_RAM      ),
  .USE_BLOOM_CRC    (USE_BLOOM_CRC      ),
  .MEM_SIZE         (MEM_SIZE           ),
  .MEM_AWIDTH       (MEM_AWIDTH         ),
  .CP_WIDTH         (CP_WIDTH           ),
  .PFX_INIT_FILE    (PFX_INIT_FILE      ),
  .LRC_INIT_FILE    (LRC_INIT_FILE      ),
  .BF_SIZE          (BF_SIZE            ),
  .BF_AWIDTH        (BF_AWIDTH          ),
  .BF_INIT_FILE     (BF_INIT_FILE       )
)u_dut(
  .rst_n            (rst_n              ),
  .ki               (ki                 ),
  .ko               (ko                 ),
  // Key Information that is passed from one match stage to the next without
  // any change
  .msbk_i           (msbk_i             ),
  .mk_i             (mk_i               ),
  // Inputs from the previous match stage's CRC computation block
  .crc_sr_i         (crc_sr_i           ),
  .tlevel_inc_i     (tlevel_inc_i       ),
  // Inputs from the previous match stage's child computation block
  .address_i        (address_i          ),
  .apk_i            (apk_i              ),
  .daddr_vld_i      (daddr_vld_i        ),
  // Inputs from the previous match stage's prefix match block
  .nhi_i            (nhi_i              ),
  .matchlength_i    (matchlength_i      ),
  // Outputs from the CRC computation block appropriately delayed
  .crc_sr_d6_o      (crc_sr_d6_o        ),
  // Outputs from the next child computation block appropriately delayed
  .tlevel_inc_d6_o  (tlevel_inc_d6_o    ),
  .address_d6_o     (address_d6_o       ),
  .apk_d6_o         (apk_d6_o           ),
  .daddr_vld_d6_o   (daddr_vld_d6_o     ),
  // Outputs from the prefix match block appropriately delayed
  .nhi_d6_o         (nhi_d6_o           ),
  .matchlength_d6_o (matchlength_d6_o   ),
  .match_found_d6_o (match_found_d6_o   )
);



task all_null;
  begin
  msbk_i        = 2'b0;
  mk_i          = 6'b0;
  crc_sr_i      = 64'b0;
  tlevel_inc_i  = 2'b0;
  address_i     = 20'b0;
  apk_i         = 64'b0;
  daddr_vld_i   = 2'b0;
  nhi_i         = 16'b0;
  matchlength_i = 12'b0;
  end
endtask

function integer clogb2;
  input [31:0] value;
  begin
    if(value > 1)
    begin
      value = value - 1;
      for (clogb2 = 0; value > 0; clogb2 = clogb2 + 1)
      begin
        value = value >> 1;
      end
    end
    else
      clogb2 = 1;
  end
endfunction

endmodule
