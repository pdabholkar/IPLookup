`timescale 1ns / 10ps
module next_child #(
  parameter IP_ADDRW = 32,
  parameter CP_WIDTH = 19
)(
  input                     clk_i,
  input                     rst_n_i,
  // From the lrc_info RAM
  input [CP_WIDTH-1:0]      lc_i,
  input [CP_WIDTH-1:0]      rc_i,
  input                     epsilon_i,

  // Key Information
  input [IP_ADDRW-1:0]      apk_i,
  input                     daddr_vld_i,

  // To next pipeline stage
  output reg                tlevel_inc_d1_o,
  output reg [CP_WIDTH-1:0] address_d1_o,
  output reg [IP_ADDRW-1:0] apk_d1_o,  
  output reg                daddr_vld_d1_o
);

wire B0k;

// address_o apk_o and daddr_vld_o are all in phase with RAM output.


assign B0k = apk_i[31];

always @(posedge clk_i or negedge rst_n_i)
begin
  if (~rst_n_i)
  begin
    tlevel_inc_d1_o <= /*#0.1*/ 1'd0;
    address_d1_o    <= /*#0.1*/ {CP_WIDTH{1'd0}};
    apk_d1_o        <= /*#0.1*/ {IP_ADDRW{1'd0}};
    daddr_vld_d1_o  <= /*#0.1*/ 1'd0;
  end
  else if (epsilon_i)                                         // Epsilon node Take the left child
  begin
    tlevel_inc_d1_o <= /*#0.1*/ 1'd0;
    address_d1_o    <= /*#0.1*/ lc_i;
    apk_d1_o        <= /*#0.1*/ apk_i;
    daddr_vld_d1_o  <= /*#0.1*/ daddr_vld_i;
  end
  else if (~epsilon_i & ~B0k & (lc_i != {CP_WIDTH{1'd1}}))    // Regular node take the left child
  begin                                                       // you would hit a non-epsilon node with all ones in the address when the next child is NULL
    tlevel_inc_d1_o <= /*#0.1*/ 1'd1;
    address_d1_o    <= /*#0.1*/ lc_i;
    apk_d1_o        <= /*#0.1*/ {apk_i[30:0],1'b0}; 
    daddr_vld_d1_o  <= /*#0.1*/ daddr_vld_i;
  end
  else if (~epsilon_i & B0k & (rc_i != {CP_WIDTH{1'd1}}))     // Regular node take the right child
  begin                                                       // you would hit a non-epsilon node with all ones in the address when the next child is NULL  
    tlevel_inc_d1_o <= /*#0.1*/ 1'd1;
    address_d1_o    <= /*#0.1*/ rc_i;
    apk_d1_o        <= /*#0.1*/ {apk_i[30:0],1'b0}; 
    daddr_vld_d1_o  <= /*#0.1*/ daddr_vld_i;
  end
  else                                                       // No child to be taken all done
  begin                                                      // There are no children to this node in the direction pointed by the B0k bit.
    tlevel_inc_d1_o <= /*#0.1*/ 1'd0;
    address_d1_o    <= /*#0.1*/ 'd0;
    apk_d1_o        <= /*#0.1*/ 'd0;
    daddr_vld_d1_o  <= /*#0.1*/ 'd0;                             // Address to the next level is set as zero and the address valid is set to zero.
  end
end

endmodule
