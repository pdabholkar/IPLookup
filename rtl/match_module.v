`timescale 1ns / 1ps
module match_module #(
  parameter TDEPTH = 1,
  parameter PTRIE = 2,
  parameter NHI_WIDTH = 8
)(
  input                     clk_i,
  input                     rst_n_i,

  input                     epsilon_i,

  // From the previous stage
  input                     msbk_i,
  input [2:0]               mk_i,

  input [31:0]              apk_a1_i,
  input [NHI_WIDTH-1:0]     nhi_i,
  input [5:0]               matchlength_i,
  input                     daddr_vld_a1_i,
  input                     pfx_notnull_a1_i,
  
  // First prefix in the node
  input [2:0]               mp1_i,
  input                     msb1_i,
  input                     lsb1_i,
  input [4:0]               lp1_i,
  input [NHI_WIDTH-1:0]     nhi1_i,
  
  // Second prefix in the node
  input [2:0]               mp2_i,
  input                     msb2_i,
  input                     lsb2_i,
  input [4:0]               lp2_i,
  input [NHI_WIDTH-1:0]     nhi2_i,

  // Third prefix in the node
  input [2:0]               mp3_i,
  input                     msb3_i,
  input                     lsb3_i,
  input [4:0]               lp3_i,
  input [NHI_WIDTH-1:0]     nhi3_i,

  // Output to the next stage
  output                    pfx_notnull_d2_o,
  output reg [NHI_WIDTH-1:0]nhi_d2_o,
  output reg [5:0]          matchlength_d2_o,

  output reg                match_found_d2_o
);

reg  [5:0]            Nzok;
wire                  B0k_a1;
reg                   match_condition1_d1;
reg                   match_condition2_d1;
reg                   match_condition3_d1;

reg [5:0]             matchlength_i_d1;
reg [NHI_WIDTH-1:0]   nhi_i_d1;
reg                   daddr_vld;
reg                   daddr_vld_d1;

reg [5:0]             matchlength_stg_d2;

reg [4:0]             lp1_d1;
reg [2:0]             mp1_d1;
reg [NHI_WIDTH-1:0]   nhi1_d1;
reg [4:0]             lp2_d1;
reg [2:0]             mp2_d1;
reg [NHI_WIDTH-1:0]   nhi2_d1;
reg [4:0]             lp3_d1;
reg [2:0]             mp3_d1;
reg [NHI_WIDTH-1:0]   nhi3_d1;
reg                   epsilon_d1;
reg                   epsilon_d2;
reg                   B0k;

reg                   pfx_notnull;
reg                   pfx_notnull_d1;
reg                   pfx_notnull_d2;

// Pipeline
always @(posedge clk_i or negedge rst_n_i)
begin
  if(~rst_n_i)
  begin
    daddr_vld     <=  #0.001 1'd0;
    daddr_vld_d1  <=  #0.001 1'd0;
  end
  else
  begin
    daddr_vld     <=  #0.001 daddr_vld_a1_i;
    daddr_vld_d1  <=  #0.001 daddr_vld;
  end
end

always @(posedge clk_i or negedge rst_n_i)
begin        
  if(~rst_n_i)
  begin
    pfx_notnull    <= #0.001 1'd0;
    pfx_notnull_d1 <= #0.001 1'd0;
    pfx_notnull_d2 <= #0.001 1'd0;
  end
  else
  begin        
    pfx_notnull    <= #0.001 pfx_notnull_a1_i;
    pfx_notnull_d1 <= #0.001 pfx_notnull;
    pfx_notnull_d2 <= #0.001 pfx_notnull_d1;
  end
end  

assign pfx_notnull_d2_o = pfx_notnull_d2;

// Extract most significant bit B0 from the apk
assign B0k_a1 = apk_a1_i[31];

// "Number of B0k valued bits following B0k" computation
always @(posedge clk_i or negedge rst_n_i)
begin
  if (~rst_n_i)
    Nzok <=  #0.001 6'd0;
  else if (daddr_vld_a1_i)
    if ((B0k_a1 ^ apk_a1_i[30]) == 1)
      Nzok <=  #0.001 1;
    else if ((B0k_a1 ^ apk_a1_i[29]) == 1)
      Nzok <=  #0.001 2;
    else if ((B0k_a1 ^ apk_a1_i[28]) == 1)
      Nzok <=  #0.001 3;
    else if ((B0k_a1 ^ apk_a1_i[27]) == 1)
      Nzok <=  #0.001 4;
    else if ((B0k_a1 ^ apk_a1_i[26]) == 1)
      Nzok <=  #0.001 5;
    else if ((B0k_a1 ^ apk_a1_i[25]) == 1)
      Nzok <=  #0.001 6;
    else if ((B0k_a1 ^ apk_a1_i[24]) == 1)
      Nzok <=  #0.001 7;
    else if ((B0k_a1 ^ apk_a1_i[23]) == 1)
      Nzok <=  #0.001 8;
    else if ((B0k_a1 ^ apk_a1_i[22]) == 1)
      Nzok <=  #0.001 9;
    else if ((B0k_a1 ^ apk_a1_i[21]) == 1)
      Nzok <=  #0.001 10;
    else if ((B0k_a1 ^ apk_a1_i[20]) == 1)
      Nzok <=  #0.001 11;
    else if ((B0k_a1 ^ apk_a1_i[19]) == 1)
      Nzok <=  #0.001 12;              
    else if ((B0k_a1 ^ apk_a1_i[18]) == 1)
      Nzok <=  #0.001 13;              
    else if ((B0k_a1 ^ apk_a1_i[17]) == 1)
      Nzok <=  #0.001 14;              
    else if ((B0k_a1 ^ apk_a1_i[16]) == 1)
      Nzok <=  #0.001 15;              
    else if ((B0k_a1 ^ apk_a1_i[15]) == 1)
      Nzok <=  #0.001 16;              
    else if ((B0k_a1 ^ apk_a1_i[14]) == 1)
      Nzok <=  #0.001 17;              
    else if ((B0k_a1 ^ apk_a1_i[13]) == 1)
      Nzok <=  #0.001 18;              
    else if ((B0k_a1 ^ apk_a1_i[12]) == 1)
      Nzok <=  #0.001 19;              
    else if ((B0k_a1 ^ apk_a1_i[11]) == 1)
      Nzok <=  #0.001 20;              
    else if ((B0k_a1 ^ apk_a1_i[10]) == 1)
      Nzok <=  #0.001 21;
    else if ((B0k_a1 ^ apk_a1_i[9]) == 1)
      Nzok <=  #0.001 22;
    else if ((B0k_a1 ^ apk_a1_i[8]) == 1)
      Nzok <=  #0.001 23;
    else if ((B0k_a1 ^ apk_a1_i[7]) == 1)
      Nzok <=  #0.001 24;
    else if ((B0k_a1 ^ apk_a1_i[6]) == 1)
      Nzok <=  #0.001 25;
    else if ((B0k_a1 ^ apk_a1_i[5]) == 1)
      Nzok <=  #0.001 26;
    else if ((B0k_a1 ^ apk_a1_i[4]) == 1)
      Nzok <=  #0.001 27;
    else if ((B0k_a1 ^ apk_a1_i[3]) == 1)
      Nzok <=  #0.001 28;
    else if ((B0k_a1 ^ apk_a1_i[2]) == 1)
      Nzok <=  #0.001 29;
    else if ((B0k_a1 ^ apk_a1_i[1]) == 1)
      Nzok <=  #0.001 30;
    else if ((B0k_a1 ^ apk_a1_i[0]) == 1)
      Nzok <=  #0.001 31;
    else
      Nzok <=  #0.001 32;
  else
    Nzok <=  #0.001 0;
end

// Pipeline
always @(posedge clk_i or negedge rst_n_i)
begin
  if (~rst_n_i)
    B0k <=  #0.001 1'd0;
  else
    B0k <=  #0.001 B0k_a1;
end

// Check match with first prefix
always @(posedge clk_i or negedge rst_n_i)
begin
  if (~rst_n_i)
    match_condition1_d1 <=  #0.001 1'd0;
  else if (mp1_i == 0)
    match_condition1_d1 <=  #0.001 1'd0;
  else if (daddr_vld)
    if ((TDEPTH == 0) && (lp1_i == 0))
      match_condition1_d1 <=  #0.001 ((msbk_i == msb1_i) & (mk_i >= mp1_i));
    else
      match_condition1_d1 <=  #0.001 ((msbk_i == msb1_i) & (mk_i == mp1_i) & (B0k == lsb1_i) & (Nzok >= lp1_i) & (lp1_i > matchlength_i));
  else
    match_condition1_d1 <=  #0.001 1'd0;
end

// Check match with second prefix
always @(posedge clk_i or negedge rst_n_i)
begin
  if (~rst_n_i)
    match_condition2_d1 <=  #0.001 1'd0;
  if (PTRIE>=2)
    if (mp2_i == 0)
      match_condition2_d1 <=  #0.001 0;
    else if (daddr_vld)
      if ((TDEPTH == 0) && (lp2_i == 0))
        match_condition2_d1 <=  #0.001 ((msbk_i == msb2_i) & (mk_i >= mp2_i));
      else
        match_condition2_d1 <=  #0.001 ((msbk_i == msb2_i) & (mk_i == mp2_i) & (B0k == lsb2_i) & (Nzok >= lp2_i) & (lp2_i > matchlength_i));
    else
      match_condition2_d1 <=  #0.001 1'd0;
  else
    match_condition2_d1 <= #0.001 1'd0;
end

// Check match with third prefix. This was added for the IAD table
always @(posedge clk_i or negedge rst_n_i)
begin
  if (~rst_n_i)
    match_condition3_d1 <=  #0.001 1'd0;
  if (PTRIE>=3)
    if (mp3_i == 0)
      match_condition3_d1 <=  #0.001 0;
    else if (daddr_vld)
      if ((TDEPTH == 0) && (lp3_i == 0))
        match_condition3_d1 <=  #0.001 ((msbk_i == msb3_i) & (mk_i >= mp3_i));
      else
        match_condition3_d1 <=  #0.001 ((msbk_i == msb3_i) & (mk_i == mp3_i) & (B0k == lsb3_i) & (Nzok >= lp3_i) & (lp3_i > matchlength_i));
    else
      match_condition3_d1 <=  #0.001 1'd0;
  else
    match_condition3_d1 = #0.001 1'd0;
end

// Pipeline
always @(posedge clk_i or negedge rst_n_i)
begin
  if (~rst_n_i)
  begin
    lp1_d1      <=  #0.001 'd0;
    lp2_d1      <=  #0.001 'd0;
    lp3_d1      <=  #0.001 'd0;
    mp1_d1      <=  #0.001 'd0;
    mp2_d1      <=  #0.001 'd0;
    mp3_d1      <=  #0.001 'd0;
    nhi1_d1     <=  #0.001 'd0;
    nhi2_d1     <=  #0.001 'd0;
    nhi3_d1     <=  #0.001 'd0;
  end
  else
  begin
    lp1_d1      <=  #0.001 lp1_i;
    lp2_d1      <=  #0.001 lp2_i;
    lp3_d1      <=  #0.001 lp3_i;
    mp1_d1      <=  #0.001 mp1_i;
    mp2_d1      <=  #0.001 mp2_i;
    mp3_d1      <=  #0.001 mp3_i;
    nhi1_d1     <=  #0.001 nhi1_i;
    nhi2_d1     <=  #0.001 nhi2_i;
    nhi3_d1     <=  #0.001 nhi3_i;
  end
end

// Pipeline
always @(posedge clk_i or negedge rst_n_i)
begin
  if (~rst_n_i)
  begin
    nhi_i_d1 <=  #0.001 {NHI_WIDTH{1'd0}};
    matchlength_i_d1 <=  #0.001 6'd0;
  end
  else
  begin
    nhi_i_d1 <= nhi_i;  
    matchlength_i_d1 <=  #0.001 matchlength_i;
  end
end

// Pipeline
always @(posedge clk_i or negedge rst_n_i)
begin
  if (~rst_n_i)
  begin
    epsilon_d1  <=  #0.001 'd0;
    epsilon_d2  <=  #0.001 'd0;
  end
  else
  begin
    epsilon_d1  <=  #0.001 epsilon_i;
    epsilon_d2  <=  #0.001 epsilon_d1;
  end
end

// Match condition check
always @(posedge clk_i or negedge rst_n_i)
begin
  if (~rst_n_i)
  begin
    nhi_d2_o           <=  #0.001 'd0;
    matchlength_stg_d2 <=  #0.001 'd0;
  end
  else if (daddr_vld_d1)
  begin
    case({match_condition3_d1,match_condition2_d1,match_condition1_d1})
      3'b111: begin
                if (lp2_d1 > lp1_d1)
                begin 
                  if (lp3_d1 > lp2_d1)
                  begin
                    nhi_d2_o           <=  #0.001 nhi3_d1; 
                    matchlength_stg_d2 <=  #0.001 mp3_d1 + lp3_d1;
                  end
                  else
                  begin
                    nhi_d2_o           <=  #0.001 nhi2_d1; 
                    matchlength_stg_d2 <=  #0.001 mp2_d1 + lp2_d1;
                  end
                end
                else
                begin 
                  if (lp3_d1 > lp1_d1)
                  begin
                    nhi_d2_o           <=  #0.001 nhi3_d1; 
                    matchlength_stg_d2 <=  #0.001 mp3_d1 + lp3_d1;
                  end
                  else
                  begin
                    nhi_d2_o           <=  #0.001 nhi1_d1; 
                    matchlength_stg_d2 <=  #0.001 mp1_d1 + lp1_d1;
                  end
                end
              end
      3'b110: begin 
                if (lp3_d1 > lp2_d1)
                begin
                  nhi_d2_o             <=  #0.001 nhi3_d1;
                  matchlength_stg_d2   <=  #0.001 mp3_d1 + lp3_d1; 
                end
                else
                begin
                  nhi_d2_o             <=  #0.001 nhi2_d1;
                  matchlength_stg_d2   <=  #0.001 mp2_d1 + lp2_d1; 
                end
              end
      3'b101: begin
                if (lp3_d1 > lp1_d1)
                begin
                  nhi_d2_o             <=  #0.001 nhi3_d1;
                  matchlength_stg_d2   <=  #0.001 mp3_d1 + lp3_d1;
                end
                else
                begin
                  nhi_d2_o             <=  #0.001 nhi1_d1;
                  matchlength_stg_d2   <=  #0.001 mp1_d1 + lp1_d1;
                end
              end
      3'b011: begin
                if (lp2_d1 > lp1_d1)
                begin 
                  nhi_d2_o           <=  #0.001 nhi2_d1; 
                  matchlength_stg_d2 <=  #0.001 mp2_d1 + lp2_d1;
                end
                else
                begin 
                  nhi_d2_o           <=  #0.001 nhi1_d1; 
                  matchlength_stg_d2 <=  #0.001 mp1_d1 + lp1_d1;
                end
              end
      3'b100: begin 
                nhi_d2_o             <=  #0.001 nhi3_d1; 
                matchlength_stg_d2   <=  #0.001 mp3_d1 + lp3_d1;
              end
      3'b010: begin 
                nhi_d2_o             <=  #0.001 nhi2_d1;
                matchlength_stg_d2   <=  #0.001 mp2_d1 + lp2_d1; 
              end
      3'b001: begin
                nhi_d2_o             <=  #0.001 nhi1_d1;
                matchlength_stg_d2   <=  #0.001 mp1_d1 + lp1_d1;
              end
      3'b000: begin 
                nhi_d2_o             <=  #0.001 nhi_i_d1; 
                matchlength_stg_d2   <=  #0.001 matchlength_i_d1; 
              end
    endcase
  end
  else
  begin
    nhi_d2_o            <= #0.001 nhi_d2_o;
    matchlength_stg_d2  <= #0.001 matchlength_stg_d2;
  end
end

// Matchlength computation
always @(*)
begin
  if (epsilon_d2)
    matchlength_d2_o = matchlength_stg_d2;
  else              
    matchlength_d2_o = 0;
end

// Match found flag
always @(posedge clk_i or negedge rst_n_i)
begin
  if (~rst_n_i)
    match_found_d2_o <=  #0.001 1'd0;
  else if (match_condition1_d1 | match_condition2_d1 | match_condition3_d1)
    match_found_d2_o <=  #0.001 1'd1;
  else
    match_found_d2_o <=  #0.001 1'd0;
end

endmodule
