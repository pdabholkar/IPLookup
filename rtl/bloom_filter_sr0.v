`timescale 1ns / 10ps
module bloom_filter_sr0 (
  input                       msbk_i,
  input      [2:0]            mk_i,
  input                       daddr_vld_i,
  output     [31:0]           crc_sr_o
);

wire DIN0;
wire DIN1;
wire DIN2;
wire DIN3;

// endianness similar to the python code
wire [0:31] crc_sr_out_le;

// Design 

///////////////////////////////////////////////////////
// Little Endian operation similar to the python code
///////////////////////////////////////////////////////

assign DIN0 = msbk_i;
assign DIN1 = mk_i[2];
assign DIN2 = mk_i[1];
assign DIN3 = mk_i[0];

assign crc_sr_out_le[31] = daddr_vld_i ? (1'b0 ^ 1'b0 ^ 1'b0 ^ 1'b0) : 1'b0;
assign crc_sr_out_le[30] = daddr_vld_i ? (1'b0 ^ 1'b0 ^ 1'b0 ^ 1'b0) : 1'b0;
assign crc_sr_out_le[29] = daddr_vld_i ? (1'b0 ^ 1'b0 ^ 1'b0 ^ DIN0) : 1'b0;
assign crc_sr_out_le[28] = daddr_vld_i ? (1'b0 ^ 1'b0 ^ DIN1 ^ 1'b0) : 1'b0;
assign crc_sr_out_le[27] = daddr_vld_i ? (1'b0 ^ DIN2 ^ 1'b0 ^ 1'b0) : 1'b0; 
assign crc_sr_out_le[26] = daddr_vld_i ? (DIN3 ^ 1'b0 ^ 1'b0 ^ DIN0) : 1'b0;
assign crc_sr_out_le[25] = daddr_vld_i ? (1'b0 ^ 1'b0 ^ DIN1 ^ DIN0) : 1'b0;
assign crc_sr_out_le[24] = daddr_vld_i ? (1'b0 ^ DIN2 ^ DIN1 ^ 1'b0) : 1'b0;
assign crc_sr_out_le[23] = daddr_vld_i ? (DIN3 ^ DIN2 ^ 1'b0 ^ 1'b0) : 1'b0;
assign crc_sr_out_le[22] = daddr_vld_i ? (DIN3 ^ 1'b0 ^ 1'b0 ^ 1'b0) : 1'b0;
assign crc_sr_out_le[21] = daddr_vld_i ? (1'b0 ^ 1'b0 ^ 1'b0 ^ 1'b0) : 1'b0;
assign crc_sr_out_le[20] = daddr_vld_i ? (1'b0 ^ 1'b0 ^ 1'b0 ^ 1'b0) : 1'b0;
assign crc_sr_out_le[19] = daddr_vld_i ? (1'b0 ^ 1'b0 ^ 1'b0 ^ DIN0) : 1'b0;
assign crc_sr_out_le[18] = daddr_vld_i ? (1'b0 ^ 1'b0 ^ DIN1 ^ 1'b0) : 1'b0;
assign crc_sr_out_le[17] = daddr_vld_i ? (1'b0 ^ DIN2 ^ 1'b0 ^ 1'b0) : 1'b0;
assign crc_sr_out_le[16] = daddr_vld_i ? (DIN3 ^ 1'b0 ^ 1'b0 ^ 1'b0) : 1'b0;
assign crc_sr_out_le[15] = daddr_vld_i ? (1'b0 ^ 1'b0 ^ 1'b0 ^ DIN0) : 1'b0;
assign crc_sr_out_le[14] = daddr_vld_i ? (1'b0 ^ 1'b0 ^ DIN1 ^ DIN0) : 1'b0;
assign crc_sr_out_le[13] = daddr_vld_i ? (1'b0 ^ DIN2 ^ DIN1 ^ DIN0) : 1'b0;
assign crc_sr_out_le[12] = daddr_vld_i ? (DIN3 ^ DIN2 ^ DIN1 ^ 1'b0) : 1'b0;
assign crc_sr_out_le[11] = daddr_vld_i ? (DIN3 ^ DIN2 ^ 1'b0 ^ DIN0) : 1'b0;
assign crc_sr_out_le[10] = daddr_vld_i ? (DIN3 ^ 1'b0 ^ DIN1 ^ DIN0) : 1'b0;
assign crc_sr_out_le[9]  = daddr_vld_i ? (1'b0 ^ DIN2 ^ DIN1 ^ 1'b0) : 1'b0;
assign crc_sr_out_le[8]  = daddr_vld_i ? (DIN3 ^ DIN2 ^ 1'b0 ^ DIN0) : 1'b0;
assign crc_sr_out_le[7]  = daddr_vld_i ? (DIN3 ^ 1'b0 ^ DIN1 ^ DIN0) : 1'b0;
assign crc_sr_out_le[6]  = daddr_vld_i ? (1'b0 ^ DIN2 ^ DIN1 ^ 1'b0) : 1'b0;
assign crc_sr_out_le[5]  = daddr_vld_i ? (DIN3 ^ DIN2 ^ 1'b0 ^ DIN0) : 1'b0;
assign crc_sr_out_le[4]  = daddr_vld_i ? (DIN3 ^ 1'b0 ^ DIN1 ^ DIN0) : 1'b0;
assign crc_sr_out_le[3]  = daddr_vld_i ? (1'b0 ^ DIN2 ^ DIN1 ^ DIN0) : 1'b0;
assign crc_sr_out_le[2]  = daddr_vld_i ? (DIN3 ^ DIN2 ^ DIN1 ^ 1'b0) : 1'b0;
assign crc_sr_out_le[1]  = daddr_vld_i ? (DIN3 ^ DIN2 ^ 1'b0 ^ 1'b0) : 1'b0;
assign crc_sr_out_le[0]  = daddr_vld_i ? (DIN3 ^ 1'b0 ^ 1'b0 ^ 1'b0) : 1'b0;  


// Changing the Endianness

assign crc_sr_o[31:0] = crc_sr_out_le[0:31];

endmodule
