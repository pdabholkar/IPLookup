`timescale 1ns/10ps
module key_stripper (
  input [31:0]  key_i,
  output        msbk_o,
  output [31:0] apk_o,
  output [5:0]  mk_o
);

wire msbk;
reg [31:0] apk;
reg [5:0] mk;

assign  msbk = key_i[31];

always @(*)
begin
  if ((msbk ^ key_i[30]) == 1)
  begin
    mk = 1;
    apk[31:0] = {key_i[30:0],1'b0};
  end
  else if ((msbk ^ key_i[29]) == 1)
  begin
    mk = 2;
    apk[31:0] = {key_i[29:0],2'b0};
  end
  else if ((msbk ^ key_i[28]) == 1)
  begin
    mk = 3;
    apk[31:0] = {key_i[28:0],3'b0};
  end
  else if ((msbk ^ key_i[27]) == 1)
  begin
    mk = 4;
    apk[31:0] = {key_i[27:0],4'b0};
  end
  else if ((msbk ^ key_i[26]) == 1)
  begin
    mk = 5;
    apk[31:0] = {key_i[26:0],5'b0};
  end
  else if ((msbk ^ key_i[25]) == 1)
  begin
    mk = 6;
    apk[31:0] = {key_i[25:0],6'b0};
  end
  else if ((msbk ^ key_i[24]) == 1)
  begin
    mk = 7;
    apk[31:0] = {key_i[24:0],7'b0};
  end
  else if ((msbk ^ key_i[23]) == 1)
  begin
    mk = 8;
    apk[31:0] = {key_i[23:0],8'b0};
  end
  else if ((msbk ^ key_i[22]) == 1)
  begin
    mk = 9;
    apk[31:0] = {key_i[22:0],9'b0};
  end
  else if ((msbk ^ key_i[21]) == 1)
  begin
    mk = 10;
    apk[31:0] = {key_i[21:0],10'b0};
  end
  else if ((msbk ^ key_i[20]) == 1)
  begin
    mk = 11;
    apk[31:0] = {key_i[20:0],11'b0};
  end
  else if ((msbk ^ key_i[19]) == 1)
  begin
    mk = 12;              
    apk[31:0] = {key_i[19:0],12'b0};
  end
  else if ((msbk ^ key_i[18]) == 1)
  begin
    mk = 13;              
    apk[31:0] = {key_i[18:0],13'b0};
  end
  else if ((msbk ^ key_i[17]) == 1)
  begin
    mk = 14;              
    apk[31:0] = {key_i[17:0],14'b0};
  end
  else if ((msbk ^ key_i[16]) == 1)
  begin
    mk = 15;              
    apk[31:0] = {key_i[16:0],15'b0};
  end
  else if ((msbk ^ key_i[15]) == 1)
  begin
    mk = 16;              
    apk[31:0] = {key_i[15:0],16'b0};
  end
  else if ((msbk ^ key_i[14]) == 1)
  begin
    mk = 17;              
    apk[31:0] = {key_i[14:0],17'b0};
  end
  else if ((msbk ^ key_i[13]) == 1)
  begin
    mk = 18;              
    apk[31:0] = {key_i[13:0],18'b0};
  end
  else if ((msbk ^ key_i[12]) == 1)
  begin
    mk = 19;              
    apk[31:0] = {key_i[12:0],19'b0};
  end
  else if ((msbk ^ key_i[11]) == 1)
  begin
    mk = 20;              
    apk[31:0] = {key_i[11:0],20'b0};
  end
  else if ((msbk ^ key_i[10]) == 1)
  begin
    mk = 21;
    apk[31:0] = {key_i[10:0],21'b0};
  end
  else if ((msbk ^ key_i[9]) == 1)
  begin
    mk = 22;
    apk[31:0] = {key_i[9:0],22'b0};
  end
  else if ((msbk ^ key_i[8]) == 1)
  begin
    mk = 23;
    apk[31:0] = {key_i[8:0],23'b0};
  end
  else if ((msbk ^ key_i[7]) == 1)
  begin
    mk = 24;
    apk[31:0] = {key_i[7:0],24'b0};
  end
  else if ((msbk ^ key_i[6]) == 1)
  begin
    mk = 25;
    apk[31:0] = {key_i[6:0],25'b0};
  end
  else if ((msbk ^ key_i[5]) == 1)
  begin
    mk = 26;
    apk[31:0] = {key_i[5:0],26'b0};
  end
  else if ((msbk ^ key_i[4]) == 1)
  begin
    mk = 27;
    apk[31:0] = {key_i[4:0],27'b0};
  end
  else if ((msbk ^ key_i[3]) == 1)
  begin
    mk = 28;
    apk[31:0] = {key_i[3:0],28'b0};
  end
  else if ((msbk ^ key_i[2]) == 1)
  begin
    mk = 29;
    apk[31:0] = {key_i[2:0],29'b0};
  end
  else if ((msbk ^ key_i[1]) == 1)
  begin
    mk = 30;
    apk[31:0] = {key_i[1:0],30'b0};
  end
  else if ((msbk ^ key_i[0]) == 1)
  begin
    mk = 31;
    apk[31:0] = {key_i[0],31'b0};
  end
  else
  begin
    mk = 32;
    apk[31:0] = 32'b0;
  end
end


assign mk_o = mk;
assign apk_o = apk;
assign msbk_o = msbk;

endmodule

