`timescale 1ns / 10ps
module compact_trie #(
  parameter IP_ADDRW  = 32,
  parameter PTRIE     = 2,
  parameter TDEPTH    = 1,
  parameter NHI_WIDTH = 8,                    //                    ,1,1,1,1,1,1,1,1,1,1,2,2,2,2,2,2,2,2,2,2,3,3
//  parameter integer USE_BLOOM [0:TOT_DEPTH-1] = {0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0}
  parameter USE_BLOOM = 1
)(
  input                       clk_i,
  input                       rst_n_i,
  input       [IP_ADDRW-1:0]  dst_addr_i,
  input                       daddr_vld_i,
  output reg  [7:0]           outport_o,
  output reg  [5:0]           matchlength_o,
  output reg                  done_o,
  output      [79:0]          match_found_o,
  input                       up_clk_i,
  input       [31:0]          up_addr_i,
  input       [31:0]          up_din_i,
  input                       up_wen_i
);

`include "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/memMap_mgm_const_param.h"

reg  [IP_ADDRW-1:0]   dst_addr_d1;
reg                   daddr_vld_d1;
reg                   daddr_vld_d2;
wire [31:0]           crc_sr_d1;
reg  [IP_ADDRW-1:0]   apk_ks_d1;
reg  [5:0]            mk_ks_d1;
reg                   msbk_ks_d1;

reg  [NHI_WIDTH-1:0]  nhi_ms_i        [TOT_DEPTH-1:0];
wire [NHI_WIDTH-1:0]  nhi_ms_o        [TOT_DEPTH-1:0];
wire [IP_ADDRW-1:0]   apk_ks;
reg  [IP_ADDRW-1:0]   apk_ms_i        [TOT_DEPTH-1:0];
wire [IP_ADDRW-1:0]   apk_ms_o        [TOT_DEPTH-1:0];
wire [5:0]            mk_ks;
reg  [5:0]            mk_ms_i         [TOT_DEPTH-1:0];
reg  [5:0]            mk_ms_i_d1      [TOT_DEPTH-1:0];
reg  [5:0]            mk_ms_i_d2      [TOT_DEPTH-1:0];
reg  [5:0]            mk_ms_i_d3      [TOT_DEPTH-1:0];
reg  [5:0]            mk_ms_i_d4      [TOT_DEPTH-1:0];
reg  [5:0]            mk_ms_i_d5      [TOT_DEPTH-1:0];
reg  [5:0]            mk_ms_i_d6      [TOT_DEPTH-1:0];
reg  [5:0]            mk_ms_o         [TOT_DEPTH-1:0];
wire                  msbk_ks;        
reg                   msbk_ms_i       [TOT_DEPTH-1:0];
reg                   msbk_ms_i_d1    [TOT_DEPTH-1:0];
reg                   msbk_ms_i_d2    [TOT_DEPTH-1:0];
reg                   msbk_ms_i_d3    [TOT_DEPTH-1:0];
reg                   msbk_ms_i_d4    [TOT_DEPTH-1:0];
reg                   msbk_ms_i_d5    [TOT_DEPTH-1:0];
reg                   msbk_ms_i_d6    [TOT_DEPTH-1:0];
reg                   msbk_ms_o       [TOT_DEPTH-1:0];
reg  [18:0]           address_ms_i    [TOT_DEPTH-1:0];
wire [18:0]           address_ms_o    [TOT_DEPTH-1:0];
reg  [5:0]            matchlength_ms_i[TOT_DEPTH-1:0];
wire [5:0]            matchlength_ms_o[TOT_DEPTH-1:0];
reg                   daddr_vld_ms_i  [TOT_DEPTH-1:0];
wire                  daddr_vld_ms_o  [TOT_DEPTH-1:0];
reg  [31:0]           crc_sr_ms_i     [TOT_DEPTH-1:0];
wire [31:0]           crc_sr_ms_o     [TOT_DEPTH-1:0];
reg  [31:0]           crc_apk_ms_i    [TOT_DEPTH-1:0];
wire [31:0]           crc_apk_ms_o    [TOT_DEPTH-1:0];
reg                   crc_vld_ms_i    [TOT_DEPTH-1:0];
wire                  crc_vld_ms_o    [TOT_DEPTH-1:0];
reg                   tlevel_inc_ms_i [TOT_DEPTH-1:0];
wire                  tlevel_inc_ms_o [TOT_DEPTH-1:0];
reg  [NHI_WIDTH-1:0]  outport;

wire        clk_buf;
wire        up_clk_buf;

/*
BUFG u_up_BUFG (
  .O                    (up_clk_buf         ), // 1-bit output: Clock output
  .I                    (up_clk_i           )  // 1-bit input: Clock input
);


BUFG u_sys_BUFG (
  .O                    (clk_buf            ), // 1-bit output: Clock output
  .I                    (clk_i              )  // 1-bit input: Clock input
);
*/

assign up_clk_buf = up_clk_i;
assign clk_buf    = clk_i;
always @(posedge clk_buf or negedge rst_n_i)
begin
  if (~rst_n_i)
  begin
    dst_addr_d1   <= #0.1 32'd0;
    daddr_vld_d1  <= #0.1 1'd0;
  end
  else
  begin
    dst_addr_d1   <= #0.1 dst_addr_i;
    daddr_vld_d1  <= #0.1 daddr_vld_i;
  end
end

key_stripper u_key_stripper (
  .key_i                (dst_addr_d1        ),
  .msbk_o               (msbk_ks            ),
  .apk_o                (apk_ks             ),
  .mk_o                 (mk_ks              )
);

always @(posedge clk_buf or negedge rst_n_i)
begin
  if (~rst_n_i)
  begin
    apk_ks_d1     <= #0.1 'd0;
    msbk_ks_d1    <= #0.1 'd0;
    mk_ks_d1      <= #0.1 'd0;
    daddr_vld_d2  <= #0.1 'd0;
  end
  else
  begin
    apk_ks_d1     <= #0.1 apk_ks;
    msbk_ks_d1    <= #0.1 msbk_ks;
    mk_ks_d1      <= #0.1 mk_ks;
    daddr_vld_d2  <= #0.1 daddr_vld_d1;
  end
end


generate
  if (USE_BLOOM == 1)
  begin: BLOOM
    bloom_filter_sr0 
    u_bloom_filter_sr0 (
      .msbk_i(msbk_ks),
      .mk_i(mk_ks),
      .daddr_vld_i(daddr_vld_d1),
      .crc_sr_o(crc_sr_d1)
    );
  end
  else
  begin: NOBLOOM
    assign crc_sr_d1 = 'd0;
  end
endgenerate


generate
  genvar i;
  for (i=0; i<= (TOT_DEPTH-1); i=i+1)
  begin: TRIE_STAGES
  
    localparam USE_BLOOM_MSTAGE = ((i>=BF_LB)&&(i<=BF_UB))? USE_BLOOM : 0; // This is defined in the table specific define file

    if (i==0)
    begin : TDEPTH0

      always @(posedge clk_buf or negedge rst_n_i)
      begin
        if (~rst_n_i)
        begin
          apk_ms_i[i]         <= #0.1 32'd0;
          nhi_ms_i[i]         <= #0.1 8'd0;
          address_ms_i[i]     <= #0.1 32'd0;
          msbk_ms_i[i]        <= #0.1 1'd0;
          mk_ms_i[i]          <= #0.1 6'd0;
          matchlength_ms_i[i] <= #0.1 6'd0;
          daddr_vld_ms_i[i]   <= #0.1 1'd0;

          crc_sr_ms_i[i]      <= #0.1 32'd0;
          crc_apk_ms_i[i]     <= #0.1 32'd0;
          crc_vld_ms_i[i]     <= #0.1 1'd0;
          tlevel_inc_ms_i[i]  <= #0.1 6'd0;
        end
        else
        begin
          apk_ms_i[i]         <= #0.1 apk_ks_d1;
          nhi_ms_i[i]         <= #0.1 8'd0;
          address_ms_i[i]     <= #0.1 20'd0;
          msbk_ms_i[i]        <= #0.1 msbk_ks_d1;
          mk_ms_i[i]          <= #0.1 mk_ks_d1;
          matchlength_ms_i[i] <= #0.1 6'd0;
          daddr_vld_ms_i[i]   <= #0.1 daddr_vld_d2;

          crc_sr_ms_i[i]      <= #0.1 crc_sr_d1;
          crc_apk_ms_i[i]     <= #0.1 apk_ks;
          crc_vld_ms_i[i]     <= #0.1 daddr_vld_d1;
          tlevel_inc_ms_i[i]  <= #0.1 6'd1;
        end
      end

    end
    else
    begin : TDEPTHALL

      always @(posedge clk_buf or negedge rst_n_i)
      begin
        if (~rst_n_i)
        begin
          apk_ms_i[i]         <= #0.1 32'd0;
          nhi_ms_i[i]         <= #0.1 8'd0;
          address_ms_i[i]     <= #0.1 32'd0;
          msbk_ms_i[i]        <= #0.1 1'd0;
          mk_ms_i[i]          <= #0.1 6'd0;
          matchlength_ms_i[i] <= #0.1 6'd0;
          daddr_vld_ms_i[i]   <= #0.1 1'd0;

          crc_sr_ms_i[i]      <= #0.1 32'd0;
          crc_apk_ms_i[i]     <= #0.1 32'd0;
          crc_vld_ms_i[i]     <= #0.1 1'd0;
          tlevel_inc_ms_i[i]  <= #0.1 6'd0;
        end
        else
        begin
          apk_ms_i[i]         <= #0.1 apk_ms_o[i-1];
          nhi_ms_i[i]         <= #0.1 nhi_ms_o[i-1];
          address_ms_i[i]     <= #0.1 address_ms_o[i-1];
          msbk_ms_i[i]        <= #0.1 msbk_ms_o[i-1];
          mk_ms_i[i]          <= #0.1 mk_ms_o[i-1];
          matchlength_ms_i[i] <= #0.1 matchlength_ms_o[i-1];
          daddr_vld_ms_i[i]   <= #0.1 daddr_vld_ms_o[i-1];

          crc_sr_ms_i[i]      <= #0.1 crc_sr_ms_o[i-1];
          crc_apk_ms_i[i]     <= #0.1 crc_apk_ms_o[i-1];
          crc_vld_ms_i[i]     <= #0.1 crc_vld_ms_o[i-1];
          tlevel_inc_ms_i[i]  <= #0.1 tlevel_inc_ms_o[i-1];
        end
      end

    end
  
    match_stage #(
      .TDEPTH           (i                  ),
      .PTRIE            (PTRIE              ),
      .IP_ADDRW         (IP_ADDRW           ),
      .NHI_WIDTH        (NHI_WIDTH          ),
      .USE_BLOOM_RAM    (USE_BLOOM_MSTAGE   ),
      .USE_BLOOM_CRC    (USE_BLOOM          )
    ) u_match_stage (
      .up_clk_i         (up_clk_buf         ),
      .up_addr_i        (up_addr_i          ),
      .up_din_i         (up_din_i           ),
      .up_wen_i         (up_wen_i           ),

      .clk_i            (clk_buf            ),
      .rst_n_i          (rst_n_i            ),

      .crc_sr_a1_i      (crc_sr_ms_i[i]     ),
      .crc_apk_a1_i     (crc_apk_ms_i[i]    ),
      .crc_vld_a1_i     (crc_vld_ms_i[i]    ),
      .tlevel_inc_a1_i  (tlevel_inc_ms_i[i] ),

      .crc_sr_d6_o      (crc_sr_ms_o[i]     ),
      .crc_apk_d6_o     (crc_apk_ms_o[i]    ),
      .crc_vld_d6_o     (crc_vld_ms_o[i]    ),
      .tlevel_inc_d6_o  (tlevel_inc_ms_o[i] ),
    
      .msbk_i           (msbk_ms_i[i]       ),
      .mk_i             (mk_ms_i[i]         ),
      .address_i        (address_ms_i[i]    ),
      .apk_i            (apk_ms_i[i]        ),
      .nhi_i            (nhi_ms_i[i]        ),
      .matchlength_i    (matchlength_ms_i[i]),
      .daddr_vld_i      (daddr_vld_ms_i[i]  ),
    
      .address_d7_o     (address_ms_o[i]    ),
      .apk_d7_o         (apk_ms_o[i]        ),
      .nhi_d7_o         (nhi_ms_o[i]        ),
      .matchlength_d7_o (matchlength_ms_o[i]),
      .match_found_d7_o (match_found_o[i]   ),
      .daddr_vld_d7_o   (daddr_vld_ms_o[i]  )
    );
    
    always @(posedge clk_buf or negedge rst_n_i)
    begin
      if (~rst_n_i)
      begin
        mk_ms_i_d1[i]   <= #0.1 'd0;
        mk_ms_i_d2[i]   <= #0.1 'd0; 
        mk_ms_i_d3[i]   <= #0.1 'd0; 
        mk_ms_i_d4[i]   <= #0.1 'd0; 
        mk_ms_i_d5[i]   <= #0.1 'd0; 
        mk_ms_i_d6[i]   <= #0.1 'd0; 
        mk_ms_o[i]      <= #0.1 'd0; 
        msbk_ms_i_d1[i] <= #0.1 'd0; 
        msbk_ms_i_d2[i] <= #0.1 'd0; 
        msbk_ms_i_d3[i] <= #0.1 'd0; 
        msbk_ms_i_d4[i] <= #0.1 'd0; 
        msbk_ms_i_d5[i] <= #0.1 'd0; 
        msbk_ms_i_d6[i] <= #0.1 'd0; 
        msbk_ms_o[i]    <= #0.1 'd0; 
      end
      else
      begin
        mk_ms_i_d1[i]   <= #0.1 mk_ms_i[i]; 
        mk_ms_i_d2[i]   <= #0.1 mk_ms_i_d1[i]; 
        mk_ms_i_d3[i]   <= #0.1 mk_ms_i_d2[i]; 
        mk_ms_i_d4[i]   <= #0.1 mk_ms_i_d3[i]; 
        mk_ms_i_d5[i]   <= #0.1 mk_ms_i_d4[i]; 
        mk_ms_i_d6[i]   <= #0.1 mk_ms_i_d5[i]; 
        mk_ms_o[i]      <= #0.1 mk_ms_i_d6[i]; 
        msbk_ms_i_d1[i] <= #0.1 msbk_ms_i[i]; 
        msbk_ms_i_d2[i] <= #0.1 msbk_ms_i_d1[i]; 
        msbk_ms_i_d3[i] <= #0.1 msbk_ms_i_d2[i]; 
        msbk_ms_i_d4[i] <= #0.1 msbk_ms_i_d3[i]; 
        msbk_ms_i_d5[i] <= #0.1 msbk_ms_i_d4[i]; 
        msbk_ms_i_d6[i] <= #0.1 msbk_ms_i_d5[i]; 
        msbk_ms_o[i]    <= #0.1 msbk_ms_i_d6[i]; 
      end
    end

  end
endgenerate  


always @(posedge clk_buf or negedge rst_n_i)
begin
  if (~rst_n_i)
  begin
    outport_o     <= #0.1 8'd0;
    matchlength_o <= #0.1 6'd0;
    done_o        <= #0.1 1'd0;
  end
  else
  begin
    outport_o     <= #0.1 nhi_ms_o[TOT_DEPTH-1];  
    matchlength_o <= #0.1 matchlength_ms_o[TOT_DEPTH-1];
    done_o        <= #0.1 daddr_vld_ms_o[TOT_DEPTH-1];
  end
end


endmodule
