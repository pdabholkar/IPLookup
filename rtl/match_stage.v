
`timescale 1ns / 10ps
module match_stage #(
  parameter TDEPTH    = 1,
  parameter PTRIE     = 2,
  parameter IP_ADDRW  = 32,
  parameter NHI_WIDTH = 8,
  parameter USE_BLOOM_RAM = 1,
  parameter USE_BLOOM_CRC = 1
)(
  input                     up_clk_i,
  input [31:0]              up_addr_i,
  input [79:0]              up_din_i,
  input                     up_wen_i,
  
  input                     clk_i,
  input                     rst_n_i,
  
  // Key Information that is passed from one match stage to the next without
  // any change
  input                     msbk_i,
  input [5:0]               mk_i,

  // Inputs from the previous match stage's CRC computation block
  input [31:0]              crc_sr_a1_i,
  input [IP_ADDRW-1:0]      crc_apk_a1_i,
  input                     crc_vld_a1_i,

  // Inputs from the previous match stage's child computation block
  input                     tlevel_inc_a1_i,
  input [MEM_AWIDTH-1:0]    address_i,
  input [IP_ADDRW-1:0]      apk_i,
  input                     daddr_vld_i,

  // Inputs from the previous match stage's prefix match block
  input [NHI_WIDTH-1:0]     nhi_i,
  input [5:0]               matchlength_i,

  // Outputs from the CRC computation block appropriately delayed
  output reg [31:0]         crc_sr_d6_o,
  output     [IP_ADDRW-1:0] crc_apk_d6_o,
  output                    crc_vld_d6_o,

  // Outputs from the next child computation block appropriately delayed
  output reg                tlevel_inc_d6_o,
  output reg [CP_WIDTH-1:0] address_d7_o,
  output reg [IP_ADDRW-1:0] apk_d7_o,
  output reg                daddr_vld_d7_o,

  // Outputs from the prefix match block appropriately delayed
  output     [NHI_WIDTH-1:0]nhi_d7_o,
  output     [5:0]          matchlength_d7_o,
  output                    match_found_d7_o
);


`include "../rtl/mem_and_param_config.h"

localparam PFX_RAM_WIDTH = (PTRIE*(1+1+3+5+NHI_WIDTH));
// PTRIE x (MSB, LSB, |x|, |z|, NHI BITS)
localparam LRC_RAM_WIDTH = (2*CP_WIDTH) + 1 + 1 ;
// 2 x CHILD_POINTER WIDTH + eps_flag + nodeinuse_flag 
 
wire [PFX_RAM_WIDTH-1:0]  pfx_info_d5;
wire [LRC_RAM_WIDTH-1:0]  lrc_info_d2;

wire [BF_AWIDTH-1:0]  bf_idx0_d1;
wire [BF_AWIDTH-1:0]  bf_idx1_d1;

wire                  msb1_d5,lsb1_d5;  // First prefix information
wire [2:0]            mp1_d5;
wire [4:0]            lp1_d5;
wire [NHI_WIDTH-1:0]  nhi1_d5;
wire                  msb2_d5,lsb2_d5;  // Second prefix information
wire [2:0]            mp2_d5;
wire [4:0]            lp2_d5;
wire [NHI_WIDTH-1:0]  nhi2_d5;
wire                  msb3_d5,lsb3_d5;  // Third prefix information relevant only if PTRIE = 3
wire [2:0]            mp3_d5;
wire [4:0]            lp3_d5;
wire [NHI_WIDTH-1:0]  nhi3_d5;

wire [CP_WIDTH-1:0]   lc_d2;
wire [CP_WIDTH-1:0]   rc_d2;
wire                  epsilon_d2;
wire                  node_inuse_d2;

wire                  bf_match_d3;

reg [MEM_AWIDTH-1:0]  address_d1;
reg [MEM_AWIDTH-1:0]  address_d2;
reg [MEM_AWIDTH-1:0]  address_d3;

reg [IP_ADDRW-1:0]    apk_d1;
reg [IP_ADDRW-1:0]    apk_d2;
reg [IP_ADDRW-1:0]    apk_d3;
reg [IP_ADDRW-1:0]    apk_d4;

reg                   msbk_d1;
reg                   msbk_d2;
reg                   msbk_d3;
reg                   msbk_d4;
reg                   msbk_d5;

reg [5:0]             mk_d1;
reg [5:0]             mk_d2;
reg [5:0]             mk_d3;
reg [5:0]             mk_d4;
reg [5:0]             mk_d5;

reg [5:0]             matchlength_d1;
reg [5:0]             matchlength_d2;
reg [5:0]             matchlength_d3;
reg [5:0]             matchlength_d4;
reg [5:0]             matchlength_d5;

reg [NHI_WIDTH-1:0]   nhi_d1;
reg [NHI_WIDTH-1:0]   nhi_d2;
reg [NHI_WIDTH-1:0]   nhi_d3;
reg [NHI_WIDTH-1:0]   nhi_d4;
reg [NHI_WIDTH-1:0]   nhi_d5;

reg                   epsilon_d3;
reg                   epsilon_d4;
reg                   epsilon_d5;

reg                   daddr_vld_d1;
reg                   daddr_vld_d2;
reg                   daddr_vld_d3;
reg                   daddr_vld_d4;

wire                  pfx_ram_enable_d3;
reg                   pfx_ram_enable_d4;
reg                   pfx_ram_enable_d5;

wire                  tlevel_inc_d3_o;
reg                   tlevel_inc_d4_o;
reg                   tlevel_inc_d5_o;

wire[CP_WIDTH-1:0]    address_d3_o;
reg [CP_WIDTH-1:0]    address_d4_o;
reg [CP_WIDTH-1:0]    address_d5_o;
reg [CP_WIDTH-1:0]    address_d6_o;

wire[IP_ADDRW-1:0]    apk_d3_o;
reg [IP_ADDRW-1:0]    apk_d4_o;
reg [IP_ADDRW-1:0]    apk_d5_o;
reg [IP_ADDRW-1:0]    apk_d6_o;

wire                  daddr_vld_d3_o;
reg                   daddr_vld_d4_o;
reg                   daddr_vld_d5_o;
reg                   daddr_vld_d6_o;

wire[31:0]            crc_sr; 
reg [31:0]            crc_sr_d1_o;
reg [31:0]            crc_sr_d2_o;
reg [31:0]            crc_sr_d3_o;
reg [31:0]            crc_sr_d4_o;
reg [31:0]            crc_sr_d5_o;

always @(posedge clk_i or negedge rst_n_i)
begin
  if (~rst_n_i)
  begin
    address_d1      <= #0.1 {MEM_AWIDTH{1'd0}};
    address_d2      <= #0.1 {MEM_AWIDTH{1'd0}};
    address_d3      <= #0.1 {MEM_AWIDTH{1'd0}};
    nhi_d1          <= #0.1 {NHI_WIDTH{1'd0}};
    nhi_d2          <= #0.1 {NHI_WIDTH{1'd0}};
    nhi_d3          <= #0.1 {NHI_WIDTH{1'd0}};
    nhi_d4          <= #0.1 {NHI_WIDTH{1'd0}};
    nhi_d5          <= #0.1 {NHI_WIDTH{1'd0}};
    apk_d1          <= #0.1 {IP_ADDRW{1'd0}};
    apk_d2          <= #0.1 {IP_ADDRW{1'd0}};
    apk_d3          <= #0.1 {IP_ADDRW{1'd0}};
    apk_d4          <= #0.1 {IP_ADDRW{1'd0}};
    msbk_d1         <= #0.1 1'd0;
    msbk_d2         <= #0.1 1'd0;
    msbk_d3         <= #0.1 1'd0;
    msbk_d4         <= #0.1 1'd0;
    msbk_d5         <= #0.1 1'd0;
    mk_d1           <= #0.1 6'd0;
    mk_d2           <= #0.1 6'd0;
    mk_d3           <= #0.1 6'd0;
    mk_d4           <= #0.1 6'd0;
    mk_d5           <= #0.1 6'd0;
    matchlength_d1  <= #0.1 1'd0;
    matchlength_d2  <= #0.1 1'd0;
    matchlength_d3  <= #0.1 1'd0;
    matchlength_d4  <= #0.1 1'd0;
    matchlength_d5  <= #0.1 1'd0;
    daddr_vld_d1    <= #0.1 1'd0;
    daddr_vld_d2    <= #0.1 1'd0;
    daddr_vld_d3    <= #0.1 1'd0;
    daddr_vld_d4    <= #0.1 1'd0;
    epsilon_d3      <= #0.1 1'd0;
    epsilon_d4      <= #0.1 1'd0;
    epsilon_d5      <= #0.1 1'd0;
  end
  else
  begin
    address_d1      <= #0.1 address_i;
    address_d2      <= #0.1 address_d1;
    address_d3      <= #0.1 address_d2;
    nhi_d1          <= #0.1 nhi_i;
    nhi_d2          <= #0.1 nhi_d1;
    nhi_d3          <= #0.1 nhi_d2;
    nhi_d4          <= #0.1 nhi_d3;
    nhi_d5          <= #0.1 nhi_d4;
    apk_d1          <= #0.1 apk_i;
    apk_d2          <= #0.1 apk_d1;
    apk_d3          <= #0.1 apk_d2;
    apk_d4          <= #0.1 apk_d3;
    msbk_d1         <= #0.1 msbk_i;
    msbk_d2         <= #0.1 msbk_d1;
    msbk_d3         <= #0.1 msbk_d2;
    msbk_d4         <= #0.1 msbk_d3;
    msbk_d5         <= #0.1 msbk_d4;
    mk_d1           <= #0.1 mk_i;
    mk_d2           <= #0.1 mk_d1;
    mk_d3           <= #0.1 mk_d2;
    mk_d4           <= #0.1 mk_d3;
    mk_d5           <= #0.1 mk_d4;
    matchlength_d1  <= #0.1 matchlength_i;
    matchlength_d2  <= #0.1 matchlength_d1;
    matchlength_d3  <= #0.1 matchlength_d2;
    matchlength_d4  <= #0.1 matchlength_d3;
    matchlength_d5  <= #0.1 matchlength_d4;
    daddr_vld_d1    <= #0.1 daddr_vld_i;
    daddr_vld_d2    <= #0.1 daddr_vld_d1;
    daddr_vld_d3    <= #0.1 daddr_vld_d2;
    daddr_vld_d4    <= #0.1 daddr_vld_d3;
    epsilon_d3      <= #0.1 epsilon_d2;
    epsilon_d4      <= #0.1 epsilon_d3;
    epsilon_d5      <= #0.1 epsilon_d4;
  end
end

always @(posedge clk_i or negedge rst_n_i)
begin
  if(~rst_n_i)
  begin
    crc_sr_d1_o <= #0.1 32'd0;
    crc_sr_d2_o <= #0.1 32'd0;
    crc_sr_d3_o <= #0.1 32'd0;
    crc_sr_d4_o <= #0.1 32'd0;
    crc_sr_d5_o <= #0.1 32'd0;
    crc_sr_d6_o <= #0.1 32'd0;
  end
  else
  begin
    crc_sr_d1_o <= #0.1 crc_sr;
    crc_sr_d2_o <= #0.1 crc_sr_d1_o;
    crc_sr_d3_o <= #0.1 crc_sr_d2_o;
    crc_sr_d4_o <= #0.1 crc_sr_d3_o;
    crc_sr_d5_o <= #0.1 crc_sr_d4_o;
    crc_sr_d6_o <= #0.1 crc_sr_d5_o;
  end
end

next_child #(
  .CP_WIDTH             (CP_WIDTH           ),
  .IP_ADDRW             (IP_ADDRW           )
) u_next_child(
  .clk_i                (clk_i              ),
  .rst_n_i              (rst_n_i            ),
  .lc_i                 (lc_d2              ),
  .rc_i                 (rc_d2              ),
  .epsilon_i            (epsilon_d2         ),
  .apk_i                (apk_d2             ),
  .daddr_vld_i          (daddr_vld_d2       ),
  // Key information for next stage
  .tlevel_inc_d1_o      (tlevel_inc_d3_o    ),
  .address_d1_o         (address_d3_o       ),
  .apk_d1_o             (apk_d3_o           ),
  .daddr_vld_d1_o       (daddr_vld_d3_o     )
);


always @(posedge clk_i or negedge rst_n_i)
begin
  if (~rst_n_i)
  begin
    tlevel_inc_d4_o <= #0.1 6'd0;
    tlevel_inc_d5_o <= #0.1 6'd0;
    tlevel_inc_d6_o <= #0.1 6'd0;
    address_d4_o    <= #0.1 {MEM_AWIDTH{1'd0}};
    address_d5_o    <= #0.1 {MEM_AWIDTH{1'd0}};
    address_d6_o    <= #0.1 {MEM_AWIDTH{1'd0}};
    address_d7_o    <= #0.1 {MEM_AWIDTH{1'd0}};
    apk_d4_o        <= #0.1 {CP_WIDTH{1'd0}};
    apk_d5_o        <= #0.1 {CP_WIDTH{1'd0}};
    apk_d6_o        <= #0.1 {CP_WIDTH{1'd0}};
    apk_d7_o        <= #0.1 {CP_WIDTH{1'd0}};
    daddr_vld_d4_o  <= #0.1 1'd0;
    daddr_vld_d5_o  <= #0.1 1'd0;
    daddr_vld_d6_o  <= #0.1 1'd0;
    daddr_vld_d7_o  <= #0.1 1'd0;
  end
  else
  begin
    tlevel_inc_d4_o <= #0.1 tlevel_inc_d3_o;
    tlevel_inc_d5_o <= #0.1 tlevel_inc_d4_o;
    tlevel_inc_d6_o <= #0.1 tlevel_inc_d5_o;
    address_d4_o    <= #0.1 address_d3_o;
    address_d5_o    <= #0.1 address_d4_o;
    address_d6_o    <= #0.1 address_d5_o;
    address_d7_o    <= #0.1 address_d6_o;
    apk_d4_o        <= #0.1 apk_d3_o;
    apk_d5_o        <= #0.1 apk_d4_o;
    apk_d6_o        <= #0.1 apk_d5_o;
    apk_d7_o        <= #0.1 apk_d6_o;
    daddr_vld_d4_o  <= #0.1 daddr_vld_d3_o;
    daddr_vld_d5_o  <= #0.1 daddr_vld_d4_o;
    daddr_vld_d6_o  <= #0.1 daddr_vld_d5_o;
    daddr_vld_d7_o  <= #0.1 daddr_vld_d6_o;
  end
end

assign crc_apk_d6_o = apk_d6_o;
assign crc_vld_d6_o = daddr_vld_d6_o;

match_module #(
  .TDEPTH               (TDEPTH             ),
  .PTRIE                (PTRIE              ),
  .NHI_WIDTH            (NHI_WIDTH          )
)u_match_module(
  .clk_i                (clk_i              ),
  .rst_n_i              (rst_n_i            ),
  // Key information  
  .msbk_i               (msbk_d5            ),
  .mk_i                 (mk_d5              ),
  .apk_a1_i             (apk_d4             ),
  .daddr_vld_a1_i       (daddr_vld_d4       ),
  // Info generated from previous pipeline stage's match stage
  .nhi_i                (nhi_d5             ),
  .matchlength_i        (matchlength_d5     ),
  // Children information 
  .epsilon_i            (epsilon_d5         ),
  // First prefix in the node
  .msb1_i               (msb1_d5            ),
  .lsb1_i               (lsb1_d5            ),
  .mp1_i                (mp1_d5             ),
  .lp1_i                (lp1_d5             ),
  .nhi1_i               (nhi1_d5            ),
  // Second prefix in the node
  .msb2_i               (msb2_d5            ),
  .lsb2_i               (lsb2_d5            ),
  .mp2_i                (mp2_d5             ),
  .lp2_i                (lp2_d5             ),
  .nhi2_i               (nhi2_d5            ),
  // Third prefix in the node
  .msb3_i               (msb3_d5            ),
  .lsb3_i               (lsb3_d5            ),
  .mp3_i                (mp3_d5             ),
  .lp3_i                (lp3_d5             ),
  .nhi3_i               (nhi3_d5            ),
  // Info generated for next pipeline stage's match stage
  .nhi_d2_o             (nhi_d7_o           ),
  .matchlength_d2_o     (matchlength_d7_o   ),
  .match_found_d2_o     (match_found_d7_o   )
);

// Left Right child storage SRAM
// An importnat thing to note here is that the size of the RAM required to
// store MEM_SIZE entries is clogb2(MEM_SIZE-1). However because we need to
// distinguish between a valid all-ones address in this address range and an
// all-ones pattern that is used to indicate a NULL location, the CP_WIDTH
// parameter is set to clogb2(MEM_SIZE-1)+1 in the .h file.
// 
// This results in the address_i bus connected to port addrb below to have
// a width 1 bit greater than the actual port size. 
// Consider a MEM_SIZE of 16. In that case the real address
// 0_1111 and the NULL flag 1_1111 will both result in the address of 1111.
// However this will never occur because in the previous stage, when the next
// stage address is being calculated, if the child address is 1_1111, then the
// address is changed to 0_0000 and the daddr_vld signal is reset to '0'. This
// prevents incorrect access.

xilinx_simple_dual_port_2_clock_ram #(
  .RAM_WIDTH            (LRC_RAM_WIDTH      ),     //  left right child pointer + flags width
  .RAM_DEPTH            (MEM_SIZE           ),     //  RAM depth (number of entries) defined by the mdepth_vs_tdepth.h file
  .RAM_PERFORMANCE      ("HIGH_PERFORMANCE" ),     // "HIGH_PERFORMANCE" or "LOW_LATENCY" 
  .INIT_FILE            (LRC_INIT_FILE      )      //  name/location of RAM initialization file
) u_lrc_RAM             (
  .clka                 (up_clk_i           ),     // Write clock
  .addra                (up_addr_i          ),     // Write address bus, width determined from RAM_DEPTH
  .dina                 (up_din_i           ),     // RAM input data, width determined from RAM_WIDTH
  .wea                  (up_wen_i           ),     // Write enable

  .clkb                 (clk_i              ),     // Read clock
  .addrb                (address_i          ),     // Read address bus, width determined from RAM_DEPTH
  .enb                  (daddr_vld_i        ),     // Read Enable, for additional power savings, Controlled by the daddr valid signal of the previous stage and Bloom filter match
  .rstb                 (~rst_n_i           ),     // Output reset (does not affect memory contents)
  .regceb               (1'b1               ),     // Output register enable
  .doutb                (lrc_info_d2        )      // RAM output data, width determined from RAM_WIDTH
);

assign pfx_ram_enable_d3 = bf_match_d3 & daddr_vld_d3;

always@(posedge clk_i or negedge rst_n_i)
begin
  if (~rst_n_i)
  begin
    pfx_ram_enable_d4 <= #0.1 'd0;
    pfx_ram_enable_d5 <= #0.1 'd0;
  end
  else
  begin
    pfx_ram_enable_d4 <= #0.1 pfx_ram_enable_d3;
    pfx_ram_enable_d5 <= #0.1 pfx_ram_enable_d4;
  end
end

// Prefix storage SRAM
xilinx_simple_dual_port_2_clock_ram #(
  .RAM_WIDTH            (PFX_RAM_WIDTH      ),     //  prefix data width
  .RAM_DEPTH            (MEM_SIZE           ),     //  RAM depth (number of entries) defined by the mdepth_vs_tdepth.h file
  .RAM_PERFORMANCE      ("HIGH_PERFORMANCE" ),     // "HIGH_PERFORMANCE" or "LOW_LATENCY" 
  .INIT_FILE            (PFX_INIT_FILE      )      //  name/location of RAM initialization file
) u_pfx_RAM (
  .clka                 (up_clk_i           ),     // Write clock
  .addra                (up_addr_i          ),     // Write address bus, width determined from RAM_DEPTH
  .dina                 (up_din_i           ),     // RAM input data, width determined from RAM_WIDTH
  .wea                  (up_wen_i           ),     // Write enable

  .clkb                 (clk_i              ),     // Read clock
  .addrb                (address_d3         ),     // 
  .enb                  (pfx_ram_enable_d3  ),     // Read Enable, for additional power savings, Controlled by the daddr valid signal of the previous stage and Bloom filter match
  .rstb                 (~rst_n_i           ),     // Output reset (does not affect memory contents)
  .regceb               (1'b1               ),     // Output register enable
  .doutb                (pfx_info_d5        )      // RAM output data, width determined from RAM_WIDTH
);


// This could have been done better, but feeling lazy right now
generate
  if (PTRIE == 2)
  begin: PT_2
    assign {msb1_d5,lsb1_d5,mp1_d5[2:0],lp1_d5[4:0],nhi1_d5[NHI_WIDTH-1:0],msb2_d5,lsb2_d5,mp2_d5[2:0],lp2_d5[4:0],nhi2_d5[NHI_WIDTH-1:0]} = pfx_ram_enable_d5 ? pfx_info_d5[PFX_RAM_WIDTH-1:0] : 'd0;
  end
  else if (PTRIE == 3)
  begin: PT_3
    assign {msb1_d5,lsb1_d5,mp1_d5[2:0],lp1_d5[4:0],nhi1_d5[NHI_WIDTH-1:0],msb2_d5,lsb2_d5,mp2_d5[2:0],lp2_d5[4:0],nhi2_d5[NHI_WIDTH-1:0],msb3_d5,lsb3_d5,mp3_d5[2:0],lp3_d5[4:0],nhi3_d5[NHI_WIDTH-1:0]} = pfx_ram_enable_d5 ? pfx_info_d5[PFX_RAM_WIDTH-1:0] : 'd0;
  end
endgenerate


assign {lc_d2[CP_WIDTH-1:0],rc_d2[CP_WIDTH-1:0],epsilon_d2,node_inuse_d2} = daddr_vld_d2 ? lrc_info_d2[LRC_RAM_WIDTH-1:0] : 'd0;


generate
  if (USE_BLOOM_CRC == 1)
  begin : BLOOM_CRC
    bloom_filter#(
      .BF_AWIDTH            (BF_AWIDTH          )
    )u_bloom_filter(
      .clk_i                (clk_i              ),
      .rst_n_i              (rst_n_i            ),
      .tlevel_inc_i         (tlevel_inc_a1_i    ),
      .apk_i                (crc_apk_a1_i[31]   ),
      .daddr_vld_i          (crc_vld_a1_i       ),
      .crc_sr_i             (crc_sr_a1_i        ),
      .crc_sr_d1_o          (crc_sr             ),
      .bf_idx0_d2_o         (bf_idx0_d1         ),
      .bf_idx1_d2_o         (bf_idx1_d1         )
    );
  end
endgenerate
    
generate
  if (USE_BLOOM_RAM == 1)
  begin: BLOOM_RAM
    // Bloom filter storage SRAM
    xilinx_true_dual_port_no_change_1_clock_ram #(
      .RAM_WIDTH            (1                  ),
      .RAM_DEPTH            (BF_SIZE            ),
      .RAM_PERFORMANCE      ("HIGH_PERFORMANCE" ),     //
      .INIT_FILE            (BF_INIT_FILE       )      //
    )u_bf_RAM (
      .addra                (bf_idx0_d1         ),     // 
      .addrb                (bf_idx1_d1         ),     // 
      .dina                 (up_din_i           ),     // 
      .dinb                 (up_din_i           ),     // 
      .clka                 (clk_i              ),     // 
      .ena                  (daddr_vld_d1       ),     // 
      .enb                  (daddr_vld_d1       ),     // 
      .wea                  (up_wen_i           ),
      .web                  (up_wen_i           ),
      .rsta                 (~rst_n_i           ),     // 
      .rstb                 (~rst_n_i           ),     // 
      .regcea               (daddr_vld_d2       ),     // 
      .regceb               (daddr_vld_d2       ),     // 
      .douta                (bf_bit0_d3         ),     // 
      .doutb                (bf_bit1_d3         )      // 
    );
  
    assign bf_match_d3 = bf_bit0_d3 & bf_bit1_d3;
  end
  else
  begin: NOBLOOM
    assign bf_match_d3 = daddr_vld_d3;   // This is to ensure that the bf_match is generated in phase with the daddr_vld signal, the way it would be if the RAM was around
  end
endgenerate

endmodule
