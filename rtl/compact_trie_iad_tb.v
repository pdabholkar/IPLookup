`timescale 1ns / 10ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 05/08/2017 11:14:18 AM
// Design Name: 
// Module Name: compact_trie_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module compact_trie_tb();
  reg clk;
  reg up_clk;
  reg rst_n;
  
  reg [31:0]  dst_addr[0:1950000];
  wire[31:0]  dst_addr_dut;
  wire        daddr_vld;
  
  reg [22:0]  tvec_counter; 
  wire [7:0]  nhi_port;
  wire [79:0] match_found;
  integer i;
  
  initial
  begin
    clk       = 1;
    up_clk    = 1;
    rst_n     = 1;
    #10 rst_n = 0;
    #50 rst_n = 1;
  end

  initial
  begin
    $readmemb("tvec_dump.log",dst_addr);
    #100000 $finish;
  end

  always #5.25 clk = ~clk;  // for IAD

  always #10 up_clk = ~up_clk;

  always @(posedge clk or negedge rst_n)
  begin
    if (~rst_n)
      tvec_counter <= #0.1 15'd0;
    else
      tvec_counter <= #0.1 tvec_counter + 'd1000;  // for IAD
  end

  assign dst_addr_dut = dst_addr[tvec_counter[22:0]];
  assign daddr_vld    = rst_n;
  
  compact_trie #(
    .IP_ADDRW  (32),
    .PTRIE     (2),
    .NHI_WIDTH (8)
  ) dut_compact_trie (
    .clk_i        (clk          ),
    .rst_n_i      (rst_n        ),
    .dst_addr_i   (dst_addr_dut ),
    .daddr_vld_i  (daddr_vld    ),
    .outport_o    (nhi_port     ),
    .match_found_o(match_found  ),
    .up_clk_i     (up_clk       ),
    .up_addr_i    (32'd0        ),
    .up_din_i     (32'd0        ),
    .up_wen_i     (1'd0         )
  );

    
endmodule
