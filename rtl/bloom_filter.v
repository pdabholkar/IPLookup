`timescale 1ns / 10ps
module bloom_filter #(
  parameter           BF_AWIDTH = 32,
  parameter           BF_DEL2   = 1       // 1 => enable the additional delay on BF index outputs to meet timing.
                                          // 0 => disable additional delay to allow use in the ncl_as_v design
)(
  input                       clk_i,
  input                       rst_n_i,
  input                       tlevel_inc_i,
  input                       apk_i,
  input                       daddr_vld_i,
  input      [31:0]           crc_sr_i,
  output reg [31:0]           crc_sr_d1_o,
  output reg [BF_AWIDTH-1:0]  bf_idx0_d2_o,
  output reg [BF_AWIDTH-1:0]  bf_idx1_d2_o
);

wire din;
wire lsb;

// endianness similar to the python code
wire [0:31] crc_sr_in_le;
wire [0:31] crc_sr_out_le;
reg         daddr_vld_d1;

// Design 

generate
genvar i;
  for (i=0;i<=31;i=i+1)
  begin: IP_ENDIAN_SWAP
    assign crc_sr_in_le[i] = crc_sr_i[31-i];
  end
endgenerate

///////////////////////////////////////////////////////
// Little Endian operation similar to the python code
///////////////////////////////////////////////////////

assign lsb = crc_sr_in_le[31];
assign din = apk_i;

  assign crc_sr_out_le[31] = tlevel_inc_i ? crc_sr_in_le[30]             : crc_sr_in_le[31] ;
  assign crc_sr_out_le[30] = tlevel_inc_i ? crc_sr_in_le[29]             : crc_sr_in_le[30] ;
  assign crc_sr_out_le[29] = tlevel_inc_i ? crc_sr_in_le[28]             : crc_sr_in_le[29] ;
  assign crc_sr_out_le[28] = tlevel_inc_i ? crc_sr_in_le[27]             : crc_sr_in_le[28] ;
  assign crc_sr_out_le[27] = tlevel_inc_i ? crc_sr_in_le[26]             : crc_sr_in_le[27] ;
  assign crc_sr_out_le[26] = tlevel_inc_i ? crc_sr_in_le[25] ^ lsb ^ din : crc_sr_in_le[26] ;
  assign crc_sr_out_le[25] = tlevel_inc_i ? crc_sr_in_le[24]             : crc_sr_in_le[25] ;
  assign crc_sr_out_le[24] = tlevel_inc_i ? crc_sr_in_le[23]             : crc_sr_in_le[24] ;
  assign crc_sr_out_le[23] = tlevel_inc_i ? crc_sr_in_le[22] ^ lsb ^ din : crc_sr_in_le[23] ;
  assign crc_sr_out_le[22] = tlevel_inc_i ? crc_sr_in_le[21] ^ lsb ^ din : crc_sr_in_le[22] ;
  assign crc_sr_out_le[21] = tlevel_inc_i ? crc_sr_in_le[20]             : crc_sr_in_le[21] ;
  assign crc_sr_out_le[20] = tlevel_inc_i ? crc_sr_in_le[19]             : crc_sr_in_le[20] ;
  assign crc_sr_out_le[19] = tlevel_inc_i ? crc_sr_in_le[18]             : crc_sr_in_le[19] ;
  assign crc_sr_out_le[18] = tlevel_inc_i ? crc_sr_in_le[17]             : crc_sr_in_le[18] ;
  assign crc_sr_out_le[17] = tlevel_inc_i ? crc_sr_in_le[16]             : crc_sr_in_le[17] ;
  assign crc_sr_out_le[16] = tlevel_inc_i ? crc_sr_in_le[15] ^ lsb ^ din : crc_sr_in_le[16] ;
  assign crc_sr_out_le[15] = tlevel_inc_i ? crc_sr_in_le[14]             : crc_sr_in_le[15] ;
  assign crc_sr_out_le[14] = tlevel_inc_i ? crc_sr_in_le[13]             : crc_sr_in_le[14] ;
  assign crc_sr_out_le[13] = tlevel_inc_i ? crc_sr_in_le[12]             : crc_sr_in_le[13] ;
  assign crc_sr_out_le[12] = tlevel_inc_i ? crc_sr_in_le[11] ^ lsb ^ din : crc_sr_in_le[12] ;
  assign crc_sr_out_le[11] = tlevel_inc_i ? crc_sr_in_le[10] ^ lsb ^ din : crc_sr_in_le[11] ;
  assign crc_sr_out_le[10] = tlevel_inc_i ? crc_sr_in_le[9]  ^ lsb ^ din : crc_sr_in_le[10] ;
  assign crc_sr_out_le[9]  = tlevel_inc_i ? crc_sr_in_le[8]              : crc_sr_in_le[9]  ;
  assign crc_sr_out_le[8]  = tlevel_inc_i ? crc_sr_in_le[7]  ^ lsb ^ din : crc_sr_in_le[8]  ;
  assign crc_sr_out_le[7]  = tlevel_inc_i ? crc_sr_in_le[6]  ^ lsb ^ din : crc_sr_in_le[7]  ;
  assign crc_sr_out_le[6]  = tlevel_inc_i ? crc_sr_in_le[5]              : crc_sr_in_le[6]  ;
  assign crc_sr_out_le[5]  = tlevel_inc_i ? crc_sr_in_le[4]  ^ lsb ^ din : crc_sr_in_le[5]  ;
  assign crc_sr_out_le[4]  = tlevel_inc_i ? crc_sr_in_le[3]  ^ lsb ^ din : crc_sr_in_le[4]  ;
  assign crc_sr_out_le[3]  = tlevel_inc_i ? crc_sr_in_le[2]              : crc_sr_in_le[3]  ;
  assign crc_sr_out_le[2]  = tlevel_inc_i ? crc_sr_in_le[1]  ^ lsb ^ din : crc_sr_in_le[2]  ;
  assign crc_sr_out_le[1]  = tlevel_inc_i ? crc_sr_in_le[0]  ^ lsb ^ din : crc_sr_in_le[1]  ;
  assign crc_sr_out_le[0]  = tlevel_inc_i ?                    lsb ^ din : crc_sr_in_le[0]  ;

////////////////////////////////////////////////
// Little Endian operation ends
////////////////////////////////////////////////


always @(posedge clk_i or negedge rst_n_i)
begin
  if(~rst_n_i)
    crc_sr_d1_o[31:0]           <= /*#0.1*/ 32'd0;
  else if (daddr_vld_i)
    crc_sr_d1_o[31:0]           <= /*#0.1*/ crc_sr_out_le[0:31];
  else
    crc_sr_d1_o[31:0]           <= /*#0.1*/ 32'd0;
end

always @(posedge clk_i or negedge rst_n_i)
begin
  if (~rst_n_i)
    daddr_vld_d1 <= /*#0.1*/ 1'd0;
  else
    daddr_vld_d1 <= /*#0.1*/ daddr_vld_i;
end


generate
if (BF_DEL2 == 1)
begin : BF_IDX_D2

  always @(posedge clk_i or negedge rst_n_i)
  begin
    if(~rst_n_i)
    begin
      bf_idx0_d2_o[BF_AWIDTH-1:0] <= /*#0.1*/ 16'd0;
      bf_idx1_d2_o[BF_AWIDTH-1:0] <= /*#0.1*/ 16'd0;
    end
    else if (daddr_vld_d1)
    begin
      bf_idx0_d2_o[BF_AWIDTH-1:0] <= /*#0.1*/ crc_sr_d1_o[31:32-BF_AWIDTH];
      bf_idx1_d2_o[BF_AWIDTH-1:0] <= /*#0.1*/ crc_sr_d1_o[23:24-BF_AWIDTH];
    end
    else
    begin
      bf_idx0_d2_o[BF_AWIDTH-1:0] <= /*#0.1*/ 16'd0;
      bf_idx1_d2_o[BF_AWIDTH-1:0] <= /*#0.1*/ 16'd0;
    end
  end

end
else
begin : BF_IDX_D1

  always @(*)
  begin
    if(~rst_n_i)
    begin
      bf_idx0_d2_o[BF_AWIDTH-1:0] <= /*#0.1*/ 16'd0;
      bf_idx1_d2_o[BF_AWIDTH-1:0] <= /*#0.1*/ 16'd0;
    end
    else if (daddr_vld_d1)
    begin
      bf_idx0_d2_o[BF_AWIDTH-1:0] <= /*#0.1*/ crc_sr_d1_o[31:32-BF_AWIDTH];
      bf_idx1_d2_o[BF_AWIDTH-1:0] <= /*#0.1*/ crc_sr_d1_o[23:24-BF_AWIDTH];
    end
    else
    begin
      bf_idx0_d2_o[BF_AWIDTH-1:0] <= /*#0.1*/ 16'd0;
      bf_idx1_d2_o[BF_AWIDTH-1:0] <= /*#0.1*/ 16'd0;
    end
  end

end
endgenerate

endmodule
