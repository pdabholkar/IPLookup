'''
Functions necessary for generating CRC of the IP prefix and key.
'''

import numpy as np
import sys,getopt,string

################################################################################
# Description : Procedure to generate a crc8 of the single input bit given the #
#               present state of the CRC shift register and the input bit      #
#               polynomial in use is CRC8 Maxim/Dallas Reversed polynomial     #
#               value is 0x8C                                                  #
# Input       : sr8   - in hex is the present state of the shift register      #
#               din   - single bit data input                                  #
# Output      : sr8                                                            #
################################################################################

def crc8(sr8,din):

  msb = sr8[7]
  din = int(din)

  sr8[7] = sr8[6]
  sr8[6] = sr8[5]
  sr8[5] = sr8[4] ^ msb ^ din
  sr8[4] = sr8[3] ^ msb ^ din
  sr8[3] = sr8[2]
  sr8[2] = sr8[1]
  sr8[1] = sr8[0]
  sr8[0] =          msb ^ din

  return sr8


################################################################################
# Description : Procedure to generate a crc16 of the single input bit given the#
#               present state of the CRC shift register and the input bit      #
#               polynomial in use is CRC16 CCITT Reversed polynomial value is  #
#               0x8408                                                         #
# Input       : sr16  - in hex is the present state of the shift register      #
#               din   - single bit data input                                  #
# Output      : sr16                                                           #
################################################################################
def crc16(sr16,din):

  msb = sr16[15]
  din = int(din)
  
  sr16[15] = sr16[14]
  sr16[14] = sr16[13]
  sr16[13] = sr16[12]
  sr16[12] = sr16[11] ^ msb ^ din
  sr16[11] = sr16[10]
  sr16[10] = sr16[9]
  sr16[9]  = sr16[8]
  sr16[8]  = sr16[7]
  sr16[7]  = sr16[6]
  sr16[6]  = sr16[5]
  sr16[5]  = sr16[4] ^ msb ^ din
  sr16[4]  = sr16[3]
  sr16[3]  = sr16[2]
  sr16[2]  = sr16[1]
  sr16[1]  = sr16[0]
  sr16[0]  =           msb ^ din

  return sr16


################################################################################
# Description : Procedure to generate a crc24 of the single input bit given the#
#               present state of the CRC shift register and the input bit      #
#               polynomial in use is CRC24 Reveresed polynomial is 0xD3B6BA    #
# Input       : sr24  - in hex is the present state of the shift register      #
#               din   - single bit data input                                  #
# Output      : sr24                                                           #
################################################################################
def crc16(sr24,din):

  msb = sr24[23]
  din = int(din)
  
  sr24[23] = sr24[22]
  sr24[22] = sr24[21] ^ msb ^ din
  sr24[21] = sr24[20]
  sr24[20] = sr24[19] ^ msb ^ din
  sr24[19] = sr24[18] ^ msb ^ din
  sr24[18] = sr24[17] ^ msb ^ din
  sr24[17] = sr24[16]
  sr24[16] = sr24[15] ^ msb ^ din
  sr24[15] = sr24[14]
  sr24[14] = sr24[13] ^ msb ^ din
  sr24[13] = sr24[12] ^ msb ^ din
  sr24[12] = sr24[11]
  sr24[11] = sr24[10] ^ msb ^ din
  sr24[10] = sr24[9]  ^ msb ^ din
  sr24[9]  = sr24[8]
  sr24[8]  = sr24[7]  ^ msb ^ din
  sr24[7]  = sr24[6]  ^ msb ^ din
  sr24[6]  = sr24[5]  ^ msb ^ din
  sr24[5]  = sr24[4]
  sr24[4]  = sr24[3]
  sr24[3]  = sr24[2]  ^ msb ^ din
  sr24[2]  = sr24[1]
  sr24[1]  = sr24[0]  ^ msb ^ din
  sr24[0]  =            msb ^ din

  return sr24


################################################################################
# Description : Procedure to generate a crc32 of the single input bit given the#
#               present state of the CRC shift register and the input bit      #
#               polynomial in use is CRC32 Reveresed polynomial is 0xEDB88320  #
# Input       : sr32  - in hex is the present state of the shift register      #
#               din   - single bit data input                                  #
# Output      : sr32                                                           #
################################################################################
def crc32(sr32,din):


  msb = sr32[31]
  din = int(din)
  
  sr32[31] = sr32[30]
  sr32[30] = sr32[29] 
  sr32[29] = sr32[28] 
  sr32[28] = sr32[27] 
  sr32[27] = sr32[26]
  sr32[26] = sr32[25] ^ msb ^ din  
  sr32[25] = sr32[24]
  sr32[24] = sr32[23] 
  sr32[23] = sr32[22] ^ msb ^ din 
  sr32[22] = sr32[21] ^ msb ^ din  
  sr32[21] = sr32[20]
  sr32[20] = sr32[19] 
  sr32[19] = sr32[18] 
  sr32[18] = sr32[17] 
  sr32[17] = sr32[16]
  sr32[16] = sr32[15] ^ msb ^ din  
  sr32[15] = sr32[14]
  sr32[14] = sr32[13] 
  sr32[13] = sr32[12] 
  sr32[12] = sr32[11] ^ msb ^ din 
  sr32[11] = sr32[10] ^ msb ^ din 
  sr32[10] = sr32[9]  ^ msb ^ din  
  sr32[9]  = sr32[8]
  sr32[8]  = sr32[7]  ^ msb ^ din  
  sr32[7]  = sr32[6]  ^ msb ^ din  
  sr32[6]  = sr32[5]  
  sr32[5]  = sr32[4]  ^ msb ^ din
  sr32[4]  = sr32[3]  ^ msb ^ din
  sr32[3]  = sr32[2]  
  sr32[2]  = sr32[1]  ^ msb ^ din
  sr32[1]  = sr32[0]  ^ msb ^ din
  sr32[0]  =            msb ^ din

  return sr32

def crc8_p(bfHashKey):
  sr8=bytearray(8)
  for i in bfHashKey:
    sr8 = crc8(sr8,i)

  sr8 = ''.join(str(j) for j in [int(i) for i in sr8])

  bf1 = sr8[0:6]
  bf2 = sr8[2:8]

  return int(sr8,base=2),int(bf1,base=2),int(bf2,base=2)
