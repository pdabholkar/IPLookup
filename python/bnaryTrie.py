class node:
	def __init__(self):
		self.data = None	# Data
		self.nxt  = None	# Link to the left child

class tnode:
	def __init__(self):
		self.prefix = 0
		self.plen   = 0
		self.data  = "x"  # Data
		self.left  = None # Left child
		self.right = None # Right child


class linked_list:
	def __init__(self):
		new_node = node()
		new_node.data = 0
		self.CurNode = new_node

	def getNode(self):
		return self.CurNode


	def add_node(self,data):
		head = self.CurNode
		while(head):
			if(head.data == None):
				head.data = data
				head = head.nxt
			elif(head.data == data):
				head = None
			else:
				if(head.nxt == None):
					new_node = node()
					new_node.data = data
					head.nxt = new_node
					head = None
				else:
					next_node = head.nxt
					if ((head.data < data ) & (next_node.data > data)):
						new_node = node()
						new_node.data = data
						head.nxt = new_node
						new_node.nxt = next_node
						head = None
					elif ((head.data < data ) & (next_node.data <= data)):
						head = next_node
			print("-----------------")


	def list_print(self):
		print("=================")
		node = self.CurNode
		while node:
			print(node.data)
			node = node.nxt
		print("=================")
		node = self.CurNode

class prefix_trie:
	depth = 32
	def __init__(self):
		new_tnode = tnode()
		new_tnode.prefix= 0
		new_tnode.plen  = 0
		new_tnode.data  = "x"  # No data
		new_tnode.left  = None # No left child
		new_tnode.right = None # No right child
		self.CurNode = new_tnode

	def prefix_bit_test(prefix,bit_offset):
		mask = 1 << (prefix_trie.depth - bit_offset)
		return (prefix & mask)

	def add_tnode(self,prefix,pfx_len,data):
		head = self.CurNode
		offset = 1
		prefix = prefix & (0xffffffff << (prefix_trie.depth-pfx_len))
		print("prefix : 0x{:0>8x}/{:<2d} 0b{:0>32b} || nexthop: 0x{:0>8x}" .format(prefix, pfx_len,prefix,data))
		while(head):
			mask = ((0xffffffff << (prefix_trie.depth - offset)) & 0xffffffff)
			if(offset <= pfx_len):  
				if(prefix_trie.prefix_bit_test(prefix,offset)):
					if(head.right == None):
						#print("R{:<3d}".format(offset),end="")
						new_tnode 			 = tnode()
						head.right       = new_tnode # Link to the new node on the right 
						new_tnode.prefix = (prefix & mask)
						new_tnode.plen   = offset   
						new_tnode.left   = None
						new_tnode.right  = None
						if(offset == pfx_len):
							new_tnode.data = data
							head = None
						else:
							new_tnode.data = "x" 
							head = head.right
							offset += 1
					else:
						head=head.right
						offset += 1
				else:
					if(head.left == None):
						#print("L{:<3d}".format(offset),end="")
						new_tnode 			 = tnode()
						head.left        = new_tnode # Link to the new node on the left 
						new_tnode.prefix = (prefix & mask)
						new_tnode.plen   = offset   
						new_tnode.left   = None
						new_tnode.right  = None
						if(offset == pfx_len):
							new_tnode.data = data
							head 					 = None
						else:
							new_tnode.data = "x" 
							head 					 = head.left
							offset 				+= 1
					else:
						head=head.left
						offset += 1
			else: # if offset > prefix length
				head = None
		# While loop ends

	def print_trie_inorder(self,head,level):
		if(head.data != "x"):
			#for i in range(level):
			#	print("   ",end="")
			print(" {:0>8x}/{:<2d} 0x{:0>8x}" .format(head.prefix, head.plen, head.data))
		else:
			print("")

		if(head.left):
			for i in range(level):
				print("   ",end="")
			print("L{:<3d}" .format(level),end="")
			pt.print_trie_inorder(head.left,(level+1))

		if(head.right):
			for i in range(level):
				print("   ",end="")
			print("R{:<3d}" .format(level),end="")
			pt.print_trie_inorder(head.right,(level+1))

def ipdec2hex(ip):
	ip_cmp = ip.split(".")
	iphex = 0
	for i in range(4):
		iphex = ((iphex << 8) | int(ip_cmp[i])) & 0xFFFFFFFF
	return iphex


#Creating and printing the prefix trie

file = open("route_file_large.txt", "r")

pt = prefix_trie()
for line in file:
	words = line.split()
	prefix,plen = words[0].split("/")
	nexthop = words[1] 
	prefix_int = ipdec2hex(prefix)
	nexthop_int = ipdec2hex(nexthop)
	pt.add_tnode(prefix_int,int(plen),nexthop_int)

print("\nNode add done")
	

#pt = prefix_trie()
#pt.add_tnode(0x0F,6,1)
#pt.add_tnode(0x3F,4,2)
#pt.add_tnode(0x2F,4,3)
#pt.add_tnode(0xBF,2,4)
#pt.add_tnode(0x00,2,5)
#
head = pt.CurNode
print("0")
pt.print_trie_inorder(head,1)
		
#    # Creating and printing the Linked list
#    ll = linked_list()
#    ll.add_node(100)
#    ll.list_print()
#    ll.add_node(1000)
#    ll.list_print()
#    ll.add_node(200)
#    ll.list_print()
#    ll.add_node(250)
#    ll.list_print()
#    ll.add_node(1000)
#    ll.list_print()
#    ll.add_node(1100)
#    ll.list_print()
#    ll.add_node(1010)
#    ll.list_print()
#    ll.add_node(61)
#    ll.list_print()
#    ll.add_node(51)
#    ll.list_print()
