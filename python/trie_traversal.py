import math
import numpy as np
import compact_trie as ct
import crc_ser as crc

################################################################################
# Trie traversal with recursion for creating memMap from cTrie                 #
################################################################################
def tt_wo_recurse(tdepth,tlevel,cTrie,curNodeAddr,memMap,curMemMapAddr,maxNodesperDepth):
  done = 0
  ct_stack = []
  lr_stack = []
  mm_stack = []
  td_stack = []
  cur_lr   = ''

  while (done == 0):
    ct_node = cTrie[curNodeAddr]
    mm_node = memMap[tdepth][curMemMapAddr]

    ## Debug prints
    #fname4 = open("tt.log","a")
    #fname4.write("{:>3}".format(tdepth))
    #for i in range(tdepth):
    #  fname4.write("-")
    #fname4.write("{} <-{}".format(curNodeAddr,ct_node))
    #fname4.close()

    #fname5=open("memMap.log","a")
    #fname5.write("{:>3}".format(tdepth))
    #for i in range(tdepth):
    #  fname5.write("-")
    #fname5.write("{} <-{} ".format(curMemMapAddr,mm_node))
    #fname5.close()

    # If left-right flag is empty it implies this is the first time we are visiting this node, 
    # so we need to process the node information and copy it to memMamp
    if (cur_lr == ''):

      mm_node[0] = ct_node[0]  # Copy the prefix information from cTrie to memMap at current tdepth
      mm_node[1] = 0x7ffff     # LC pointer is 0xfffff := NULL
      mm_node[2] = 0x7ffff     # LC pointer is 0xfffff := NULL
      mm_node[3] = 0x1         # Set the node used bit in field [3] of the node info
      ct_node[4] = (tdepth,tlevel)

      ## Debug prints
      #fname4 = open("tt.log","a")
      #fname4.write("Node processed")
      #fname4.close()
      #fname5=open("memMap.log","a")
      #fname5.write("Node processed")
      #fname5.close()

    # If left child exists and current left-right flag is empty i.e. neither left
    # nor right child has been handled at this level
    if ((ct_node[1] != 0) and (cur_lr != 'l') and (cur_lr != 'r')):
      # Push present ct_node address to ct_stack
      ct_stack.append(curNodeAddr)
      # Push 'l' branch taken status to lr_stack
      lr_stack.append('l')
      # Push present tdepth to td_stack
      td_stack.append(tdepth)
      # Push present mm_node address to mm_stack
      mm_stack.append(curMemMapAddr)

      # Find a free location in memMap in next tdepth
      freeLoc = [memMap[tdepth+1][i][3] for i in range(maxNodesperDepth)].index(0)   
      # Update the lc pointer of present mm_node as this free location 
      mm_node[1] = freeLoc          
      # Increment tdepth 
      tdepth += 1
      # If epsilon link, then set the epsilon used field in memMap
      if (ct_node[3] == 1):
        mm_node[3] |= 0x2
      else:
        tlevel += 1

      # Load left child address as curNodeAddr
      curNodeAddr = ct_node[1]
      # and mm_node's left child i.e. freeLoc as curMemMapAddr
      curMemMapAddr = mm_node[1]
      cur_lr = ''

      ## Debug prints
      #fname4 = open("tt.log","a")
      #fname5 = open("memMap.log","a")
      #if (ct_node[3] != 1):
      #  fname4.write(" L to tdepth {} at {}\n".format(tdepth,curNodeAddr))
      #  fname5.write(" L to tdepth {} at {} memMap is now {}\n".format(tdepth,curMemMapAddr,mm_node))
      #else:
      #  fname4.write(" E to tdepth {} at {}\n".format(tdepth,curNodeAddr))
      #  fname5.write(" E to tdepth {} at {} memMap is now {}\n".format(tdepth,curMemMapAddr,mm_node))
      #fname4.close()
      #fname5.close()



    # If right child exists and current left-right flag is not 'r' i.e.
    # right child has not been  handled at this level
    elif ((ct_node[2] != 0) and (cur_lr != 'r')):
      # Push present ct_node address to ct_stack
      ct_stack.append(curNodeAddr)
      # Push 'r' branch taken status to lr_stack
      lr_stack.append('r')
      # Push present tdepth to td_stack
      td_stack.append(tdepth)
      # Push present mm_node address to mm_stack
      mm_stack.append(curMemMapAddr)

      # Find a free location in memMap in next tdepth
      freeLoc = [memMap[tdepth+1][i][3] for i in range(maxNodesperDepth)].index(0)   
      # Update the rc pointer of present mm_node as this free location 
      mm_node[2] = freeLoc          
      # Increment tdepth 
      tdepth += 1
      tlevel += 1

      # Load right child address as curNodeAddr
      curNodeAddr = ct_node[2]
      # and mm_node's right child i.e. freeLoc as curMemMapAddr
      curMemMapAddr = mm_node[2]
      cur_lr = ''

      ## Debug prints
      #fname4 = open("tt.log","a")
      #fname5 = open("memMap.log","a")
      #if (ct_node[3] != 1):
      #  fname4.write(" R to tdepth {} at {}\n".format(tdepth,curNodeAddr))
      #  fname5.write(" R to tdepth {} at {} memMap is now {}\n".format(tdepth,curMemMapAddr,mm_node))
      #else:
      #  fname4.write(" E to tdepth {} at {}\n".format(tdepth,curNodeAddr))
      #  fname5.write(" E to tdepth {} at {} memMap is now {}\n".format(tdepth,curMemMapAddr,mm_node))
      #fname4.close()
      #fname5.close()

    else:
      #print(len(lr_stack))
      if (len(lr_stack) == 0):
        done = 1
      else:
        # Pop the address on the ct_stack as curNodeAddr i.e. going back up one level
        curNodeAddr = ct_stack.pop()
        # Pop the address on the mm_stack as curMemMapAddr i.e. going back up one level 
        curMemMapAddr = mm_stack.pop()
        cur_lr = lr_stack.pop()
        # Decrement tdepth
        tdepth -=1
        if (cTrie[curNodeAddr][3] != 1):
          tlevel -= 1

        ## Debug prints
        #fname4 = open("tt.log","a")
        #fname4.write(" \nback from {} to tdepth {} at {}".format(cur_lr,tdepth,curNodeAddr))
        #fname4.close()

        #fname5 = open("memMap.log","a")
        #fname5.write(" \nback from {} to tdepth {} at {}".format(cur_lr,tdepth,curMemMapAddr))
        #fname5.close()
    
################################################################################
# Create the Bloom filter                                                      #
################################################################################
def tt_bf_wo_recurse(tdepth,tlevel,cTrie,curNodeAddr,maxNodesperDepth,bf):
  done = 0
  ct_stack = []
  lr_stack = []
  mm_stack = []
  td_stack = []
  cur_lr   = ''  # Current Left-right branch taken history stored in a string
  bfapp    = ''  # Active part of prefix for BF calculation

  while (done == 0):
    ct_node = cTrie[curNodeAddr]

    # If left-right flag is empty it implies this is the first time we are visiting this node, 
    # so we need to process the node information and copy it to memMamp
    #fname = open("bfcreatedump.txt","a")
    #fname.write("lr_stack = {} and cur_lr = {}\n".format(lr_stack,cur_lr))
    if (cur_lr == ''):
      if(bf[tdepth] != bytearray(b'')):                   # Do this only if the bloom filter required at this level is non-zero
        if(ct_node[0] !=0):                               # and the prefix information exists
          applen = len(bfapp)
          for pfx in ct_node[0]:
            msbp  = pfx[0]
            lsbp  = pfx[1]
            xlenp = pfx[2]
            zlenp = pfx[3]
            bfsize = len(bf[tdepth])
            bfHashKey = ''.join("{:1b}{:03b}{}{:1b}".format(int(msbp),int(xlenp),bfapp,int(lsbp))) 
            bfidxlen = int(math.ceil(math.log((bfsize-1),2)))

            sr = bytearray(32);
            for i in bfHashKey:
              sr = crc.crc32(sr,i)
            
            sr = ''.join(str(j) for j in [int(i) for i in sr])

            # For 64 bit CRC
            #sr=sr+sr

            bf[tdepth][int(sr[0:bfidxlen],base=2)] = 1          # First BF index

            bf[tdepth][int(sr[8:(8+bfidxlen)],base=2)] = 1     # Second BF index for 32 bit CRC
            #bf[tdepth][int(sr[16:(16+bfidxlen)],base=2)] = 1    # Second BF index for 64 bit CRC

            #bf[tdepth][int(sr[32:(32+bfidxlen)],base=2)] = 1    # Third BF index for 64 bit CRC

            #fname = open("bfcreatedump.txt","a")
            #fname.write("{:2} {:2} {:1} {:1} {:3} {:5} {:32} {:32} {:32} {:2} {:3} {:3}\n".format(tdepth,tlevel,msbp,lsbp,xlenp,zlenp,bfapp,bfHashKey,sr,bfidxlen,int(sr[0:bfidxlen],base=2),int(sr[8:(8+bfidxlen)],base=2)))
            #fname.close()
        else:
          pass
      else:
        pass

    # If left child exists and current left-right flag is empty i.e. neither left
    # nor right child has been handled at this level
    if ((ct_node[1] != 0) and (cur_lr != 'l') and (cur_lr != 'r')):
      # Push present ct_node address to ct_stack
      ct_stack.append(curNodeAddr)
      # Push 'l' branch taken status to lr_stack
      lr_stack.append('l')
      # Push present tdepth to td_stack
      td_stack.append(tdepth)

      # Increment tdepth 
      tdepth += 1
      # If epsilon link, then set the epsilon used field in memMap
      if (ct_node[3] == 1):
        pass
      else:
        bfapp += '0'
        tlevel += 1

      # Load left child address as curNodeAddr
      curNodeAddr = ct_node[1]
      cur_lr = ''

    # If right child exists and current left-right flag is not 'r' i.e.
    # right child has not been  handled at this level
    elif ((ct_node[2] != 0) and (cur_lr != 'r')):
      # Push present ct_node address to ct_stack
      ct_stack.append(curNodeAddr)
      # Push 'r' branch taken status to lr_stack
      lr_stack.append('r')
      # Push present tdepth to td_stack
      td_stack.append(tdepth)

      # Increment tdepth 
      bfapp += '1'
      tdepth += 1
      tlevel += 1

      # Load right child address as curNodeAddr
      curNodeAddr = ct_node[2]
      cur_lr = ''

    else:
      #print(len(lr_stack))
      if (len(lr_stack) == 0):
        done = 1
      else:
        # Pop the address on the ct_stack as curNodeAddr i.e. going back up one level
        curNodeAddr = ct_stack.pop()
        cur_lr = lr_stack.pop()
        # Decrement tdepth
        tdepth -=1
        if (cTrie[curNodeAddr][3] != 1):
          tlevel -= 1
          bfapp = bfapp[:-1]
  return bf
    

################################################################################
# Trie traversl without recursion for creating memMap from cTrie               #
################################################################################
def tt_w_recurse(tdepth,tlevel,cTrie,curNodeAddr,linkbackAddr,memMap,curMemMapAddr,linkbackMemMapAddr,maxNodesperDepth):
  
  # Save Node information
  curNode = cTrie[curNodeAddr]
  curNode[4] = (tdepth,tlevel)

  memMap[tdepth][curMemMapAddr][0] = cTrie[curNodeAddr][0]  # Copy the prefix information from cTrie to memMap at current tdepth
  memMap[tdepth][curMemMapAddr][1] = 0x7ffff                # LC pointer is 0xfffff := NULL`
  memMap[tdepth][curMemMapAddr][2] = 0x7ffff                # LC pointer is 0xfffff := NULL`
  memMap[tdepth][curMemMapAddr][3] = 0x1                    # Set the node used bit in field [3] of the node info                             

  ## Debug prints
  #fname4 = open("tt.log","a")
  #fname4.write("\n")
  #for i in range(tdepth):
  #  fname4.write("-")
  #fname4.write("{} <-{} {} ".format(curNodeAddr,linkbackAddr,curNode))
  #fname4.close()

  #fname5=open("memMap.log","a")
  #fname5.write("\n")
  #for i in range(tdepth):
  #  fname5.write("-")
  #fname5.write("{} <-{} {} ".format(curMemMapAddr,linkbackMemMapAddr,memMap[tdepth][curMemMapAddr]))
  #fname5.close()

  # Taking the left pointer
  if (curNode[1] != 0):
    curLinkbackAddr = curNodeAddr                       # Storing the current node address to link back
    curNodeAddr = curNode[1]                            # loading the next node address as current node address.

    freeLoc = [memMap[tdepth+1][i][3] for i in range(maxNodesperDepth)].index(0)
                                                        # Find the first empty location in the next depth in memMap
    memMap[tdepth][curMemMapAddr][1] = freeLoc          # Update L-child pointer as this free location

    if (curNode[3] != 1):                               # Regular links 
      tdepth += 1                                       # increment depth and level
      tlevel += 1
    else:                                               # Epsilon links
      memMap[tdepth][curMemMapAddr][3] |= 0x2           # Set the epsilon used bit in field[3] of the node info
      tdepth += 1                                       # incremenet only depth and not the level

    ## Debug prints 
    #fname4 = open("tt.log","a")
    #fname5=open("memMap.log","a")
    #if (curNode[3] != 1):
    #  fname4.write(" L to tdepth {} and tlevel {} at {}".format(tdepth,tlevel,curNodeAddr))
    #  fname5.write(" L to tdepth {} from {} to {} memMap is now {}".format(tdepth,curMemMapAddr,freeLoc,memMap[tdepth-1][curMemMapAddr]))
    #else:
    #  fname4.write(" E to tdepth {} and tlevel {} at {}".format(tdepth,tlevel,curNodeAddr))
    #  fname5.write(" E to tdepth {} from {} to {} memMap is now {}".format(tdepth,curMemMapAddr,freeLoc,memMap[tdepth-1][curMemMapAddr]))
    #fname4.close()
    #fname5.close()

    # Recursive function call
    curNodeAddr,curMemMapAddr = tt_w_recurse(tdepth,tlevel,cTrie,curNodeAddr,curLinkbackAddr,memMap,freeLoc,curMemMapAddr,maxNodesperDepth) 
   
    # Back from the recursive function call 
    curNode = cTrie[curNodeAddr]                        # Resetting node addresses back to head node 
    if (curNode[3] != 1):                               # Regular link 
      tdepth -= 1                                       # decrement depth and level
      tlevel -= 1
    else:                                               # Epsilon links
      tdepth -=1                                        # decrement only depth and not level
    
    ## Debug prints
    #fname4 = open("tt.log","a")
    #fname4.write(" back from L to tdepth {} and tlevel {} at {}".format(tdepth,tlevel,curNodeAddr))
    #fname4.close()

    #fname5 = open("memMap.log","a")
    #fname5.write(" back from L to tdepth {} at {}".format(tdepth,curMemMapAddr))
    #fname5.close()
    

  # Taking the right pointer
  if (curNode[2] != 0):
    curLinkbackAddr = curNodeAddr                       # Storing the current node address to link back
    curNodeAddr = curNode[2]                            # loading the next node address as current node address.

    freeLoc = [memMap[tdepth+1][i][3] for i in range(maxNodesperDepth)].index(0)   
                                                        # Find the first empty location in the next depth in memMap
    memMap[tdepth][curMemMapAddr][2] = freeLoc          # Update R-child pointer as this free location

    tdepth += 1                                         # increment depth and level
    tlevel += 1                                                                                 
    
    ## Debug prints
    #fname4 = open("tt.log","a")
    #fname4.write(" R to tdepth {} and tlevel {} at {}".format(tdepth,tlevel,curNodeAddr))
    #fname4.close()

    #fname5=open("memMap.log","a")
    #fname5.write(" R to tdepth {} from {} to {} memMap is now {}".format(tdepth,curMemMapAddr,freeLoc,memMap[tdepth-1][curMemMapAddr]))
    #fname5.close()

    # Recursive function call
    curNodeAddr,curMemMapAddr = tt_w_recurse(tdepth,tlevel,cTrie,curNodeAddr,curLinkbackAddr,memMap,freeLoc,curMemMapAddr,maxNodesperDepth) 
    
    # Back from the recursive function call
    curNode = cTrie[curNodeAddr]                        # Resetting node addresses back to head node 
    if (curNode[3] != 1):                               # Regular link 
      tdepth -= 1                                       # decrement depth and level
      tlevel -= 1
    else:                                               # Epsilon links
      tdepth -=1                                        # decrement only depth and not level

    # Debug prints
    #fname4 = open("tt.log","a") 
    #fname4.write(" back from R to tdepth {} and tlevel {} at {}".format(tdepth,tlevel,curNodeAddr))
    #fname4.close()

    #fname5 = open("memMap.log","a")
    #fname5.write(" back from R to tdepth {} at {}".format(tdepth,curMemMapAddr))
    #fname5.close()

  ## Finished with the head and also both the left and right branches 
  ## Debug prints
  #fname4 = open("tt.log","a")
  #fname4.write(" going back up to {}\n".format(linkbackAddr))
  #fname4.close()

  #fname5 = open("memMap.log","a")
  #fname5.write(" going back up to {}\n".format(linkbackMemMapAddr))
  #fname5.close()

  return linkbackAddr,linkbackMemMapAddr


