function integer clogb2;
  input [31:0] value;
  begin
    if(value > 1)
    begin
      value = value - 1;
      for (clogb2 = 0; value > 0; clogb2 = clogb2 + 1)
      begin
        value = value >> 1;
      end
    end
    else
      clogb2 = 1;
  end
endfunction

localparam MEM_SIZE = 
(TDEPTH ==  0) ?      1 : (
(TDEPTH ==  1) ?      2 : (
(TDEPTH ==  2) ?      4 : (
(TDEPTH ==  3) ?      8 : (
(TDEPTH ==  4) ?     15 : (
(TDEPTH ==  5) ?     22 : (
(TDEPTH ==  6) ?     31 : (
(TDEPTH ==  7) ?     45 : (
(TDEPTH ==  8) ?     60 : (
(TDEPTH ==  9) ?     72 : (
(TDEPTH == 10) ?     84 : (
(TDEPTH == 11) ?     92 : (
(TDEPTH == 12) ?    114 : (
(TDEPTH == 13) ?    139 : (
(TDEPTH == 14) ?    174 : (
(TDEPTH == 15) ?    221 : (
(TDEPTH == 16) ?    276 : (
(TDEPTH == 17) ?    291 : (
(TDEPTH == 18) ?    390 : (
(TDEPTH == 19) ?    517 : (
(TDEPTH == 20) ?    718 : (
(TDEPTH == 21) ?    908 : (
(TDEPTH == 22) ?    807 : (
(TDEPTH == 23) ?    264 : (
(TDEPTH == 24) ?    247 : (
(TDEPTH == 25) ?    167 : (
(TDEPTH == 26) ?     89 : (
(TDEPTH == 27) ?     35 : (
(TDEPTH == 28) ?     20 : (
(TDEPTH == 29) ?      2 : (
(TDEPTH == 30) ?      3 : (
(TDEPTH == 31) ?      3 : (
0))))))))))))))))))))))))))))))));

localparam MEM_AWIDTH = 
(TDEPTH ==  0) ? clogb2(     1) + 1 : (
(TDEPTH ==  1) ? clogb2(     2) + 1 : (
(TDEPTH ==  2) ? clogb2(     4) + 1 : (
(TDEPTH ==  3) ? clogb2(     8) + 1 : (
(TDEPTH ==  4) ? clogb2(    15) + 1 : (
(TDEPTH ==  5) ? clogb2(    22) + 1 : (
(TDEPTH ==  6) ? clogb2(    31) + 1 : (
(TDEPTH ==  7) ? clogb2(    45) + 1 : (
(TDEPTH ==  8) ? clogb2(    60) + 1 : (
(TDEPTH ==  9) ? clogb2(    72) + 1 : (
(TDEPTH == 10) ? clogb2(    84) + 1 : (
(TDEPTH == 11) ? clogb2(    92) + 1 : (
(TDEPTH == 12) ? clogb2(   114) + 1 : (
(TDEPTH == 13) ? clogb2(   139) + 1 : (
(TDEPTH == 14) ? clogb2(   174) + 1 : (
(TDEPTH == 15) ? clogb2(   221) + 1 : (
(TDEPTH == 16) ? clogb2(   276) + 1 : (
(TDEPTH == 17) ? clogb2(   291) + 1 : (
(TDEPTH == 18) ? clogb2(   390) + 1 : (
(TDEPTH == 19) ? clogb2(   517) + 1 : (
(TDEPTH == 20) ? clogb2(   718) + 1 : (
(TDEPTH == 21) ? clogb2(   908) + 1 : (
(TDEPTH == 22) ? clogb2(   807) + 1 : (
(TDEPTH == 23) ? clogb2(   264) + 1 : (
(TDEPTH == 24) ? clogb2(   247) + 1 : (
(TDEPTH == 25) ? clogb2(   167) + 1 : (
(TDEPTH == 26) ? clogb2(    89) + 1 : (
(TDEPTH == 27) ? clogb2(    35) + 1 : (
(TDEPTH == 28) ? clogb2(    20) + 1 : (
(TDEPTH == 29) ? clogb2(     2) + 1 : (
(TDEPTH == 30) ? clogb2(     3) + 1 : (
(TDEPTH == 31) ? clogb2(     3) + 1 : (
2))))))))))))))))))))))))))))))));

localparam CP_WIDTH = 
(TDEPTH ==  0) ? clogb2(     2) + 1 : (
(TDEPTH ==  1) ? clogb2(     4) + 1 : (
(TDEPTH ==  2) ? clogb2(     8) + 1 : (
(TDEPTH ==  3) ? clogb2(    15) + 1 : (
(TDEPTH ==  4) ? clogb2(    22) + 1 : (
(TDEPTH ==  5) ? clogb2(    31) + 1 : (
(TDEPTH ==  6) ? clogb2(    45) + 1 : (
(TDEPTH ==  7) ? clogb2(    60) + 1 : (
(TDEPTH ==  8) ? clogb2(    72) + 1 : (
(TDEPTH ==  9) ? clogb2(    84) + 1 : (
(TDEPTH == 10) ? clogb2(    92) + 1 : (
(TDEPTH == 11) ? clogb2(   114) + 1 : (
(TDEPTH == 12) ? clogb2(   139) + 1 : (
(TDEPTH == 13) ? clogb2(   174) + 1 : (
(TDEPTH == 14) ? clogb2(   221) + 1 : (
(TDEPTH == 15) ? clogb2(   276) + 1 : (
(TDEPTH == 16) ? clogb2(   291) + 1 : (
(TDEPTH == 17) ? clogb2(   390) + 1 : (
(TDEPTH == 18) ? clogb2(   517) + 1 : (
(TDEPTH == 19) ? clogb2(   718) + 1 : (
(TDEPTH == 20) ? clogb2(   908) + 1 : (
(TDEPTH == 21) ? clogb2(   807) + 1 : (
(TDEPTH == 22) ? clogb2(   264) + 1 : (
(TDEPTH == 23) ? clogb2(   247) + 1 : (
(TDEPTH == 24) ? clogb2(   167) + 1 : (
(TDEPTH == 25) ? clogb2(    89) + 1 : (
(TDEPTH == 26) ? clogb2(    35) + 1 : (
(TDEPTH == 27) ? clogb2(    20) + 1 : (
(TDEPTH == 28) ? clogb2(     2) + 1 : (
(TDEPTH == 29) ? clogb2(     3) + 1 : (
(TDEPTH == 30) ? clogb2(     3) + 1 : (
2)))))))))))))))))))))))))))))));

localparam PFX_INIT_FILE = 
(TDEPTH ==  0) ? "pfxMap0.mem" : (
(TDEPTH ==  1) ? "pfxMap1.mem" : (
(TDEPTH ==  2) ? "pfxMap2.mem" : (
(TDEPTH ==  3) ? "pfxMap3.mem" : (
(TDEPTH ==  4) ? "pfxMap4.mem" : (
(TDEPTH ==  5) ? "pfxMap5.mem" : (
(TDEPTH ==  6) ? "pfxMap6.mem" : (
(TDEPTH ==  7) ? "pfxMap7.mem" : (
(TDEPTH ==  8) ? "pfxMap8.mem" : (
(TDEPTH ==  9) ? "pfxMap9.mem" : (
(TDEPTH == 10) ? "pfxMap10.mem" : (
(TDEPTH == 11) ? "pfxMap11.mem" : (
(TDEPTH == 12) ? "pfxMap12.mem" : (
(TDEPTH == 13) ? "pfxMap13.mem" : (
(TDEPTH == 14) ? "pfxMap14.mem" : (
(TDEPTH == 15) ? "pfxMap15.mem" : (
(TDEPTH == 16) ? "pfxMap16.mem" : (
(TDEPTH == 17) ? "pfxMap17.mem" : (
(TDEPTH == 18) ? "pfxMap18.mem" : (
(TDEPTH == 19) ? "pfxMap19.mem" : (
(TDEPTH == 20) ? "pfxMap20.mem" : (
(TDEPTH == 21) ? "pfxMap21.mem" : (
(TDEPTH == 22) ? "pfxMap22.mem" : (
(TDEPTH == 23) ? "pfxMap23.mem" : (
(TDEPTH == 24) ? "pfxMap24.mem" : (
(TDEPTH == 25) ? "pfxMap25.mem" : (
(TDEPTH == 26) ? "pfxMap26.mem" : (
(TDEPTH == 27) ? "pfxMap27.mem" : (
(TDEPTH == 28) ? "pfxMap28.mem" : (
(TDEPTH == 29) ? "pfxMap29.mem" : (
(TDEPTH == 30) ? "pfxMap30.mem" : (
(TDEPTH == 31) ? "pfxMap31.mem" : (
0))))))))))))))))))))))))))))))));

localparam LRC_INIT_FILE = 
(TDEPTH ==  0) ? "lrcMap0.mem" : (
(TDEPTH ==  1) ? "lrcMap1.mem" : (
(TDEPTH ==  2) ? "lrcMap2.mem" : (
(TDEPTH ==  3) ? "lrcMap3.mem" : (
(TDEPTH ==  4) ? "lrcMap4.mem" : (
(TDEPTH ==  5) ? "lrcMap5.mem" : (
(TDEPTH ==  6) ? "lrcMap6.mem" : (
(TDEPTH ==  7) ? "lrcMap7.mem" : (
(TDEPTH ==  8) ? "lrcMap8.mem" : (
(TDEPTH ==  9) ? "lrcMap9.mem" : (
(TDEPTH == 10) ? "lrcMap10.mem" : (
(TDEPTH == 11) ? "lrcMap11.mem" : (
(TDEPTH == 12) ? "lrcMap12.mem" : (
(TDEPTH == 13) ? "lrcMap13.mem" : (
(TDEPTH == 14) ? "lrcMap14.mem" : (
(TDEPTH == 15) ? "lrcMap15.mem" : (
(TDEPTH == 16) ? "lrcMap16.mem" : (
(TDEPTH == 17) ? "lrcMap17.mem" : (
(TDEPTH == 18) ? "lrcMap18.mem" : (
(TDEPTH == 19) ? "lrcMap19.mem" : (
(TDEPTH == 20) ? "lrcMap20.mem" : (
(TDEPTH == 21) ? "lrcMap21.mem" : (
(TDEPTH == 22) ? "lrcMap22.mem" : (
(TDEPTH == 23) ? "lrcMap23.mem" : (
(TDEPTH == 24) ? "lrcMap24.mem" : (
(TDEPTH == 25) ? "lrcMap25.mem" : (
(TDEPTH == 26) ? "lrcMap26.mem" : (
(TDEPTH == 27) ? "lrcMap27.mem" : (
(TDEPTH == 28) ? "lrcMap28.mem" : (
(TDEPTH == 29) ? "lrcMap29.mem" : (
(TDEPTH == 30) ? "lrcMap30.mem" : (
(TDEPTH == 31) ? "lrcMap31.mem" : (
0))))))))))))))))))))))))))))))));

localparam TOT_DEPTH = 32;
