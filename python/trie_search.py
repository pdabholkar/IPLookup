'''
Implements a Bloom Filter using the Jenkins hash function
Size of Bloom Filter (m) 1,000,000
Number of hashes     (k)         2
Number of entries    (n)   500,000 
supported

The program may be extended in the future to use the Murmur or FNV negedge or CRC hash 

The input is fed using a BGP RIB file that has entries listed in the following format

=================================
BGP table version is 0, local router ID is 74.80.127.5
Status codes: s suppressed, d damped, h history, * valid, > best, i - internal,
              r RIB-failure, S Stale, R Removed
              Origin codes: i - IGP, e - EGP, ? - incomplete

Network          Next Hop            Metric LocPrf Weight Path
*> 2.16.36.0/24     218.100.52.6             0             0 4826 12222 12222 12222 i
*                   218.100.52.6             0             0 4826 12222 12222 12222 i
*  5.10.21.0/24     218.100.52.119           0             0 63956 i
*>                  218.100.52.119           0             0 63956 i
*  5.10.64.0/19     218.100.52.100           0             0 36351 i
*                   218.100.52.100           0             0 36351 i
*>                  218.100.52.100           0             0 36351 i
*  5.10.96.0/19     218.100.52.100           0             0 36351 i
*                   218.100.52.100           0             0 36351 i
*>                  218.100.52.100           0             0 36351 i
*> 5.62.21.0/24     218.100.52.100           0             0 36351 i
*                   218.100.52.100           0             0 36351 i
=================================

These entries need to parsed and entered into the Bloom filter and the false positive probability needs to be evaluated.
'''

import os
import math
import numpy as np
import sys,getopt
import binary_trie as bt
import compact_trie as ct
import trie_traversal as tt
import time


def bincount_percent(a):

  abin = np.bincount(np.asarray(a))
  asum = float(np.sum(abin))
  anorm = abin/asum*100
  return anorm
 
def bincount_percent_pad(a,oplen):
  anorm = bincount_percent(a)
  if (len(anorm) > oplen):
    raise Exception('Length of output array is insufficient')
  
  apad = np.pad(anorm, (0, oplen - len(anorm)%oplen), 'constant')
  return(apad)

def main():  

  maxdepth = 80
  maxNodesperDepth = 100000
  np.set_printoptions(precision=3,linewidth=100000,suppress='true')

  # Processing of the routing table to enable insertion into a binary trie.
  if (len(sys.argv) > 2):
    fname1 = sys.argv[1] # The routing table file name is supplied as a command line argument
    opdirname = sys.argv[2]
  else:  
    print("Incorrect number of arguments supplied")  
    print("Usage : bloom_filter.py <routing table file> <output_directory>")
    sys.exit(0)

  if not os.path.exists(opdirname):
    os.makedirs(opdirname)

  try:
    os.remove("tt.log")
  except OSError:
    pass

  try:
    os.remove("memMap.log")
  except OSError:
    pass

  try:
    os.remove("search_dump.log")
  except OSError:
    pass

  try:
    os.remove("bloomfilter.txt")
  except OSError:
    pass

  # Regular expression to be used for parsing the routing table
  regexp = r"([0-9]*\.[0-9]*\.[0-9]*\.[\/.0-9]*)\ *([0-9]*\.[.0-9]*)\ *.*"
  # Parse the routing table file. First entry is the prefix w/ len. Second entry is NHI
  output = np.fromregex(fname1,regexp,'str')
  prefixtable = output[:,0]
  nhtable     = output[:,1]
  
  # Split the first entry into the prefix and prefix length and 
  prefixtable = np.char.split(prefixtable,sep='/')
  prefixiptable = [prefix[0] for prefix in prefixtable]
  prefixiptable = np.array(prefixiptable)

  # Formatting the IP address into bytes and then to hex and finally to a binary value
  prefixiptablebytes = [ip.split('.') for ip in prefixiptable]
  prefixiptablebin = [(''.join('{:08b}'.format(int(ipbyte)) for ipbyte in ipaddr)) for ipaddr in prefixiptablebytes]
  prefixiptablebin = np.array(prefixiptablebin)

  nhtable = [nh.split('.') for nh in nhtable]
  nhtable = [(''.join('{:02x}'.format(int(nhbyte)) for nhbyte in nhip)) for nhip in nhtable]

  prefixlentable = [ ]
  for prefix in prefixtable:
    try:
      prefixlentable.append(prefix[1])
    except IndexError:
      prefixlentable.append(32)
  prefixlentable = np.array(prefixlentable)

  prefixlutable = np.column_stack((prefixiptablebin,prefixlentable,nhtable))


  for Ptrie in ([4]):#):#,2,3,5,10):
    for bfScaling in ([8]):#1,2,4,8,16):

      print("Binary and Compact Tries initialised")
      bTrie,btvecram = bt.inittrie()
      cTrie,ctvecram = ct.inittrie()
      bfsize = bfScaling * 2**int(math.ceil(math.log(len(prefixlutable),2)))

      bf = bytearray(bfsize) 
      #bf = 0 # at the moment we do not want the general one size fits all Bloom filter. The Bloom filter will be generated in the trie_traversal step
      blastram = 0
      clastram = 0
      prefixcnt = 0
      totalpfxMembits = 0
      totallrcMembits = 0

      print("Inserting prefixes from {} into the binary and compact tries (Ptrie = {}, bfScaling = {})".format(fname1,Ptrie,bfScaling))
      #print("Inserting prefixes from {} into the binary and compact tries (Ptrie = {}, bfScaling = 0)".format(fname1,Ptrie))

      #######################################################
      # Insert prefixes into Binary and Compact Trie
      #######################################################
      for prefix in prefixlutable:
        blastram,btvecram  = bt.insertitem(bTrie,prefix[0],prefix[1],prefix[2],blastram,btvecram)
        clastram,ctvecram  = ct.insertitem(bf,bfsize,cTrie,prefix[0],prefix[1],prefix[2],clastram,ctvecram,Ptrie)
        prefixcnt += 1
        #bfint = [int(i) for i in bf]
        #bfstr = ''.join(str(j) for j in bfint)
        #fbf = open("bloomfilter.txt","a")
        #fbf.write("{} \n\n bf entries {}\n".format(bfstr,bfint.count(1)))
        #fbf.close()

      #######################################################
      # Trie Traversal and memMap creation
      #######################################################
      tlevel=0 
      tdepth=0
      curNodeAddr=0
      linkbackAddr=0
      curMemMapAddr=0 
      linkbackMemMapAddr=0
      finalAddr=0
      finalMemMapAddr=0

      print("Insertion complete. Trie traversal and creating memMap structure now")
      memMap = np.zeros([maxdepth,maxNodesperDepth,4],dtype='int')
      memMap = memMap.tolist()

      t0 =time.time()
      #finalAddr,finalMemMapAddr = tt.tt_w_recurse(tdepth,tlevel,cTrie,curNodeAddr,linkbackAddr,memMap,curMemMapAddr,linkbackMemMapAddr,maxNodesperDepth)
      tt.tt_wo_recurse(tdepth,tlevel,cTrie,curNodeAddr,memMap,curMemMapAddr,maxNodesperDepth)
      print("Time taken is {}".format(time.time() - t0))
      print("Trie traversal complete memMap structure created")

      cTrie_depth = [cTrie[i][4] for i in range(len(cTrie))]                                    # 4th element of each node in the cTrie holds depth and level in the trie
      i = 0
      ct_depth = []                                                                             
      while(cTrie_depth[i] != 0):
        ct_depth.append(cTrie_depth[i][0])                                                      # 0th element of 4th field in each cTrie entry is the depth. 1st element is level 
        i += 1

      ct_depth_distr = np.bincount(np.asarray(ct_depth))                                        # histogram of number of nodes at each depth in the cTrie 
      ct_addrbits_perdepth = [(int(math.ceil(math.log(numentries,2))) + 1) for numentries in ct_depth_distr ] 
                                                                                                # list to hold number of bits required to represent all node at 
                                                                                                # certain depth in the cTrie. The +1 above is to ensure that the all-ones 
                                                                                                # entry is stored correctly.

      mm_pfx_per_depth = np.zeros(maxdepth).tolist()
      for i in range(maxdepth):
        for j in range(maxNodesperDepth):
          if(memMap[i][j][0] != 0):
            mm_pfx_per_depth[i] += len(memMap[i][j][0])
          if (memMap[i][j][3] == 0):
            break


      ##################################################
      # Bloom filter creation
      ##################################################
      bfsize_per_depth=[]                                                                       # create an array of bfsizes looking at the number of nodes at each depth
      for pfx_at_depth in mm_pfx_per_depth:
        bfsize_per_depth.append(bfScaling * 2**int(math.ceil(math.log(pfx_at_depth,2)))) if (pfx_at_depth != 0) else bfsize_per_depth.append(0)

      print(mm_pfx_per_depth)
      print(bfsize_per_depth)

      bfarray = [bytearray(bfsize_at_depth) for bfsize_at_depth in bfsize_per_depth]

      tdepth = 0 
      tlevel = 0
      curNodeAddr = 0

      bfarray = tt.tt_bf_wo_recurse(tdepth,tlevel,cTrie,curNodeAddr,maxNodesperDepth,bfarray)   # Call to Bloom filter creation function. The BF is generated using cTrie but is used with memMap


      ##################################################
      # Printing information to files
      ##################################################
      print("Writing bloom filter information to files")
      bfdepth_vs_tdepth_fname = str(opdirname)+"/bfdepth_vs_tdepth_"+str(opdirname)+".h"
      fname6 = open(bfdepth_vs_tdepth_fname,"w")
      fname6.write("localparam BF_SIZE = \n")
      for i in range(len(bfarray)):
        bfInitFname = str(opdirname)+"/bf"+str(i)+".mem"
        fname6.write("(TDEPTH == {:2}) ? {:6} : (\n".format(i,bfsize_per_depth[i]))
        fname7 = open(bfInitFname,"w")
        bfarrayInts = [j for j in bfarray[i]]
        for j in range(bfsize_per_depth[i]):
          fname7.write("@{:x} {:x}\n".format(j,bfarrayInts[j]))
        fname7.close()

      fname6.write("0")
      for i in range(len(bfarray)):
        fname6.write(")")

      fname6.write(";\n\nlocalparam BF_AWIDTH = \n")
      for i in range(len(bfarray)):
        fname6.write("(TDEPTH == {:2}) ? clogb2({:6}) : (\n".format(i,bfsize_per_depth[i]))
      fname6.write("0")
      for i in range(len(bfarray)):
        fname6.write(")")

      fname6.write(";\n\nlocalparam BF_INIT_FILE = \n")
      for i in range(len(bfarray)):
        fname6.write("(TDEPTH == {:2}) ? \"bf{}.mem\" : (\n".format(i,i))
      fname6.write("0")
      for i in range(len(bfarray)):
        fname6.write(")")
      fname6.write(";\n\n")
      fname6.close()


      if (Ptrie):# == 2):                                                                          # print to file for FPGA design  
        print("Writing memMap information to files")
        mdepth_vs_tdepth_fname = str(opdirname)+"/mdepth_vs_tdepth_"+str(opdirname)+".h"
        fname8=open(mdepth_vs_tdepth_fname,"w")
        fname8.write("function integer clogb2;\n")
        fname8.write("  input [31:0] value;\n")
        fname8.write("  begin\n")
        fname8.write("    if(value > 1)\n")
        fname8.write("    begin\n")
        fname8.write("      value = value - 1;\n")
        fname8.write("      for (clogb2 = 0; value > 0; clogb2 = clogb2 + 1)\n")
        fname8.write("      begin\n")
        fname8.write("        value = value >> 1;\n")
        fname8.write("      end\n")
        fname8.write("    end\n")
        fname8.write("    else\n")
        fname8.write("      clogb2 = 1;\n")
        fname8.write("  end\n")
        fname8.write("endfunction\n\n")

        fname8.write("localparam MEM_SIZE = \n")

      for i in range(len(ct_depth_distr)):                                                      # for each valid depth in the cTrie
        if (Ptrie):# == 2):
          fname8.write("(TDEPTH == {:2}) ? {:6} : (\n".format(i,ct_depth_distr[i]))
        if (Ptrie):# == 2):                                                                        # print to file for FPGA design
          mempfxFname = str(opdirname)+"/pfxMap"+str(i)+".mem"
          memlrcFname = str(opdirname)+"/lrcMap"+str(i)+".mem"
          fname6=open(mempfxFname,"w")
          fname7=open(memlrcFname,"w")
          pass
       
        lrcinfoLen = 0                                                                         # initialise nodeinfolen to be used for computation later
        pfxinfoLen = 0
        j=0  
        
        while True:                                                                             # for each non-zero entry at this depth
          pfxinfo=''
          if (memMap[i][j][0] == 0):                                                            # If prefix info is empty, fill the field with a dummy
            for numpfx in range(Ptrie):                                                         # fill the pfxinfo string with pTrie times an 
              pfxinfo+str("000000000000000000")                                                 # all zeros string
          else:
            for pfx in memMap[i][j][0]:                                                         # for each prefix in the node 
              pfxinfo = pfxinfo+str("{:1}{:1}{:03b}{:05b}{:08b}".format(pfx[0],pfx[1],pfx[2],pfx[3],(pfx[4] & 0xff)))   
                                                                                                # copy prefix info into pfxinfo (18 bits each) 
          if (i == 0):
            lcaddr = str("{:019b}".format(memMap[i][j][1]))[17:]                                # For the 1st depth stage, only two bits needed, so pick
            rcaddr = str("{:019b}".format(memMap[i][j][2]))[17:]                                # bit at index 17 and 18 
          elif ((i>0) & (i < (len(ct_depth_distr)-1))):
            lcaddr = str("{:019b}".format(memMap[i][j][1]))[(19-ct_addrbits_perdepth[i+1]):]    # Format the lc and rc addresses so that they use only as
            rcaddr = str("{:019b}".format(memMap[i][j][2]))[(19-ct_addrbits_perdepth[i+1]):]    # many bits as necessary to address the next depth nodes
          else:
            lcaddr = str("{:019b}".format(memMap[i][j][1]))[(19-ct_addrbits_perdepth[i]):]      # Format the lc and rc addresses so that they use only as
            rcaddr = str("{:019b}".format(memMap[i][j][2]))[(19-ct_addrbits_perdepth[i]):]      # many bits as necessary as there are no next depth nodes
          
          lrcinfo = lcaddr+rcaddr+str("{:02b}".format(memMap[i][j][3]))

          newpfxinfoLen = len(str(pfxinfo))
          pfxinfoLen = newpfxinfoLen if (newpfxinfoLen > pfxinfoLen) else pfxinfoLen
          newlrcinfoLen = len(str(lrcinfo))
          lrcinfoLen = newlrcinfoLen if (newlrcinfoLen > lrcinfoLen) else lrcinfoLen

          if (Ptrie):# == 2):                                                                      # print to file for FPGA design   
            if (pfxinfo != ''):
              fname6.write("@{:x} {:x}\n".format(j,int(pfxinfo,2)))
            else:
              fname6.write("@{:x} 0\n".format(j))
            fname7.write("@{:x} {:x}\n".format(j,int(lrcinfo,2)))
            pass

          j +=1                                                                                 # check if next entry at this depth has zero depth    
          if (memMap[i][j][3] == 0):                                                            # indicating unused node and so "break"
            break
        ### while loop ends here
          
        if (Ptrie):# == 2):                                                                        # print to file for FPGA design
          fname6.close()
          fname7.close()
          pass
        
        totalpfxMembits += ct_depth_distr[i] * pfxinfoLen
        totallrcMembits += ct_depth_distr[i] * lrcinfoLen

      ### i loop for depth of cTrie ends here

      if (Ptrie):# == 2):                                                                          # print to file for FPGA design
        fname8.write("0")
        for i in range(len(ct_depth_distr)):
          fname8.write(")")
          
        fname8.write(";\n\nlocalparam MEM_AWIDTH = \n")
        for i in range(len(ct_depth_distr)):
          fname8.write("(TDEPTH == {:2}) ? clogb2({:6}) + 1 : (\n".format(i,ct_depth_distr[i]))
        fname8.write("2")
        for i in range(len(ct_depth_distr)):
          fname8.write(")")

        fname8.write(";\n\nlocalparam CP_WIDTH = \n")
        for i in range(len(ct_depth_distr)-1):
          fname8.write("(TDEPTH == {:2}) ? clogb2({:6}) + 1 : (\n".format(i,ct_depth_distr[i+1]))
        fname8.write("2")
        for i in range(len(ct_depth_distr)-1):
          fname8.write(")")

        fname8.write(";\n\nlocalparam PFX_INIT_FILE = \n")
        for i in range(len(ct_depth_distr)):
          fname8.write("(TDEPTH == {:2}) ? \"pfxMap{}.mem\" : (\n".format(i,i))
        fname8.write("0")
        for i in range(len(ct_depth_distr)):
          fname8.write(")")

        fname8.write(";\n\nlocalparam LRC_INIT_FILE = \n")
        for i in range(len(ct_depth_distr)):
          fname8.write("(TDEPTH == {:2}) ? \"lrcMap{}.mem\" : (\n".format(i,i))
        fname8.write("0")
        for i in range(len(ct_depth_distr)):
          fname8.write(")")

        fname8.write(";\n\nlocalparam TOT_DEPTH = {};\n".format(len(ct_depth_distr)))
        fname8.close()
        print("Writing memMap information to files complete")


      #######################################################
      # cTrie statistics collection 
      #######################################################
      totalentries = 0
      total_pnodes = 0
      total_enodes = 0
      numpfx_raw   = []
      xlen_raw     = []
      zlen_raw     = []
   
      print("cTrie Statistic collection")
      for entry in cTrie:
        if (entry[0] != 0):
          totalentries += len(entry[0])
          total_pnodes += 1
          numpfx_raw.append(len(entry[0]))
          for pfx in entry[0]:
            xlen_raw.append(pfx[2])
            zlen_raw.append(pfx[3])
        elif ((entry[0] == 0) and ((entry[1] != 0) or (entry[2] != 0))):
          total_enodes += 1
      
      maxpfxpernode = 20
      maxzlen = 32
      maxxlen = 32

      numpfxdistr = bincount_percent_pad(numpfx_raw,maxpfxpernode)
      xlendistr = bincount_percent_pad(xlen_raw,maxxlen)
      zlendistr = bincount_percent_pad(zlen_raw,maxzlen)

      trie_creation_dump = str(opdirname)+"/trie_creation_dump.log"
      fname2 = open(trie_creation_dump,"a") # Debug file
      fname2.write("{} | {:2} | {:2} | {} | {} | {} | {:8} | {:8} | {:8} | {:8} | {:8} | {:8} | {:8}\n".format(fname1,Ptrie,bfScaling,numpfxdistr,xlendistr,zlendistr,totalentries,total_pnodes,total_enodes,totalpfxMembits,totallrcMembits,clastram,blastram))
      fname2.close()
      print("cTrie Statistic collection complete")

      print("Searching the trie now")
      
      #######################################################
      # Search the Binary Trie and Compact Trie for a DA
      #######################################################
      bfoundcnt       = 0
      btotalhitdata   = 0
      btotalhitnull   = 0
      bts_lvl_raw     = []

      cfoundcnt       = 0
      ctotalhitdata   = 0
      ctotalhitdatap  = 0
      ctotalhitdatap_nfound = 0
      ctotalhitdatac  = 0
      ctotalhitnull   = 0
      ctotalhitnullp  = 0
      ctotalhitnullc  = 0
      cts_lvl_raw     = []
      cts_pstages_raw = []
      chitdatap_nfound_max   = 0
      chitdatap_nfound_min   = maxdepth
      chitdatap_max   = 0
      chitdatap_min   = maxdepth

      mfoundcnt       = 0
      mtotalhitdatap_TT  = 0
      mtotalhitdatap_TFT = 0
      mtotalhitdatap_TFF = 0
      mtotalhitdatac  = 0
      mtotalhitnull   = 0
      mtotalhitnullp  = 0
      mtotalhitnullc  = 0
      m_lvl_raw     = []
      m_pstages_raw = []
      #mhitdatap_nfound_max   = 0
      #mhitdatap_nfound_min   = maxdepth
      mhitdatap_max   = 0
      mhitdatap_min   = maxdepth

      tvec_dump = str(opdirname)+"/tvec_dump.log"
      try:
        os.remove(tvec_dump)
      except:
        pass

      #btvecram = btvecram[:1]
      #btvecram=[690758529,690268813,689963187]
      for tvec in btvecram:

        ######## Binary Trie Search  ##########################
        bfound = 0
        bfound,bhitdata,bhitnull,bts_lvl = bt.finditem(bTrie,'{0:0>32b}'.format(tvec))

        fname9 = open(tvec_dump, "a")                 # Test vector dump file
        fname9.write("{0:0>32b}\n".format(tvec))
        fname9.close()
        
        bfoundcnt += bfound
        btotalhitdata += bhitdata
        btotalhitnull += bhitnull
        bts_lvl_raw.append(bts_lvl)
        
        ######## Compact Trie Search ##########################
        cfound = 0
        chitdatap =0
        chitdatap_nfound = 0
        chitdatac= 0 
        chitnullp= 0 
        chitnullc = 0
        cts_lvl = 0
        cts_pstages = 0

        cfound,chitdatap,chitdatap_nfound,chitdatac,chitnullp,chitnullc,cts_lvl,cts_pstages = ct.finditem(opdirname,bf,cTrie,'{0:0>32b}'.format(tvec))
        #cfound,chitdatap,chitdatac,chitnullp,chitnullc,cts_lvl,cts_pstages = ct.finditem(opdirname,0,cTrie,'{0:0>32b}'.format(tvec))

        if (cfound != 0):
          cfoundcnt += 1
          cts_lvl_raw.append(cts_lvl)
          cts_pstages_raw.append(cts_pstages)
          chitdatap_nfound_max = chitdatap_nfound if (chitdatap_nfound > chitdatap_nfound_max) else chitdatap_nfound_max
          chitdatap_nfound_min = chitdatap_nfound if (chitdatap_nfound < chitdatap_nfound_min) else chitdatap_nfound_min
          chitdatap_max = chitdatap if (chitdatap > chitdatap_max) else chitdatap_max
          chitdatap_min = chitdatap if (chitdatap < chitdatap_min) else chitdatap_min

        ctotalhitdata += (chitdatap + chitdatac)
        ctotalhitdatap += chitdatap
        ctotalhitdatap_nfound += chitdatap_nfound
        ctotalhitdatac += chitdatac
        ctotalhitnull += (chitnullp + chitnullc)
        ctotalhitnullp += chitnullp
        ctotalhitnullc += chitnullc

        ######## memMap Search  ###############################
        mfound = 0
        mfound,mhitdatap_bfbitT_pfxT,mhitdatap_bfbitT_pfxF_bffldT,mhitdatap_bfbitT_pfxF_bffldF,mhitdatac,mhitnullp,mhitnullc,m_lvl,m_pstages = ct.finditem_in_mem(opdirname,bfarray,memMap,'{0:0>32b}'.format(tvec))
        #mfound,mhitdatap_bfbitT_pfxT,mhitdatap_bfbitT_pfxF_bffldT,mhitdatap_bfbitT_pfxF_bffldF,mhitdatac,mhitnullp,mhitnullc,m_lvl,m_pstages = ct.finditem_in_mem(opdirname,0,memMap,'{0:0>32b}'.format(tvec))

        if (mfound != 0):
          mfoundcnt += 1
          m_lvl_raw.append(m_lvl)
          m_pstages_raw.append(m_pstages)
          #mhitdatap_nfound_max = mhitdatap_nfound if (mhitdatap_nfound > mhitdatap_nfound_max) else mhitdatap_nfound_max
          #mhitdatap_nfound_min = mhitdatap_nfound if (mhitdatap_nfound < mhitdatap_nfound_min) else mhitdatap_nfound_min
          mhitdatap_total = mhitdatap_bfbitT_pfxT +  mhitdatap_bfbitT_pfxF_bffldT +  mhitdatap_bfbitT_pfxF_bffldF + mhitnullp
          mhitdatap_max = mhitdatap_total  if (mhitdatap_total > mhitdatap_max) else mhitdatap_max
          mhitdatap_min = mhitdatap_total  if (mhitdatap_total < mhitdatap_min) else mhitdatap_min

        mtotalhitdatap_TT  += mhitdatap_bfbitT_pfxT
        mtotalhitdatap_TFT += mhitdatap_bfbitT_pfxF_bffldT
        mtotalhitdatap_TFF += mhitdatap_bfbitT_pfxF_bffldF

        mtotalhitdatac += mhitdatac

        mtotalhitnull += (mhitnullp + mhitnullc)
        mtotalhitnullp += mhitnullp
        mtotalhitnullc += mhitnullc

      ########################## 
      # tvecram loop ends here
    
      #######################################################
      # Statistics collection for Trie Search
      #######################################################

      bts_lvl_avg   = np.mean(bts_lvl_raw)
      bts_lvl_worst = np.amax(bts_lvl_raw)
      bts_lvl_distr = bincount_percent_pad(bts_lvl_raw,40)

      cts_lvl_avg   = 0 #np.mean(cts_lvl_raw)
      cts_lvl_worst = 0 #np.amax(cts_lvl_raw)
      cts_lvl_distr = 0 #bincount_percent_pad(cts_lvl_raw,40)

      cts_pstages_avg   = 0 # np.mean(cts_pstages_raw)
      cts_pstages_worst = 0 # np.amax(cts_pstages_raw)
      cts_pstages_distr = 0 # bincount_percent_pad(cts_pstages_raw,120)

      ts_lvlxaxis = np.arange(0,40,1)

      trie_search_dump = str(opdirname)+"/trie_search_dump.log"
      fname3 = open(trie_search_dump,"a")
      fname3.write("{} | {:2} | {:2} || {:8} | {:8} | {:8} | {:0.2f} | {:2} | {} || {:8} | {:8} | {:8} | {:8} | {:8} | {:8} | {:8} | {:8} | {:8} | {:8} | {:0.2f} | {:2} | {} | {:0.2f} | {:2} | {} || {:8} | {:8} | {:8} | {:8} | {:8} | {:8} | {:8} | {:8} | {:8}  \n"
      .format(fname1,Ptrie,bfScaling,
              bfoundcnt,btotalhitdata,btotalhitnull,bts_lvl_avg,bts_lvl_worst,bts_lvl_distr,
              cfoundcnt,ctotalhitdatap,chitdatap_min,chitdatap_max,ctotalhitdatap_nfound,chitdatap_nfound_min,chitdatap_nfound_max,ctotalhitdatac,ctotalhitnullp,ctotalhitnullc,cts_lvl_avg,cts_lvl_worst,cts_lvl_distr,cts_pstages_avg,cts_pstages_worst,cts_pstages_distr,
              mfoundcnt,mtotalhitdatap_TT,mhitdatap_min,mhitdatap_max,mtotalhitdatap_TFT,mtotalhitdatap_TFF,mtotalhitdatac,mtotalhitnullp,mtotalhitnullc))
      fname3.close()
      print("Trie Search complete")
      print("------------------------------------")
    ########################## 
    # bfScaling loop ends here

  ########################## 
  # Ptrie loop ends here


if __name__ == "__main__":
  main()
