localparam BF_SIZE = 
(TDEPTH ==  0) ?      1 : (
(TDEPTH ==  1) ?      1 : (
(TDEPTH ==  2) ?      8 : (
(TDEPTH ==  3) ?      8 : (
(TDEPTH ==  4) ?    128 : (
(TDEPTH ==  5) ?    128 : (
(TDEPTH ==  6) ?    256 : (
(TDEPTH ==  7) ?    512 : (
(TDEPTH ==  8) ?   1024 : (
(TDEPTH ==  9) ?   2048 : (
(TDEPTH == 10) ?   2048 : (
(TDEPTH == 11) ?   4096 : (
(TDEPTH == 12) ?   8192 : (
(TDEPTH == 13) ?  16384 : (
(TDEPTH == 14) ?  16384 : (
(TDEPTH == 15) ?  32768 : (
(TDEPTH == 16) ?  65536 : (
(TDEPTH == 17) ?  65536 : (
(TDEPTH == 18) ?  65536 : (
(TDEPTH == 19) ? 131072 : (
(TDEPTH == 20) ? 131072 : (
(TDEPTH == 21) ? 131072 : (
(TDEPTH == 22) ? 262144 : (
(TDEPTH == 23) ?  65536 : (
(TDEPTH == 24) ?   8192 : (
(TDEPTH == 25) ?   2048 : (
(TDEPTH == 26) ?    512 : (
(TDEPTH == 27) ?    512 : (
(TDEPTH == 28) ?    256 : (
(TDEPTH == 29) ?      8 : (
(TDEPTH == 30) ?      0 : (
(TDEPTH == 31) ?      0 : (
(TDEPTH == 32) ?      0 : (
(TDEPTH == 33) ?      0 : (
(TDEPTH == 34) ?      0 : (
(TDEPTH == 35) ?      0 : (
(TDEPTH == 36) ?      0 : (
(TDEPTH == 37) ?      0 : (
(TDEPTH == 38) ?      0 : (
(TDEPTH == 39) ?      0 : (
(TDEPTH == 40) ?      0 : (
(TDEPTH == 41) ?      0 : (
(TDEPTH == 42) ?      0 : (
(TDEPTH == 43) ?      0 : (
(TDEPTH == 44) ?      0 : (
(TDEPTH == 45) ?      0 : (
(TDEPTH == 46) ?      0 : (
(TDEPTH == 47) ?      0 : (
(TDEPTH == 48) ?      0 : (
(TDEPTH == 49) ?      0 : (
(TDEPTH == 50) ?      0 : (
(TDEPTH == 51) ?      0 : (
(TDEPTH == 52) ?      0 : (
(TDEPTH == 53) ?      0 : (
(TDEPTH == 54) ?      0 : (
(TDEPTH == 55) ?      0 : (
(TDEPTH == 56) ?      0 : (
(TDEPTH == 57) ?      0 : (
(TDEPTH == 58) ?      0 : (
(TDEPTH == 59) ?      0 : (
(TDEPTH == 60) ?      0 : (
(TDEPTH == 61) ?      0 : (
(TDEPTH == 62) ?      0 : (
(TDEPTH == 63) ?      0 : (
(TDEPTH == 64) ?      0 : (
(TDEPTH == 65) ?      0 : (
(TDEPTH == 66) ?      0 : (
(TDEPTH == 67) ?      0 : (
(TDEPTH == 68) ?      0 : (
(TDEPTH == 69) ?      0 : (
(TDEPTH == 70) ?      0 : (
(TDEPTH == 71) ?      0 : (
(TDEPTH == 72) ?      0 : (
(TDEPTH == 73) ?      0 : (
(TDEPTH == 74) ?      0 : (
(TDEPTH == 75) ?      0 : (
(TDEPTH == 76) ?      0 : (
(TDEPTH == 77) ?      0 : (
(TDEPTH == 78) ?      0 : (
(TDEPTH == 79) ?      0 : (
0))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))));

localparam BF_AWIDTH = 
(TDEPTH ==  0) ? clogb2(     0) : (
(TDEPTH ==  1) ? clogb2(     0) : (
(TDEPTH ==  2) ? clogb2(     8) : (
(TDEPTH ==  3) ? clogb2(     8) : (
(TDEPTH ==  4) ? clogb2(   128) : (
(TDEPTH ==  5) ? clogb2(   128) : (
(TDEPTH ==  6) ? clogb2(   256) : (
(TDEPTH ==  7) ? clogb2(   512) : (
(TDEPTH ==  8) ? clogb2(  1024) : (
(TDEPTH ==  9) ? clogb2(  2048) : (
(TDEPTH == 10) ? clogb2(  2048) : (
(TDEPTH == 11) ? clogb2(  4096) : (
(TDEPTH == 12) ? clogb2(  8192) : (
(TDEPTH == 13) ? clogb2( 16384) : (
(TDEPTH == 14) ? clogb2( 16384) : (
(TDEPTH == 15) ? clogb2( 32768) : (
(TDEPTH == 16) ? clogb2( 65536) : (
(TDEPTH == 17) ? clogb2( 65536) : (
(TDEPTH == 18) ? clogb2( 65536) : (
(TDEPTH == 19) ? clogb2(131072) : (
(TDEPTH == 20) ? clogb2(131072) : (
(TDEPTH == 21) ? clogb2(131072) : (
(TDEPTH == 22) ? clogb2(262144) : (
(TDEPTH == 23) ? clogb2( 65536) : (
(TDEPTH == 24) ? clogb2(  8192) : (
(TDEPTH == 25) ? clogb2(  2048) : (
(TDEPTH == 26) ? clogb2(   512) : (
(TDEPTH == 27) ? clogb2(   512) : (
(TDEPTH == 28) ? clogb2(   256) : (
(TDEPTH == 29) ? clogb2(     8) : (
(TDEPTH == 30) ? clogb2(     0) : (
(TDEPTH == 31) ? clogb2(     0) : (
(TDEPTH == 32) ? clogb2(     0) : (
(TDEPTH == 33) ? clogb2(     0) : (
(TDEPTH == 34) ? clogb2(     0) : (
(TDEPTH == 35) ? clogb2(     0) : (
(TDEPTH == 36) ? clogb2(     0) : (
(TDEPTH == 37) ? clogb2(     0) : (
(TDEPTH == 38) ? clogb2(     0) : (
(TDEPTH == 39) ? clogb2(     0) : (
(TDEPTH == 40) ? clogb2(     0) : (
(TDEPTH == 41) ? clogb2(     0) : (
(TDEPTH == 42) ? clogb2(     0) : (
(TDEPTH == 43) ? clogb2(     0) : (
(TDEPTH == 44) ? clogb2(     0) : (
(TDEPTH == 45) ? clogb2(     0) : (
(TDEPTH == 46) ? clogb2(     0) : (
(TDEPTH == 47) ? clogb2(     0) : (
(TDEPTH == 48) ? clogb2(     0) : (
(TDEPTH == 49) ? clogb2(     0) : (
(TDEPTH == 50) ? clogb2(     0) : (
(TDEPTH == 51) ? clogb2(     0) : (
(TDEPTH == 52) ? clogb2(     0) : (
(TDEPTH == 53) ? clogb2(     0) : (
(TDEPTH == 54) ? clogb2(     0) : (
(TDEPTH == 55) ? clogb2(     0) : (
(TDEPTH == 56) ? clogb2(     0) : (
(TDEPTH == 57) ? clogb2(     0) : (
(TDEPTH == 58) ? clogb2(     0) : (
(TDEPTH == 59) ? clogb2(     0) : (
(TDEPTH == 60) ? clogb2(     0) : (
(TDEPTH == 61) ? clogb2(     0) : (
(TDEPTH == 62) ? clogb2(     0) : (
(TDEPTH == 63) ? clogb2(     0) : (
(TDEPTH == 64) ? clogb2(     0) : (
(TDEPTH == 65) ? clogb2(     0) : (
(TDEPTH == 66) ? clogb2(     0) : (
(TDEPTH == 67) ? clogb2(     0) : (
(TDEPTH == 68) ? clogb2(     0) : (
(TDEPTH == 69) ? clogb2(     0) : (
(TDEPTH == 70) ? clogb2(     0) : (
(TDEPTH == 71) ? clogb2(     0) : (
(TDEPTH == 72) ? clogb2(     0) : (
(TDEPTH == 73) ? clogb2(     0) : (
(TDEPTH == 74) ? clogb2(     0) : (
(TDEPTH == 75) ? clogb2(     0) : (
(TDEPTH == 76) ? clogb2(     0) : (
(TDEPTH == 77) ? clogb2(     0) : (
(TDEPTH == 78) ? clogb2(     0) : (
(TDEPTH == 79) ? clogb2(     0) : (
0))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))));

localparam BF_INIT_FILE = 
(TDEPTH ==  0) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/bf0.mem" : (
(TDEPTH ==  1) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/bf1.mem" : (
(TDEPTH ==  2) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/bf2.mem" : (
(TDEPTH ==  3) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/bf3.mem" : (
(TDEPTH ==  4) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/bf4.mem" : (
(TDEPTH ==  5) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/bf5.mem" : (
(TDEPTH ==  6) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/bf6.mem" : (
(TDEPTH ==  7) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/bf7.mem" : (
(TDEPTH ==  8) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/bf8.mem" : (
(TDEPTH ==  9) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/bf9.mem" : (
(TDEPTH == 10) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/bf10.mem" : (
(TDEPTH == 11) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/bf11.mem" : (
(TDEPTH == 12) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/bf12.mem" : (
(TDEPTH == 13) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/bf13.mem" : (
(TDEPTH == 14) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/bf14.mem" : (
(TDEPTH == 15) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/bf15.mem" : (
(TDEPTH == 16) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/bf16.mem" : (
(TDEPTH == 17) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/bf17.mem" : (
(TDEPTH == 18) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/bf18.mem" : (
(TDEPTH == 19) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/bf19.mem" : (
(TDEPTH == 20) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/bf20.mem" : (
(TDEPTH == 21) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/bf21.mem" : (
(TDEPTH == 22) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/bf22.mem" : (
(TDEPTH == 23) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/bf23.mem" : (
(TDEPTH == 24) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/bf24.mem" : (
(TDEPTH == 25) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/bf25.mem" : (
(TDEPTH == 26) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/bf26.mem" : (
(TDEPTH == 27) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/bf27.mem" : (
(TDEPTH == 28) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/bf28.mem" : (
(TDEPTH == 29) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/bf29.mem" : (
(TDEPTH == 30) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/bf30.mem" : (
(TDEPTH == 31) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/bf31.mem" : (
(TDEPTH == 32) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/bf32.mem" : (
(TDEPTH == 33) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/bf33.mem" : (
(TDEPTH == 34) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/bf34.mem" : (
(TDEPTH == 35) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/bf35.mem" : (
(TDEPTH == 36) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/bf36.mem" : (
(TDEPTH == 37) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/bf37.mem" : (
(TDEPTH == 38) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/bf38.mem" : (
(TDEPTH == 39) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/bf39.mem" : (
(TDEPTH == 40) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/bf40.mem" : (
(TDEPTH == 41) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/bf41.mem" : (
(TDEPTH == 42) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/bf42.mem" : (
(TDEPTH == 43) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/bf43.mem" : (
(TDEPTH == 44) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/bf44.mem" : (
(TDEPTH == 45) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/bf45.mem" : (
(TDEPTH == 46) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/bf46.mem" : (
(TDEPTH == 47) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/bf47.mem" : (
(TDEPTH == 48) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/bf48.mem" : (
(TDEPTH == 49) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/bf49.mem" : (
(TDEPTH == 50) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/bf50.mem" : (
(TDEPTH == 51) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/bf51.mem" : (
(TDEPTH == 52) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/bf52.mem" : (
(TDEPTH == 53) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/bf53.mem" : (
(TDEPTH == 54) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/bf54.mem" : (
(TDEPTH == 55) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/bf55.mem" : (
(TDEPTH == 56) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/bf56.mem" : (
(TDEPTH == 57) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/bf57.mem" : (
(TDEPTH == 58) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/bf58.mem" : (
(TDEPTH == 59) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/bf59.mem" : (
(TDEPTH == 60) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/bf60.mem" : (
(TDEPTH == 61) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/bf61.mem" : (
(TDEPTH == 62) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/bf62.mem" : (
(TDEPTH == 63) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/bf63.mem" : (
(TDEPTH == 64) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/bf64.mem" : (
(TDEPTH == 65) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/bf65.mem" : (
(TDEPTH == 66) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/bf66.mem" : (
(TDEPTH == 67) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/bf67.mem" : (
(TDEPTH == 68) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/bf68.mem" : (
(TDEPTH == 69) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/bf69.mem" : (
(TDEPTH == 70) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/bf70.mem" : (
(TDEPTH == 71) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/bf71.mem" : (
(TDEPTH == 72) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/bf72.mem" : (
(TDEPTH == 73) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/bf73.mem" : (
(TDEPTH == 74) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/bf74.mem" : (
(TDEPTH == 75) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/bf75.mem" : (
(TDEPTH == 76) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/bf76.mem" : (
(TDEPTH == 77) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/bf77.mem" : (
(TDEPTH == 78) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/bf78.mem" : (
(TDEPTH == 79) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/bf79.mem" : (
0))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))));

