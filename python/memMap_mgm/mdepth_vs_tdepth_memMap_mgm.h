function integer clogb2;
  input [31:0] value;
  begin
    if(value > 1)
    begin
      value = value - 1;
      for (clogb2 = 0; value > 0; clogb2 = clogb2 + 1)
      begin
        value = value >> 1;
      end
    end
    else
      clogb2 = 1;
  end
endfunction

localparam MEM_SIZE = 
(TDEPTH ==  0) ?      1 : (
(TDEPTH ==  1) ?      2 : (
(TDEPTH ==  2) ?      4 : (
(TDEPTH ==  3) ?      8 : (
(TDEPTH ==  4) ?     16 : (
(TDEPTH ==  5) ?     31 : (
(TDEPTH ==  6) ?     61 : (
(TDEPTH ==  7) ?    121 : (
(TDEPTH ==  8) ?    241 : (
(TDEPTH ==  9) ?    457 : (
(TDEPTH == 10) ?    860 : (
(TDEPTH == 11) ?   1550 : (
(TDEPTH == 12) ?   2671 : (
(TDEPTH == 13) ?   4351 : (
(TDEPTH == 14) ?   6294 : (
(TDEPTH == 15) ?   9048 : (
(TDEPTH == 16) ?  12548 : (
(TDEPTH == 17) ?  16633 : (
(TDEPTH == 18) ?  20388 : (
(TDEPTH == 19) ?  24044 : (
(TDEPTH == 20) ?  26566 : (
(TDEPTH == 21) ?  27348 : (
(TDEPTH == 22) ?  25073 : (
(TDEPTH == 23) ?   4305 : (
(TDEPTH == 24) ?    722 : (
(TDEPTH == 25) ?    259 : (
(TDEPTH == 26) ?     43 : (
(TDEPTH == 27) ?     35 : (
(TDEPTH == 28) ?     29 : (
(TDEPTH == 29) ?      1 : (
0))))))))))))))))))))))))))))));

localparam MEM_AWIDTH = 
(TDEPTH ==  0) ? clogb2(     1) + 1 : (
(TDEPTH ==  1) ? clogb2(     2) + 1 : (
(TDEPTH ==  2) ? clogb2(     4) + 1 : (
(TDEPTH ==  3) ? clogb2(     8) + 1 : (
(TDEPTH ==  4) ? clogb2(    16) + 1 : (
(TDEPTH ==  5) ? clogb2(    31) + 1 : (
(TDEPTH ==  6) ? clogb2(    61) + 1 : (
(TDEPTH ==  7) ? clogb2(   121) + 1 : (
(TDEPTH ==  8) ? clogb2(   241) + 1 : (
(TDEPTH ==  9) ? clogb2(   457) + 1 : (
(TDEPTH == 10) ? clogb2(   860) + 1 : (
(TDEPTH == 11) ? clogb2(  1550) + 1 : (
(TDEPTH == 12) ? clogb2(  2671) + 1 : (
(TDEPTH == 13) ? clogb2(  4351) + 1 : (
(TDEPTH == 14) ? clogb2(  6294) + 1 : (
(TDEPTH == 15) ? clogb2(  9048) + 1 : (
(TDEPTH == 16) ? clogb2( 12548) + 1 : (
(TDEPTH == 17) ? clogb2( 16633) + 1 : (
(TDEPTH == 18) ? clogb2( 20388) + 1 : (
(TDEPTH == 19) ? clogb2( 24044) + 1 : (
(TDEPTH == 20) ? clogb2( 26566) + 1 : (
(TDEPTH == 21) ? clogb2( 27348) + 1 : (
(TDEPTH == 22) ? clogb2( 25073) + 1 : (
(TDEPTH == 23) ? clogb2(  4305) + 1 : (
(TDEPTH == 24) ? clogb2(   722) + 1 : (
(TDEPTH == 25) ? clogb2(   259) + 1 : (
(TDEPTH == 26) ? clogb2(    43) + 1 : (
(TDEPTH == 27) ? clogb2(    35) + 1 : (
(TDEPTH == 28) ? clogb2(    29) + 1 : (
(TDEPTH == 29) ? clogb2(     1) + 1 : (
2))))))))))))))))))))))))))))));

localparam CP_WIDTH = 
(TDEPTH ==  0) ? clogb2(     2) + 1 : (
(TDEPTH ==  1) ? clogb2(     4) + 1 : (
(TDEPTH ==  2) ? clogb2(     8) + 1 : (
(TDEPTH ==  3) ? clogb2(    16) + 1 : (
(TDEPTH ==  4) ? clogb2(    31) + 1 : (
(TDEPTH ==  5) ? clogb2(    61) + 1 : (
(TDEPTH ==  6) ? clogb2(   121) + 1 : (
(TDEPTH ==  7) ? clogb2(   241) + 1 : (
(TDEPTH ==  8) ? clogb2(   457) + 1 : (
(TDEPTH ==  9) ? clogb2(   860) + 1 : (
(TDEPTH == 10) ? clogb2(  1550) + 1 : (
(TDEPTH == 11) ? clogb2(  2671) + 1 : (
(TDEPTH == 12) ? clogb2(  4351) + 1 : (
(TDEPTH == 13) ? clogb2(  6294) + 1 : (
(TDEPTH == 14) ? clogb2(  9048) + 1 : (
(TDEPTH == 15) ? clogb2( 12548) + 1 : (
(TDEPTH == 16) ? clogb2( 16633) + 1 : (
(TDEPTH == 17) ? clogb2( 20388) + 1 : (
(TDEPTH == 18) ? clogb2( 24044) + 1 : (
(TDEPTH == 19) ? clogb2( 26566) + 1 : (
(TDEPTH == 20) ? clogb2( 27348) + 1 : (
(TDEPTH == 21) ? clogb2( 25073) + 1 : (
(TDEPTH == 22) ? clogb2(  4305) + 1 : (
(TDEPTH == 23) ? clogb2(   722) + 1 : (
(TDEPTH == 24) ? clogb2(   259) + 1 : (
(TDEPTH == 25) ? clogb2(    43) + 1 : (
(TDEPTH == 26) ? clogb2(    35) + 1 : (
(TDEPTH == 27) ? clogb2(    29) + 1 : (
(TDEPTH == 28) ? clogb2(     1) + 1 : (
2)))))))))))))))))))))))))))));

localparam PFX_INIT_FILE = 
(TDEPTH ==  0) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/pfxMap0.mem" : (
(TDEPTH ==  1) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/pfxMap1.mem" : (
(TDEPTH ==  2) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/pfxMap2.mem" : (
(TDEPTH ==  3) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/pfxMap3.mem" : (
(TDEPTH ==  4) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/pfxMap4.mem" : (
(TDEPTH ==  5) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/pfxMap5.mem" : (
(TDEPTH ==  6) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/pfxMap6.mem" : (
(TDEPTH ==  7) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/pfxMap7.mem" : (
(TDEPTH ==  8) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/pfxMap8.mem" : (
(TDEPTH ==  9) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/pfxMap9.mem" : (
(TDEPTH == 10) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/pfxMap10.mem" : (
(TDEPTH == 11) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/pfxMap11.mem" : (
(TDEPTH == 12) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/pfxMap12.mem" : (
(TDEPTH == 13) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/pfxMap13.mem" : (
(TDEPTH == 14) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/pfxMap14.mem" : (
(TDEPTH == 15) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/pfxMap15.mem" : (
(TDEPTH == 16) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/pfxMap16.mem" : (
(TDEPTH == 17) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/pfxMap17.mem" : (
(TDEPTH == 18) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/pfxMap18.mem" : (
(TDEPTH == 19) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/pfxMap19.mem" : (
(TDEPTH == 20) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/pfxMap20.mem" : (
(TDEPTH == 21) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/pfxMap21.mem" : (
(TDEPTH == 22) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/pfxMap22.mem" : (
(TDEPTH == 23) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/pfxMap23.mem" : (
(TDEPTH == 24) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/pfxMap24.mem" : (
(TDEPTH == 25) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/pfxMap25.mem" : (
(TDEPTH == 26) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/pfxMap26.mem" : (
(TDEPTH == 27) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/pfxMap27.mem" : (
(TDEPTH == 28) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/pfxMap28.mem" : (
(TDEPTH == 29) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/pfxMap29.mem" : (
0))))))))))))))))))))))))))))));

localparam LRC_INIT_FILE = 
(TDEPTH ==  0) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/lrcMap0.mem" : (
(TDEPTH ==  1) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/lrcMap1.mem" : (
(TDEPTH ==  2) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/lrcMap2.mem" : (
(TDEPTH ==  3) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/lrcMap3.mem" : (
(TDEPTH ==  4) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/lrcMap4.mem" : (
(TDEPTH ==  5) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/lrcMap5.mem" : (
(TDEPTH ==  6) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/lrcMap6.mem" : (
(TDEPTH ==  7) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/lrcMap7.mem" : (
(TDEPTH ==  8) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/lrcMap8.mem" : (
(TDEPTH ==  9) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/lrcMap9.mem" : (
(TDEPTH == 10) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/lrcMap10.mem" : (
(TDEPTH == 11) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/lrcMap11.mem" : (
(TDEPTH == 12) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/lrcMap12.mem" : (
(TDEPTH == 13) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/lrcMap13.mem" : (
(TDEPTH == 14) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/lrcMap14.mem" : (
(TDEPTH == 15) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/lrcMap15.mem" : (
(TDEPTH == 16) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/lrcMap16.mem" : (
(TDEPTH == 17) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/lrcMap17.mem" : (
(TDEPTH == 18) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/lrcMap18.mem" : (
(TDEPTH == 19) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/lrcMap19.mem" : (
(TDEPTH == 20) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/lrcMap20.mem" : (
(TDEPTH == 21) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/lrcMap21.mem" : (
(TDEPTH == 22) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/lrcMap22.mem" : (
(TDEPTH == 23) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/lrcMap23.mem" : (
(TDEPTH == 24) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/lrcMap24.mem" : (
(TDEPTH == 25) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/lrcMap25.mem" : (
(TDEPTH == 26) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/lrcMap26.mem" : (
(TDEPTH == 27) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/lrcMap27.mem" : (
(TDEPTH == 28) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/lrcMap28.mem" : (
(TDEPTH == 29) ? "/home/prashant/repositories/gitlab/IPLookup/python/memMap_mgm/lrcMap29.mem" : (
0))))))))))))))))))))))))))))));

