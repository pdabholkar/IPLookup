''' 
Functions for plotting the various results obtained from the trie traversal algorithms
'''

import csv
import math
import numpy as np
import matplotlib.pyplot as plt
import sys,getopt
import jenkins_hash as jhash
import binary_trie as bt
import compact_trie as ct
from matplotlib.gridspec import GridSpec
import matplotlib.ticker as ticker


if __name__ == "__main__":

  np.set_printoptions(precision = 3,linewidth=10000,suppress='true')

  if (len(sys.argv) > 2):
    ftcreate = sys.argv[1]
    ftsearch = sys.argv[2] 
  else:  
    print("Incorrect number of arguments supplied")  
    print("Usage : trie_plot.py <trie_create_log> <trie_search_log_file> ")
    sys.exit(0)

  with open(ftcreate,'r') as create_f:
     create_iter = csv.reader(create_f, delimiter = '|',  quotechar = '"')
     create = [create for create in create_iter]

  create_array = np.asarray(create)  

  with open(ftsearch,'r') as search_f:
     search_iter = csv.reader(search_f, delimiter = '|',  quotechar = '"')
     search = [search for search in search_iter]

  search_array = np.asarray(search)  
   

  Ptries = [1, 2, 3, 4, 5, 8, 10, 15, 20]
  Tables = ['CAI', 'SDB', 'MAN', 'LYS', 'DXB', 'MGM', 'MIA', 'IAD', 'FRA', 'PAO']
  #Tables = ['CAI', 'SDB', 'MAN', 'LYS', 'DXB', 'MGM', 'IAD', 'FRA', 'PAO']
  numptries = len(Ptries)
  numtables = len(Tables)

  pfxlen_distr = []
  xlen_distr   = []
  zlen_distr   = []
  num_nodes_distr = [[0 for j in range(numptries)] for i in range(numtables)]
  mem_util_distr  = [[0 for j in range(numptries)] for i in range(numtables)]
  opt_mem_util_distr  = [[0 for j in range(numptries)] for i in range(numtables)]
  bt_num_nodes_distr = [0 for i in range(numtables)]
  bt_mem_util_distr = [0 for i in range(numtables)]

  
  for i in range(numtables): # this is the number of tables loop
    pfxlen_str = create_array[(numptries*(i+1)-1)][2]
    pfxlen_str = pfxlen_str.replace('[','').replace(']','')
    pfxlen_npa = np.fromstring(pfxlen_str, dtype='float', sep= ' ')
    pfxlen_distr.append(pfxlen_npa)
    
    xlen_str = create_array[(numptries*(i+1)-1)][3]
    xlen_str = xlen_str.replace('[','').replace(']','')
    xlen_npa = np.fromstring(xlen_str, dtype='float', sep= ' ')
    xlenb_npa = [sum(xlen_npa[0:2]),sum(xlen_npa[2:4]),sum(xlen_npa[4:8]),sum(xlen_npa[8:16]),sum(xlen_npa[16:])]
    xlen_distr.append(xlenb_npa)

    zlen_str = create_array[(numptries*(i+1)-1)][4]
    zlen_str = zlen_str.replace('[','').replace(']','')
    zlen_npa = np.fromstring(zlen_str, dtype='float', sep= ' ')
    zlenb_npa = [sum(zlen_npa[0:2]),sum(zlen_npa[2:4]),sum(zlen_npa[4:8]),sum(zlen_npa[8:16]),sum(zlen_npa[16:])]
    zlen_distr.append(zlenb_npa)

    bt_num_nodes_distr[i] = int(create_array[numptries*i][9])
    bt_mem_util_distr[i]  = bt_num_nodes_distr[i] * ((math.ceil(math.log(bt_num_nodes_distr[i],2)) * 2) + 8)/1.0E6

    for j in range(numptries): # This is the number of Ptrie values loop.
      num_nodes_distr[i][j] = int(create_array[numptries*i+j][8])
      mem_util_distr[i][j]  = num_nodes_distr[i][j] * ((math.ceil(math.log(num_nodes_distr[i][j],2)) * 2) + (18 * Ptries[j])) / 1.0E6
      opt_mem_util_distr[i][j] = ((num_nodes_distr[i][j] * (math.ceil(math.log(num_nodes_distr[i][j],2)) * 2)) + \
                                 (int(create_array[numptries*i+j][6]) * 18 * Ptries[j])) / 1.0E6
      


  pfxlen_distr    = np.asarray(pfxlen_distr) 
  xlen_distr      = np.asarray(xlen_distr) 
  zlen_distr      = np.asarray(zlen_distr) 
  num_nodes_distr = np.asarray(num_nodes_distr)
  mem_util_distr  = np.asarray(mem_util_distr)
  opt_mem_util_distr = np.asarray(opt_mem_util_distr)
  bt_num_nodes_distr = np.asarray(bt_num_nodes_distr) 
  bt_mem_util_distr  = np.asarray(bt_mem_util_distr)


  print("Prefix per node distribution")
  print(pfxlen_distr)
  print("Xlen bits distribution")
  print(xlen_distr)
  print("Zlen bits distribution")
  print(zlen_distr)

  print("Number of nodes in CTrie")
  print(num_nodes_distr)
  print("E-CTrie memory utilization")
  print(mem_util_distr)
  print("Optiminsed E-CTrie memory utilization")
  print(opt_mem_util_distr)

  print("Number of nodes in BTrie")
  print(bt_num_nodes_distr)
  print(len(bt_num_nodes_distr),numtables)
  print("BTrie memory utilization")
  print(bt_mem_util_distr)

  fname3 = open("ptrie_vs_mem_n_lat.txt","w")
  fname4 = open("mem_n_lat_bt_vs_ct.txt","w")

  for i in range(numtables):
    fname3.write("{:8}\n".format(Tables[i]))
    fname4.write("{:8}".format(Tables[i]))
#    fname4.write("{6.1f}({5.1f})/{:5}\n".format((bt_num_nodes_distr[i]/1000),bt_mem_util_distr[i],search_array[numptries*i][13])) 
                                                                   # (num_nodes_distr[i][0]/1000),mem_util_distr[i][0],search_array[numptries*i][16]))
    fname4.write("{:6.1f}({:5.1f})/{:5}, {:6.1f}({:5.1f})/{:5}, {:6.1f}({:5.1f})/{:5} \n"
                                                                  .format((bt_num_nodes_distr[i]/1000),bt_mem_util_distr[i],search_array[numptries*i][5],
                                                                          (num_nodes_distr[i][1]/1000),mem_util_distr[i][1],search_array[numptries*i+1][15],
                                                                          (num_nodes_distr[i][1]/1000),opt_mem_util_distr[i][1],search_array[numptries*i+1][15]))
    fname3.write("Num of Nodes (K) , ")
    for j in range(numptries):
      fname3.write("{:9.1f}, ".format(num_nodes_distr[i][j]/1000))
    fname3.write("\n")
    fname3.write("Memory bits (M)  , ")
    for j in range(numptries):
      fname3.write("{:9.1f}, ".format(mem_util_distr[i][j]))
    fname3.write("\n")
    fname3.write("Avg Lat/Worst Lat, ")
    for j in range(numptries):
      fname3.write("{:>5.2f}/{:<3}, ".format(float(search_array[numptries*i+j][15]), int(search_array[numptries*i+j][16])))
    fname3.write("\n")
    
  fname3.write("\n")
  fname4.write("\n")

  fname3.close()
  fname4.close()
  ######################################################3
 
  fig1 = plt.figure()
  pfxlen_xaxis = np.arange(0,20)

  ax = fig1.add_subplot(1,1,1)
  bar_width=0.085
  opacity = 0.9
  rects0 = ax.bar(pfxlen_xaxis-4*bar_width,pfxlen_distr[0],bar_width,alpha=opacity,label=Tables[0])
  rects1 = ax.bar(pfxlen_xaxis-3*bar_width,pfxlen_distr[1],bar_width,alpha=opacity,label=Tables[1])
  rects2 = ax.bar(pfxlen_xaxis-2*bar_width,pfxlen_distr[2],bar_width,alpha=opacity,label=Tables[2])
  rects3 = ax.bar(pfxlen_xaxis-1*bar_width,pfxlen_distr[3],bar_width,alpha=opacity,label=Tables[3])
  rects4 = ax.bar(pfxlen_xaxis            ,pfxlen_distr[4],bar_width,alpha=opacity,label=Tables[4])
  rects5 = ax.bar(pfxlen_xaxis+1*bar_width,pfxlen_distr[5],bar_width,alpha=opacity,label=Tables[5])
  rects6 = ax.bar(pfxlen_xaxis+2*bar_width,pfxlen_distr[6],bar_width,alpha=opacity,label=Tables[6])
  rects7 = ax.bar(pfxlen_xaxis+3*bar_width,pfxlen_distr[7],bar_width,alpha=opacity,label=Tables[7])
  rects8 = ax.bar(pfxlen_xaxis+4*bar_width,pfxlen_distr[8],bar_width,alpha=opacity,label=Tables[8])
  #rects9 = ax.bar(pfxlen_xaxis+5*bar_width,pfxlen_distr[9],bar_width,alpha=opacity,label=Tables[9])

  ax.set_xlabel('Number of prefixes per node')
  ax.set_ylabel('Number of nodes (%)')
  #plt.title('Scores by person')
  ax.set_xticks(pfxlen_xaxis, list(pfxlen_xaxis))
  ax.xaxis.set_major_locator(ticker.MultipleLocator(1))
  ax.set_xlim(0,10)
  ax.legend()
  #plt.tight_layout()
  
  myFigName = "Num_prefixes_per_node.eps"

  print(myFigName) 
  
  plt.savefig(myFigName,format='eps',dpi=1000)

  ######################################################3
 
  fig2 = plt.figure()
  xlen_xaxis = np.arange(1,6)
  
  ax = fig2.add_subplot(1,1,1)
  bar_width=0.1
  opacity = 0.8
  rects0 = ax.bar(xlen_xaxis-4*bar_width,xlen_distr[0],bar_width,alpha=opacity,label=Tables[0])
  rects1 = ax.bar(xlen_xaxis-3*bar_width,xlen_distr[1],bar_width,alpha=opacity,label=Tables[1])
  rects2 = ax.bar(xlen_xaxis-2*bar_width,xlen_distr[2],bar_width,alpha=opacity,label=Tables[2])
  rects3 = ax.bar(xlen_xaxis-1*bar_width,xlen_distr[3],bar_width,alpha=opacity,label=Tables[3])
  rects4 = ax.bar(xlen_xaxis            ,xlen_distr[4],bar_width,alpha=opacity,label=Tables[4])
  rects5 = ax.bar(xlen_xaxis+1*bar_width,xlen_distr[5],bar_width,alpha=opacity,label=Tables[5])
  rects6 = ax.bar(xlen_xaxis+2*bar_width,xlen_distr[6],bar_width,alpha=opacity,label=Tables[6])
  rects7 = ax.bar(xlen_xaxis+3*bar_width,xlen_distr[7],bar_width,alpha=opacity,label=Tables[7])
  rects8 = ax.bar(xlen_xaxis+4*bar_width,xlen_distr[8],bar_width,alpha=opacity,label=Tables[8])
  #rects9 = ax.bar(xlen_xaxis+5*bar_width,xlen_distr[9],bar_width,alpha=opacity,label=Tables[9])

  ax.set_xlabel('Number of bits in |x|')
  ax.set_ylabel('Number of prefixes (%)')
  #plt.title('Scores by person')
  #ax.set_xticks(xlen_xaxis, list(xlen_xaxis))
  #ax.xaxis.set_major_locator(ticker.MultipleLocator(1))
  ax.set_xlim(0,6)
  ax.set_ylim(0,90)
  ax.legend()
  #plt.tight_layout()
  
  myFigName = "xlen_distribution.eps"

  print(myFigName) 
  
  plt.savefig(myFigName,format='eps',dpi=1000)

  ######################################################3
 
  fig3 = plt.figure()
  zlen_xaxis = np.arange(1,6)
  
  ax = fig3.add_subplot(1,1,1)
  bar_width=0.1
  opacity = 0.8
  rects0 = ax.bar(zlen_xaxis-4*bar_width,zlen_distr[0],bar_width,alpha=opacity,label=Tables[0])
  rects1 = ax.bar(zlen_xaxis-3*bar_width,zlen_distr[1],bar_width,alpha=opacity,label=Tables[1])
  rects2 = ax.bar(zlen_xaxis-2*bar_width,zlen_distr[2],bar_width,alpha=opacity,label=Tables[2])
  rects3 = ax.bar(zlen_xaxis-1*bar_width,zlen_distr[3],bar_width,alpha=opacity,label=Tables[3])
  rects4 = ax.bar(zlen_xaxis            ,zlen_distr[4],bar_width,alpha=opacity,label=Tables[4])
  rects5 = ax.bar(zlen_xaxis+1*bar_width,zlen_distr[5],bar_width,alpha=opacity,label=Tables[5])
  rects6 = ax.bar(zlen_xaxis+2*bar_width,zlen_distr[6],bar_width,alpha=opacity,label=Tables[6])
  rects7 = ax.bar(zlen_xaxis+3*bar_width,zlen_distr[7],bar_width,alpha=opacity,label=Tables[7])
  rects8 = ax.bar(zlen_xaxis+4*bar_width,zlen_distr[8],bar_width,alpha=opacity,label=Tables[8])
  #rects9 = ax.bar(zlen_xaxis+5*bar_width,zlen_distr[9],bar_width,alpha=opacity,label=Tables[9])

  ax.set_xlabel('Number of bits in |z|')
  ax.set_ylabel('Number of prefixes (%)')
  #plt.title('Scores by person')
  #ax.set_xticks(zlen_xaxis, list(zlen_xaxis))
  #ax.xaxis.set_major_locator(ticker.MultipleLocator(1))
  ax.set_xlim(0,6)
  ax.set_ylim(0,90)
  ax.legend()
  #plt.tight_layout()
  
  myFigName = "zlen_distribution.eps"

  print(myFigName) 
  
  plt.savefig(myFigName,format='eps',dpi=1000)

  ### mem_vs_ptrie_distr = []
  ### for j in range(1,10):
  ###   mem_vs_ptrie_str = create_array[71+i][8]
  ###   mem_vs_ptrie_str = mem_vs_ptrie_str.replace('[','').replace(']','')
  ###   mem_vs_ptrie_npa = np.fromstring(mem_vs_ptrie_str, dtype='float', sep=' ')
  ###   mem_vs_ptrie_distr.append(mem_vs_ptrie_npa)
  ### 
  ### mem_vs_ptrie_distr = np.asarray(mem_vs_ptrie_distr)
     
