import numpy as np
import math
import sys
from numpy import genfromtxt
import matplotlib.pyplot as plt

############################################
counter_wobf_values = genfromtxt("../run_wobf_woNull/counter_dump.log",dtype=int)

trie_levels = counter_wobf_values[:,0]
bf_hit_counter_wobf = counter_wobf_values[:,1]
bf_not_hit_counter_wobf = counter_wobf_values[:,2]

pfx_null_ko_counter_wobf = counter_wobf_values[:,3]
pfx_non_null_ko_counter_wobf = counter_wobf_values[:,4]

lrc_null_ko_counter_wobf = counter_wobf_values[:,5]
lrc_non_null_ko_counter_wobf = counter_wobf_values[:,6]

total_accesses_wobf = bf_hit_counter_wobf + bf_not_hit_counter_wobf

pfx_ram_energy_per_level_wobf_wonc = (pfx_null_ko_counter_wobf + pfx_non_null_ko_counter_wobf) * 2.69 
energy_per_level_wobf_wonc = pfx_ram_energy_per_level_wobf_wonc 

pfx_ram_energy_per_level_wobf_wnc = pfx_null_ko_counter_wobf * 1.10 + pfx_non_null_ko_counter_wobf * 2.69 
energy_per_level_wobf_wnc = pfx_ram_energy_per_level_wobf_wnc 

print("Energy without BF without Null")
print(energy_per_level_wobf_wonc)
print("Energy without BF with Null")
print(energy_per_level_wobf_wnc)

print(sum(total_accesses_wobf))

############################################
counter_wbf_values = genfromtxt("../run_wbf_woNull/counter_dump.log",dtype=int)

bf_hit_counter_wbf = counter_wbf_values[:,1]
bf_not_hit_counter_wbf = counter_wbf_values[:,2]

pfx_null_ko_counter_wbf = counter_wbf_values[:,3]
pfx_non_null_ko_counter_wbf = counter_wbf_values[:,4]

lrc_null_ko_counter_wbf = counter_wbf_values[:,5]
lrc_non_null_ko_counter_wbf = counter_wbf_values[:,6]

total_accesses_wbf = bf_hit_counter_wbf + bf_not_hit_counter_wbf

bf_energy_per_level_wbf_wonc = total_accesses_wbf * (0.7 + 1.0) # BF computation + BF RAM access
pfx_ram_energy_per_level_wbf_wonc = (pfx_null_ko_counter_wbf + pfx_non_null_ko_counter_wbf) * 2.69 
energy_per_level_wbf_wonc = bf_energy_per_level_wbf_wonc + pfx_ram_energy_per_level_wbf_wonc 

bf_energy_per_level_wbf_wnc = total_accesses_wbf * (0.7 + 1.0) # BF computation + BF RAM access
pfx_ram_energy_per_level_wbf_wnc = (pfx_null_ko_counter_wbf * 1.10) + (pfx_non_null_ko_counter_wbf * 2.69)
energy_per_level_wbf_wnc = bf_energy_per_level_wbf_wnc + pfx_ram_energy_per_level_wbf_wnc 

print("Energy with BF without Null")
print(energy_per_level_wbf_wonc)
print("Energy with BF with Null")
print(energy_per_level_wbf_wnc)

print(sum(total_accesses_wbf))

print("******************************************\n")
print("Type        Total Energy  Avg Energy/ pfx lookup\n")
print("WOBF WONC   {:.3f}                {:.3f} ".format(sum(energy_per_level_wobf_wonc),sum(energy_per_level_wobf_wonc)/10000))#,sum(energy_per_level_wobf_wonc)/sum(total_accesses_wobf))
print("WOBF WNC    {:.3f}                {:.3f} ".format(sum(energy_per_level_wobf_wnc) ,sum(energy_per_level_wobf_wnc) /10000))#,sum(energy_per_level_wobf_wnc) /sum(total_accesses_wobf)
print("WBF  WONC   {:.3f}                {:.3f} ".format(sum(energy_per_level_wbf_wonc) ,sum(energy_per_level_wbf_wonc) /10000))#,sum(energy_per_level_wbf_wonc) /sum(total_accesses_wbf) 
print("WBF  WNC    {:.3f}                {:.3f} ".format(sum(energy_per_level_wbf_wnc)  ,sum(energy_per_level_wbf_wnc)  /10000))#,sum(energy_per_level_wbf_wnc)  /sum(total_accesses_wbf) 
############################################
fig,ax = plt.subplots()
wobf_wonc_plt = ax.bar(trie_levels-0.4,(energy_per_level_wobf_wonc/1000) ,color='red'    ,width=0.2)
wobf_wnc_plt  = ax.bar(trie_levels-0.2,(energy_per_level_wobf_wnc /1000) ,color='black'  ,width=0.2)
wbf_wonc_plt  = ax.bar(trie_levels+0.0,(energy_per_level_wbf_wonc /1000) ,color='magenta',width=0.2)
wbf_wnc_plt   = ax.bar(trie_levels+0.2,(energy_per_level_wbf_wnc  /1000) ,color='blue'   ,width=0.2)

ax.legend( (wobf_wonc_plt[0],wobf_wnc_plt[0],wbf_wonc_plt[0],wbf_wnc_plt[0]),("WOBF_WONC","WOBF_WNC","WBF_WONC","WBF_WNC"))
ax.set_xlabel("Trie Levels")
ax.set_ylabel("Total Energy consumption for all prefixes (x10-9 J)")
plt.show()
