function integer clogb2;
  input [31:0] value;
  begin
    if(value > 1)
    begin
      value = value - 1;
      for (clogb2 = 0; value > 0; clogb2 = clogb2 + 1)
      begin
        value = value >> 1;
      end
    end
    else
      clogb2 = 1;
  end
endfunction

localparam MEM_SIZE = 
(TDEPTH ==  0) ?      1 : (
(TDEPTH ==  1) ?      2 : (
(TDEPTH ==  2) ?      4 : (
(TDEPTH ==  3) ?      8 : (
(TDEPTH ==  4) ?     16 : (
(TDEPTH ==  5) ?     31 : (
(TDEPTH ==  6) ?     61 : (
(TDEPTH ==  7) ?    119 : (
(TDEPTH ==  8) ?    227 : (
(TDEPTH ==  9) ?    399 : (
(TDEPTH == 10) ?    687 : (
(TDEPTH == 11) ?   1112 : (
(TDEPTH == 12) ?   1669 : (
(TDEPTH == 13) ?   2339 : (
(TDEPTH == 14) ?   3210 : (
(TDEPTH == 15) ?   4383 : (
(TDEPTH == 16) ?   5806 : (
(TDEPTH == 17) ?   7385 : (
(TDEPTH == 18) ?   9456 : (
(TDEPTH == 19) ?  11317 : (
(TDEPTH == 20) ?  12312 : (
(TDEPTH == 21) ?  12038 : (
(TDEPTH == 22) ?  11119 : (
(TDEPTH == 23) ?   3089 : (
(TDEPTH == 24) ?    947 : (
(TDEPTH == 25) ?    341 : (
(TDEPTH == 26) ?    101 : (
(TDEPTH == 27) ?     74 : (
(TDEPTH == 28) ?      4 : (
(TDEPTH == 29) ?      2 : (
(TDEPTH == 30) ?      1 : (
0)))))))))))))))))))))))))))))));

localparam MEM_AWIDTH = 
(TDEPTH ==  0) ? clogb2(     1) + 1 : (
(TDEPTH ==  1) ? clogb2(     2) + 1 : (
(TDEPTH ==  2) ? clogb2(     4) + 1 : (
(TDEPTH ==  3) ? clogb2(     8) + 1 : (
(TDEPTH ==  4) ? clogb2(    16) + 1 : (
(TDEPTH ==  5) ? clogb2(    31) + 1 : (
(TDEPTH ==  6) ? clogb2(    61) + 1 : (
(TDEPTH ==  7) ? clogb2(   119) + 1 : (
(TDEPTH ==  8) ? clogb2(   227) + 1 : (
(TDEPTH ==  9) ? clogb2(   399) + 1 : (
(TDEPTH == 10) ? clogb2(   687) + 1 : (
(TDEPTH == 11) ? clogb2(  1112) + 1 : (
(TDEPTH == 12) ? clogb2(  1669) + 1 : (
(TDEPTH == 13) ? clogb2(  2339) + 1 : (
(TDEPTH == 14) ? clogb2(  3210) + 1 : (
(TDEPTH == 15) ? clogb2(  4383) + 1 : (
(TDEPTH == 16) ? clogb2(  5806) + 1 : (
(TDEPTH == 17) ? clogb2(  7385) + 1 : (
(TDEPTH == 18) ? clogb2(  9456) + 1 : (
(TDEPTH == 19) ? clogb2( 11317) + 1 : (
(TDEPTH == 20) ? clogb2( 12312) + 1 : (
(TDEPTH == 21) ? clogb2( 12038) + 1 : (
(TDEPTH == 22) ? clogb2( 11119) + 1 : (
(TDEPTH == 23) ? clogb2(  3089) + 1 : (
(TDEPTH == 24) ? clogb2(   947) + 1 : (
(TDEPTH == 25) ? clogb2(   341) + 1 : (
(TDEPTH == 26) ? clogb2(   101) + 1 : (
(TDEPTH == 27) ? clogb2(    74) + 1 : (
(TDEPTH == 28) ? clogb2(     4) + 1 : (
(TDEPTH == 29) ? clogb2(     2) + 1 : (
(TDEPTH == 30) ? clogb2(     1) + 1 : (
2)))))))))))))))))))))))))))))));

localparam CP_WIDTH = 
(TDEPTH ==  0) ? clogb2(     2) + 1 : (
(TDEPTH ==  1) ? clogb2(     4) + 1 : (
(TDEPTH ==  2) ? clogb2(     8) + 1 : (
(TDEPTH ==  3) ? clogb2(    16) + 1 : (
(TDEPTH ==  4) ? clogb2(    31) + 1 : (
(TDEPTH ==  5) ? clogb2(    61) + 1 : (
(TDEPTH ==  6) ? clogb2(   119) + 1 : (
(TDEPTH ==  7) ? clogb2(   227) + 1 : (
(TDEPTH ==  8) ? clogb2(   399) + 1 : (
(TDEPTH ==  9) ? clogb2(   687) + 1 : (
(TDEPTH == 10) ? clogb2(  1112) + 1 : (
(TDEPTH == 11) ? clogb2(  1669) + 1 : (
(TDEPTH == 12) ? clogb2(  2339) + 1 : (
(TDEPTH == 13) ? clogb2(  3210) + 1 : (
(TDEPTH == 14) ? clogb2(  4383) + 1 : (
(TDEPTH == 15) ? clogb2(  5806) + 1 : (
(TDEPTH == 16) ? clogb2(  7385) + 1 : (
(TDEPTH == 17) ? clogb2(  9456) + 1 : (
(TDEPTH == 18) ? clogb2( 11317) + 1 : (
(TDEPTH == 19) ? clogb2( 12312) + 1 : (
(TDEPTH == 20) ? clogb2( 12038) + 1 : (
(TDEPTH == 21) ? clogb2( 11119) + 1 : (
(TDEPTH == 22) ? clogb2(  3089) + 1 : (
(TDEPTH == 23) ? clogb2(   947) + 1 : (
(TDEPTH == 24) ? clogb2(   341) + 1 : (
(TDEPTH == 25) ? clogb2(   101) + 1 : (
(TDEPTH == 26) ? clogb2(    74) + 1 : (
(TDEPTH == 27) ? clogb2(     4) + 1 : (
(TDEPTH == 28) ? clogb2(     2) + 1 : (
(TDEPTH == 29) ? clogb2(     1) + 1 : (
2))))))))))))))))))))))))))))));

localparam MEM_INIT_FILE = 
(TDEPTH ==  0) ? "memMap0.mem" : (
(TDEPTH ==  1) ? "memMap1.mem" : (
(TDEPTH ==  2) ? "memMap2.mem" : (
(TDEPTH ==  3) ? "memMap3.mem" : (
(TDEPTH ==  4) ? "memMap4.mem" : (
(TDEPTH ==  5) ? "memMap5.mem" : (
(TDEPTH ==  6) ? "memMap6.mem" : (
(TDEPTH ==  7) ? "memMap7.mem" : (
(TDEPTH ==  8) ? "memMap8.mem" : (
(TDEPTH ==  9) ? "memMap9.mem" : (
(TDEPTH == 10) ? "memMap10.mem" : (
(TDEPTH == 11) ? "memMap11.mem" : (
(TDEPTH == 12) ? "memMap12.mem" : (
(TDEPTH == 13) ? "memMap13.mem" : (
(TDEPTH == 14) ? "memMap14.mem" : (
(TDEPTH == 15) ? "memMap15.mem" : (
(TDEPTH == 16) ? "memMap16.mem" : (
(TDEPTH == 17) ? "memMap17.mem" : (
(TDEPTH == 18) ? "memMap18.mem" : (
(TDEPTH == 19) ? "memMap19.mem" : (
(TDEPTH == 20) ? "memMap20.mem" : (
(TDEPTH == 21) ? "memMap21.mem" : (
(TDEPTH == 22) ? "memMap22.mem" : (
(TDEPTH == 23) ? "memMap23.mem" : (
(TDEPTH == 24) ? "memMap24.mem" : (
(TDEPTH == 25) ? "memMap25.mem" : (
(TDEPTH == 26) ? "memMap26.mem" : (
(TDEPTH == 27) ? "memMap27.mem" : (
(TDEPTH == 28) ? "memMap28.mem" : (
(TDEPTH == 29) ? "memMap29.mem" : (
(TDEPTH == 30) ? "memMap30.mem" : (
0)))))))))))))))))))))))))))))));

localparam TOT_DEPTH = 31;
