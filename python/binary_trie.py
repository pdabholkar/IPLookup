'''
Functions necessary for building and traversing a binary trie.

'''

import numpy as np
import sys,getopt

################################################################################
# Description : Procedure to insert an item in a binary trie.                  #
# Inputs      : btrie   - Pointer to the head of the binary trie where the     #
#                         entry is to be inserted                              #
#                         Each entry in the ram is as follows                  #
#                         [0] Next hop information                             #
#                         [1] Left pointer address                             #
#                         [2] Right pointer address                            #
#               prefix  - Prefix to be inserted in Hex format                  #
#               plen    - length of prefix in decimal                          #
#               nh      - next hop information                                 #
#               lastram - address of the last node inserted into the binary    #
#                         trie                                                 #
#               tvecram - RAM containing the test vectors for testing          #
# Return      : tvecram - updated with the entry added to the end of the       #
#                         tvecram                                              #
#               lastram - address of the lastest node inserted into the binary #
#                         trie                                                 #
################################################################################
def insertitem(btrie,prefix,plen,nh,lastram,tvecram):
  head = 0 
  tdepth = 0
  done = 0
  plen = int(plen)
  nh = int(nh,base=16)
  nh = nh if (nh !=0) else 1
  
  while (done == 0):
    if (tdepth < plen):
      if (prefix[tdepth] == '1'): # check if prefix msb is '1'
        new_head = btrie[head][2]    # next node is right child
        #print(tdepth,prefix[tdepth],btrie[head], new_head)
        if(new_head == 0): # New node needs to be inserted
          btrie[head][2] = lastram + 1  
          head = lastram + 1
          lastram += 1
        else: # Move to the next node in the trie
          head = new_head
      else:                          # if prefix msb is '0'
        new_head = btrie[head][1]    # next node is left child
        #print(tdepth,prefix[tdepth],btrie[head], new_head)
        if(new_head == 0): # New node needs to be inserted
          btrie[head][1] = lastram + 1  
          head = lastram + 1
          lastram += 1
        else: # Move to the next node in the trie.
          head = new_head
      tdepth += 1 # Increase the trie depth
    else:
      done=1
      btrie[head][0] = nh

  # print("Range is ")
  # Create a test vector list for this entry
  lb = int(prefix,base=2) & (~np.uint32(2**(32-plen)-1))  # Lower bound
  ub = int(prefix,base=2) | ( np.uint32(2**(32-plen)-1))  # Upper bound

  # Convert to hex
  x_lb = '{0:0<8x}'.format(lb,base=2)                      
  x_ub = '{0:0<8x}'.format(ub,base=2)                      
  x_prefix = '{0:0<8x}'.format(int(prefix,base=2))

  #print("prefix "+str(x_prefix)+"/"+str(plen)+" inserted at address "+str(head) + " lb and ub "+str(x_lb)+ " "+str(x_ub)+ " NH :"+str(nh))

  # Number of test vectors
  num_tvec = np.random.random_integers(0,10)

  # Generate a np.array of test vectors within the lower and upper bound
  tvecs = np.random.random_integers(lb,ub,num_tvec)

  # Converting the np.array to a list and add to the test vector RAM
  tvecs = list(tvecs)
  tvecram += tvecs

  return lastram,tvecram


################################################################################
# Description : Find an item in the Binary Trie                                #
# Inputs      : btrie   - The pointer to the the head of the trie contained    #
#                         in a ram structure.                                  #
#                         Each entry in the ram is as follows                  #
#                         [0] Next hop information                             #
#                         [1] Left pointer address                             #
#                         [2] Right pointer address                            #
#               key     - The key to be searched                               #
# Return      : found   - 1/0 value indicating key found or not found in the   #
#                         trie                                                 #
#               hitdata - number of reads resulting in data read from the      #
#                         SRAM                                                 #
#               hitnull - number of reads resulting in NULL read from the      #
#                         SRAM                                                 #
################################################################################
def finditem(btrie,key):
  head = 0 # To start with, the head is the first location in the btrie
  tdepth = 0 # Trie depth traversed
  done = 0 # implies trie traversal complete
  found = 0 # implies key found in the trie.

  klen = len(key)
  #keymatch = [] # debug
  #print(key) # debug
  pathintrie=[''] # This is for backtracking through the trie.
  hitdata = 0 # This is for statistics collection purposes.
  hitnull = 0 # This is for statistics collection purposes.
  deepesttdepth = 0

  while (done == 0):
    if (tdepth <= (klen-1)): # Check that we are not at the last level in the trie
      pathintrie.append(head)
      if (key[tdepth] == '1'): # If MSB of the key is '1'
        new_head = btrie[head][2] # next head is right child
      else:                       # if MSB of the key is '0' 
        new_head = btrie[head][1] # next head is left child

      #print(key,tdepth,btrie[head],key[tdepth], new_head) # debug
      if (new_head == 0): # If next node is empty, i.e. present node is leaf node
        hitnull += 1
        if (btrie[head][0] != 0): # Check if NHI available
          found = 1
          deepesttdepth = tdepth
          #keymatch=key[:tdepth] # debug
          done = 1
        else: # Else start backtracking
          hitnull += 1
          while((tdepth >= 0) and (found == 0)): # to the top of the trie and till node is not found 
            # Go up one level in the trie along the path followed while coming down
            head = pathintrie[tdepth] # Notice the slightly modified range
            #print(tdepth, btrie[head],key[tdepth], head) # debug
            if (btrie[head][0] != 0): # Check if NHI available 
              found = 1
              #keymatch=key[:tdepth] # Notice the slightly modified range #debug
              done = 1
              hitdata += 1
            else: # This is not a prefix node
              hitnull += 1

            tdepth -= 1
      else: # if next node is not empty, move down the trie.
        head = new_head
        tdepth += 1
        hitdata += 1
    elif (tdepth == klen): #Last node in the trie reached
      #print(tdepth,btrie[head]) #debug
      if (btrie[head][0] != 0): # Check if NHI available
        found = 1
        deepesttdepth = tdepth
        #keymatch=key #debug
        done = 1
        hitdata += 1
      else: # key not found in the trie
        done = 1
        hitnull += 1
    else: # key not found in the trie
      done = 1
      hitnull += 1
      #print("done, not found")

  #keymatch = '{0:0<32s}'.format(keymatch) #debug
  #keymatch = '{0:0<8x}'.format(int(keymatch,base=2)) #debug
  x_key     = '{0:0<8x}'.format(int(key,base=2))
  #if(found == 1):
  #  print("DA "+str(x_key)+" found at address "+str(head)+" ; NH - "+str(btrie[head][0])) 

  #if(deepesttdepth == 32):
  #  print("Prefix is {}, found = {}".format(x_key,found))

  #print(btrie[4498])  
  return found,hitdata,hitnull,deepesttdepth
  

################################################################################
# Description : Initialise the trie structure for storage                      #
# Inputs      : None                                                           #
# Returns     : btrie  - A pointer to the the head of the trie stored          #
#                        in a ram structure. Space allocated for 10000000      #
#                        entries in the RAM.                                   #
#                        Each entry in the ram is as follows                   #
#                        [0] Next hop information                              #
#                        [1] Left pointer address                              #
#                        [2] Right pointer address                             #
#               tvecram- This is the test vector ram. Not initialised          #
################################################################################
def inittrie():
  tdepth = 0
  btrie = np.zeros([10000000,3],dtype='int') 
  tvecram = []
  return btrie,tvecram



if __name__ == "__main__":

  prefix = '{0:0<32}'.format(sys.argv[1])
  plen = int(sys.argv[2])
  findprefix = '{0:0<32}'.format(sys.argv[3])

  #print(prefix)
  insertitem(prefix,plen,145,0)
  #print("========")
  #print(findprefix)
  finditem(findprefix)



