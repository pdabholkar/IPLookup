'''
Implements a modified version of the Jenkins hash for 32-bit IPv4 addresses. 
Only the 'final' function as defined by http://burtleburtle.net/bob/c/lookup3.c is implemented here.
To hash a 128-bit IPv6 address, I have used a technique similar to the one suggested 
on the same website

If you want to find a hash of, say, exactly 7 integers, do
  a = i1;  b = i2;  c = i3;
  mix(a,b,c);
  a += i4; b += i5; c += i6;
  mix(a,b,c);
  a += i7;
  final(a,b,c);
then use c as the hash value.

For the 4-word IPv6 address I will be doing
  a=i1; b=i2; c = i3
'''

import sys,getopt
import numpy as np

def split_every(s,n):
  return [s[i:i+n] for i in range(0,len(s),n)]

def printfh (a,b,c):
  #print("{0:10x} {1:10x} {2:10x}".format(a,b,c))
  return

def rol32 (din, rol_bits):
  dout = (((din << rol_bits) & 0xffffffff) | ((din >> (32-rol_bits)) & 0xffffffff))
  return dout


def mix (a,b,c):

  a &= 0xffffffff; b &= 0xffffffff; c &= 0xffffffff;
 
  a = (a - c) & 0xffffffff ; a ^= rol32(c,4)   ; c = (c + b) & 0xffffffff;
  b = (b - a) & 0xffffffff ; b ^= rol32(a,6)   ; a = (a + c) & 0xffffffff;
  c = (c - b) & 0xffffffff ; c ^= rol32(b,8)   ; b = (b + a) & 0xffffffff;
  a = (a - c) & 0xffffffff ; a ^= rol32(c,16)  ; c = (c + b) & 0xffffffff;
  b = (b - a) & 0xffffffff ; b ^= rol32(a,19)  ; a = (a + c) & 0xffffffff;
  c = (c - b) & 0xffffffff ; c ^= rol32(b,4)   ; b = (b + a) & 0xffffffff;

  return a,b,c

def final(a,b,c):

  a &= 0xffffffff; b &= 0xffffffff; c &= 0xffffffff;
 
  #print("Final operation started") 
  c ^= b; c = (c - rol32(b,14)) & 0xffffffff
  printfh(a,b,c)
  a ^= c; a = (a - rol32(c,11)) & 0xffffffff 
  printfh(a,b,c)
  b ^= a; b = (b - rol32(a,25)) & 0xffffffff
  printfh(a,b,c)
  c ^= b; c = (c - rol32(b,16)) & 0xffffffff
  printfh(a,b,c)
  a ^= c; a = (a - rol32(c,4))  & 0xffffffff
  printfh(a,b,c)
  b ^= a; b = (b - rol32(a,14)) & 0xffffffff
  printfh(a,b,c)
  c ^= b; c = (c - rol32(b,24)) & 0xffffffff
  printfh(a,b,c)

  return a,b,c

def xorfold (a,numbits):
  if (numbits <=16): 
    return ((a & ((2**numbits)-1)) ^ ((a >> numbits) & ((2**numbits)-1)))
  else:
    return (a & ((2**numbits)-1))
  

def hashword2 (ipaddr,seed,hashsize):
  a = b = c = (0xdeadbeef + (4<<2) + seed[0]) 
  c += seed[1]

  #print("IP address array is ")
  #print(ipaddr)

  #print("Initialisation of a,b,c registers")
  printfh(a,b,c)

  if (len(ipaddr) == 4):
    a += int(ipaddr[0],base=16)
    b += int(ipaddr[1],base=16)
    c += int(ipaddr[2],base=16)
    
    a,b,c = mix(a,b,c)
    
    a += int(ipaddr[3],base=16)
  
  elif (len(ipaddr) == 1):
    a += int(ipaddr[0],base=16)
  
  #print("Before Final operation")
  printfh(a,b,c)
  
  a,b,c = final(a,b,c)
  #print("Hash computation done. Final a,b,c")
  printfh(a,b,c)
  
  c = xorfold(c,hashsize)
  b = xorfold(b,hashsize)
  
  #print("After XOR folding")
  printfh(0,b,c)
  return c,b

def makehash(ipaddr,length,seed=[1,2]): 
  #print("IP address is "+ipaddr)
  if ('.' in ipaddr):
    ipaddr = ipaddr.split('.')
    ipaddrstr = ''.join(['{:02x}'.format(int(ipbyte)) for ipbyte in ipaddr])
  elif(':' in ipaddr):
    ipaddr = ipaddr.split(':')
    ipaddrstr = ''.join(['{:04x}'.format(int(ipbyte,base=16)) for ipbyte in ipaddr])
  else:
    print("Incorrect IP address specified\n")
  
  ipaddr_arr = split_every(ipaddrstr,8)
  c,b = hashword2(ipaddr_arr,seed,int(length))   
  return [c,b]
  #print("b = {0:d} c = {1:d}".format(int(b),int(c)))
  #print("b = {0:b} c = {1:b}".format(int(b),int(c)))
  
if __name__ == "__main__":
  ipaddr = sys.argv[1]
  length = sys.argv[2]
  makehash(ipaddr,length)
