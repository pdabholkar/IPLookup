'''
Functions necessary for building and traversing a binary trie.
'''

import numpy as np
import sys,getopt,string
import crc_ser as crc
import math

################################################################################
# Description : Procedure to split an IP address prefix into its constituent   #
#               elements - msb, lsb, xlen ap and zlen                          #
# Input       : prefix - in binary                                             #
#               plen   - length of the prefix in decimal                       #
# Output      : msb, lsb, xlen, active part (ap) and zlen                      #
################################################################################
def splitPrefix(prefix,plen):
  prefix = prefix[:plen] # This is to ensure that x, ap and z are computed only on the valid part of the prefix
  msb = prefix[0]
  lsb = prefix[plen-1]
  xlen = prefix.find('0') if (msb == '1') else prefix.find('1')
  if (xlen == -1):
    xlen = plen
    zlen = 0
    ap = '*'
  else:
    lssrb = prefix.rfind('0') if (lsb == '1') else prefix.rfind('1')
    zlen = plen - 1 - lssrb
    ap = prefix[xlen:(plen - zlen)] if ((xlen+zlen) < int(plen)) else '*'

  return msb,lsb,xlen,ap,zlen


################################################################################
# Description : Procedure to split an IP address to be looked up into its      #
#               constituent elements - msb, lsb, xlen and ap                   #
# Input       : key   - in binary                                              #
#               klen  - length of the key in decimal                           #
# Output      : msb, lsb, xlen and active part (ap)                            #
################################################################################
def splitKey(key,klen):
  msb = key[0]
  lsb = key[klen-1]
  xlen = key.find('0') if (msb =='1') else key.find('1')
  if (xlen == -1):
    xlen = klen
    ap =  '*'
  else:
    ap = key[xlen:]

  return msb,lsb,xlen,ap


################################################################################
# Description : Procedure to insert an item in a compact trie. Details of what #
#               is a compact trie has been presented in a paper by Erdem. The  #
#               trie traversal algorithm proposed by that paper is however     #
#               insufficient to handle all prefixes and therefore this improved#
#               algorithm                                                      #
# Inputs      : ctrie  - Pointer to the head of the compact trie               #
#                        Each entry in the compact trie is as follows          #
#                        [0] list of prefixes with their MSB, LSB, |x|, |z|    #
#                            and NHI values stored as a list                   #
#                        [1] left child                                        #
#                        [2] right child                                       #
#                        [3] epsilon node                                      #
#                        [4] level and depth information for the node          #
#                                                                              #
#               prefix - Prefix to be inserted in a hex format.                #
#               plen   - length of prefix to be inserted. The active component #
#                        of the prefix and the values of |x| and |z| would all #
#                        be computed inside this function                      #
#               nh     - next hop information                                  #
#               lastram- address of the last node inserted into the ctrie      #
#               tvecram- RAM containing the test vectors for testing           #
# Return      : tvecram- updated with the test vectors added to the end of the #
#                        tvecram                                               #
#               lastram- address of the latest node inserted into the ctrie    #
################################################################################
def insertitem(bf,bfsize,ctrie,prefix,plen,nh,lastram,tvecram,Ptrie):
  head    = 0                               # Head is location '0' in cTrie to start with 
  tlevel  = 0                               # Running level in trie 
  done    = 0                               # Flag to indicate prefix insertion done

  plen    = int(plen)
  nh      = int(nh,base=16)
  nh      = nh if (nh != 0) else 1          # Massage NH information obtained from table in the correct form

  msbp,lsbp,xlenp,app,zlenp = splitPrefix(prefix,plen)
  applen = len(app) if (app != '*') else 0

  if(bf != 0):                              # Do this only if the bloom filter passed is non zero
    if (app != '*'):
      bfHashKey = ''.join("{:1b}{:03b}{}{:1b}".format(int(msbp),int(xlenp),app,int(lsbp))) 
    else:
      bfHashKey = ''.join("{:1b}{:03b}{:1b}".format(int(msbp),int(xlenp),int(lsbp))) 

    bfidxlen = int(math.ceil(math.log((bfsize-1),2)))

    sr = bytearray(32);
    for i in bfHashKey:
      sr = crc.crc32(sr,i)
    
    sr = ''.join(str(j) for j in [int(i) for i in sr])

    #sr=sr+sr                                  # for 64 bit CRC

    bf[int(sr[0:bfidxlen],base=2)] = 1        # first BF index for 32 and 64 bit CRC
    bf[int(sr[8:(8+bfidxlen)],base=2)] = 1    # second BF index for 32 bit CRC
    #bf[int(sr[16:(16+bfidxlen)],base=2)] = 1  # second BF index for 64 bit CRC
    #bf[int(sr[32:(32+bfidxlen)],base=2)] = 1  # second BF index for 64 bit CRC

  else:
    pass

  infop  = [msbp,lsbp,xlenp,zlenp,nh]       # collect information to be stored in the leaf node
  while (done == 0):
    if (tlevel < applen):                   # check if current level goes beyond prefix AP length
      if (ctrie[head][3] == 0):             # not a link node so check the bits  
        if (app[tlevel] == '1'):            # check if msb of APk is '1'
          new_head = ctrie[head][2]         # next node is right child
          if (new_head == 0):               # if child node doesn't exist
            ctrie[head][2] = lastram+ 1
            head = lastram + 1;             # insert a new node
            lastram += 1
          else:                             # else if child exists
            head = new_head                 # make child the new head
        else:                               # else if msb of APk is '0'
          new_head = ctrie[head][1]         # next node is left child
          if (new_head == 0):               # if child node doesn't exist
            ctrie[head][1] = lastram + 1
            head = lastram + 1              # insert a new node
            lastram += 1
          else:                             # else if child exists
            head = new_head                 # make child the new head
        tlevel += 1                         # increase the trie level  
      else:                                 # not a link node, so do not increment tlevel and move down to next node
        new_head = ctrie[head][1]           # Only the left child address is valid and points to the next node down
        head = new_head
    else:                                   # reached the last level in the existing cTrie
      done = 1                              # Go no further. You are done :)
      if (ctrie[head][0] == 0):             # If prefix information does not already exist
        ctrie[head][0] = [list(infop)]      # Store the current prefix information in location [0] of the node
      elif (len(ctrie[head][0]) == Ptrie):  # If prefix already exists and num(prefixes) already equal to Ptrie
        node = lastram + 1                  # Create a new node
        ctrie[node][0] = ctrie[head][0]     # Copy information from old node to new node
        ctrie[node][1] = ctrie[head][1]
        ctrie[node][2] = ctrie[head][2]
        ctrie[node][3] = ctrie[head][3]

        ctrie[head][0] = [list(infop)]      # Store the new prefix information in the old node
        ctrie[head][1] = node;              # New node is epsilon child of new node
        ctrie[head][2] = 0
        ctrie[head][3] = 1;                 # Create an epsilon link to new node
        lastram +=1
      else : # append to the same node      # If prefix exists, but node not full
        ctrie[head][0].append(list(infop))  # append new prefix information to location [0] of the node

  return lastram,tvecram 

################################################################################
# Description : Find an item in the Compact Trie                               #
#               In the function a new term Nzo is defined for a prefix         #
#               This is similar to the 'x' substring of the prefix, but        #
#               computed instead on the instantaneous apk string.              #
#               It tells us the number of consecutive bits with the same value #
#               as the most significant bit in the apk                         #
# Inputs      :cbtrie   - The pointer to the the head of the trie contained    #
#                         in a ram structure.                                  #
#                         Each entry in the ram is as follows                  #
#                         Each entry in the compact trie is as follows         #
#                         [0] list of prefixes with their MSB, LSB, |x|, |z|   #
#                             and NHI values stored as a list                  #
#                         [1] left child                                       #
#                         [2] right child                                      #
#                         [3] Epsilon or regular node                          #
#                         [4] level and depth information for the node         #
#               key     - The key to be searched                               #
# Return      : nhik    - Next hop information for the key, if not found the   #
#                         nhik value is 0                                      #
#               hitdatap- number of reads resulting in data read of prefix info#
#                         in SRAM                                              #
#               hitdatac- number of reads resulting in data read of child info #
#                         in SRAM                                              #
#               hitnullp- number of reads resulting in NULL read of the prefix #
#                         field of the trie in the SRAM                        #
#               hitnullc- number of reads resulting in NULL read of the child  #
#                         field of the trie in the SRAM                        #
#               tlevel  - level in the trie, where the search ends             #
#               tdepth  - depth in the trie (pipeline stages), where the search#
#                         ends                                                 #
################################################################################
def finditem(opdirname,bf,ctrie,key):
  head        = 0                           # head is the first location in the ctrie
  tlevel      = 0                           # trie level traversed
  tdepth      = 0                           # number of pipeline stages required.
  done        = 0                           # flag for trie traversal complete
  found       = 0                           # flag for key found in trie
  foundtdepth = 0                           # unused at the moment               
  foundtlevel = 0                           # unused at the moment                            
  foundapk    = 0                           # unused at the moment

  hitdatap = 0                              # This is for statistics collection purposes
  hitdatap_nfound = 0
  hitdatac = 0                              # This is for statistics collection purposes
  hitnullp = 0                              # This is for statistics collection purposes
  hitnullc = 0                              # This is for statistics collection purposes

  klen = len(key)
  msbk,lsbk,xlenk,apk = splitKey(key,klen)
  apklen = klen-xlenk

  bfapk = ''#apk[0]

  nhik=0
  zlen_match = 0                            # zlen of match prefix in this CT node, needed when > 1 prefix matches in the same node

  #print("--key--")
  #print(key)
  #print("-------")
  while (done == 0):
    epsilon = 0

    if (bf != 0): 
      bfHashKey = ''.join("{:1b}{:03b}{}{:1b}".format(int(msbk),int(xlenk),bfapk,int(apk[0]))) 

      sr = bytearray(32);
      for i in bfHashKey:
        sr = crc.crc32(sr,i)
      
      sr = ''.join(str(j) for j in [int(i) for i in sr])

      #sr=sr+sr        # for 64 bit CRC

      bfidxlen = int(math.ceil(math.log((len(bf)-1),2)))
      bfFound = 1 if(( bf[int(sr[0:bfidxlen],base=2)] == 1) and (bf[int(sr[8:(8+bfidxlen)],base=2)] == 1)) else 0   # for 32 bit CRC
      #bfFound = 1 if(( bf[int(sr[0:bfidxlen],base=2)] == 1) and (bf[int(sr[16:(16+bfidxlen)],base=2)] == 1) and (bf[int(sr[32:(32+bfidxlen)],base=2)] == 1)) else 0   # for 64 bit CRC
      #print(key,bfapk,bfHashKey,sr,bfidxlen,bfFound)
    else:
      bfFound = 1
    
    if (bfFound == 1):
      if(ctrie[head][0] != 0):                # Prefix information is not empty
        pcnt = 0                              # count of prefixes checked in this CT node
        plist = ctrie[head][0]                # list of prefixes in this CT node
        if (apk[0] == '1'):                   # if apk[0] == 1, find where the next '0' is and this will tell you the 
          Nzo = apk.find('0')                 # length of the all-ones string.
        else:                                 # if apk[0] == 0, find where the next '1' is and this will tell you the 
          Nzo = apk.find('1')                 # length of the all-zeros string
        Nzo = len(apk) if (Nzo == -1) else Nzo# if Nzo is -1 then Nzo is the whole length of the active part
        while (pcnt < len(plist)):            # Perform this test for all prefixes in the node
          if (((tlevel     == 0) and          # special for head node with prefixes whose zlen is 0  
               (msbk       == plist[pcnt][0]) and
               (xlenk      >= plist[pcnt][2]) and
               (0          == plist[pcnt][3])
              ) or  
              (
               (msbk       == plist[pcnt][0]) and
               (apk[0]     == plist[pcnt][1]) and 
               (xlenk      == plist[pcnt][2]) and
               (Nzo        >= plist[pcnt][3]) and
               (zlen_match <  plist[pcnt][3])
              ) 
             ):
              nhik        = plist[pcnt][4]
              zlen_match  = plist[pcnt][3]
              found       = 1
              foundtdepth = tdepth
              foundtlevel = tlevel
              foundapk    = apk

          pcnt += 1
        if (foundtdepth != tdepth):  
          hitdatap_nfound += 1                # matching prefix not found, so increment nofound count by 1
        hitdatap += 1                         # node contains prefixes, so increment hit data count
      else:
        hitnullp += 1                         # node contains no prefixes, so increment hit null prefix count
      
    #fbf = open("bloomfilter.txt","a")
    #fbf.write("found = {} ctrie[head][0] = {}\n".format(found,ctrie[head][0]))
    #fbf.close()
    
    if (ctrie[head][3] == 1):               # check child nodes to move down the trie
      epsilon = 1                           # set the epsilon flag 
     
    # if epsilon flag is set, look only at the left child, else check apk[0] bit and decide whether to take the right
    # or the left child branch
    new_head = ctrie[head][1] if (epsilon == 1) else (ctrie[head][2] if (apk[0] == '1') else ctrie[head][1])
    if (new_head == 0):                     # no child nodes
      hitnullc += 1                         # increment hit null child counter.
      done = 1                              # search done
    else:
      if(epsilon == 0):                     # if this is a regular link and not an epsilon link
        zlen_match = 0                      # Reset zlen_match
        tlevel +=1                          # and increment the trie level
        bfapkl = list(bfapk)
        #print(bfapk,bfapkl,apk[0])
        bfapkl.extend(apk[0])
        bfapk = ''.join(bfapkl)
        #print(bfapk,bfapkl,apk[0])
        #print("------------------------")
        apk = apk[1:]                       # shift APk one bit left before moving to the next level

      head = new_head                       # reassign head as the new head
      hitdatac +=1                          # because child exists, increment hit data counter

    tdepth +=1
    if (tlevel == apklen):                  # If new trie level is greater than length of active part of key
      done = 1                              # Go no further. Done with the search :)

  if (found == 0 ):                         # If not found
    print("key "+str(key)+" not found in cTrie")     # Raise an error message on the console
  #else:
  #  print("key "+str(key)+" found in memMap at tdepth{} and tlevel {}".format(foundtlevel, foundtdepth)) # Raise an error message on the console

  #print(foundtdepth,foundtlevel)
  
  return nhik,hitdatap,hitdatap_nfound,hitdatac,hitnullp,hitnullc,tlevel,tdepth

################################################################################
# Description : Initialise the trie structure for storage                      #
# Inputs      : None                                                           #
# Returns     : ctrie  - A pointer to the the head of the trie stored          #
#                        in a ram structure. Space allocated for 10000000      #
#                        entries in the RAM.                                   #
#                        Each entry in the ram is as follows                   #
#                        [0] list of prefixes with their MSB, LSB, |x|, |z|    #
#                            and NHI values stored as a list                   #
#                        [1] Left pointer address                              #
#                        [2] Right pointer address                             #
#                        [3] Epsilon or regular node                           #
#                        [4] level and depth information for the node          #
#               tvecram- This is the test vector ram. Not initialised          #
################################################################################
def inittrie():
  ctrie = np.zeros([1000000,5],dtype='int') 
  ctrie = ctrie.tolist()
  tvecram = []
  return ctrie,tvecram

################################################################################
#                                                                              # 
################################################################################
def finditem_in_mem (opdirname,bf,memMap,key):
  head        = 0                                 # head is the first location in the ctrie
  tlevel      = 0                                 # trie level traversed
  tdepth      = 0                                 # number of pipeline stages required.
  done        = 0                                 # flag for trie traversal complete
  found       = 0                                 # flag for key found in trie
  foundtdepth = 0                                 # unused at the moment               
  foundtlevel = 0                                 # unused at the moment                            
  foundapk    = 0                                 # unused at the moment

  hitdatap_bfbitT_pfxT = 0
  hitdatap_bfbitT_pfxF_bffldT = 0
  hitdatap_bfbitT_pfxF_bffldF = 0
  hitdatap_nmatch = 0 
  hitdatac = 0                                    # This is for statistics collection purposes
  hitnullp = 0                                    # This is for statistics collection purposes
  hitnullc = 0                                    # This is for statistics collection purposes

  klen = len(key)
  msbk,lsbk,xlenk,apk = splitKey(key,klen)
  apklen = klen-xlenk

  bfapk = ''
  #bfdump_fname = str(opdirname)+"bfdump.log"
  nhik=0
  zlen_match = 0                                  # zlen of match prefix in this CT node, needed when > 1 prefix matches in the same node
  try:
    bfused = len(bf)
  except:
    bfused = 0

  while (done == 0):
    epsilon = 0
    curNode = memMap[tdepth][head]

    if (bfused != 0):
      if (len(bf[tdepth]) != 0): 
        bfHashKey = ''.join("{:1b}{:03b}{}{:1b}".format(int(msbk),int(xlenk),bfapk,int(apk[0]))) 

        sr = bytearray(32);
        for i in bfHashKey:
          sr = crc.crc32(sr,i)
        
        sr = ''.join(str(j) for j in [int(i) for i in sr])

        #sr=sr+sr # for 64 bit CRC
        bfidxlen = int(math.ceil(math.log((len(bf[tdepth])-1),2)))
        
        # bfidxlen is used as is because sub-string extraction in python is not lower bound inclusive
        bfFound = 1 if(( bf[tdepth][int(sr[0:bfidxlen],base=2)] == 1) and (bf[tdepth][int(sr[8:(8+bfidxlen)],base=2)] == 1)) else 0  # for 32 bit CRC
        #bfFound = 1 if(( bf[tdepth][int(sr[0:bfidxlen],base=2)] == 1) and (bf[tdepth][int(sr[16:(16+bfidxlen)],base=2)] == 1) and (bf[tdepth][int(sr[32:(32+bfidxlen)],base=2)] == 1)) else 0   # for 64 bit CRC
        #fname = open(bfdump_fname,"a")
        #fname.write("{:2} {:4} {:32} {:25} {:30} {:32} {:2} {:3} {:3} {:1}\n".format(tdepth,head,key,bfapk,bfHashKey,sr,bfidxlen,int(sr[0:bfidxlen],base=2),int(sr[8:(8+bfidxlen)],base=2),bfFound))
        #fname.close()
      else:
        bfFound = 0
    else:
      bfFound = 1

    if (bfFound == 1):
      if(curNode[0] != 0):                          # Prefix information is not empty
        pcnt = 0                                    # count of prefixes checked in this CT node
        plist = curNode[0]                          # list of prefixes in this CT node
        if (apk[0] == '1'):                         # if apk[0] == 1, find where the next '0' is and this will tell you the 
          Nzo = apk.find('0')                       # length of the all-ones string.
        else:                                       # if apk[0] == 0, find where the next '1' is and this will tell you the 
          Nzo = apk.find('1')                       # length of the all-zeros string
        Nzo = len(apk) if (Nzo == -1) else Nzo      # if Nzo is -1 then Nzo is the whole length of the active part
        #fname = open(bfdump_fname,"a")
        bf_fields_match = 0
        while (pcnt < len(plist)):                  # Perform this test for all prefixes in the node
          if (((tlevel     == 0) and                # special for head node with prefixes whose zlen is 0  
               (msbk       == plist[pcnt][0]) and   # MSBs match
               (xlenk      >= plist[pcnt][2]) and
               (0          == plist[pcnt][3])
              ) or  
              (
               (msbk       == plist[pcnt][0]) and   # MSBs match
               (apk[0]     == plist[pcnt][1]) and   # MSB of APk matches LSBp 
               (xlenk      == plist[pcnt][2]) and   # Mp and Mk match i.e. |x| for both prefix and key are same 
               (Nzo        >= plist[pcnt][3]) and   # Number of zeros/ones after the MSB of APk is greater than |z| of prefix
               (zlen_match <  plist[pcnt][3])       # |z| of this prefix is greater than that of previously matched prefix in the same node or same level
              ) 
             ):
              nhik        = plist[pcnt][4]
              zlen_match  = plist[pcnt][3]
              found       = 1
              foundtdepth = tdepth
              foundtlevel = tlevel
              foundapk    = apk

          # Check if at least the fields that were used to compute the BF are matching. If they are not, it implies that the BF gave a false positive
          if(
             (msbk       == plist[pcnt][0]) and   # MSBs match
             (apk[0]     == plist[pcnt][1]) and   # MSB of APk matches LSBp 
             (xlenk      == plist[pcnt][2])       # Mp and Mk match i.e. |x| for both prefix and key are same 
            ):
              bf_fields_match = 1

          #fname.write("\t\tkey : {:1} {:1} {:3} {:3} {:3} | pfx : {:1} {:1} {:3} {:3} |\n".format(msbk,apk[0],xlenk,Nzo,zlen_match,plist[pcnt][0],plist[pcnt][1],plist[pcnt][2],plist[pcnt][3]))
          pcnt += 1
        

        if (foundtdepth == tdepth):
          hitdatap_bfbitT_pfxT += 1
          #fname.write(" bf bits match, prefix matched, found nhik {}\n".format(nhik))
        else:
          if (bf_fields_match == 0):
            hitdatap_bfbitT_pfxF_bffldF += 1
            #fname.write(" bf bits match, prefix not matched, BF fields not matched \n")
          else:
            hitdatap_bfbitT_pfxF_bffldT +=1
            #fname.write(" bf bits match, prefix not matched, BF fields match \n")
        #fname.close()
      else:
        #fname = open(bfdump_fname,"a")
        #fname.write(" bf bits match, no prefix info present\n")
        #fname.close()
        hitnullp += 1                               # node contains no prefixes, so increment hit null prefix count
   
    if (curNode[3] & 0x2 == 0x2):                 # This is an epsilon node
      new_head = curNode[1]
    else:
      bfapkl = list(bfapk)
      #print(bfapk,bfapkl,apk[0])
      bfapkl.extend(apk[0])
      bfapk = ''.join(bfapkl)
      #print(bfapk,bfapkl,apk[0])
      #print("------------------------")
      new_head = curNode[2] if (apk[0] == '1') else curNode[1]
      if (new_head == 0x7ffff):                   # No children
        hitnullc += 1                             # for statistics purposes
        done = 1                                  # stop the search
      else:
        zlen_match = 0                            # Reset zlen_match
        tlevel +=1                                # and increment the trie level
        apk = apk[1:]                             # shift APk one bit left before moving to the next level
        hitdatac += 1                             # Node has children, so increment the hitdatac count
    head = new_head
    tdepth += 1                                   # incremenet the trie depth  
    if (tlevel == apklen):                        # If new trie level is greater than length of active part of key
      done = 1                                    # Go no further. Done with the search :)


  #fname = open(bfdump_fname,"a")
  if (found == 0 ):                               # If not found
    print("key "+str(key)+" not found in memMap") # Raise an error message on the console
    #fname.write("{} not found\n".format(key))
  else:
    #fname.write("{} found at depth {} level {} NHI is {}\n".format(key,foundtlevel,foundtdepth,nhik))
    #print("key "+str(key)+" found in memMap at tdepth{} and tlevel {}".format(foundtlevel, foundtdepth)) # Found message on the console
    pass
  #fname.close()


  return nhik,hitdatap_bfbitT_pfxT,hitdatap_bfbitT_pfxF_bffldT,hitdatap_bfbitT_pfxF_bffldF,hitdatac,hitnullp,hitnullc,tlevel,tdepth

if __name__ == "__main__":


  #prefixluttable = np.array([['11'        ,2,  '1' ], 
  #                           ['111'       ,3,  '2' ],
  #                           ['11101'     ,5,  '3' ],
  #                           ['0011110'   ,7,  '4' ],
  #                           ['11110'     ,5,  '5' ],
  #                           ['11100'     ,5,  '6' ],
  #                           ['110101'    ,6,  '7' ],
  #                           ['1110100010',10, '8' ],
  #                           ['111011'    ,6,  '9' ],
  #                           ['110001'    ,6,  '11'],
  #                           ['111010'    ,6,  '12'],
  #                           ['11101000'  ,8,  '13'],
  #                           ['111010001' ,9,  '14'],
  #                           ['1001'      ,4,  '15'],
  #                           ['0011010'   ,7,  '16']
  #                          ])
  #prefixluttable = np.array([['1100000000',2,  '1' ], 
  #                           ['1110000000',3,  '2' ],
  #                           ['1110100000',5,  '3' ],
  #                           ['0011110000',7,  '4' ],
  #                           ['1111000000',5,  '5' ],
  #                           ['1110000000',5,  '6' ],
  #                           ['1101010000',6,  '7' ],
  #                           ['1110100010',10, '8' ],
  #                           ['1110110000',6,  '9' ],
  #                           ['1100010000',6,  '11'],
  #                           ['1110100000',6,  '12'],
  #                           ['1110100000',8,  '13'],
  #                           ['1110100010',9,  '14'],
  #                           ['1001000000',4,  '15'],
  #                           ['0011010000',7,  '16'],
  #                           ['1110111000',7,  '17']
  #                          ])
  prefixluttable = np.array([['10011010011101110000000000000000',19,  'a1' ],
                             ['10011010011101110000000000000000',20,  'a1' ],
                             ['10011010011101110000000000000000',21,  'a1' ],
                             ['10011010011101110000000000000000',22,  'a1' ],
                             ['10011010011101110000000000000000',23,  'a1' ],
                             ['10011010011101110000000000000000',24,  'a1' ],
                            ]) 


  ctrie,tvecram = inittrie()
  lastram = 0 

  for prefix in prefixluttable:
    lastram,tvecram = insertitem(ctrie,prefix[0],prefix[1],prefix[2],lastram,tvecram)
    

  #for i in range(0,lastram+1):
    #print(ctrie[i])
    # print("========")

  #insertitem(ctrie,prefix,plen,'45',0,0)
  #print("==#########==")



