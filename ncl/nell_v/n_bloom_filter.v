`timescale 1ns/1ps

module n_bloom_filter  (
        input wire [1:0] tlevel_inc_i ,
        input wire [1:0] apk_i ,
        input wire [63:0] crc_sr_i ,
        output wire ko ,
        input wire ki ,
        output wire [63:0] crc_sr_d1_o ,
        output wire [23:0] bf_idx0_d1_o ,
        output wire [23:0] bf_idx1_d1_o  );
    wire [1:0] tlevel_inc_i_0_;
    assign tlevel_inc_i_0_ = tlevel_inc_i;
    wire [1:0] apk_i_0_;
    assign apk_i_0_ = apk_i;
    wire [63:0] crc_sr_i_0_;
    assign crc_sr_i_0_ = crc_sr_i;
    wire ko_0_;
    assign ko = ko_0_;
    wire ki_0_;
    assign ki_0_ = ki;
    wire [63:0] crc_sr_d1_o_0_;
    assign crc_sr_d1_o = crc_sr_d1_o_0_;
    wire [23:0] bf_idx0_d1_o_0_;
    assign bf_idx0_d1_o = bf_idx0_d1_o_0_;
    wire [23:0] bf_idx1_d1_o_0_;
    assign bf_idx1_d1_o = bf_idx1_d1_o_0_;
    wire [1:0] lsb_0_;
    wire [1:0] din_0_;
    wire [63:0] crc_sr_out_le_0_;
    wire [1:0] crc_xin_0_;
    wire [63:0] crc_sr_in_le_0_;
    wire ko_crc_0_;
    wire [1:0] crc_sr_in_le_25_0_;
    wire [1:0] crc_sr_x_25_0_;
    wire [1:0] crc_sr_in_le_22_0_;
    wire [1:0] crc_sr_x_22_0_;
    wire [1:0] crc_sr_in_le_21_0_;
    wire [1:0] crc_sr_x_21_0_;
    wire [1:0] crc_sr_in_le_15_0_;
    wire [1:0] crc_sr_x_15_0_;
    wire [1:0] crc_sr_in_le_11_0_;
    wire [1:0] crc_sr_x_11_0_;
    wire [1:0] crc_sr_in_le_10_0_;
    wire [1:0] crc_sr_x_10_0_;
    wire [1:0] crc_sr_in_le_9_0_;
    wire [1:0] crc_sr_x_9_0_;
    wire [1:0] crc_sr_in_le_7_0_;
    wire [1:0] crc_sr_x_7_0_;
    wire [1:0] crc_sr_in_le_6_0_;
    wire [1:0] crc_sr_x_6_0_;
    wire [1:0] crc_sr_in_le_4_0_;
    wire [1:0] crc_sr_x_4_0_;
    wire [1:0] crc_sr_in_le_3_0_;
    wire [1:0] crc_sr_x_3_0_;
    wire [1:0] crc_sr_in_le_1_0_;
    wire [1:0] crc_sr_x_1_0_;
    wire [1:0] crc_sr_in_le_0_0_;
    wire [1:0] crc_sr_x_0_0_;
//  Optimizer removed wire [1:0] \\24_0_ ;
//  Optimizer removed wire [1:0] \\25_0_ ;
//  Optimizer removed wire [1:0] \\26_0_ ;
//  Optimizer removed wire [1:0] \\27_0_ ;
//  Optimizer removed wire [1:0] \\28_0_ ;
//  Optimizer removed wire [1:0] \\29_0_ ;
//  Optimizer removed wire [1:0] \\30_0_ ;
//  Optimizer removed wire [1:0] \\31_0_ ;
//  Optimizer removed wire [1:0] \\32_0_ ;
//  Optimizer removed wire [1:0] \\33_0_ ;
//  Optimizer removed wire [1:0] \\34_0_ ;
//  Optimizer removed wire [1:0] \\35_0_ ;
//  Optimizer removed wire [1:0] \\36_0_ ;
//  Optimizer removed wire [1:0] \\37_0_ ;
//  Optimizer removed wire [1:0] \\38_0_ ;
//  Optimizer removed wire [1:0] \\39_0_ ;
//  Optimizer removed wire [1:0] \\40_0_ ;
//  Optimizer removed wire [1:0] \\41_0_ ;
//  Optimizer removed wire [1:0] \\42_0_ ;
//  Optimizer removed wire [1:0] \\43_0_ ;
//  Optimizer removed wire [1:0] \\44_0_ ;
//  Optimizer removed wire [1:0] \\45_0_ ;
//  Optimizer removed wire [1:0] \\46_0_ ;
//  Optimizer removed wire [1:0] \\47_0_ ;
//  Optimizer removed wire [1:0] \\48_0_ ;
//  Optimizer removed wire [1:0] \\49_0_ ;
//  Optimizer removed wire [1:0] \\50_0_ ;
//  Optimizer removed wire [1:0] \\51_0_ ;
//  Optimizer removed wire [1:0] \\52_0_ ;
//  Optimizer removed wire [1:0] \\53_0_ ;
//  Optimizer removed wire [1:0] \\54_0_ ;
//  Optimizer removed wire [1:0] \\55_0_ ;
//  Optimizer removed wire [1:0] \\56_0_ ;
//  Optimizer removed wire [1:0] \\57_0_ ;
//  Optimizer removed wire [1:0] \\58_0_ ;
//  Optimizer removed wire [1:0] \\59_0_ ;
//  Optimizer removed wire [1:0] \\60_0_ ;
//  Optimizer removed wire [1:0] \\61_0_ ;
//  Optimizer removed wire [1:0] \\62_0_ ;
//  Optimizer removed wire [1:0] \\63_0_ ;
//  Optimizer removed wire [1:0] \\64_0_ ;
//  Optimizer removed wire [1:0] \\65_0_ ;
//  Optimizer removed wire [1:0] \\66_0_ ;
//  Optimizer removed wire [1:0] \\67_0_ ;
//  Optimizer removed wire [1:0] \\68_0_ ;
//  Optimizer removed wire [1:0] \\69_0_ ;
//  Optimizer removed wire [1:0] \\70_0_ ;
//  Optimizer removed wire [1:0] \\71_0_ ;
//  Optimizer removed wire [1:0] \\72_0_ ;
//  Optimizer removed wire [1:0] \\73_0_ ;
//  Optimizer removed wire [1:0] \\74_0_ ;
//  Optimizer removed wire [1:0] \\75_0_ ;
//  Optimizer removed wire [1:0] \\76_0_ ;
//  Optimizer removed wire [1:0] \\77_0_ ;
//  Optimizer removed wire [1:0] \\78_0_ ;
//  Optimizer removed wire [1:0] \\79_0_ ;
//  Optimizer removed wire [1:0] \\80_0_ ;
//  Optimizer removed wire [1:0] \\81_0_ ;
//  Optimizer removed wire [1:0] \\82_0_ ;
//  Optimizer removed wire [1:0] \\83_0_ ;
//  Optimizer removed wire [1:0] \\84_0_ ;
//  Optimizer removed wire [1:0] \\85_0_ ;
//  Optimizer removed wire [1:0] \\86_0_ ;
//  Optimizer removed wire [1:0] \\87_0_ ;
    wire \\23_0_ ;
    wire \\88_0_ ;
    wire \\89_0_ ;
    wire \\90_0_ ;
    wire \\91_0_ ;
    wire \\92_0_ ;
    wire \\93_0_ ;
    wire \\94_0_ ;
    wire \\95_0_ ;
    wire \\96_0_ ;
    wire \\97_0_ ;
    wire \\98_0_ ;
    wire \\99_0_ ;
    wire \\100_0_ ;
    wire \\101_0_ ;
    wire \\102_0_ ;
    wire \\103_0_ ;
    wire \\104_0_ ;
    wire \\105_0_ ;
    wire \\106_0_ ;
    wire \\107_0_ ;
    wire \\108_0_ ;
    wire \\109_0_ ;
    wire \\110_0_ ;
    wire \\111_0_ ;
    wire \\112_0_ ;
    wire \\113_0_ ;
    wire \\114_0_ ;
    wire \\115_0_ ;
    wire \\116_0_ ;
    wire \\117_0_ ;
    wire \\118_0_ ;
    wire \\119_0_ ;
    wire \\120_0_ ;
    wire \\121_0_ ;
//    Nell_Version_15_07_24_10_33__
    TH54W22_1x n_bloom_filter_BF_AWIDTH_12_0_15( .Z ( ko_0_ ), .A ( ko_crc_0_ ), .B ( \\23_0_  ), .C ( tlevel_inc_i_0_[0] ), .D ( tlevel_inc_i_0_[1] ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_16( .Z ( crc_sr_d1_o_0_[0] ), .A ( crc_sr_out_le_0_[62] ), .B ( \\88_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_17( .Z ( crc_sr_d1_o_0_[1] ), .A ( crc_sr_out_le_0_[63] ), .B ( \\88_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_18( .Z ( crc_sr_d1_o_0_[2] ), .A ( crc_sr_out_le_0_[60] ), .B ( \\89_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_19( .Z ( crc_sr_d1_o_0_[3] ), .A ( crc_sr_out_le_0_[61] ), .B ( \\89_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_20( .Z ( crc_sr_d1_o_0_[4] ), .A ( crc_sr_out_le_0_[58] ), .B ( \\90_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_21( .Z ( crc_sr_d1_o_0_[5] ), .A ( crc_sr_out_le_0_[59] ), .B ( \\90_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_22( .Z ( crc_sr_d1_o_0_[6] ), .A ( crc_sr_out_le_0_[56] ), .B ( \\91_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_23( .Z ( crc_sr_d1_o_0_[7] ), .A ( crc_sr_out_le_0_[57] ), .B ( \\91_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_24( .Z ( crc_sr_d1_o_0_[8] ), .A ( crc_sr_out_le_0_[54] ), .B ( \\92_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_25( .Z ( crc_sr_d1_o_0_[9] ), .A ( crc_sr_out_le_0_[55] ), .B ( \\92_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_26( .Z ( crc_sr_d1_o_0_[10] ), .A ( crc_sr_out_le_0_[52] ), .B ( \\93_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_27( .Z ( crc_sr_d1_o_0_[11] ), .A ( crc_sr_out_le_0_[53] ), .B ( \\93_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_28( .Z ( crc_sr_d1_o_0_[12] ), .A ( crc_sr_out_le_0_[50] ), .B ( \\94_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_29( .Z ( crc_sr_d1_o_0_[13] ), .A ( crc_sr_out_le_0_[51] ), .B ( \\94_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_30( .Z ( crc_sr_d1_o_0_[14] ), .A ( crc_sr_out_le_0_[48] ), .B ( \\95_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_31( .Z ( crc_sr_d1_o_0_[15] ), .A ( crc_sr_out_le_0_[49] ), .B ( \\95_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_32( .Z ( crc_sr_d1_o_0_[16] ), .A ( crc_sr_out_le_0_[46] ), .B ( \\96_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_33( .Z ( crc_sr_d1_o_0_[17] ), .A ( crc_sr_out_le_0_[47] ), .B ( \\96_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_34( .Z ( crc_sr_d1_o_0_[18] ), .A ( crc_sr_out_le_0_[44] ), .B ( \\97_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_35( .Z ( crc_sr_d1_o_0_[19] ), .A ( crc_sr_out_le_0_[45] ), .B ( \\97_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_36( .Z ( crc_sr_d1_o_0_[20] ), .A ( crc_sr_out_le_0_[42] ), .B ( \\98_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_37( .Z ( crc_sr_d1_o_0_[21] ), .A ( crc_sr_out_le_0_[43] ), .B ( \\98_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_38( .Z ( crc_sr_d1_o_0_[22] ), .A ( crc_sr_out_le_0_[40] ), .B ( \\99_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_39( .Z ( crc_sr_d1_o_0_[23] ), .A ( crc_sr_out_le_0_[41] ), .B ( \\99_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_40( .Z ( crc_sr_d1_o_0_[24] ), .A ( crc_sr_out_le_0_[38] ), .B ( \\100_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_41( .Z ( crc_sr_d1_o_0_[25] ), .A ( crc_sr_out_le_0_[39] ), .B ( \\100_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_42( .Z ( crc_sr_d1_o_0_[26] ), .A ( crc_sr_out_le_0_[36] ), .B ( \\101_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_43( .Z ( crc_sr_d1_o_0_[27] ), .A ( crc_sr_out_le_0_[37] ), .B ( \\101_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_44( .Z ( crc_sr_d1_o_0_[28] ), .A ( crc_sr_out_le_0_[34] ), .B ( \\102_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_45( .Z ( crc_sr_d1_o_0_[29] ), .A ( crc_sr_out_le_0_[35] ), .B ( \\102_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_46( .Z ( crc_sr_d1_o_0_[30] ), .A ( crc_sr_out_le_0_[32] ), .B ( \\103_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_47( .Z ( crc_sr_d1_o_0_[31] ), .A ( crc_sr_out_le_0_[33] ), .B ( \\103_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_48( .Z ( crc_sr_d1_o_0_[32] ), .A ( crc_sr_out_le_0_[30] ), .B ( \\104_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_49( .Z ( crc_sr_d1_o_0_[33] ), .A ( crc_sr_out_le_0_[31] ), .B ( \\104_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_50( .Z ( crc_sr_d1_o_0_[34] ), .A ( crc_sr_out_le_0_[28] ), .B ( \\105_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_51( .Z ( crc_sr_d1_o_0_[35] ), .A ( crc_sr_out_le_0_[29] ), .B ( \\105_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_52( .Z ( crc_sr_d1_o_0_[36] ), .A ( crc_sr_out_le_0_[26] ), .B ( \\106_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_53( .Z ( crc_sr_d1_o_0_[37] ), .A ( crc_sr_out_le_0_[27] ), .B ( \\106_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_54( .Z ( crc_sr_d1_o_0_[38] ), .A ( crc_sr_out_le_0_[24] ), .B ( \\107_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_55( .Z ( crc_sr_d1_o_0_[39] ), .A ( crc_sr_out_le_0_[25] ), .B ( \\107_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_56( .Z ( crc_sr_d1_o_0_[40] ), .A ( crc_sr_out_le_0_[22] ), .B ( \\108_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_57( .Z ( crc_sr_d1_o_0_[41] ), .A ( crc_sr_out_le_0_[23] ), .B ( \\108_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_58( .Z ( crc_sr_d1_o_0_[42] ), .A ( crc_sr_out_le_0_[20] ), .B ( \\109_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_59( .Z ( crc_sr_d1_o_0_[43] ), .A ( crc_sr_out_le_0_[21] ), .B ( \\109_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_60( .Z ( crc_sr_d1_o_0_[44] ), .A ( crc_sr_out_le_0_[18] ), .B ( \\110_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_61( .Z ( crc_sr_d1_o_0_[45] ), .A ( crc_sr_out_le_0_[19] ), .B ( \\110_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_62( .Z ( crc_sr_d1_o_0_[46] ), .A ( crc_sr_out_le_0_[16] ), .B ( \\111_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_63( .Z ( crc_sr_d1_o_0_[47] ), .A ( crc_sr_out_le_0_[17] ), .B ( \\111_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_64( .Z ( crc_sr_d1_o_0_[48] ), .A ( crc_sr_out_le_0_[14] ), .B ( \\112_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_65( .Z ( crc_sr_d1_o_0_[49] ), .A ( crc_sr_out_le_0_[15] ), .B ( \\112_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_66( .Z ( crc_sr_d1_o_0_[50] ), .A ( crc_sr_out_le_0_[12] ), .B ( \\113_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_67( .Z ( crc_sr_d1_o_0_[51] ), .A ( crc_sr_out_le_0_[13] ), .B ( \\113_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_68( .Z ( crc_sr_d1_o_0_[52] ), .A ( crc_sr_out_le_0_[10] ), .B ( \\114_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_69( .Z ( crc_sr_d1_o_0_[53] ), .A ( crc_sr_out_le_0_[11] ), .B ( \\114_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_70( .Z ( crc_sr_d1_o_0_[54] ), .A ( crc_sr_out_le_0_[8] ), .B ( \\115_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_71( .Z ( crc_sr_d1_o_0_[55] ), .A ( crc_sr_out_le_0_[9] ), .B ( \\115_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_72( .Z ( crc_sr_d1_o_0_[56] ), .A ( crc_sr_out_le_0_[6] ), .B ( \\116_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_73( .Z ( crc_sr_d1_o_0_[57] ), .A ( crc_sr_out_le_0_[7] ), .B ( \\116_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_74( .Z ( crc_sr_d1_o_0_[58] ), .A ( crc_sr_out_le_0_[4] ), .B ( \\117_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_75( .Z ( crc_sr_d1_o_0_[59] ), .A ( crc_sr_out_le_0_[5] ), .B ( \\117_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_76( .Z ( crc_sr_d1_o_0_[60] ), .A ( crc_sr_out_le_0_[2] ), .B ( \\118_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_77( .Z ( crc_sr_d1_o_0_[61] ), .A ( crc_sr_out_le_0_[3] ), .B ( \\118_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_78( .Z ( crc_sr_d1_o_0_[62] ), .A ( crc_sr_out_le_0_[0] ), .B ( \\119_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_79( .Z ( crc_sr_d1_o_0_[63] ), .A ( crc_sr_out_le_0_[1] ), .B ( \\119_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_80( .Z ( bf_idx0_d1_o_0_[0] ), .A ( crc_sr_d1_o_0_[40] ), .B ( \\120_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_81( .Z ( bf_idx0_d1_o_0_[1] ), .A ( crc_sr_d1_o_0_[41] ), .B ( \\120_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_82( .Z ( bf_idx0_d1_o_0_[2] ), .A ( crc_sr_d1_o_0_[42] ), .B ( \\120_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_83( .Z ( bf_idx0_d1_o_0_[3] ), .A ( crc_sr_d1_o_0_[43] ), .B ( \\120_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_84( .Z ( bf_idx0_d1_o_0_[4] ), .A ( crc_sr_d1_o_0_[44] ), .B ( \\120_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_85( .Z ( bf_idx0_d1_o_0_[5] ), .A ( crc_sr_d1_o_0_[45] ), .B ( \\120_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_86( .Z ( bf_idx0_d1_o_0_[6] ), .A ( crc_sr_d1_o_0_[46] ), .B ( \\120_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_87( .Z ( bf_idx0_d1_o_0_[7] ), .A ( crc_sr_d1_o_0_[47] ), .B ( \\120_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_88( .Z ( bf_idx0_d1_o_0_[8] ), .A ( crc_sr_d1_o_0_[48] ), .B ( \\120_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_89( .Z ( bf_idx0_d1_o_0_[9] ), .A ( crc_sr_d1_o_0_[49] ), .B ( \\120_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_90( .Z ( bf_idx0_d1_o_0_[10] ), .A ( crc_sr_d1_o_0_[50] ), .B ( \\120_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_91( .Z ( bf_idx0_d1_o_0_[11] ), .A ( crc_sr_d1_o_0_[51] ), .B ( \\120_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_92( .Z ( bf_idx0_d1_o_0_[12] ), .A ( crc_sr_d1_o_0_[52] ), .B ( \\120_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_93( .Z ( bf_idx0_d1_o_0_[13] ), .A ( crc_sr_d1_o_0_[53] ), .B ( \\120_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_94( .Z ( bf_idx0_d1_o_0_[14] ), .A ( crc_sr_d1_o_0_[54] ), .B ( \\120_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_95( .Z ( bf_idx0_d1_o_0_[15] ), .A ( crc_sr_d1_o_0_[55] ), .B ( \\120_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_96( .Z ( bf_idx0_d1_o_0_[16] ), .A ( crc_sr_d1_o_0_[56] ), .B ( \\120_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_97( .Z ( bf_idx0_d1_o_0_[17] ), .A ( crc_sr_d1_o_0_[57] ), .B ( \\120_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_98( .Z ( bf_idx0_d1_o_0_[18] ), .A ( crc_sr_d1_o_0_[58] ), .B ( \\120_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_99( .Z ( bf_idx0_d1_o_0_[19] ), .A ( crc_sr_d1_o_0_[59] ), .B ( \\120_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_100( .Z ( bf_idx0_d1_o_0_[20] ), .A ( crc_sr_d1_o_0_[60] ), .B ( \\120_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_101( .Z ( bf_idx0_d1_o_0_[21] ), .A ( crc_sr_d1_o_0_[61] ), .B ( \\120_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_102( .Z ( bf_idx0_d1_o_0_[22] ), .A ( crc_sr_d1_o_0_[62] ), .B ( \\120_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_103( .Z ( bf_idx0_d1_o_0_[23] ), .A ( crc_sr_d1_o_0_[63] ), .B ( \\120_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_104( .Z ( bf_idx1_d1_o_0_[0] ), .A ( crc_sr_d1_o_0_[24] ), .B ( \\121_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_105( .Z ( bf_idx1_d1_o_0_[1] ), .A ( crc_sr_d1_o_0_[25] ), .B ( \\121_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_106( .Z ( bf_idx1_d1_o_0_[2] ), .A ( crc_sr_d1_o_0_[26] ), .B ( \\121_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_107( .Z ( bf_idx1_d1_o_0_[3] ), .A ( crc_sr_d1_o_0_[27] ), .B ( \\121_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_108( .Z ( bf_idx1_d1_o_0_[4] ), .A ( crc_sr_d1_o_0_[28] ), .B ( \\121_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_109( .Z ( bf_idx1_d1_o_0_[5] ), .A ( crc_sr_d1_o_0_[29] ), .B ( \\121_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_110( .Z ( bf_idx1_d1_o_0_[6] ), .A ( crc_sr_d1_o_0_[30] ), .B ( \\121_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_111( .Z ( bf_idx1_d1_o_0_[7] ), .A ( crc_sr_d1_o_0_[31] ), .B ( \\121_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_112( .Z ( bf_idx1_d1_o_0_[8] ), .A ( crc_sr_d1_o_0_[32] ), .B ( \\121_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_113( .Z ( bf_idx1_d1_o_0_[9] ), .A ( crc_sr_d1_o_0_[33] ), .B ( \\121_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_114( .Z ( bf_idx1_d1_o_0_[10] ), .A ( crc_sr_d1_o_0_[34] ), .B ( \\121_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_115( .Z ( bf_idx1_d1_o_0_[11] ), .A ( crc_sr_d1_o_0_[35] ), .B ( \\121_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_116( .Z ( bf_idx1_d1_o_0_[12] ), .A ( crc_sr_d1_o_0_[36] ), .B ( \\121_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_117( .Z ( bf_idx1_d1_o_0_[13] ), .A ( crc_sr_d1_o_0_[37] ), .B ( \\121_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_118( .Z ( bf_idx1_d1_o_0_[14] ), .A ( crc_sr_d1_o_0_[38] ), .B ( \\121_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_119( .Z ( bf_idx1_d1_o_0_[15] ), .A ( crc_sr_d1_o_0_[39] ), .B ( \\121_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_120( .Z ( bf_idx1_d1_o_0_[16] ), .A ( crc_sr_d1_o_0_[40] ), .B ( \\121_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_121( .Z ( bf_idx1_d1_o_0_[17] ), .A ( crc_sr_d1_o_0_[41] ), .B ( \\121_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_122( .Z ( bf_idx1_d1_o_0_[18] ), .A ( crc_sr_d1_o_0_[42] ), .B ( \\121_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_123( .Z ( bf_idx1_d1_o_0_[19] ), .A ( crc_sr_d1_o_0_[43] ), .B ( \\121_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_124( .Z ( bf_idx1_d1_o_0_[20] ), .A ( crc_sr_d1_o_0_[44] ), .B ( \\121_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_125( .Z ( bf_idx1_d1_o_0_[21] ), .A ( crc_sr_d1_o_0_[45] ), .B ( \\121_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_126( .Z ( bf_idx1_d1_o_0_[22] ), .A ( crc_sr_d1_o_0_[46] ), .B ( \\121_0_  ) );
    TH22_1x n_bloom_filter_BF_AWIDTH_12_0_127( .Z ( bf_idx1_d1_o_0_[23] ), .A ( crc_sr_d1_o_0_[47] ), .B ( \\121_0_  ) );
    THXOR_1x n_bloom_filter_BF_AWIDTH_12_0_128( .Z ( crc_sr_out_le_0_[0] ), .A ( tlevel_inc_i_0_[1] ), .B ( crc_xin_0_[0] ), .C ( tlevel_inc_i_0_[0] ), .D ( crc_sr_i_0_[62] ) );
    THXOR_1x n_bloom_filter_BF_AWIDTH_12_0_129( .Z ( crc_sr_out_le_0_[1] ), .A ( tlevel_inc_i_0_[1] ), .B ( crc_xin_0_[1] ), .C ( tlevel_inc_i_0_[0] ), .D ( crc_sr_i_0_[63] ) );
    THXOR_1x n_bloom_filter_BF_AWIDTH_12_0_130( .Z ( crc_sr_out_le_0_[2] ), .A ( tlevel_inc_i_0_[1] ), .B ( crc_sr_x_0_0_[0] ), .C ( tlevel_inc_i_0_[0] ), .D ( crc_sr_i_0_[60] ) );
    THXOR_1x n_bloom_filter_BF_AWIDTH_12_0_131( .Z ( crc_sr_out_le_0_[3] ), .A ( tlevel_inc_i_0_[1] ), .B ( crc_sr_x_0_0_[1] ), .C ( tlevel_inc_i_0_[0] ), .D ( crc_sr_i_0_[61] ) );
    THXOR_1x n_bloom_filter_BF_AWIDTH_12_0_132( .Z ( crc_sr_out_le_0_[4] ), .A ( tlevel_inc_i_0_[1] ), .B ( crc_sr_x_1_0_[0] ), .C ( tlevel_inc_i_0_[0] ), .D ( crc_sr_i_0_[58] ) );
    THXOR_1x n_bloom_filter_BF_AWIDTH_12_0_133( .Z ( crc_sr_out_le_0_[5] ), .A ( tlevel_inc_i_0_[1] ), .B ( crc_sr_x_1_0_[1] ), .C ( tlevel_inc_i_0_[0] ), .D ( crc_sr_i_0_[59] ) );
    THXOR_1x n_bloom_filter_BF_AWIDTH_12_0_134( .Z ( crc_sr_out_le_0_[6] ), .A ( tlevel_inc_i_0_[1] ), .B ( crc_sr_i_0_[58] ), .C ( tlevel_inc_i_0_[0] ), .D ( crc_sr_i_0_[56] ) );
    THXOR_1x n_bloom_filter_BF_AWIDTH_12_0_135( .Z ( crc_sr_out_le_0_[7] ), .A ( tlevel_inc_i_0_[1] ), .B ( crc_sr_i_0_[59] ), .C ( tlevel_inc_i_0_[0] ), .D ( crc_sr_i_0_[57] ) );
    THXOR_1x n_bloom_filter_BF_AWIDTH_12_0_136( .Z ( crc_sr_out_le_0_[8] ), .A ( tlevel_inc_i_0_[1] ), .B ( crc_sr_x_3_0_[0] ), .C ( tlevel_inc_i_0_[0] ), .D ( crc_sr_i_0_[54] ) );
    THXOR_1x n_bloom_filter_BF_AWIDTH_12_0_137( .Z ( crc_sr_out_le_0_[9] ), .A ( tlevel_inc_i_0_[1] ), .B ( crc_sr_x_3_0_[1] ), .C ( tlevel_inc_i_0_[0] ), .D ( crc_sr_i_0_[55] ) );
    THXOR_1x n_bloom_filter_BF_AWIDTH_12_0_138( .Z ( crc_sr_out_le_0_[10] ), .A ( tlevel_inc_i_0_[1] ), .B ( crc_sr_x_4_0_[0] ), .C ( tlevel_inc_i_0_[0] ), .D ( crc_sr_i_0_[52] ) );
    THXOR_1x n_bloom_filter_BF_AWIDTH_12_0_139( .Z ( crc_sr_out_le_0_[11] ), .A ( tlevel_inc_i_0_[1] ), .B ( crc_sr_x_4_0_[1] ), .C ( tlevel_inc_i_0_[0] ), .D ( crc_sr_i_0_[53] ) );
    THXOR_1x n_bloom_filter_BF_AWIDTH_12_0_140( .Z ( crc_sr_out_le_0_[12] ), .A ( tlevel_inc_i_0_[1] ), .B ( crc_sr_i_0_[52] ), .C ( tlevel_inc_i_0_[0] ), .D ( crc_sr_i_0_[50] ) );
    THXOR_1x n_bloom_filter_BF_AWIDTH_12_0_141( .Z ( crc_sr_out_le_0_[13] ), .A ( tlevel_inc_i_0_[1] ), .B ( crc_sr_i_0_[53] ), .C ( tlevel_inc_i_0_[0] ), .D ( crc_sr_i_0_[51] ) );
    THXOR_1x n_bloom_filter_BF_AWIDTH_12_0_142( .Z ( crc_sr_out_le_0_[14] ), .A ( tlevel_inc_i_0_[1] ), .B ( crc_sr_x_6_0_[0] ), .C ( tlevel_inc_i_0_[0] ), .D ( crc_sr_i_0_[48] ) );
    THXOR_1x n_bloom_filter_BF_AWIDTH_12_0_143( .Z ( crc_sr_out_le_0_[15] ), .A ( tlevel_inc_i_0_[1] ), .B ( crc_sr_x_6_0_[1] ), .C ( tlevel_inc_i_0_[0] ), .D ( crc_sr_i_0_[49] ) );
    THXOR_1x n_bloom_filter_BF_AWIDTH_12_0_144( .Z ( crc_sr_out_le_0_[16] ), .A ( tlevel_inc_i_0_[1] ), .B ( crc_sr_x_7_0_[0] ), .C ( tlevel_inc_i_0_[0] ), .D ( crc_sr_i_0_[46] ) );
    THXOR_1x n_bloom_filter_BF_AWIDTH_12_0_145( .Z ( crc_sr_out_le_0_[17] ), .A ( tlevel_inc_i_0_[1] ), .B ( crc_sr_x_7_0_[1] ), .C ( tlevel_inc_i_0_[0] ), .D ( crc_sr_i_0_[47] ) );
    THXOR_1x n_bloom_filter_BF_AWIDTH_12_0_146( .Z ( crc_sr_out_le_0_[18] ), .A ( tlevel_inc_i_0_[1] ), .B ( crc_sr_i_0_[46] ), .C ( tlevel_inc_i_0_[0] ), .D ( crc_sr_i_0_[44] ) );
    THXOR_1x n_bloom_filter_BF_AWIDTH_12_0_147( .Z ( crc_sr_out_le_0_[19] ), .A ( tlevel_inc_i_0_[1] ), .B ( crc_sr_i_0_[47] ), .C ( tlevel_inc_i_0_[0] ), .D ( crc_sr_i_0_[45] ) );
    THXOR_1x n_bloom_filter_BF_AWIDTH_12_0_148( .Z ( crc_sr_out_le_0_[20] ), .A ( tlevel_inc_i_0_[1] ), .B ( crc_sr_x_9_0_[0] ), .C ( tlevel_inc_i_0_[0] ), .D ( crc_sr_i_0_[42] ) );
    THXOR_1x n_bloom_filter_BF_AWIDTH_12_0_149( .Z ( crc_sr_out_le_0_[21] ), .A ( tlevel_inc_i_0_[1] ), .B ( crc_sr_x_9_0_[1] ), .C ( tlevel_inc_i_0_[0] ), .D ( crc_sr_i_0_[43] ) );
    THXOR_1x n_bloom_filter_BF_AWIDTH_12_0_150( .Z ( crc_sr_out_le_0_[22] ), .A ( tlevel_inc_i_0_[1] ), .B ( crc_sr_x_10_0_[0] ), .C ( tlevel_inc_i_0_[0] ), .D ( crc_sr_i_0_[40] ) );
    THXOR_1x n_bloom_filter_BF_AWIDTH_12_0_151( .Z ( crc_sr_out_le_0_[23] ), .A ( tlevel_inc_i_0_[1] ), .B ( crc_sr_x_10_0_[1] ), .C ( tlevel_inc_i_0_[0] ), .D ( crc_sr_i_0_[41] ) );
    THXOR_1x n_bloom_filter_BF_AWIDTH_12_0_152( .Z ( crc_sr_out_le_0_[24] ), .A ( tlevel_inc_i_0_[1] ), .B ( crc_sr_x_11_0_[0] ), .C ( tlevel_inc_i_0_[0] ), .D ( crc_sr_i_0_[38] ) );
    THXOR_1x n_bloom_filter_BF_AWIDTH_12_0_153( .Z ( crc_sr_out_le_0_[25] ), .A ( tlevel_inc_i_0_[1] ), .B ( crc_sr_x_11_0_[1] ), .C ( tlevel_inc_i_0_[0] ), .D ( crc_sr_i_0_[39] ) );
    THXOR_1x n_bloom_filter_BF_AWIDTH_12_0_154( .Z ( crc_sr_out_le_0_[26] ), .A ( tlevel_inc_i_0_[1] ), .B ( crc_sr_i_0_[38] ), .C ( tlevel_inc_i_0_[0] ), .D ( crc_sr_i_0_[36] ) );
    THXOR_1x n_bloom_filter_BF_AWIDTH_12_0_155( .Z ( crc_sr_out_le_0_[27] ), .A ( tlevel_inc_i_0_[1] ), .B ( crc_sr_i_0_[39] ), .C ( tlevel_inc_i_0_[0] ), .D ( crc_sr_i_0_[37] ) );
    THXOR_1x n_bloom_filter_BF_AWIDTH_12_0_156( .Z ( crc_sr_out_le_0_[28] ), .A ( tlevel_inc_i_0_[1] ), .B ( crc_sr_i_0_[36] ), .C ( tlevel_inc_i_0_[0] ), .D ( crc_sr_i_0_[34] ) );
    THXOR_1x n_bloom_filter_BF_AWIDTH_12_0_157( .Z ( crc_sr_out_le_0_[29] ), .A ( tlevel_inc_i_0_[1] ), .B ( crc_sr_i_0_[37] ), .C ( tlevel_inc_i_0_[0] ), .D ( crc_sr_i_0_[35] ) );
    THXOR_1x n_bloom_filter_BF_AWIDTH_12_0_158( .Z ( crc_sr_out_le_0_[30] ), .A ( tlevel_inc_i_0_[1] ), .B ( crc_sr_i_0_[34] ), .C ( tlevel_inc_i_0_[0] ), .D ( crc_sr_i_0_[32] ) );
    THXOR_1x n_bloom_filter_BF_AWIDTH_12_0_159( .Z ( crc_sr_out_le_0_[31] ), .A ( tlevel_inc_i_0_[1] ), .B ( crc_sr_i_0_[35] ), .C ( tlevel_inc_i_0_[0] ), .D ( crc_sr_i_0_[33] ) );
    THXOR_1x n_bloom_filter_BF_AWIDTH_12_0_160( .Z ( crc_sr_out_le_0_[32] ), .A ( tlevel_inc_i_0_[1] ), .B ( crc_sr_x_15_0_[0] ), .C ( tlevel_inc_i_0_[0] ), .D ( crc_sr_i_0_[30] ) );
    THXOR_1x n_bloom_filter_BF_AWIDTH_12_0_161( .Z ( crc_sr_out_le_0_[33] ), .A ( tlevel_inc_i_0_[1] ), .B ( crc_sr_x_15_0_[1] ), .C ( tlevel_inc_i_0_[0] ), .D ( crc_sr_i_0_[31] ) );
    THXOR_1x n_bloom_filter_BF_AWIDTH_12_0_162( .Z ( crc_sr_out_le_0_[34] ), .A ( tlevel_inc_i_0_[1] ), .B ( crc_sr_i_0_[30] ), .C ( tlevel_inc_i_0_[0] ), .D ( crc_sr_i_0_[28] ) );
    THXOR_1x n_bloom_filter_BF_AWIDTH_12_0_163( .Z ( crc_sr_out_le_0_[35] ), .A ( tlevel_inc_i_0_[1] ), .B ( crc_sr_i_0_[31] ), .C ( tlevel_inc_i_0_[0] ), .D ( crc_sr_i_0_[29] ) );
    THXOR_1x n_bloom_filter_BF_AWIDTH_12_0_164( .Z ( crc_sr_out_le_0_[36] ), .A ( tlevel_inc_i_0_[1] ), .B ( crc_sr_i_0_[28] ), .C ( tlevel_inc_i_0_[0] ), .D ( crc_sr_i_0_[26] ) );
    THXOR_1x n_bloom_filter_BF_AWIDTH_12_0_165( .Z ( crc_sr_out_le_0_[37] ), .A ( tlevel_inc_i_0_[1] ), .B ( crc_sr_i_0_[29] ), .C ( tlevel_inc_i_0_[0] ), .D ( crc_sr_i_0_[27] ) );
    THXOR_1x n_bloom_filter_BF_AWIDTH_12_0_166( .Z ( crc_sr_out_le_0_[38] ), .A ( tlevel_inc_i_0_[1] ), .B ( crc_sr_i_0_[26] ), .C ( tlevel_inc_i_0_[0] ), .D ( crc_sr_i_0_[24] ) );
    THXOR_1x n_bloom_filter_BF_AWIDTH_12_0_167( .Z ( crc_sr_out_le_0_[39] ), .A ( tlevel_inc_i_0_[1] ), .B ( crc_sr_i_0_[27] ), .C ( tlevel_inc_i_0_[0] ), .D ( crc_sr_i_0_[25] ) );
    THXOR_1x n_bloom_filter_BF_AWIDTH_12_0_168( .Z ( crc_sr_out_le_0_[40] ), .A ( tlevel_inc_i_0_[1] ), .B ( crc_sr_i_0_[24] ), .C ( tlevel_inc_i_0_[0] ), .D ( crc_sr_i_0_[22] ) );
    THXOR_1x n_bloom_filter_BF_AWIDTH_12_0_169( .Z ( crc_sr_out_le_0_[41] ), .A ( tlevel_inc_i_0_[1] ), .B ( crc_sr_i_0_[25] ), .C ( tlevel_inc_i_0_[0] ), .D ( crc_sr_i_0_[23] ) );
    THXOR_1x n_bloom_filter_BF_AWIDTH_12_0_170( .Z ( crc_sr_out_le_0_[42] ), .A ( tlevel_inc_i_0_[1] ), .B ( crc_sr_i_0_[22] ), .C ( tlevel_inc_i_0_[0] ), .D ( crc_sr_i_0_[20] ) );
    THXOR_1x n_bloom_filter_BF_AWIDTH_12_0_171( .Z ( crc_sr_out_le_0_[43] ), .A ( tlevel_inc_i_0_[1] ), .B ( crc_sr_i_0_[23] ), .C ( tlevel_inc_i_0_[0] ), .D ( crc_sr_i_0_[21] ) );
    THXOR_1x n_bloom_filter_BF_AWIDTH_12_0_172( .Z ( crc_sr_out_le_0_[44] ), .A ( tlevel_inc_i_0_[1] ), .B ( crc_sr_x_21_0_[0] ), .C ( tlevel_inc_i_0_[0] ), .D ( crc_sr_i_0_[18] ) );
    THXOR_1x n_bloom_filter_BF_AWIDTH_12_0_173( .Z ( crc_sr_out_le_0_[45] ), .A ( tlevel_inc_i_0_[1] ), .B ( crc_sr_x_21_0_[1] ), .C ( tlevel_inc_i_0_[0] ), .D ( crc_sr_i_0_[19] ) );
    THXOR_1x n_bloom_filter_BF_AWIDTH_12_0_174( .Z ( crc_sr_out_le_0_[46] ), .A ( tlevel_inc_i_0_[1] ), .B ( crc_sr_x_22_0_[0] ), .C ( tlevel_inc_i_0_[0] ), .D ( crc_sr_i_0_[16] ) );
    THXOR_1x n_bloom_filter_BF_AWIDTH_12_0_175( .Z ( crc_sr_out_le_0_[47] ), .A ( tlevel_inc_i_0_[1] ), .B ( crc_sr_x_22_0_[1] ), .C ( tlevel_inc_i_0_[0] ), .D ( crc_sr_i_0_[17] ) );
    THXOR_1x n_bloom_filter_BF_AWIDTH_12_0_176( .Z ( crc_sr_out_le_0_[48] ), .A ( tlevel_inc_i_0_[1] ), .B ( crc_sr_i_0_[16] ), .C ( tlevel_inc_i_0_[0] ), .D ( crc_sr_i_0_[14] ) );
    THXOR_1x n_bloom_filter_BF_AWIDTH_12_0_177( .Z ( crc_sr_out_le_0_[49] ), .A ( tlevel_inc_i_0_[1] ), .B ( crc_sr_i_0_[17] ), .C ( tlevel_inc_i_0_[0] ), .D ( crc_sr_i_0_[15] ) );
    THXOR_1x n_bloom_filter_BF_AWIDTH_12_0_178( .Z ( crc_sr_out_le_0_[50] ), .A ( tlevel_inc_i_0_[1] ), .B ( crc_sr_i_0_[14] ), .C ( tlevel_inc_i_0_[0] ), .D ( crc_sr_i_0_[12] ) );
    THXOR_1x n_bloom_filter_BF_AWIDTH_12_0_179( .Z ( crc_sr_out_le_0_[51] ), .A ( tlevel_inc_i_0_[1] ), .B ( crc_sr_i_0_[15] ), .C ( tlevel_inc_i_0_[0] ), .D ( crc_sr_i_0_[13] ) );
    THXOR_1x n_bloom_filter_BF_AWIDTH_12_0_180( .Z ( crc_sr_out_le_0_[52] ), .A ( tlevel_inc_i_0_[1] ), .B ( crc_sr_x_25_0_[0] ), .C ( tlevel_inc_i_0_[0] ), .D ( crc_sr_i_0_[10] ) );
    THXOR_1x n_bloom_filter_BF_AWIDTH_12_0_181( .Z ( crc_sr_out_le_0_[53] ), .A ( tlevel_inc_i_0_[1] ), .B ( crc_sr_x_25_0_[1] ), .C ( tlevel_inc_i_0_[0] ), .D ( crc_sr_i_0_[11] ) );
    THXOR_1x n_bloom_filter_BF_AWIDTH_12_0_182( .Z ( crc_sr_out_le_0_[54] ), .A ( tlevel_inc_i_0_[1] ), .B ( crc_sr_i_0_[10] ), .C ( tlevel_inc_i_0_[0] ), .D ( crc_sr_i_0_[8] ) );
    THXOR_1x n_bloom_filter_BF_AWIDTH_12_0_183( .Z ( crc_sr_out_le_0_[55] ), .A ( tlevel_inc_i_0_[1] ), .B ( crc_sr_i_0_[11] ), .C ( tlevel_inc_i_0_[0] ), .D ( crc_sr_i_0_[9] ) );
    THXOR_1x n_bloom_filter_BF_AWIDTH_12_0_184( .Z ( crc_sr_out_le_0_[56] ), .A ( tlevel_inc_i_0_[1] ), .B ( crc_sr_i_0_[8] ), .C ( tlevel_inc_i_0_[0] ), .D ( crc_sr_i_0_[6] ) );
    THXOR_1x n_bloom_filter_BF_AWIDTH_12_0_185( .Z ( crc_sr_out_le_0_[57] ), .A ( tlevel_inc_i_0_[1] ), .B ( crc_sr_i_0_[9] ), .C ( tlevel_inc_i_0_[0] ), .D ( crc_sr_i_0_[7] ) );
    THXOR_1x n_bloom_filter_BF_AWIDTH_12_0_186( .Z ( crc_sr_out_le_0_[58] ), .A ( tlevel_inc_i_0_[1] ), .B ( crc_sr_i_0_[6] ), .C ( tlevel_inc_i_0_[0] ), .D ( crc_sr_i_0_[4] ) );
    THXOR_1x n_bloom_filter_BF_AWIDTH_12_0_187( .Z ( crc_sr_out_le_0_[59] ), .A ( tlevel_inc_i_0_[1] ), .B ( crc_sr_i_0_[7] ), .C ( tlevel_inc_i_0_[0] ), .D ( crc_sr_i_0_[5] ) );
    THXOR_1x n_bloom_filter_BF_AWIDTH_12_0_188( .Z ( crc_sr_out_le_0_[60] ), .A ( tlevel_inc_i_0_[1] ), .B ( crc_sr_i_0_[4] ), .C ( tlevel_inc_i_0_[0] ), .D ( crc_sr_i_0_[2] ) );
    THXOR_1x n_bloom_filter_BF_AWIDTH_12_0_189( .Z ( crc_sr_out_le_0_[61] ), .A ( tlevel_inc_i_0_[1] ), .B ( crc_sr_i_0_[5] ), .C ( tlevel_inc_i_0_[0] ), .D ( crc_sr_i_0_[3] ) );
    THXOR_1x n_bloom_filter_BF_AWIDTH_12_0_190( .Z ( crc_sr_out_le_0_[62] ), .A ( tlevel_inc_i_0_[1] ), .B ( crc_sr_i_0_[2] ), .C ( tlevel_inc_i_0_[0] ), .D ( crc_sr_i_0_[0] ) );
    THXOR_1x n_bloom_filter_BF_AWIDTH_12_0_191( .Z ( crc_sr_out_le_0_[63] ), .A ( tlevel_inc_i_0_[1] ), .B ( crc_sr_i_0_[3] ), .C ( tlevel_inc_i_0_[0] ), .D ( crc_sr_i_0_[1] ) );
    // rail \\22_0_  removed by optimizer
    T12_1x n_bloom_filter_BF_AWIDTH_12_0_192( .Z ( \\23_0_  ), .A ( apk_i_0_[0] ), .B ( apk_i_0_[1] ) );
    // rail \\24_0_ [0] removed by optimizer
    // rail \\24_0_ [1] removed by optimizer
    // rail \\25_0_ [0] removed by optimizer
    // rail \\25_0_ [1] removed by optimizer
    // rail \\26_0_ [0] removed by optimizer
    // rail \\26_0_ [1] removed by optimizer
    // rail \\27_0_ [0] removed by optimizer
    // rail \\27_0_ [1] removed by optimizer
    // rail \\28_0_ [0] removed by optimizer
    // rail \\28_0_ [1] removed by optimizer
    // rail \\29_0_ [0] removed by optimizer
    // rail \\29_0_ [1] removed by optimizer
    // rail \\30_0_ [0] removed by optimizer
    // rail \\30_0_ [1] removed by optimizer
    // rail \\31_0_ [0] removed by optimizer
    // rail \\31_0_ [1] removed by optimizer
    // rail \\32_0_ [0] removed by optimizer
    // rail \\32_0_ [1] removed by optimizer
    // rail \\33_0_ [0] removed by optimizer
    // rail \\33_0_ [1] removed by optimizer
    // rail \\34_0_ [0] removed by optimizer
    // rail \\34_0_ [1] removed by optimizer
    // rail \\35_0_ [0] removed by optimizer
    // rail \\35_0_ [1] removed by optimizer
    // rail \\36_0_ [0] removed by optimizer
    // rail \\36_0_ [1] removed by optimizer
    // rail \\37_0_ [0] removed by optimizer
    // rail \\37_0_ [1] removed by optimizer
    // rail \\38_0_ [0] removed by optimizer
    // rail \\38_0_ [1] removed by optimizer
    // rail \\39_0_ [0] removed by optimizer
    // rail \\39_0_ [1] removed by optimizer
    // rail \\40_0_ [0] removed by optimizer
    // rail \\40_0_ [1] removed by optimizer
    // rail \\41_0_ [0] removed by optimizer
    // rail \\41_0_ [1] removed by optimizer
    // rail \\42_0_ [0] removed by optimizer
    // rail \\42_0_ [1] removed by optimizer
    // rail \\43_0_ [0] removed by optimizer
    // rail \\43_0_ [1] removed by optimizer
    // rail \\44_0_ [0] removed by optimizer
    // rail \\44_0_ [1] removed by optimizer
    // rail \\45_0_ [0] removed by optimizer
    // rail \\45_0_ [1] removed by optimizer
    // rail \\46_0_ [0] removed by optimizer
    // rail \\46_0_ [1] removed by optimizer
    // rail \\47_0_ [0] removed by optimizer
    // rail \\47_0_ [1] removed by optimizer
    // rail \\48_0_ [0] removed by optimizer
    // rail \\48_0_ [1] removed by optimizer
    // rail \\49_0_ [0] removed by optimizer
    // rail \\49_0_ [1] removed by optimizer
    // rail \\50_0_ [0] removed by optimizer
    // rail \\50_0_ [1] removed by optimizer
    // rail \\51_0_ [0] removed by optimizer
    // rail \\51_0_ [1] removed by optimizer
    // rail \\52_0_ [0] removed by optimizer
    // rail \\52_0_ [1] removed by optimizer
    // rail \\53_0_ [0] removed by optimizer
    // rail \\53_0_ [1] removed by optimizer
    // rail \\54_0_ [0] removed by optimizer
    // rail \\54_0_ [1] removed by optimizer
    // rail \\55_0_ [0] removed by optimizer
    // rail \\55_0_ [1] removed by optimizer
    // rail \\56_0_ [0] removed by optimizer
    // rail \\56_0_ [1] removed by optimizer
    // rail \\57_0_ [0] removed by optimizer
    // rail \\57_0_ [1] removed by optimizer
    // rail \\58_0_ [0] removed by optimizer
    // rail \\58_0_ [1] removed by optimizer
    // rail \\59_0_ [0] removed by optimizer
    // rail \\59_0_ [1] removed by optimizer
    // rail \\60_0_ [0] removed by optimizer
    // rail \\60_0_ [1] removed by optimizer
    // rail \\61_0_ [0] removed by optimizer
    // rail \\61_0_ [1] removed by optimizer
    // rail \\62_0_ [0] removed by optimizer
    // rail \\62_0_ [1] removed by optimizer
    // rail \\63_0_ [0] removed by optimizer
    // rail \\63_0_ [1] removed by optimizer
    // rail \\64_0_ [0] removed by optimizer
    // rail \\64_0_ [1] removed by optimizer
    // rail \\65_0_ [0] removed by optimizer
    // rail \\65_0_ [1] removed by optimizer
    // rail \\66_0_ [0] removed by optimizer
    // rail \\66_0_ [1] removed by optimizer
    // rail \\67_0_ [0] removed by optimizer
    // rail \\67_0_ [1] removed by optimizer
    // rail \\68_0_ [0] removed by optimizer
    // rail \\68_0_ [1] removed by optimizer
    // rail \\69_0_ [0] removed by optimizer
    // rail \\69_0_ [1] removed by optimizer
    // rail \\70_0_ [0] removed by optimizer
    // rail \\70_0_ [1] removed by optimizer
    // rail \\71_0_ [0] removed by optimizer
    // rail \\71_0_ [1] removed by optimizer
    // rail \\72_0_ [0] removed by optimizer
    // rail \\72_0_ [1] removed by optimizer
    // rail \\73_0_ [0] removed by optimizer
    // rail \\73_0_ [1] removed by optimizer
    // rail \\74_0_ [0] removed by optimizer
    // rail \\74_0_ [1] removed by optimizer
    // rail \\75_0_ [0] removed by optimizer
    // rail \\75_0_ [1] removed by optimizer
    // rail \\76_0_ [0] removed by optimizer
    // rail \\76_0_ [1] removed by optimizer
    // rail \\77_0_ [0] removed by optimizer
    // rail \\77_0_ [1] removed by optimizer
    // rail \\78_0_ [0] removed by optimizer
    // rail \\78_0_ [1] removed by optimizer
    // rail \\79_0_ [0] removed by optimizer
    // rail \\79_0_ [1] removed by optimizer
    // rail \\80_0_ [0] removed by optimizer
    // rail \\80_0_ [1] removed by optimizer
    // rail \\81_0_ [0] removed by optimizer
    // rail \\81_0_ [1] removed by optimizer
    // rail \\82_0_ [0] removed by optimizer
    // rail \\82_0_ [1] removed by optimizer
    // rail \\83_0_ [0] removed by optimizer
    // rail \\83_0_ [1] removed by optimizer
    // rail \\84_0_ [0] removed by optimizer
    // rail \\84_0_ [1] removed by optimizer
    // rail \\85_0_ [0] removed by optimizer
    // rail \\85_0_ [1] removed by optimizer
    // rail \\86_0_ [0] removed by optimizer
    // rail \\86_0_ [1] removed by optimizer
    // rail \\87_0_ [0] removed by optimizer
    // rail \\87_0_ [1] removed by optimizer
    T11B_1x n_bloom_filter_BF_AWIDTH_12_0_193_COMPINV( .Z ( \\88_0_  ), .A ( ki_0_ ) );
    T11B_1x n_bloom_filter_BF_AWIDTH_12_0_194_COMPINV( .Z ( \\89_0_  ), .A ( ki_0_ ) );
    T11B_1x n_bloom_filter_BF_AWIDTH_12_0_195_COMPINV( .Z ( \\90_0_  ), .A ( ki_0_ ) );
    T11B_1x n_bloom_filter_BF_AWIDTH_12_0_196_COMPINV( .Z ( \\91_0_  ), .A ( ki_0_ ) );
    T11B_1x n_bloom_filter_BF_AWIDTH_12_0_197_COMPINV( .Z ( \\92_0_  ), .A ( ki_0_ ) );
    T11B_1x n_bloom_filter_BF_AWIDTH_12_0_198_COMPINV( .Z ( \\93_0_  ), .A ( ki_0_ ) );
    T11B_1x n_bloom_filter_BF_AWIDTH_12_0_199_COMPINV( .Z ( \\94_0_  ), .A ( ki_0_ ) );
    T11B_1x n_bloom_filter_BF_AWIDTH_12_0_200_COMPINV( .Z ( \\95_0_  ), .A ( ki_0_ ) );
    T11B_1x n_bloom_filter_BF_AWIDTH_12_0_201_COMPINV( .Z ( \\96_0_  ), .A ( ki_0_ ) );
    T11B_1x n_bloom_filter_BF_AWIDTH_12_0_202_COMPINV( .Z ( \\97_0_  ), .A ( ki_0_ ) );
    T11B_1x n_bloom_filter_BF_AWIDTH_12_0_203_COMPINV( .Z ( \\98_0_  ), .A ( ki_0_ ) );
    T11B_1x n_bloom_filter_BF_AWIDTH_12_0_204_COMPINV( .Z ( \\99_0_  ), .A ( ki_0_ ) );
    T11B_1x n_bloom_filter_BF_AWIDTH_12_0_205_COMPINV( .Z ( \\100_0_  ), .A ( ki_0_ ) );
    T11B_1x n_bloom_filter_BF_AWIDTH_12_0_206_COMPINV( .Z ( \\101_0_  ), .A ( ki_0_ ) );
    T11B_1x n_bloom_filter_BF_AWIDTH_12_0_207_COMPINV( .Z ( \\102_0_  ), .A ( ki_0_ ) );
    T11B_1x n_bloom_filter_BF_AWIDTH_12_0_208_COMPINV( .Z ( \\103_0_  ), .A ( ki_0_ ) );
    T11B_1x n_bloom_filter_BF_AWIDTH_12_0_209_COMPINV( .Z ( \\104_0_  ), .A ( ki_0_ ) );
    T11B_1x n_bloom_filter_BF_AWIDTH_12_0_210_COMPINV( .Z ( \\105_0_  ), .A ( ki_0_ ) );
    T11B_1x n_bloom_filter_BF_AWIDTH_12_0_211_COMPINV( .Z ( \\106_0_  ), .A ( ki_0_ ) );
    T11B_1x n_bloom_filter_BF_AWIDTH_12_0_212_COMPINV( .Z ( \\107_0_  ), .A ( ki_0_ ) );
    T11B_1x n_bloom_filter_BF_AWIDTH_12_0_213_COMPINV( .Z ( \\108_0_  ), .A ( ki_0_ ) );
    T11B_1x n_bloom_filter_BF_AWIDTH_12_0_214_COMPINV( .Z ( \\109_0_  ), .A ( ki_0_ ) );
    T11B_1x n_bloom_filter_BF_AWIDTH_12_0_215_COMPINV( .Z ( \\110_0_  ), .A ( ki_0_ ) );
    T11B_1x n_bloom_filter_BF_AWIDTH_12_0_216_COMPINV( .Z ( \\111_0_  ), .A ( ki_0_ ) );
    T11B_1x n_bloom_filter_BF_AWIDTH_12_0_217_COMPINV( .Z ( \\112_0_  ), .A ( ki_0_ ) );
    T11B_1x n_bloom_filter_BF_AWIDTH_12_0_218_COMPINV( .Z ( \\113_0_  ), .A ( ki_0_ ) );
    T11B_1x n_bloom_filter_BF_AWIDTH_12_0_219_COMPINV( .Z ( \\114_0_  ), .A ( ki_0_ ) );
    T11B_1x n_bloom_filter_BF_AWIDTH_12_0_220_COMPINV( .Z ( \\115_0_  ), .A ( ki_0_ ) );
    T11B_1x n_bloom_filter_BF_AWIDTH_12_0_221_COMPINV( .Z ( \\116_0_  ), .A ( ki_0_ ) );
    T11B_1x n_bloom_filter_BF_AWIDTH_12_0_222_COMPINV( .Z ( \\117_0_  ), .A ( ki_0_ ) );
    T11B_1x n_bloom_filter_BF_AWIDTH_12_0_223_COMPINV( .Z ( \\118_0_  ), .A ( ki_0_ ) );
    T11B_1x n_bloom_filter_BF_AWIDTH_12_0_224_COMPINV( .Z ( \\119_0_  ), .A ( ki_0_ ) );
    T11B_1x n_bloom_filter_BF_AWIDTH_12_0_225_COMPINV( .Z ( \\120_0_  ), .A ( ki_0_ ) );
    T11B_1x n_bloom_filter_BF_AWIDTH_12_0_226_COMPINV( .Z ( \\121_0_  ), .A ( ki_0_ ) );
    cmplt32 n_bloom_filter_BF_AWIDTH_12_0_0 ( .din(crc_sr_i_0_), .cmplt(ko_crc_0_) );
    xor_1b n_bloom_filter_BF_AWIDTH_12_0_1 ( .din0(crc_xin_0_), .din1(crc_sr_in_le_25_0_), .dout(crc_sr_x_25_0_) );
    xor_1b n_bloom_filter_BF_AWIDTH_12_0_2 ( .din0(crc_xin_0_), .din1(crc_sr_in_le_22_0_), .dout(crc_sr_x_22_0_) );
    xor_1b n_bloom_filter_BF_AWIDTH_12_0_3 ( .din0(crc_xin_0_), .din1(crc_sr_in_le_21_0_), .dout(crc_sr_x_21_0_) );
    xor_1b n_bloom_filter_BF_AWIDTH_12_0_4 ( .din0(crc_xin_0_), .din1(crc_sr_in_le_15_0_), .dout(crc_sr_x_15_0_) );
    xor_1b n_bloom_filter_BF_AWIDTH_12_0_5 ( .din0(crc_xin_0_), .din1(crc_sr_in_le_11_0_), .dout(crc_sr_x_11_0_) );
    xor_1b n_bloom_filter_BF_AWIDTH_12_0_6 ( .din0(crc_xin_0_), .din1(crc_sr_in_le_10_0_), .dout(crc_sr_x_10_0_) );
    xor_1b n_bloom_filter_BF_AWIDTH_12_0_7 ( .din0(crc_xin_0_), .din1(crc_sr_in_le_9_0_), .dout(crc_sr_x_9_0_) );
    xor_1b n_bloom_filter_BF_AWIDTH_12_0_8 ( .din0(crc_xin_0_), .din1(crc_sr_in_le_7_0_), .dout(crc_sr_x_7_0_) );
    xor_1b n_bloom_filter_BF_AWIDTH_12_0_9 ( .din0(crc_xin_0_), .din1(crc_sr_in_le_6_0_), .dout(crc_sr_x_6_0_) );
    xor_1b n_bloom_filter_BF_AWIDTH_12_0_10 ( .din0(crc_xin_0_), .din1(crc_sr_in_le_4_0_), .dout(crc_sr_x_4_0_) );
    xor_1b n_bloom_filter_BF_AWIDTH_12_0_11 ( .din0(crc_xin_0_), .din1(crc_sr_in_le_3_0_), .dout(crc_sr_x_3_0_) );
    xor_1b n_bloom_filter_BF_AWIDTH_12_0_12 ( .din0(crc_xin_0_), .din1(crc_sr_in_le_1_0_), .dout(crc_sr_x_1_0_) );
    xor_1b n_bloom_filter_BF_AWIDTH_12_0_13 ( .din0(crc_xin_0_), .din1(crc_sr_in_le_0_0_), .dout(crc_sr_x_0_0_) );
    xor_1b n_bloom_filter_BF_AWIDTH_12_0_14 ( .din0(lsb_0_), .din1(din_0_), .dout(crc_xin_0_) );
endmodule

// =========== /home/s3471830/pb-research-repo/design/nell/component/xor_1b.n  ===========

// 212 (212 basic) Cells (area    873.50)
//            T11B:    34 uses, area:     34.00 ( 3.9%)
//             T12:     1 uses, area:      2.00 ( 0.2%)
//            TH22:   112 uses, area:    448.00 (51.3%)
//         TH54W22:     1 uses, area:      5.50 ( 0.6%)
//           THXOR:    64 uses, area:    384.00 (44.0%)

//   0(-> 15, ^  0): 227/227 cells: n_bloom_filter_BF_AWIDTH_12:  line 2, "n_bloom_filter.n"
//     0:  area  873.5 100.0% ( 873.5, 100.0%)  *****
//   1(->  1, ^  0): 0/0 cells:      cmplt32:  line 31, "n_bloom_filter.n"
//     1:  area    0.0   0.0% (   0.0,   0.0%)
//   2(->  2, ^  0): 0/0 cells:       xor_1b:  line 70, "n_bloom_filter.n"
//     2:  area    0.0   0.0% (   0.0,   0.0%)
//   3(->  3, ^  0): 0/0 cells:       xor_1b:  line 73, "n_bloom_filter.n"
//     3:  area    0.0   0.0% (   0.0,   0.0%)
//   4(->  4, ^  0): 0/0 cells:       xor_1b:  line 74, "n_bloom_filter.n"
//     4:  area    0.0   0.0% (   0.0,   0.0%)
//   5(->  5, ^  0): 0/0 cells:       xor_1b:  line 80, "n_bloom_filter.n"
//     5:  area    0.0   0.0% (   0.0,   0.0%)
//   6(->  6, ^  0): 0/0 cells:       xor_1b:  line 84, "n_bloom_filter.n"
//     6:  area    0.0   0.0% (   0.0,   0.0%)
//   7(->  7, ^  0): 0/0 cells:       xor_1b:  line 85, "n_bloom_filter.n"
//     7:  area    0.0   0.0% (   0.0,   0.0%)
//   8(->  8, ^  0): 0/0 cells:       xor_1b:  line 86, "n_bloom_filter.n"
//     8:  area    0.0   0.0% (   0.0,   0.0%)
//   9(->  9, ^  0): 0/0 cells:       xor_1b:  line 88, "n_bloom_filter.n"
//     9:  area    0.0   0.0% (   0.0,   0.0%)
//  10(-> 10, ^  0): 0/0 cells:       xor_1b:  line 89, "n_bloom_filter.n"
//    10:  area    0.0   0.0% (   0.0,   0.0%)
//  11(-> 11, ^  0): 0/0 cells:       xor_1b:  line 91, "n_bloom_filter.n"
//    11:  area    0.0   0.0% (   0.0,   0.0%)
//  12(-> 12, ^  0): 0/0 cells:       xor_1b:  line 92, "n_bloom_filter.n"
//    12:  area    0.0   0.0% (   0.0,   0.0%)
//  13(-> 13, ^  0): 0/0 cells:       xor_1b:  line 94, "n_bloom_filter.n"
//    13:  area    0.0   0.0% (   0.0,   0.0%)
//  14(-> 14, ^  0): 0/0 cells:       xor_1b:  line 95, "n_bloom_filter.n"
//    14:  area    0.0   0.0% (   0.0,   0.0%)
//  15(-> 15, ^  0): 0/0 cells:       xor_1b:  line 96, "n_bloom_filter.n"
//    15:  area    0.0   0.0% (   0.0,   0.0%)

// 65 cell optimizations were done
//        64:   Or(2) => THXOR(4) 
//         1:   NCL And(3) => TH54W22(4) 
