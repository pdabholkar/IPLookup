`timescale 1ns/1ps

module n_bloom_filter_sr0  (
        input wire [1:0] msbk_i ,
        input wire [11:0] mk_i ,
        output wire ko ,
        input wire ki ,
        output wire [63:0] crc_sr_d1_o  );
    wire [1:0] msbk_i_0_;
    assign msbk_i_0_ = msbk_i;
    wire [11:0] mk_i_0_;
    assign mk_i_0_ = mk_i;
    wire ko_0_;
    assign ko = ko_0_;
    wire ki_0_;
    assign ki_0_ = ki;
    wire [63:0] crc_sr_d1_o_0_;
    assign crc_sr_d1_o = crc_sr_d1_o_0_;
    wire [63:0] crc_sr_out_0_;
    wire [1:0] din_xor_03_0_;
    wire [1:0] din_xor_10_0_;
    wire [1:0] din_xor_21_0_;
    wire [1:0] din_xor_32_0_;
    wire [1:0] din_xor_032_0_;
    wire [1:0] din_xor_103_0_;
    wire [1:0] din_xor_210_0_;
    wire [1:0] din_xor_321_0_;
    wire [1:0] z_0_;
    wire [1:0] DIN0_0_;
    wire [1:0] DIN1_0_;
    wire [1:0] DIN2_0_;
    wire [1:0] DIN3_0_;
    wire \\4_0_ ;
    wire \\5_0_ ;
    wire \\6_0_ ;
    wire \\8_0_ ;
    wire \\9_0_ ;
//    Nell_Version_15_07_24_10_33__
    TH44_1x n_bloom_filter_sr0_0_8( .Z ( ko_0_ ), .A ( \\4_0_  ), .B ( \\5_0_  ), .C ( \\6_0_  ), .D ( \\8_0_  ) );
    TH22_1x n_bloom_filter_sr0_0_9( .Z ( crc_sr_d1_o_0_[0] ), .A ( mk_i_0_[0] ), .B ( \\9_0_  ) );
    TH22_1x n_bloom_filter_sr0_0_10( .Z ( crc_sr_d1_o_0_[1] ), .A ( mk_i_0_[1] ), .B ( \\9_0_  ) );
    TH22_1x n_bloom_filter_sr0_0_11( .Z ( crc_sr_d1_o_0_[2] ), .A ( din_xor_32_0_[0] ), .B ( \\9_0_  ) );
    TH22_1x n_bloom_filter_sr0_0_12( .Z ( crc_sr_d1_o_0_[3] ), .A ( din_xor_32_0_[1] ), .B ( \\9_0_  ) );
    TH22_1x n_bloom_filter_sr0_0_13( .Z ( crc_sr_d1_o_0_[4] ), .A ( din_xor_321_0_[0] ), .B ( \\9_0_  ) );
    TH22_1x n_bloom_filter_sr0_0_14( .Z ( crc_sr_d1_o_0_[5] ), .A ( din_xor_321_0_[1] ), .B ( \\9_0_  ) );
    TH22_1x n_bloom_filter_sr0_0_15( .Z ( crc_sr_d1_o_0_[6] ), .A ( din_xor_210_0_[0] ), .B ( \\9_0_  ) );
    TH22_1x n_bloom_filter_sr0_0_16( .Z ( crc_sr_d1_o_0_[7] ), .A ( din_xor_210_0_[1] ), .B ( \\9_0_  ) );
    TH22_1x n_bloom_filter_sr0_0_17( .Z ( crc_sr_d1_o_0_[8] ), .A ( din_xor_103_0_[0] ), .B ( \\9_0_  ) );
    TH22_1x n_bloom_filter_sr0_0_18( .Z ( crc_sr_d1_o_0_[9] ), .A ( din_xor_103_0_[1] ), .B ( \\9_0_  ) );
    TH22_1x n_bloom_filter_sr0_0_19( .Z ( crc_sr_d1_o_0_[10] ), .A ( din_xor_032_0_[0] ), .B ( \\9_0_  ) );
    TH22_1x n_bloom_filter_sr0_0_20( .Z ( crc_sr_d1_o_0_[11] ), .A ( din_xor_032_0_[1] ), .B ( \\9_0_  ) );
    TH22_1x n_bloom_filter_sr0_0_21( .Z ( crc_sr_d1_o_0_[12] ), .A ( din_xor_21_0_[0] ), .B ( \\9_0_  ) );
    TH22_1x n_bloom_filter_sr0_0_22( .Z ( crc_sr_d1_o_0_[13] ), .A ( din_xor_21_0_[1] ), .B ( \\9_0_  ) );
    TH22_1x n_bloom_filter_sr0_0_23( .Z ( crc_sr_d1_o_0_[14] ), .A ( din_xor_103_0_[0] ), .B ( \\9_0_  ) );
    TH22_1x n_bloom_filter_sr0_0_24( .Z ( crc_sr_d1_o_0_[15] ), .A ( din_xor_103_0_[1] ), .B ( \\9_0_  ) );
    TH22_1x n_bloom_filter_sr0_0_25( .Z ( crc_sr_d1_o_0_[16] ), .A ( din_xor_032_0_[0] ), .B ( \\9_0_  ) );
    TH22_1x n_bloom_filter_sr0_0_26( .Z ( crc_sr_d1_o_0_[17] ), .A ( din_xor_032_0_[1] ), .B ( \\9_0_  ) );
    TH22_1x n_bloom_filter_sr0_0_27( .Z ( crc_sr_d1_o_0_[18] ), .A ( din_xor_21_0_[0] ), .B ( \\9_0_  ) );
    TH22_1x n_bloom_filter_sr0_0_28( .Z ( crc_sr_d1_o_0_[19] ), .A ( din_xor_21_0_[1] ), .B ( \\9_0_  ) );
    TH22_1x n_bloom_filter_sr0_0_29( .Z ( crc_sr_d1_o_0_[20] ), .A ( din_xor_103_0_[0] ), .B ( \\9_0_  ) );
    TH22_1x n_bloom_filter_sr0_0_30( .Z ( crc_sr_d1_o_0_[21] ), .A ( din_xor_103_0_[1] ), .B ( \\9_0_  ) );
    TH22_1x n_bloom_filter_sr0_0_31( .Z ( crc_sr_d1_o_0_[22] ), .A ( din_xor_032_0_[0] ), .B ( \\9_0_  ) );
    TH22_1x n_bloom_filter_sr0_0_32( .Z ( crc_sr_d1_o_0_[23] ), .A ( din_xor_032_0_[1] ), .B ( \\9_0_  ) );
    TH22_1x n_bloom_filter_sr0_0_33( .Z ( crc_sr_d1_o_0_[24] ), .A ( din_xor_321_0_[0] ), .B ( \\9_0_  ) );
    TH22_1x n_bloom_filter_sr0_0_34( .Z ( crc_sr_d1_o_0_[25] ), .A ( din_xor_321_0_[1] ), .B ( \\9_0_  ) );
    TH22_1x n_bloom_filter_sr0_0_35( .Z ( crc_sr_d1_o_0_[26] ), .A ( din_xor_210_0_[0] ), .B ( \\9_0_  ) );
    TH22_1x n_bloom_filter_sr0_0_36( .Z ( crc_sr_d1_o_0_[27] ), .A ( din_xor_210_0_[1] ), .B ( \\9_0_  ) );
    TH22_1x n_bloom_filter_sr0_0_37( .Z ( crc_sr_d1_o_0_[28] ), .A ( din_xor_10_0_[0] ), .B ( \\9_0_  ) );
    TH22_1x n_bloom_filter_sr0_0_38( .Z ( crc_sr_d1_o_0_[29] ), .A ( din_xor_10_0_[1] ), .B ( \\9_0_  ) );
    TH22_1x n_bloom_filter_sr0_0_39( .Z ( crc_sr_d1_o_0_[30] ), .A ( msbk_i_0_[0] ), .B ( \\9_0_  ) );
    TH22_1x n_bloom_filter_sr0_0_40( .Z ( crc_sr_d1_o_0_[31] ), .A ( msbk_i_0_[1] ), .B ( \\9_0_  ) );
    TH22_1x n_bloom_filter_sr0_0_41( .Z ( crc_sr_d1_o_0_[32] ), .A ( mk_i_0_[0] ), .B ( \\9_0_  ) );
    TH22_1x n_bloom_filter_sr0_0_42( .Z ( crc_sr_d1_o_0_[33] ), .A ( mk_i_0_[1] ), .B ( \\9_0_  ) );
    TH22_1x n_bloom_filter_sr0_0_43( .Z ( crc_sr_d1_o_0_[34] ), .A ( mk_i_0_[2] ), .B ( \\9_0_  ) );
    TH22_1x n_bloom_filter_sr0_0_44( .Z ( crc_sr_d1_o_0_[35] ), .A ( mk_i_0_[3] ), .B ( \\9_0_  ) );
    TH22_1x n_bloom_filter_sr0_0_45( .Z ( crc_sr_d1_o_0_[36] ), .A ( mk_i_0_[4] ), .B ( \\9_0_  ) );
    TH22_1x n_bloom_filter_sr0_0_46( .Z ( crc_sr_d1_o_0_[37] ), .A ( mk_i_0_[5] ), .B ( \\9_0_  ) );
    TH22_1x n_bloom_filter_sr0_0_47( .Z ( crc_sr_d1_o_0_[38] ), .A ( msbk_i_0_[0] ), .B ( \\9_0_  ) );
    TH22_1x n_bloom_filter_sr0_0_48( .Z ( crc_sr_d1_o_0_[39] ), .A ( msbk_i_0_[1] ), .B ( \\9_0_  ) );
    TH22_1x n_bloom_filter_sr0_0_49( .Z ( crc_sr_d1_o_0_[40] ), .A ( ko_0_ ), .B ( \\9_0_  ) );
    TH22_1x n_bloom_filter_sr0_0_50( .Z ( crc_sr_d1_o_0_[42] ), .A ( ko_0_ ), .B ( \\9_0_  ) );
    TH22_1x n_bloom_filter_sr0_0_51( .Z ( crc_sr_d1_o_0_[44] ), .A ( mk_i_0_[0] ), .B ( \\9_0_  ) );
    TH22_1x n_bloom_filter_sr0_0_52( .Z ( crc_sr_d1_o_0_[45] ), .A ( mk_i_0_[1] ), .B ( \\9_0_  ) );
    TH22_1x n_bloom_filter_sr0_0_53( .Z ( crc_sr_d1_o_0_[46] ), .A ( din_xor_32_0_[0] ), .B ( \\9_0_  ) );
    TH22_1x n_bloom_filter_sr0_0_54( .Z ( crc_sr_d1_o_0_[47] ), .A ( din_xor_32_0_[1] ), .B ( \\9_0_  ) );
    TH22_1x n_bloom_filter_sr0_0_55( .Z ( crc_sr_d1_o_0_[48] ), .A ( din_xor_21_0_[0] ), .B ( \\9_0_  ) );
    TH22_1x n_bloom_filter_sr0_0_56( .Z ( crc_sr_d1_o_0_[49] ), .A ( din_xor_21_0_[1] ), .B ( \\9_0_  ) );
    TH22_1x n_bloom_filter_sr0_0_57( .Z ( crc_sr_d1_o_0_[50] ), .A ( din_xor_10_0_[0] ), .B ( \\9_0_  ) );
    TH22_1x n_bloom_filter_sr0_0_58( .Z ( crc_sr_d1_o_0_[51] ), .A ( din_xor_10_0_[1] ), .B ( \\9_0_  ) );
    TH22_1x n_bloom_filter_sr0_0_59( .Z ( crc_sr_d1_o_0_[52] ), .A ( din_xor_03_0_[0] ), .B ( \\9_0_  ) );
    TH22_1x n_bloom_filter_sr0_0_60( .Z ( crc_sr_d1_o_0_[53] ), .A ( din_xor_03_0_[1] ), .B ( \\9_0_  ) );
    TH22_1x n_bloom_filter_sr0_0_61( .Z ( crc_sr_d1_o_0_[54] ), .A ( mk_i_0_[2] ), .B ( \\9_0_  ) );
    TH22_1x n_bloom_filter_sr0_0_62( .Z ( crc_sr_d1_o_0_[55] ), .A ( mk_i_0_[3] ), .B ( \\9_0_  ) );
    TH22_1x n_bloom_filter_sr0_0_63( .Z ( crc_sr_d1_o_0_[56] ), .A ( mk_i_0_[4] ), .B ( \\9_0_  ) );
    TH22_1x n_bloom_filter_sr0_0_64( .Z ( crc_sr_d1_o_0_[57] ), .A ( mk_i_0_[5] ), .B ( \\9_0_  ) );
    TH22_1x n_bloom_filter_sr0_0_65( .Z ( crc_sr_d1_o_0_[58] ), .A ( msbk_i_0_[0] ), .B ( \\9_0_  ) );
    TH22_1x n_bloom_filter_sr0_0_66( .Z ( crc_sr_d1_o_0_[59] ), .A ( msbk_i_0_[1] ), .B ( \\9_0_  ) );
    TH22_1x n_bloom_filter_sr0_0_67( .Z ( crc_sr_d1_o_0_[60] ), .A ( ko_0_ ), .B ( \\9_0_  ) );
    TH22_1x n_bloom_filter_sr0_0_68( .Z ( crc_sr_d1_o_0_[62] ), .A ( ko_0_ ), .B ( \\9_0_  ) );
    THCOMP_1x n_bloom_filter_sr0_0_69( .Z ( \\4_0_  ), .A ( mk_i_0_[0] ), .B ( mk_i_0_[1] ), .C ( mk_i_0_[2] ), .D ( mk_i_0_[3] ) );
    THCOMP_1x n_bloom_filter_sr0_0_70( .Z ( \\5_0_  ), .A ( mk_i_0_[4] ), .B ( mk_i_0_[5] ), .C ( mk_i_0_[6] ), .D ( mk_i_0_[7] ) );
    THCOMP_1x n_bloom_filter_sr0_0_71( .Z ( \\6_0_  ), .A ( mk_i_0_[8] ), .B ( mk_i_0_[9] ), .C ( mk_i_0_[10] ), .D ( mk_i_0_[11] ) );
    // rail \\7_0_  removed by optimizer
    T12_1x n_bloom_filter_sr0_0_72( .Z ( \\8_0_  ), .A ( msbk_i_0_[0] ), .B ( msbk_i_0_[1] ) );
    T11B_1x n_bloom_filter_sr0_0_73_COMPINV( .Z ( \\9_0_  ), .A ( ki_0_ ) );
    assign crc_sr_d1_o_0_[41] = 1'b0;
    assign crc_sr_d1_o_0_[43] = 1'b0;
    assign crc_sr_d1_o_0_[61] = 1'b0;
    assign crc_sr_d1_o_0_[63] = 1'b0;
    xor_1b n_bloom_filter_sr0_0_0 ( .din0(DIN0_0_), .din1(DIN3_0_), .dout(din_xor_03_0_) );
    xor_1b n_bloom_filter_sr0_0_1 ( .din0(DIN1_0_), .din1(DIN0_0_), .dout(din_xor_10_0_) );
    xor_1b n_bloom_filter_sr0_0_2 ( .din0(DIN2_0_), .din1(DIN1_0_), .dout(din_xor_21_0_) );
    xor_1b n_bloom_filter_sr0_0_3 ( .din0(DIN3_0_), .din1(DIN2_0_), .dout(din_xor_32_0_) );
    xor_1b n_bloom_filter_sr0_0_4 ( .din0(DIN0_0_), .din1(din_xor_32_0_), .dout(din_xor_032_0_) );
    xor_1b n_bloom_filter_sr0_0_5 ( .din0(DIN1_0_), .din1(din_xor_03_0_), .dout(din_xor_103_0_) );
    xor_1b n_bloom_filter_sr0_0_6 ( .din0(DIN2_0_), .din1(din_xor_10_0_), .dout(din_xor_210_0_) );
    xor_1b n_bloom_filter_sr0_0_7 ( .din0(DIN3_0_), .din1(din_xor_21_0_), .dout(din_xor_321_0_) );
endmodule

// =========== /home/s3471830/pb-research-repo/design/nell/component/xor_1b.n  ===========

// 66 (66 basic) Cells (area    265.50)
//            T11B:     1 uses, area:      1.00 ( 0.4%)
//             T12:     1 uses, area:      2.00 ( 0.8%)
//            TH22:    60 uses, area:    240.00 (90.4%)
//            TH44:     1 uses, area:      6.00 ( 2.3%)
//          THCOMP:     3 uses, area:     16.50 ( 6.2%)

//   0(->  8, ^  0): 74/74 cells: n_bloom_filter_sr0:  line 3, "n_bloom_filter_sr0.n"
//     0:  area  265.5 100.0% ( 265.5, 100.0%)  *****
//   1(->  1, ^  0): 0/0 cells:       xor_1b:  line 30, "n_bloom_filter_sr0.n"
//     1:  area    0.0   0.0% (   0.0,   0.0%)
//   2(->  2, ^  0): 0/0 cells:       xor_1b:  line 31, "n_bloom_filter_sr0.n"
//     2:  area    0.0   0.0% (   0.0,   0.0%)
//   3(->  3, ^  0): 0/0 cells:       xor_1b:  line 32, "n_bloom_filter_sr0.n"
//     3:  area    0.0   0.0% (   0.0,   0.0%)
//   4(->  4, ^  0): 0/0 cells:       xor_1b:  line 33, "n_bloom_filter_sr0.n"
//     4:  area    0.0   0.0% (   0.0,   0.0%)
//   5(->  5, ^  0): 0/0 cells:       xor_1b:  line 34, "n_bloom_filter_sr0.n"
//     5:  area    0.0   0.0% (   0.0,   0.0%)
//   6(->  6, ^  0): 0/0 cells:       xor_1b:  line 35, "n_bloom_filter_sr0.n"
//     6:  area    0.0   0.0% (   0.0,   0.0%)
//   7(->  7, ^  0): 0/0 cells:       xor_1b:  line 36, "n_bloom_filter_sr0.n"
//     7:  area    0.0   0.0% (   0.0,   0.0%)
//   8(->  8, ^  0): 0/0 cells:       xor_1b:  line 37, "n_bloom_filter_sr0.n"
//     8:  area    0.0   0.0% (   0.0,   0.0%)

// 1 cell optimizations were done
//         1:   NCL And(2) => NCL And(4) 
