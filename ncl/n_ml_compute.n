module n_ml_compute (
  input dual mp[3],
  input dual lp[5],
  output dual ml[6]
);

dual mp_plus_lp[4];
dual mp_plus_lp_cout;

dual mp_msb_is_0 = {^mp,0};

dual lp_4[4];

lp_4 = lp[3:0];

// The four bits of match length are computed using the mp_match padded with a zero in the msb and 4 bits of the lsb.
ks_adder_4b_nocin (.ain({mp,mp_msb_is_0}),.bin(lp_4),.sout(mp_plus_lp),.cout(mp_plus_lp_cout));

// The msb of the lp_match is then added with the carry out from the previous 4-bit carry using a 
// Half adder without carry. This is done to avoid using a 5 bit ks adder.
dual ml_msb_0  = {((lp[4]/0 & mp_plus_lp_cout/0) | (lp[4]/1 & mp_plus_lp_cout/1)),((lp[4]/0 & mp_plus_lp_cout/1) | (lp[4]/1 & mp_plus_lp_cout/0))};  
dual ml_msb_1 =  {((lp[4]/0 & mp_plus_lp_cout/0) | (lp[4]/0 & mp_plus_lp_cout/1) | (lp[4]/1 & mp_plus_lp_cout/0)),(lp[4]/1 & mp_plus_lp_cout/1)};

ml = {mp_plus_lp,ml_msb_0,ml_msb_1};

endmodule
