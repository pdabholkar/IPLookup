module n_next_child (
  input dual  lc_i[33],
  input dual  rc_i[33],
  input dual  epsilon_i,
  input dual  apk_i[32],
  output      ko,
  input       ki,
  output dual tlevel_inc_d1_o,
  output dual address_d1_o[32],
  output dual apk_d1_o[32]
);

rail ko_lc,ko_rc,ko_apk;
dual B0k;
rail lc_not_null;
rail rc_not_null;
rail child_not_null;

dual apk_shifted[32];

dual lc_int[32];
dual rc_int[32];
dual apk_int[32];

lc_int = lc_i[31:0];
rc_int = rc_i[31:0];
apk_int = apk_i[31:0];

cmplt32 (lc_int,ko_lc);
cmplt32 (rc_int,ko_rc);
cmplt32 (apk_int,ko_apk);

ko = ko_lc & ko_rc & ko_apk & ^epsilon_i;

dual z = {ko,0};   // Constant '0' in phase with all other inputs

B0k = apk_i[31];

lc_not_null = lc_i[32]/0;
rc_not_null = rc_i[32]/0;

child_not_null = (lc_i[32]/0 & B0k/0) | (rc_i[32]/0 & B0k/1);

apk_shifted = {z,apk_i[30:0]};

address_d1_o = ~ki & ((lc_i[31:0] & lc_not_null & B0k/0 & epsilon_i/0) | (rc_i[31:0] & rc_not_null & B0k/1 & epsilon_i/0) | (lc_i[31:0] & epsilon_i/1));
tlevel_inc_d1_o = ~ki & child_not_null & {epsilon_i/1,epsilon_i/0};
apk_d1_o[31:0] = ~ki & child_not_null & ((epsilon_i/0 &apk_shifted) | (epsilon_i/1 & apk_i));

endmodule

