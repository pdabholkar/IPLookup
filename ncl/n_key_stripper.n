module n_key_stripper (
  input   dual key_i[32],
  output       ko (null),
  input        ki,
  output  dual msbk_o (null),
  output  dual apk_o[32] (null),
  output  dual mk_o[6] (null)
);

dual msbk = key_i[31];

dual msbk_31[31];
dual key_31[31];
dual key_xor_msb[31];

ko = ^key_i;

for i = 0:30 {
  msbk_31[i] = msbk;
}

key_31 = key_i[30:0];

xor_31b(key_31,msbk_31,key_xor_msb);

dual z = {^key_xor_msb[30],0};
dual o = {0,^key_xor_msb[30]};
dual mk[6];
dual apk[32];

mk   =                    ( key_xor_msb[30] ?     
          {o,z,z,z,z,z} : ( key_xor_msb[29] ? 
          {z,o,z,z,z,z} : ( key_xor_msb[28] ? 
          {o,o,z,z,z,z} : ( key_xor_msb[27] ? 
          {z,z,o,z,z,z} : ( key_xor_msb[26] ? 
          {o,z,o,z,z,z} : ( key_xor_msb[25] ? 
          {z,o,o,z,z,z} : ( key_xor_msb[24] ? 
          {o,o,o,z,z,z} : ( key_xor_msb[23] ? 
          {z,z,z,o,z,z} : ( key_xor_msb[22] ? 
          {o,z,z,o,z,z} : ( key_xor_msb[21] ? 
          {z,o,z,o,z,z} : ( key_xor_msb[20] ? 
          {o,o,z,o,z,z} : ( key_xor_msb[19] ? 
          {z,z,o,o,z,z} : ( key_xor_msb[18] ? 
          {o,z,o,o,z,z} : ( key_xor_msb[17] ? 
          {z,o,o,o,z,z} : ( key_xor_msb[16] ? 
          {o,o,o,o,z,z} : ( key_xor_msb[15] ? 
          {z,z,z,z,o,z} : ( key_xor_msb[14] ? 
          {o,z,z,z,o,z} : ( key_xor_msb[13] ? 
          {z,o,z,z,o,z} : ( key_xor_msb[12] ? 
          {o,o,z,z,o,z} : ( key_xor_msb[11] ? 
          {z,z,o,z,o,z} : ( key_xor_msb[10] ? 
          {o,z,o,z,o,z} : ( key_xor_msb[9]  ? 
          {z,o,o,z,o,z} : ( key_xor_msb[8]  ? 
          {o,o,o,z,o,z} : ( key_xor_msb[7]  ? 
          {z,z,z,o,o,z} : ( key_xor_msb[6]  ? 
          {o,z,z,o,o,z} : ( key_xor_msb[5]  ? 
          {z,o,z,o,o,z} : ( key_xor_msb[4]  ? 
          {o,o,z,o,o,z} : ( key_xor_msb[3]  ? 
          {z,z,o,o,o,z} : ( key_xor_msb[2]  ? 
          {o,z,o,o,o,z} : ( key_xor_msb[1]  ? 
          {z,o,o,o,o,z} : ( key_xor_msb[0]  ? 
          {o,o,o,o,o,z} : {z,z,z,z,z,o}   )))))))))))))))))))))))))))))));

apk    =                   ( key_xor_msb[30] ? 
          {z,key_i[30:0] } : ( key_xor_msb[29] ? 
          {z,z,key_i[29:0] } : ( key_xor_msb[28] ? 
          {z,z,z,key_i[28:0] } : ( key_xor_msb[27] ? 
          {z,z,z,z,key_i[27:0] } : ( key_xor_msb[26] ? 
          {z,z,z,z,z,key_i[26:0] } : ( key_xor_msb[25] ? 
          {z,z,z,z,z,z,key_i[25:0] } : ( key_xor_msb[24] ? 
          {z,z,z,z,z,z,z,key_i[24:0] } : ( key_xor_msb[23] ? 
          {z,z,z,z,z,z,z,z,key_i[23:0] } : ( key_xor_msb[22] ? 
          {z,z,z,z,z,z,z,z,z,key_i[22:0] } : ( key_xor_msb[21] ? 
          {z,z,z,z,z,z,z,z,z,z,key_i[21:0] } : ( key_xor_msb[20] ? 
          {z,z,z,z,z,z,z,z,z,z,z,key_i[20:0] } : ( key_xor_msb[19] ? 
          {z,z,z,z,z,z,z,z,z,z,z,z,key_i[19:0] } : ( key_xor_msb[18] ? 
          {z,z,z,z,z,z,z,z,z,z,z,z,z,key_i[18:0] } : ( key_xor_msb[17] ? 
          {z,z,z,z,z,z,z,z,z,z,z,z,z,z,key_i[17:0] } : ( key_xor_msb[16] ? 
          {z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,key_i[16:0] } : ( key_xor_msb[15] ? 
          {z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,key_i[15:0] } : ( key_xor_msb[14] ? 
          {z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,key_i[14:0] } : ( key_xor_msb[13] ? 
          {z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,key_i[13:0] } : ( key_xor_msb[12] ? 
          {z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,key_i[12:0] } : ( key_xor_msb[11] ? 
          {z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,key_i[11:0] } : ( key_xor_msb[10] ? 
          {z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,key_i[10:0] } : ( key_xor_msb[9]  ? 
          {z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,key_i[9:0]  } : ( key_xor_msb[8]  ? 
          {z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,key_i[8:0]  } : ( key_xor_msb[7]  ? 
          {z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,key_i[7:0]  } : ( key_xor_msb[6]  ? 
          {z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,key_i[6:0]  } : ( key_xor_msb[5]  ? 
          {z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,key_i[5:0]  } : ( key_xor_msb[4]  ? 
          {z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,key_i[4:0]  } : ( key_xor_msb[3]  ? 
          {z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,key_i[3:0]  } : ( key_xor_msb[2]  ? 
          {z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,key_i[2:0]  } : ( key_xor_msb[1]  ? 
          {z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,key_i[1:0]  } : ( key_xor_msb[0]  ? 
          {z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,key_i[0]    } : {z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z} 
                                                                                          )))))))))))))))))))))))))))))));


apk_o  = ~ki & apk;
msbk_o = ~ki & msbk;
mk_o   = ~ki & mk;


endmodule

