module n_Nzok_extract (
  input dual apk_a1_i[32],
  output ko(null),
  input  ki,
  output dual Nzok_o[6](null)
);

dual B0k_a1 = apk_a1_i[31];

dual B0k_31[31];
dual apk_31[31];
dual apk_xor_msb[31];

ko = ^apk_a1_i;

for i = 0:30 {
  B0k_31[i] = B0k_a1;
}

apk_31 = apk_a1_i[30:0];

xor_31b(apk_31,B0k_31,apk_xor_msb);

dual z = {^apk_xor_msb[30],0};
dual o = {0,^apk_xor_msb[30]};

dual Nzok[6];

Nzok  =                   ( apk_xor_msb[30] ?     
          {o,z,z,z,z,z} : ( apk_xor_msb[29] ? 
          {z,o,z,z,z,z} : ( apk_xor_msb[28] ? 
          {o,o,z,z,z,z} : ( apk_xor_msb[27] ? 
          {z,z,o,z,z,z} : ( apk_xor_msb[26] ? 
          {o,z,o,z,z,z} : ( apk_xor_msb[25] ? 
          {z,o,o,z,z,z} : ( apk_xor_msb[24] ? 
          {o,o,o,z,z,z} : ( apk_xor_msb[23] ? 
          {z,z,z,o,z,z} : ( apk_xor_msb[22] ? 
          {o,z,z,o,z,z} : ( apk_xor_msb[21] ? 
          {z,o,z,o,z,z} : ( apk_xor_msb[20] ? 
          {o,o,z,o,z,z} : ( apk_xor_msb[19] ? 
          {z,z,o,o,z,z} : ( apk_xor_msb[18] ? 
          {o,z,o,o,z,z} : ( apk_xor_msb[17] ? 
          {z,o,o,o,z,z} : ( apk_xor_msb[16] ? 
          {o,o,o,o,z,z} : ( apk_xor_msb[15] ? 
          {z,z,z,z,o,z} : ( apk_xor_msb[14] ? 
          {o,z,z,z,o,z} : ( apk_xor_msb[13] ? 
          {z,o,z,z,o,z} : ( apk_xor_msb[12] ? 
          {o,o,z,z,o,z} : ( apk_xor_msb[11] ? 
          {z,z,o,z,o,z} : ( apk_xor_msb[10] ? 
          {o,z,o,z,o,z} : ( apk_xor_msb[9]  ? 
          {z,o,o,z,o,z} : ( apk_xor_msb[8]  ? 
          {o,o,o,z,o,z} : ( apk_xor_msb[7]  ? 
          {z,z,z,o,o,z} : ( apk_xor_msb[6]  ? 
          {o,z,z,o,o,z} : ( apk_xor_msb[5]  ? 
          {z,o,z,o,o,z} : ( apk_xor_msb[4]  ? 
          {o,o,z,o,o,z} : ( apk_xor_msb[3]  ? 
          {z,z,o,o,o,z} : ( apk_xor_msb[2]  ? 
          {o,z,o,o,o,z} : ( apk_xor_msb[1]  ? 
          {z,o,o,o,o,z} : ( apk_xor_msb[0]  ? 
          {o,o,o,o,o,z} : {z,z,z,z,z,o} )))))))))))))))))))))))))))))));

Nzok_o = ~ki & Nzok;
endmodule

