parameter BF_AWIDTH = 12;
module n_bloom_filter_sr0 (
  input  dual msbk_i,
  input  dual mk_i[6],
  output      ko,
  input       ki,
  output dual crc_sr_d1_o[32],
);

// le stands for Little Endian

dual crc_sr_out[32];

dual din_xor_03;
dual din_xor_10;
dual din_xor_21;
dual din_xor_32;
dual din_xor_032;
dual din_xor_103;
dual din_xor_210;
dual din_xor_321;

dual z = {ko,0};//cast[2,1]0;

dual DIN0 = msbk_i;
dual DIN1 = mk_i[2];
dual DIN2 = mk_i[1];
dual DIN3 = mk_i[0];

xor_1b(DIN0,DIN3,din_xor_03);
xor_1b(DIN1,DIN0,din_xor_10);
xor_1b(DIN2,DIN1,din_xor_21);
xor_1b(DIN3,DIN2,din_xor_32);
xor_1b(DIN0,din_xor_32,din_xor_032);
xor_1b(DIN1,din_xor_03,din_xor_103);
xor_1b(DIN2,din_xor_10,din_xor_210);
xor_1b(DIN3,din_xor_21,din_xor_321);

ko = ^mk_i & ^msbk_i;

crc_sr_out[31] =  z ;
crc_sr_out[30] =  z ;
crc_sr_out[29] =  DIN0;
crc_sr_out[28] =  DIN1;
crc_sr_out[27] =  DIN2;
crc_sr_out[26] =  din_xor_03;
crc_sr_out[25] =  din_xor_10;
crc_sr_out[24] =  din_xor_21;
crc_sr_out[23] =  din_xor_32;
crc_sr_out[22] =  DIN3;
crc_sr_out[21] =  z;
crc_sr_out[20] =  z;
crc_sr_out[19] =  DIN0;
crc_sr_out[18] =  DIN1;
crc_sr_out[17] =  DIN2;
crc_sr_out[16] =  DIN3;
crc_sr_out[15] =  DIN0;
crc_sr_out[14] =  din_xor_10;
crc_sr_out[13] =  din_xor_210;
crc_sr_out[12] =  din_xor_321;
crc_sr_out[11] =  din_xor_032;
crc_sr_out[10] =  din_xor_103;
crc_sr_out[9]  =  din_xor_21;
crc_sr_out[8]  =  din_xor_032;
crc_sr_out[7]  =  din_xor_103;
crc_sr_out[6]  =  din_xor_21;
crc_sr_out[5]  =  din_xor_032;
crc_sr_out[4]  =  din_xor_103;
crc_sr_out[3]  =  din_xor_210;
crc_sr_out[2]  =  din_xor_321;
crc_sr_out[1]  =  din_xor_32;
crc_sr_out[0]  =  DIN3;

for i = 0:31{
  crc_sr_d1_o[i]  = crc_sr_out[31-i] & ~ki;
}

bf_idx0_d1_o = crc_sr_d1_o[31:32-BF_AWIDTH];
bf_idx1_d1_o = crc_sr_d1_o[23:24-BF_AWIDTH];

endmodule
