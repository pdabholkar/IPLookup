module n_bloom_filter #BF_AWIDTH(12) (
  input  dual tlevel_inc_i,
  input  dual apk_i,
  input  dual crc_sr_i[32],
  output      ko,
  input       ki,
  output dual crc_sr_d1_o[32],
  output dual bf_idx0_d1_o[BF_AWIDTH],
  output dual bf_idx1_d1_o[BF_AWIDTH]
);

// le stands for Little Endian
dual lsb;
dual din;

dual crc_sr_out_le[32];
dual crc_xin;
dual crc_sr_in_le[32];

rail ko_crc;

for i = 0:31{
  crc_sr_in_le[i] = crc_sr_i[31-i];
}


lsb = crc_sr_in_le[31];
din = apk_i;


cmplt32 (crc_sr_i,ko_crc);
ko = ko_crc & ^tlevel_inc_i & ^apk_i;

dual crc_sr_in_le_25 = crc_sr_in_le[25];
dual crc_sr_x_25;
dual crc_sr_in_le_22 = crc_sr_in_le[22];
dual crc_sr_x_22;
dual crc_sr_in_le_21 = crc_sr_in_le[21];
dual crc_sr_x_21;
dual crc_sr_in_le_15 = crc_sr_in_le[15];
dual crc_sr_x_15;
dual crc_sr_in_le_11 = crc_sr_in_le[11];
dual crc_sr_x_11;
dual crc_sr_in_le_10 = crc_sr_in_le[10];
dual crc_sr_x_10;
dual crc_sr_in_le_9 = crc_sr_in_le[9];
dual crc_sr_x_9;
dual crc_sr_in_le_7 = crc_sr_in_le[7];
dual crc_sr_x_7;
dual crc_sr_in_le_6 = crc_sr_in_le[6];
dual crc_sr_x_6;
dual crc_sr_in_le_4 = crc_sr_in_le[4];
dual crc_sr_x_4;
dual crc_sr_in_le_3 = crc_sr_in_le[3];
dual crc_sr_x_3;
dual crc_sr_in_le_1 = crc_sr_in_le[1];
dual crc_sr_x_1;
dual crc_sr_in_le_0 = crc_sr_in_le[0];
dual crc_sr_x_0;


// crc_sr_out_le[0] is the MSB. The sr is written as 0 on the left to 31 on the right 
// din is xored in with the sr[31] bit at the right and then fed into sr[0] and the
// other xors between register stages.
crc_sr_out_le[31] = (tlevel_inc_i/1 & crc_sr_in_le[30]) | (tlevel_inc_i/0 & crc_sr_in_le[31]);
crc_sr_out_le[30] = (tlevel_inc_i/1 & crc_sr_in_le[29]) | (tlevel_inc_i/0 & crc_sr_in_le[30]);
crc_sr_out_le[29] = (tlevel_inc_i/1 & crc_sr_in_le[28]) | (tlevel_inc_i/0 & crc_sr_in_le[29]);
crc_sr_out_le[28] = (tlevel_inc_i/1 & crc_sr_in_le[27]) | (tlevel_inc_i/0 & crc_sr_in_le[28]);
crc_sr_out_le[27] = (tlevel_inc_i/1 & crc_sr_in_le[26]) | (tlevel_inc_i/0 & crc_sr_in_le[27]);
crc_sr_out_le[26] = (tlevel_inc_i/1 & crc_sr_x_25 )     | (tlevel_inc_i/0 & crc_sr_in_le[26]); xor_1b(crc_xin,crc_sr_in_le_25,crc_sr_x_25);
crc_sr_out_le[25] = (tlevel_inc_i/1 & crc_sr_in_le[24]) | (tlevel_inc_i/0 & crc_sr_in_le[25]);
crc_sr_out_le[24] = (tlevel_inc_i/1 & crc_sr_in_le[23]) | (tlevel_inc_i/0 & crc_sr_in_le[24]);
crc_sr_out_le[23] = (tlevel_inc_i/1 & crc_sr_x_22 )     | (tlevel_inc_i/0 & crc_sr_in_le[23]); xor_1b(crc_xin,crc_sr_in_le_22,crc_sr_x_22);
crc_sr_out_le[22] = (tlevel_inc_i/1 & crc_sr_x_21 )     | (tlevel_inc_i/0 & crc_sr_in_le[22]); xor_1b(crc_xin,crc_sr_in_le_21,crc_sr_x_21);
crc_sr_out_le[21] = (tlevel_inc_i/1 & crc_sr_in_le[20]) | (tlevel_inc_i/0 & crc_sr_in_le[21]);
crc_sr_out_le[20] = (tlevel_inc_i/1 & crc_sr_in_le[19]) | (tlevel_inc_i/0 & crc_sr_in_le[20]);
crc_sr_out_le[19] = (tlevel_inc_i/1 & crc_sr_in_le[18]) | (tlevel_inc_i/0 & crc_sr_in_le[19]);
crc_sr_out_le[18] = (tlevel_inc_i/1 & crc_sr_in_le[17]) | (tlevel_inc_i/0 & crc_sr_in_le[18]);
crc_sr_out_le[17] = (tlevel_inc_i/1 & crc_sr_in_le[16]) | (tlevel_inc_i/0 & crc_sr_in_le[17]);
crc_sr_out_le[16] = (tlevel_inc_i/1 & crc_sr_x_15 )     | (tlevel_inc_i/0 & crc_sr_in_le[16]); xor_1b(crc_xin,crc_sr_in_le_15,crc_sr_x_15);
crc_sr_out_le[15] = (tlevel_inc_i/1 & crc_sr_in_le[14]) | (tlevel_inc_i/0 & crc_sr_in_le[15]);
crc_sr_out_le[14] = (tlevel_inc_i/1 & crc_sr_in_le[13]) | (tlevel_inc_i/0 & crc_sr_in_le[14]);
crc_sr_out_le[13] = (tlevel_inc_i/1 & crc_sr_in_le[12]) | (tlevel_inc_i/0 & crc_sr_in_le[13]);
crc_sr_out_le[12] = (tlevel_inc_i/1 & crc_sr_x_11 )     | (tlevel_inc_i/0 & crc_sr_in_le[12]); xor_1b(crc_xin,crc_sr_in_le_11,crc_sr_x_11);
crc_sr_out_le[11] = (tlevel_inc_i/1 & crc_sr_x_10 )     | (tlevel_inc_i/0 & crc_sr_in_le[11]); xor_1b(crc_xin,crc_sr_in_le_10,crc_sr_x_10);
crc_sr_out_le[10] = (tlevel_inc_i/1 & crc_sr_x_9  )     | (tlevel_inc_i/0 & crc_sr_in_le[10]); xor_1b(crc_xin,crc_sr_in_le_9,crc_sr_x_9);
crc_sr_out_le[9]  = (tlevel_inc_i/1 & crc_sr_in_le[8] ) | (tlevel_inc_i/0 & crc_sr_in_le[9] );
crc_sr_out_le[8]  = (tlevel_inc_i/1 & crc_sr_x_7  )     | (tlevel_inc_i/0 & crc_sr_in_le[8] ); xor_1b(crc_xin,crc_sr_in_le_7,crc_sr_x_7);
crc_sr_out_le[7]  = (tlevel_inc_i/1 & crc_sr_x_6  )     | (tlevel_inc_i/0 & crc_sr_in_le[7] ); xor_1b(crc_xin,crc_sr_in_le_6,crc_sr_x_6);
crc_sr_out_le[6]  = (tlevel_inc_i/1 & crc_sr_in_le[5] ) | (tlevel_inc_i/0 & crc_sr_in_le[6] );
crc_sr_out_le[5]  = (tlevel_inc_i/1 & crc_sr_x_4  )     | (tlevel_inc_i/0 & crc_sr_in_le[5] ); xor_1b(crc_xin,crc_sr_in_le_4,crc_sr_x_4);
crc_sr_out_le[4]  = (tlevel_inc_i/1 & crc_sr_x_3  )     | (tlevel_inc_i/0 & crc_sr_in_le[4] ); xor_1b(crc_xin,crc_sr_in_le_3,crc_sr_x_3);
crc_sr_out_le[3]  = (tlevel_inc_i/1 & crc_sr_in_le[2] ) | (tlevel_inc_i/0 & crc_sr_in_le[3] );
crc_sr_out_le[2]  = (tlevel_inc_i/1 & crc_sr_x_1  )     | (tlevel_inc_i/0 & crc_sr_in_le[2] ); xor_1b(crc_xin,crc_sr_in_le_1,crc_sr_x_1);
crc_sr_out_le[1]  = (tlevel_inc_i/1 & crc_sr_x_0  )     | (tlevel_inc_i/0 & crc_sr_in_le[1] ); xor_1b(crc_xin,crc_sr_in_le_0,crc_sr_x_0);
crc_sr_out_le[0]  = (tlevel_inc_i/1 & crc_xin     )     | (tlevel_inc_i/0 & crc_sr_in_le[0] ); xor_1b(lsb,din,crc_xin); 

for i = 0:31{
  crc_sr_d1_o[i]  = crc_sr_out_le[31-i] & ~ki;
}

bf_idx0_d1_o = crc_sr_d1_o[31:32-BF_AWIDTH];
bf_idx1_d1_o = crc_sr_d1_o[23:24-BF_AWIDTH];

endmodule
