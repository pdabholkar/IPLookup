parameter TDEPTH = 1;
module n_match_module (
  // This information comes from the next_child module of the previous stage except for the first
  // stage in the pipeline in which this comes from the key_stripper.
  input dual  epsilon_i,
  input dual  msbk_i,
  input dual  mk_i[3],
  input dual  apk_a1_i[32],   // This signal is advanced with respect to eps, msb and mk info of the key

  // This information comes from the match_module of the previous stage except for the first stage
  // of the pipeline in which case it is tied to '0'
  input dual  nhi_i[8],
  input dual  matchlength_i[6],

  output      ko_apk,         // ko for apk. This is separate because the apk may be out of phase from the key info
  output      ko_nhi_ml,      // ko for nhi and matchlength from the previous match module
  output      ko_key_info,    // ko for eps, msbk and mk
  output      ko_pfx1,        // ko for first prefix received
  output      ko_pfx2,        // ko for second prefix received

  // First prefix in the node from pfx RAM
  input dual  mp1_i[3],
  input dual  msb1_i,
  input dual  lsb1_i,
  input dual  lp1_i[5],
  input dual  nhi1_i[8],

  // Second prefix in the node from pfx RAM
  input dual  mp2_i[3], 
  input dual  msb2_i,
  input dual  lsb2_i,
  input dual  lp2_i[5],
  input dual  nhi2_i[8],

  input       ki,             // ki for matchlength, nhi and match_found outputs 
  output dual nhi_d2_o[8],
  output dual matchlength_d2_o[6],
  output      match_found_d2_o

);

dual B0k_a1;
dual apk_xor_msb[31];
dual Nzok[6];

// Completion detection signals for all inputs
cmplt32(apk_a1_i,ko_apk); 

ko_nhi_ml = ^nhi_i & ^matchlength_i;
ko_key_info = ^mk_i & ^msbk_i & ^epsilon_i;

ko_pfx1 = ^mp1_i & ^msb1_i & ^lsb1_i & ^lp1_i & ^nhi1_i ;
ko_pfx2 = ^mp2_i & ^msb2_i & ^lsb2_i & ^lp2_i & ^nhi2_i ;


// Extract the Nzok value out of apk. This has a behavioral + timing model
n_Nzok_extract(apk_a1_i,Nzok);

// Nell does not have equality and inequality. So these two functions have to be implemented 
// as arithmetic and logical operations that are handcrafted using individual gates or small 
// modules.
// binary_eq is a module that checks equality
// binary_ageb_32b is a module that check if A greater than or equal to B (ageb) or A less than B (altb)

rail eq_msb1,ne_msb1,eq_mp1,ne_mp1,eq_lsb1,ne_lsb1,nzok_ge_lp1,nzok_lt_lp1,lp1_ge_ml,lp1_lt_ml;
rail eq_msb2,ne_msb2,eq_mp2,ne_mp2,eq_lsb2,ne_lsb2,nzok_ge_lp2,nzok_lt_lp2,lp2_ge_ml,lp2_lt_ml;
rail lp1_ge_lp2,lp1_lt_lp2;

// Check for match condition 1
binary_eq_1b (msbk_i,msb1_i,eq_msb1,ne_msb1);
binary_eq_3b (mk_i,mp1_i,eq_mp1,ne_mp1);
binary_eq_1b (B0k_a1,lsb1_i,eq_lsb1,ne_lsb1);

dual lp1_msb_is_0 = {^lp1_i,0};
dual lp2_msb_is_0 = {^lp2_i,0};

binary_ageb_6b (Nzok,{lp1_i,lp1_msb_is_0},nzok_ge_lp1,nzok_lt_lp1);
binary_ageb_6b ({lp1_i,lp1_msb_is_0},matchlength_i,lp1_ge_ml,lp1_lt_ml);

rail match_condition_1 = eq_msb1 & eq_mp1 & eq_lsb1 & nzok_ge_lp1 & lp1_ge_ml;
rail not_match_cond_1 = ne_msb1 | ne_mp1 | ne_lsb1 | nzok_lt_lp1 | lp1_lt_ml;

// Check for match condition 2
binary_eq_1b (msbk_i,msb2_i,eq_msb2,ne_msb2);
binary_eq_3b (mk_i,mp2_i,eq_mp2,ne_mp2);
binary_eq_1b (B0k_a1,lsb2_i,eq_lsb2,ne_lsb2);

binary_ageb_6b (Nzok,{lp2_i,lp2_msb_is_0},nzok_ge_lp2,nzok_lt_lp2);
binary_ageb_6b ({lp2_i,lp2_msb_is_0},matchlength_i,lp2_ge_ml,lp2_lt_ml);

rail match_condition_2 = eq_msb2 & eq_mp2 & eq_lsb2 & nzok_ge_lp2 & lp2_ge_ml;
rail not_match_cond_2 = ne_msb2 | ne_mp2 | ne_lsb2 | nzok_lt_lp2 | lp2_lt_ml;

// Check which of the prefixes have the longer lp value
binary_ageb_6b ({lp1_i,lp1_msb_is_0},{lp2_i,lp2_msb_is_0},lp1_ge_lp2,lp1_lt_lp2);

// NHI comptation based on match condition and prefix lp length comparison
nhi_d2_o = ~ki &  ((nhi2_i & match_condition_2 & not_match_cond_1)
                  |(nhi1_i & match_condition_1 & not_match_cond_2)
                  |(nhi1_i & match_condition_1 & match_condition_2 & lp1_ge_lp2)
                  |(nhi2_i & match_condition_1 & match_condition_2 & lp1_lt_lp2)
                  |(nhi_i  & not_match_cond_1  & not_match_cond_2));

// This is for match length computation
dual mp_match[3];
dual lp_match[5];

// Select which mp and lp values need to be used for computing the match length
mp_match = (mp2_i & match_condition_2 & not_match_cond_1) 
         | (mp1_i & match_condition_1 & not_match_cond_2)
         | (mp1_i & match_condition_1 & match_condition_2 & lp1_ge_lp2)
         | (mp2_i & match_condition_1 & match_condition_2 & lp1_lt_lp2);

lp_match = (lp2_i & match_condition_2 & not_match_cond_1) 
         | (lp1_i & match_condition_1 & not_match_cond_2)
         | (lp1_i & match_condition_1 & match_condition_2 & lp1_ge_lp2)
         | (lp2_i & match_condition_1 & match_condition_2 & lp1_lt_lp2);

dual ml_matched[6];

n_ml_compute (.mp(mp_match), .lp(lp_match), .ml(ml_matched));

// If no prefix matched then match length is the same as received at the input
matchlength_d2_o = ~ki & ((matchlength_i & not_match_cond_1 & not_match_cond_2) | ml_matched);

match_found_d2_o = ~ki & (match_condition_1 | match_condition_2);
endmodule
